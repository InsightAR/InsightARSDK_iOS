//
//  ARDynamicModelInput.h
//  InsightSDK
//
//  Created by Dikey on 28/11/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ARDynamicModel;

/**
 动态下载输入模型
 */
@interface ARDynamicModelInput : NSObject

/**
 获取ARDynamicModelInput

 @param pid pid不能为空
 @param materailID mid不能为空
 @return ARDynamicModelInput
 */
- (instancetype)initWithPid:(NSString *)pid
                 materailID:(NSString *)materailID;

/**use initWithPid:materailID insead**/
- (instancetype)__unavailable init;
- (ARDynamicModel *)realModel;

@end
