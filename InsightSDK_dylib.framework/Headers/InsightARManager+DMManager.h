//
//  InsightARManager+DMManager.h
//  InsightSDK
//
//  Created by Dikey on 01/12/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "InsightARManager.h"

@class ARDynamicModelInput;

@interface InsightARManager ()

#pragma mark - Dynamic Model Download

/**
 检查状态，如是否需要更新，本地是否已下载等等；
 如果未联网或者联网失败，返回本地已有信息和无网络的ERROR
 如果有网络，会返回最新结果
 
 @param inputModel 传入pid和mid不能为空
 @param finishBlock 返回是否需要更新
 */
- (void)checkDynamicModelStatus:(ARDynamicModelInput *)inputModel
                       finished:(void(^)(NSError *error, ARDynamicModel *model))finishBlock;

/**
 下载动态模型文件
 
 @param inputModel pid和Mid不能为空
 @param progressBlock 进度回调
 @param finishBlock 下载完成，成功Error为空，同时返回模型状态
 */
- (void)downloadDynamicModel:(ARDynamicModelInput *)inputModel
                    progress:(ARDownloadProgressBlock)progressBlock
                    finished:(void(^)(NSError *error, ARDynamicModel *dynamicModel))finishBlock;
/**
 更新动态模型文件，更新完成后会删除旧的资源；更新的时候不能使用旧资源
 
 @param inputModel pid和Mid不能为空
 @param progressBlock 进度回调
 @param finishBlock 更新完成，成功Error为空，同时返回模型状态
 */
- (void)updateDynamicModel:(ARDynamicModelInput *)inputModel
                  progress:(ARDownloadProgressBlock)progressBlock
                  finished:(void(^)(NSError *error, ARDynamicModel *dynamicModel))finishBlock;

/**
 取消下载
 
 @param model pid和Mid不能为空
 */
- (void)cancelDownloadDynamicModel:(ARDynamicModelInput *)model;

/**
 暂停下载
 
 @param model pid和Mid不能为空
 */
- (void)pauseDownloadDynamicModel:(ARDynamicModelInput *)model;

/**
 删除动态模型
 
 @param model pid和Mid不能为空
 */
- (BOOL)deleteDynamicModel:(ARDynamicModelInput *)model;

@end
