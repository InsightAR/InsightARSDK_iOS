//
//  InsightAREvent.h
//  InsightSDK
//
//  Created by Dikey on 2018/9/6.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InsightARState.h"

extern NSString* const kInsightAREventNotification;

@interface InsightAREvent:NSObject

@property (assign, nonatomic) InsightAREventType type;
@property (strong, nonatomic) NSDictionary *params;

@end
