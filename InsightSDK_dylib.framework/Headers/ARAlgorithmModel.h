//
//  ARAlgorithmModel.h
//  InsightSDK
//
//  Created by Dikey on 25/09/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ARInsightButtonOption;

/**
 算法模型文件用于初始化云识别算法：云识别模式下，预先不知道当前场景pid，识别场景后会返回pid，再根据pid初始化。
 
 整体的云识别流程为：-> 云识别初始化-> 获取当前pid -> 下载pid对应的资源 -> 初始化pid对应的场景
 
 1. 下载云识别模型 ：
 
     [[InsightARManager shareManager] prepareAlgorithmModel:algOptions
                                           downloadProgress:^(NSProgress *downloadProgress) {
     } completion:^(NSError *error) {
         //可以在此处初始化云识别
     }];
 
 2. 云识别初始化 ：
 
      ARAlgorithmModel *arAlgModel =  [[InsightARManager shareManager] getAlgorithmModel];
      [[InsightARManager shareManager] startInsightARWithCloud:arAlgModel withARDelegate:self];
 
 3. 获取识别的pid ：
 
     - (void)insightAREvent:(InsightAREvent *)event
     {
        InsightAREventType type = event.type;
        if(type == InsightAREvent_Function_ReloadARProduct){
           NSString *productID  = [event.params objectForKey:@"name"];
 
        }
     }
 
 4. 下载ARProduct
 
     ARPrepareOptions *options = [ARPrepareOptions new];
     options.productID = productID;
     [[InsightARManager shareManager] prepareARWithOptions:options
                                          downloadProgress:^(NSProgress *downloadProgress) {
     } completion:^(NSError *error) {
 
     }];
 
 5. 初始化AR ：
 
     ARProduct *product = [[InsightARManager shareManager]getARProduct:options.productID];
     [[InsightARManager shareManager] reloadInsightAR:product];
 
 */

/**
 整体的云识别流程为：-> 云识别初始化-> 获取当前pid -> 下载pid对应的资源 -> 初始化pid对应的场景
 
 这里太短写不下 ，请看开发者网站的云识别模式或者框架中的ARAlgorithmModel头文件
 */
@interface ARAlgorithmModel : NSObject

/**
 最后更新时间
 */
@property (nonatomic,assign) NSTimeInterval lastUpdateTime;

/**
 AR按键；SDK内的推广按键
 */
@property (nonatomic,strong) ARInsightButtonOption *insightButtonOption;

/**
 产品总大小：单位Byte
 */
@property (nonatomic, assign) float size;

/**
 传入本地算法路径，初始化云识别
 
 @param algModelPath 本地算法路径
 @return ARAlgorithmModel
 */
- (instancetype)initWithAlgModelPath:(NSString *)algModelPath;

/**
 传入本地算法路径，初始化云识别
 
 @param algModelPath 本地算法路径
 @param insightButtonOption AR按键
 @return ARAlgorithmModel
 */
- (instancetype)initWithAlgModelPath:(NSString *)algModelPath
                 insightButtonOption:(ARInsightButtonOption *)insightButtonOption;

- (instancetype)initWithOnlineCloudMode;

@end
