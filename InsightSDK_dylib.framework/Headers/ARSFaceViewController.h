////
////  ARSFaceViewController.h
////  InsightSDK
////
////  Created by Dikey on 2019/1/17.
////  Copyright © 2019 DikeyKing. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//
//@class ARSFaceManager;
//@class ARSFaceViewData;
//
//@interface ARSFaceViewController : UIViewController
//
//@property (nonatomic, strong) ARSFaceManager *arsFaceManager;
//@property (nonatomic, strong) ARSFaceViewData *viewData;
//@property (nonatomic, strong) UIButton *albumButton;
//@property (nonatomic, strong) UIButton *stickerButton;
//@property (nonatomic, strong) UIButton *filterButton;
//@property (nonatomic, strong) UIButton *beautyButton;
//@property (nonatomic, strong) UIButton *bottomBackButton;
//
//- (void)shareButtonAction:(UIButton *)action;
//
//@end
