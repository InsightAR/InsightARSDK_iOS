//
//  InsightRecordDefine.h
//  InsightSDK
//
//  Created by Dikey on 2018/10/9.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#ifndef InsightRecordDefine_h
#define InsightRecordDefine_h

typedef NS_ENUM(NSUInteger, InsightARRecorderState){
    InsightARRecorderStateInitSuccess               = 0,
    InsightARRecorderStateInitFail                  = 1,
    InsightARRecorderStateError                     = 2,
    InsightARRecorderStateSaveSuccess               = 3,
    InsightARRecorderStateStart                     = 4,
    InsightARRecorderStateAuthorizationStatusDenied = 5,
    InsightARRecorderStateDonotStart                = 6,
    InsightARRecorderStateEnterBackground           = 7,
};

#endif /* InsightRecordDefine_h */
