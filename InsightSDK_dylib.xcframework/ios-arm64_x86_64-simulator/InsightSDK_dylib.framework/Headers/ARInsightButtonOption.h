//
//  ARInsightButtonOption.h
//  InsightSDK
//
//  Created by Dikey on 2018/5/9.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 AR推广按键设置类，默认无视。云识别推广时可以使用。
 */
@interface ARInsightButtonOption : NSObject

/**
 使用自定义设置
 */
@property (nonatomic, assign) BOOL customSetting;

/**
 限定在当前屏幕内且居中
 */
@property (nonatomic, assign) float buttonYPosition;

/**
 字体颜色
 */
@property (nonatomic, strong) UIColor *buttonTextColor;



@end
