//
//  ARDynamicModel.h
//  InsightSDK
//
//  Created by Dikey on 20/11/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "ARProduct.h"

@class ARSItem;

/**
 这个类和ARItem的一些细节关系还需要再细化清楚
 */
@interface ARDynamicModel : ARProduct

- (instancetype)initWithPid:(NSString *)pid
                 materailID:(NSString *)materailID;

- (instancetype)initWithARItem:(ARSItem *)item
                    materailID:(NSString *)materailID;

/**
use initWithPid:materailID insead
 **/
-(instancetype) __unavailable init;

/**
 同pid下保证唯一
 */
@property (nonatomic, copy) NSString *materailID;

/**
 是否已经过期（服务器无返回的时候）
 */
@property (nonatomic, assign) BOOL expired;

/**
 本地是否已经下载
 */
@property (nonatomic, assign) BOOL hasDownloaded;

/**
 是否有更新内容
 */
@property (nonatomic, assign) BOOL hasUpdate;

@end
