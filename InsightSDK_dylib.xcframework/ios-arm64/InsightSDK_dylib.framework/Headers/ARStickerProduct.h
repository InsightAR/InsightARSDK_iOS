//
//  ARStickerProduct.h
//  InsightSDK
//
//  Created by Carmine on 2018/9/3.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "ARProduct.h"

/**
 ARSticker场景类
 */
@interface ARStickerProduct : ARProduct

/**
 实例化自定义的sticker模型，目前支持传入
 模型资源路径，sticker封面图，sticker作者的姓名、头像和简介。

 @param resourcePath sticker资源路径，必须是绝对路径，且是根目录
                             如:
                             res
                             |- config
                             |- resources
 @param avatar sticker封面图
 @param authorName sticker作者姓名，10个字以内
 @param authorAvatar sticker作者头像
 @param authorIntro sticker作者介绍
 @return ARStickerProduct实例
 */
- (instancetype)initWithResourcePath:(NSString *)resourcePath
                              avatar:(NSString *)avatar
                          authorName:(NSString *)authorName
                        authorAvatar:(NSString *)authorAvatar
                         authorIntro:(NSString *)authorIntro;


/**
 实例化自定义的sticker模型，只接收sticker资源路径和封面图，无作者信息

 @param resourcePath sticker资源路径，必须是绝对路径，且是根目录
                             如:
                             res
                             |- config
                             |- resources
 @param avatar sticker封面图
 @return ARStickerProduct实例
 */
- (instancetype)initWithResourcePath:(NSString *)resourcePath avatar:(NSString *)avatar;

@end
