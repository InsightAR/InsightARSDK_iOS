////
////  ARSFaceViewData.h
////  ARSFaceDemo
////
////  Created by Dikey on 2019/1/24.
////  Copyright © 2019 Dikey. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//#import "ARSFaceDataDefine.h"
//#import <UIKit/UIKit.h>
//
//@class ARProduct;
//@class ARSFaceItem;
//
//@interface ARSFaceViewData : NSObject
//
//- (instancetype)init;
//
//#pragma mark - ARSFaceViewType
//
///**
// 界面的状态
// */
//@property (nonatomic, assign) ARSFaceViewType viewType;
//
//@property (nonatomic, assign) ARSFaceViewType lastRecordType;
//
///**
// 界面元素
// */
//@property (nonatomic, strong) NSMutableArray *viewsArray;
//
//#pragma mark - ScreenRecord
//
///**
// 当前录屏时间
// */
//@property (nonatomic, assign) float currentRecordTime;
//
///**
// 总共录屏时间, 支持小于60秒以内的时间（产品的想法是不是很奇怪？）
// */
//@property (nonatomic, assign) float totalRecordTime;
//
///**
// 保存路径
// */
//@property (nonatomic, strong) NSString *recordingPath;
//
///**
// 最后拍的缩略图
// */
//@property (nonatomic, strong) UIImage *lastCapturedImage;
//
///**
// 选中的Sticker
// */
//@property (nonatomic, strong) ARProduct * selectedSticker;
//
///**
// 选中的Face
// */
//@property (nonatomic, strong) ARSFaceItem * selectedFaceItem;
//
//
//@end
//
