//
//  InsightARCloudVC.h
//  InsightSDK
//
//  Created by Carmine on 2018/12/29.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol InsightARDelegate;
@class InsightARCloudVC;

@protocol InsightARCloudVCDelegate <NSObject>

@optional
- (void)insightARCloudVC:(InsightARCloudVC *_Nonnull)cloudVC downloading:(NSProgress *_Nonnull)progress;
- (void)insightARCloudVC:(InsightARCloudVC *_Nonnull)cloudVC finishedDownloading:(NSError *_Nullable)error;

@end

NS_ASSUME_NONNULL_BEGIN

@interface InsightARCloudVC : UIViewController

/// 接入方若需要监听事件回调，需要自行实现 InsightARDelegate 方法
@property (nonatomic, weak) id<InsightARDelegate> delegate;

/// 下载进度回调
@property (nonatomic, weak) id<InsightARCloudVCDelegate> progressDelegate;

/// 是否隐藏默认视图，默认为NO，接入方自行选择是否展示
@property (nonatomic, assign) BOOL hideDefaultLayout;

/// 扫描网格的颜色值，十六进制色码
@property (nonatomic, copy) NSString * detectingHexColor;

/// 聚焦框的颜色值，十六进制色码
@property (nonatomic, copy) NSString * focusHexColor;

/// 更多AR的文本颜色，十六进制色码
@property (nonatomic, copy) NSString * moreARTextColor;

/// 更多AR的按钮颜色，十六进制色码
@property (nonatomic, copy) NSString * moreARButtonColor;

/// 更多AR的按钮偏移（锚点为屏幕中心），默认不启用
@property (nonatomic, assign) CGFloat moreARButtonAlignCenterY;

/// 对焦视图的偏移（锚点为屏幕中心），默认不启用
@property (nonatomic, assign) CGFloat focusViewsAlignCenterY;

/// longitude for LBS
@property (nonatomic, assign) double longitude;

/// latitude for LBS
@property (nonatomic, assign) double latitude;

// 按照云识别方式初始化
- (instancetype)initWithOnlineCloudMode:(BOOL)online;

- (void)startAR;

@end

NS_ASSUME_NONNULL_END
