//
//  ARAlgPrepareOption.h
//  InsightSDK
//
//  Created by Dikey on 26/09/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 用于下载云识别场景资源的配置项
 */
@interface ARAlgPrepareOption : NSObject

/**
 设置代理服务器,可以为空,仅需要设置一次
 */
@property (copy,nonatomic) NSArray<Class> *protocolClasses;

/**
 默认 ** YES **
 更新到最新版本的数据;数据更新需要耗费一定流量，一般在几十KB~几MB;如果更新失败则返回本地已有算法模型
 */
@property (assign,nonatomic) BOOL getLastestVersion;

/**
 getLastestVersion = YES 时生效
 服务器访问最小时间间隔；默认为300秒
 本地已经存在算法模型的情况下，最小更新间隔300秒
 */
@property (assign,nonatomic) NSTimeInterval updateTimeInterval;

@end
