//
//  ARSCodeParserResult.h
//  InsightSDK
//
//  Created by Dikey on 2020/8/6.
//  Copyright © 2020 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ARSCodeParserResultCode) {
    /// successful result
    ARSCodeParserResultCodeSuccess = 0,
    
    /// others: fail code
};

/// ARCode 识别结果
@interface ARSCodeParserResult : NSObject

/// arCode 识别结果
@property (nonatomic, copy) NSString *arCode;

/// arCode 识别状态
@property (nonatomic, assign) ARSCodeParserResultCode resultCode;

@end
