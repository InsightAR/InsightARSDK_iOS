//
//  ARSCodeParser.h
//  InsightSDK
//
//  Created by Dikey on 2020/8/6.
//  Copyright © 2020 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>

@class InsightAREvent;
@class ARSCodeParserResult;

/// 使用前需要在InsightARManager中调用registerAppKey注册SDK
@interface ARSCodeParser : NSObject

/// 默认 hFL = 1100 、 vFL = 1100、mainPointX = 640、mainPointY = 480；inputWidth = 1280；inputHeight = 720
- (instancetype)init;

/// 解析ARCode码为ARCode；需要传入灰度图
/// @param hFL  横向对应焦距
/// @param vFL 纵向对应焦距
/// @param mainPointX 主点（中心点）的x值
/// @param mainPointY 主点（中心点）的y值
/// @param inputWidth 输入image宽度
/// @param inputHeight 输入image高度
- (instancetype)initWithHorizontalFocalLength:(float)hFL
                          verticalFocalLength:(float)vFL
                                   mainPointX:(float)mainPointX
                                   mainPointY:(float)mainPointY
                                        Width:(float)inputWidth
                                       height:(float)inputHeight;

/// 解析ARCode码为ARCode；需要传入灰度图
/// @param greyPtr 灰度图；宽高必须在 ARSCodeParser 初始化时候设置
- (ARSCodeParserResult *)parseARCode:(const void *)greyPtr;

/// 从服务器获取ARCode对应的事件信息
/// @param arcode parseARCode获取的arCode值
/// @param resultBlock ARCode对应的事件信息
- (void)getARCodeInfo:(NSString *)arcode
               result:(void(^)(NSError *error, InsightAREvent *eventDic))resultBlock;

@end
