Pod::Spec.new do |s|

  s.name         = "InsightSDK_dylib"
  s.version      = "2.10.1"
  s.summary      = "Insight AR SDK."

  s.homepage     = "https://gitlab.com/InsightAR/InsightARSDK_iOS.git"
  s.license      = "MIT"
  s.author             = { "Dikey" => "dikeyking@gmail.com" }
  s.social_media_url   = "https://gitlab.com/DikeyKing"

  # 支持平台
  s.platform     = :ios
  s.platform     = :ios, "11.0"
  s.ios.deployment_target = "11.0"

  # 路径地址
  s.source       = { :git => 'https://gitlab.com/InsightAR/InsightARSDK_iOS.git', :tag => s.version }

  # s.source_files  = 'InsightSDK_dylib.framework/Headers/*.{h}'
  
  # old way to integarate images
  # s.resource = "InsightSDK.bundle"
  
  # new way to integarate images, after 2.4.4
  # see https://github.com/CocoaPods/CocoaPods/issues/8122 for reason
  s.resource_bundles = {'InsightSDK' => ['Resources/*.xcassets',
                                          'Resources/*.p12',
                                          'Resources/*.cer',]}

  # s.resource = ['Resources/*']
  # s.resource = "InsightSDK.bundle"
  # s.resources = "InsightSDK.xcassets"
  
  s.vendored_frameworks = 'InsightSDK_dylib.xcframework'

end
