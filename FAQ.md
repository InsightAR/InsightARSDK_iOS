****

## 洞见SDK - iOS接入常见问题汇总
****
###集成文档请看根目录README.MD
****
**问题：**
> 请问洞见SDK，现在能扫什么？

**解决：**
您好，由于您集成的SDK是我们的轻量级SDK，它能够：1）2D图片识别（扫描识别出一张2D图片） 2）场景识别（扫描识别出一个平面）；
备注：3D识别（识别3D物体）和SLAM等其他方案在轻量级SDK中并不支持，因为它们是重量级的，会大大增加主App的体积。

****

**问题：**

> 有能测试的Key，Secret和eventID么？



**解决：**



评估阶段您只能使用Demo工程中的BundleID（Package name）、Key和Secret体验洞见SDK的AR效果。
新的Key和Secret需要确认合作后洞见方产品或技术支持拿到您App的BundleID（Package name）后配置生成，配置后会尽快给到您

****
**问题：**

> 测试和正式的Key和Secret的区别是什么？



**解决：**



一个AR资源在洞见后台上传后会先关联到您测试的Key和Secret下，此时只有测试Key和Secret能访问到该AR资源；

AR资源测试ok后，洞见平台上会进行提交操作，提交后该AR资源会同时关联到您的测试和正式Key和Secret下，此时正式和测试Key和Secret都能拿到它。

因此测试的能力大于正式。

【！！！请务必确保正式提交商店时使用正式Key和Secret！！！】

这样可以实现用测试Key和Secret去尝试修复一些问题而互不干扰。

****
**问题：**
> framework很大，大概56M左右，打包会不会增加很大体积？

**解决：**

不会的，我们这边测试过，也就3～4M，您可以实际测量一下。
****

**问题：**
>  编译报错:
>  ![image](img/FAQ/img06.jpg)
>
>  ![image](img/FAQ/img07.jpg)

**解决：**

请不要用自己下载的opencv2.framework，请使用demo中提供的opencv2framework。请确保openCV.framework的版本为3.3.0（Mac OS 右键 显示简介 查看版本号）

![image](img/FAQ/img08.jpg)
****
**问题：**
> 编译报错:
>
> ![image](img/FAQ/img09.jpg)

**解决：**

请重新添加一遍opencv2.framework。可能是cv没有链接上，Xcode中“直接拖进去”和“通过文件->添加”是不一样的。
****
**问题：**
> 为什么最后ipa包导不出来?![image](img/FAQ/img10.jpg)
>
> ![image](img/FAQ/img11.png)

**解决：**

请您参考以下链接：
 https://www.jianshu.com/p/49f94e711fc2
https://stackoverflow.com/questions/26559948/found-an-unexpected-mach-o-header-code-1918975009-in-xcode-6

****
**问题：**

> 报错如下图：
![image](img/FAQ/img12.jpg)


**解决：**

添加arkit
****

**问题：**



> 下载的AR资源用iTools看到解压失败，文件夹下是空的是什么原因？



**解决：**



InsightSDK iOS 使用到了SSZipArchive。如您的工程中有如miniZIP这样的其他解压库，就会产生冲突。
如果是这个原因导致，建议您更换成SSZipArchive，安全性稳定性都好很多



****


**问题：**



> 编译出现问题：
![image](img/FAQ/img13.png)





**解决：**



应该是项目里加了 -all_load 导致链接错误，去掉就行了。-all-load 目前这个版本暂时不能使用。

****









