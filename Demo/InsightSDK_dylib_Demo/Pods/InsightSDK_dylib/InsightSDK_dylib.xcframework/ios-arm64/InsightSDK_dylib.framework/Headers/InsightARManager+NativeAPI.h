//
//  InsightARManager+arengine.h
//  只用于camera pixel buffer由集成方法传入
//  并且只使用洞见算法，无法使用设备ar算法（比如arkit）

//  Created by Dikey on 21/03/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ARDataDefine.h"

@interface InsightARManager()

/**
 开始运行云识别场景
 需要事先将云识别包ARAlgorithmModel下载到本地
 重要！:  使用完成之后必须调用stopInsightAR 释放资源（包括算法、渲染）。

 @param algorithmModel 不能为nil，通过getAlgorithmModel获取
 @param param AREngineCameraParam
 @param arDelegate 用于获取AR场景的State和Event，如果为空，将无法获取AR消息
 */
- (void)startCloudAREngine:(ARAlgorithmModel *)algorithmModel
           withCameraParam:(AREngineCameraParam)param
            withARDelegate:(id<InsightARDelegate>)arDelegate;

/**
 开始运行指定pid的AR场景
 重要:  使用完成之后必须调用stopInsightAR 释放资源（包括算法、渲染）。


 @param arProduct 不能为nil，通过getARProduct
 @param param AREngineCameraParam
 @param arDelegate 用于获取AR场景的State和Event，如果为空，将无法获取AR消息
 */
- (void)startAREngine:(ARProduct *)arProduct withCameraParam:(AREngineCameraParam)param withARDelegate:(id<InsightARDelegate>)arDelegate;

/**
  重新打开AR场景

 @param arProduct 为nil时，这默认重新加载当前AR Product;
 @param param AREngineCameraParam
 */
- (void)reloadAREngine:(ARProduct *)arProduct
       withCameraParam:(AREngineCameraParam)param;

/**
 注意：第三方目前无场景使用该方法
 
 关闭AR场景；
 释放资源（包括算法、渲染）
 */
- (void)stopAREngine;

/**
 Process Buffer
 
 @param inputBufferRef inputBufferRef color space must be kCVPixelFormatType_420YpCbCr8BiPlanarFullRange && AVCaptureSessionPreset1280x720 && AVCaptureVideoOrientationLandscapeRight
 @param timestamp 当前图像时间戳
 
 @return processed Buffer
 */
- (CVPixelBufferRef)processPixelBuffer:(CVPixelBufferRef )inputBufferRef timestamp:(double)timestamp;
- (CVPixelBufferRef)processPixelBufferSynchronously:(CVPixelBufferRef)inputBufferRef
                                          timestamp:(double)timestamp;
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event withView:(UIView *)view;
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event withView:(UIView *)view;
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event withView:(UIView *)view;
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event withView:(UIView *)view;

@end
