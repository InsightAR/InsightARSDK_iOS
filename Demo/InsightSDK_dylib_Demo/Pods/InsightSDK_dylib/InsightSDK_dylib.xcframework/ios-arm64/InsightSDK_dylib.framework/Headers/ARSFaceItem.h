////
////  ARSFaceItem.h
////  ARSFaceDemo
////
////  Created by Dikey on 2019/1/28.
////  Copyright © 2019 Dikey. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//
//@interface ARSFaceItem : NSObject
//
//@property (nonatomic, copy) NSString *name; //效果名字
//@property (nonatomic, copy) NSString *script; //scirpt名字
//@property (nonatomic, copy) NSString *image; //默认图
//@property (nonatomic, copy) NSString *highlighted; //高亮图
//@property (nonatomic, copy) NSString *zero; //初始值 - 用于重置
//@property (nonatomic, copy) NSString *current; //当前值 - 用于修改效果，重置时应为zero
//@property (nonatomic, copy) NSString *ars_imagePath;
//@property (nonatomic, copy) NSString *ars_highlightedPath;
//
//@end
//
