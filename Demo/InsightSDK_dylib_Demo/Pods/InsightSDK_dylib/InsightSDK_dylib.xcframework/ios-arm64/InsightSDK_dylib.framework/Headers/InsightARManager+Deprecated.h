//
//  InsightARManager_Deprecated.h
//  InsightSDK
//
//  Created by Dikey on 07/09/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "InsightARManager.h"

@interface InsightARManager ()

#pragma mark - DEPRECATED_ATTRIBUTE API After v2.5.0

/**
  get all ARProducts online
  获取当前注册AppKey对应的所有ARProduct
 
 @param completionHandler ARCompletionBlock
 @param result NSArray <ARProduct *>*arProducts
 */
- (void)checkProductStatus:(ARCompletionBlock)completionHandler
                    result:(void(^)(NSArray <ARProduct *>* arProducts))result;

#pragma mark - DEPRECATED_ATTRIBUTE API After v1.3

/**
 DEPRECATED_ATTRIBUTE ：use getARProduct instead.
 
 OFFLINE API..
 
 @param pid pid
 @param error if error occurs
 @param finishBlock return YES if downloaded
 */
- (void)checkDownloadStatus:(NSString *)pid
                      error:(void(^)(NSError *error))error
                     result:(void(^)(BOOL result))finishBlock;

/**
 DEPRECATED_ATTRIBUTE: use checkProductStatus instead.
 获取产品的大小，单位是Byte，
 
 @param pids pid of ARProduct
 @param completionHandler ARCompletionBlock
 @param sizeBlock ARUpdateBlock @{@"pid":@"NSNumber -> Byte"}
 */
- (void)getProductSize:(NSSet <NSString *>*)pids
           productList:(ARCompletionBlock )completionHandler
                result:(ARUpdateBlock )sizeBlock DEPRECATED_ATTRIBUTE;

/**
 DEPRECATED_ATTRIBUTE: use checkProductStatus instead.
 获取产品的最后更新时间,返回pid和对应的毫秒数（1970.1.1）
 @param pids pid of ARProduct
 @param completionHandler ARCompletionBlock
 @param updateTime ARUpdateBlock @{@"pid":@"lastUpdateTime"}
 */
- (void)getUpdateTime:(NSSet <NSString *>*)pids
          productList:(ARCompletionBlock )completionHandler
               result:(ARUpdateBlock )updateTime DEPRECATED_ATTRIBUTE;

@end
