//
//  ARProduct.h
//  InsightSDK
//
//  Created by Dikey on 01/09/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ARProductType) {
    ARProductTypeNormal = 0,//普通事件 它的productlist为空 单一事件
    ARProductTypeSticker = 1, //AR相机Sticker  因为sticker下载和普通下载共用 所以有了这个type 后续下载分开 可以删掉
    ARProductTypeFaceSticker = 2, // 专题里面的每个
};

@class ARStartOption;

/** AR场景类 */
@interface ARProduct : NSObject

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *pid;


@property (nonatomic, copy) NSString *cover;

@property (nonatomic, copy) NSString *version; // 内容版本号

@property (nonatomic, copy) NSString *modelDescription; // 产品描述

@property (nonatomic, strong) NSArray *pics;

@property (nonatomic, strong) NSArray *markers; // 云识别图

/**
 是否可以更新：根据服务器返回的最后更改时间做判断..(发生文件更新等操作)
 */
@property (nonatomic, assign) BOOL shouldUpdate;

/**
 Product Orientation;云识别无此属性
 */
@property (nonatomic, assign) UIInterfaceOrientation direction;

/**
 产品总大小：单位Byte
 */
@property (nonatomic, assign) float size;

/**
 是否已下载：如果已下载，可以选择直接加载本地资源
 */
@property (nonatomic, assign) BOOL downloaded;

/**
 如需要使用本地方式加载，则需要设置此参数；默认这个参数留空，走线上
 */
@property (strong, nonatomic) ARStartOption *startOption;

/**
 *  是否是sticker资源
 */
@property (nonatomic, assign) BOOL isSticker;

/**
 *  是否是sticker资源
 */
@property (nonatomic, assign) ARProductType type;

/* for H5 to Lua */
@property (nonatomic, copy) NSString *arShowData;

/**
 本地加载时候使用

 @param resourcePath 本地路径
 @return ARProduct
 */
- (instancetype)initWithLocalResourcePath:(NSString *)resourcePath;

@end

