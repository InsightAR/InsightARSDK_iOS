//
//  ARManager.h
//  LightSDKDemo
//
//  Created by Dikey on 21/03/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ARDataDefine.h"
#import "InsightARDelegate.h"

/** AR核心控制器 */
@interface InsightARManager : NSObject

#pragma mark - SDK Init & Register

/**
 Apple A9 & iOS 11.0 required ; x86 simulator is not supported
 需要A9及以上处理器的机型（iPhone 6S 及以上）；系统 iOS 11.0 及以上； 不支持x86架构
  */
+ (BOOL)isSDKSupported;

/*
 singleton of InsightARManager
 */
+ (instancetype)shareManager;

/**
 register SDK ，return error if key is invalide
 注册SDK
 
 @param key key
 @param secret secret
 @return return error if key is invalide ，nil when OK
 */
- (NSError *)registerAppKey:(NSString *)key
                  appSecret:(NSString *)secret;

# pragma mark - download cloud recognition ar product

/**
 get ARAlgorithmModel Model status online
 从服务器获取运识别算法模型的最新信息
 
 @param completionHandler ARCompletionBlock
 @param result ARAlgorithmModel
 */
- (void)checkAlgorithmModel:(ARCompletionBlock)completionHandler
                     result:(void(^)(ARAlgorithmModel *algModel))result;

/**
 get Cloud AR AlgorithmModel
 go ARAlgorithmModel.h file to see detail
 从服务器获取云识别算法模型
 
 @param options ARAlgPrepareOption
 @param progressBlock ARDownloadProgressBlock
 @param completionHandler ARCompletionBlock
 */
- (void)prepareAlgorithmModel:(ARAlgPrepareOption *)options
             downloadProgress:(ARDownloadProgressBlock)progressBlock
                   completion:(ARCompletionBlock)completionHandler;

/**
 stop Downloading AlgorithmModel
 停止下载云识别算法模型，该未完成下载的数据会继续保留在内存中用于断点续传
 */
- (void)stopPrepareAlgorithmModel;

/**
 return local cloud recognition ARProduct,nil if not downloaded
 RegisterAppKey first,else return nil
 !IMPORTANT: Read ARAlgorithmModel.h for detail
 从本地获取云识别算法模型，需要先保证SDK已注册
 */
- (ARAlgorithmModel *)getAlgorithmModel;

#pragma mark - download pid ar product

/**
 get pid ar product
 根据Pid（ProductID）获取获取 ARProduct
 
 @param options ARPrepareOptions
 @param progressBlock ARDownloadProgressBlock
 @param completionHandler ARCompletionBlock
 */
- (void)prepareARWithOptions:(ARPrepareOptions *)options
            downloadProgress:(ARDownloadProgressBlock)progressBlock
                  completion:(ARCompletionBlock)completionHandler;

/**
 stop Downloading ARProduct
 停止下载ARProduct，该未完成下载的数据会继续保留在内存中用于断点续传
 @param pid pid
 */
- (void)stopPrepareARProduct:(NSString *)pid;

/**
 stop Downloading ARProduct && ARAlgorithmModel
 停止下载ARProduct 和 ARAlgorithmModel，该未完成下载的数据会继续保留在内存中用于断点续传
 */
- (void)stopAllPrepare;

/**
 return local pid ARProduct,nil if not downloaded
 RegisterAppKey first,else return nil
 返回本地
 
 @param pid pid
 @return ARProduct
 */
- (ARProduct *)getARProduct:(NSString *)pid;

/**
 return local ARProduct by specified prepare options, nil if not downloaded
 RegisterAppKey first,else return nil
 根据ARPrepareOptions返回ARProduct
 
 @param options ARPrepareOptions
 @return ARProduct
 */
- (ARProduct *)getARProductByOption:(ARPrepareOptions *)options;

#pragma mark - Native Resource & Manage

/**
  检查单个product信息
 */
- (void)checkProductStatus:(NSString *)pid
                    error:(ARCompletionBlock)completionHandler
                    result:(void(^)(ARProduct *product))result;

/**
 checkProductStatusByOption
 根据ARPrepareOptions返回ARProduct状态

 @param option ARPrepareOptions
 @param completionHandler ARCompletionBlock
 @param result ARProduct
 */
- (void)checkProductStatusByOption:(ARPrepareOptions *)option
                        completion:(ARCompletionBlock)completionHandler
                            result:(void (^)(ARProduct *))result;

/**
 Delete local files of product with product ID
 根据pid删除本地资源

 @param pids pids
 @return BOOL
 */
- (BOOL)removeResource:(NSSet <NSString *>*)pids;

/**
 Delete All data ,should register first
 删除所有当前Appkey对应的资源
 */
- (void)clearAllData;

#pragma mark - AR LifeCycle & AR Manage

/**
 Open Camera & Start Insight AR with cloud
 打开摄像机，开始运行云识别场景
 重要: !!! 使用完成之后必须调用stopInsightAR 释放资源（包括算法、渲染、相机）。
 
 @param algorithmModel 不能为nil，通过getAlgorithmModel获取
 @param arDelegate 用于获取AR场景的State和Event，如果为空，将无法获取AR消息
 @return 是否初始化成功；如果是后台初始化则无法判断（会直接返回空）;如果失败请检查资源版本号
 */
- (NSError *)startInsightARWithCloud:(ARAlgorithmModel *)algorithmModel withARDelegate:(id<InsightARDelegate>)arDelegate;

/**
 Start Insight AR
 打开摄像机，开始运行指定pid的AR场景
 重要: !!! 使用完成之后必须调用stopInsightAR 释放资源（包括算法、渲染、相机）。

 @param arProduct 为nil则打开空相机
 @param arDelegate 用于获取AR场景的State和Event，如果为空，将无法获取AR消息
 @return 是否初始化成功；如果是后台初始化则无法判断（会直接返回空）;如果失败请检查资源版本号
 */
- (NSError *)startInsightAR:(ARProduct *)arProduct withARDelegate:(id<InsightARDelegate>)arDelegate;

/**
 Reload Insight AR without closing & reinit
 重新打开AR场景， 不关闭摄像机
 
 @param arProduct 为nil时，这默认重新加载当前AR Product;
 @return 是否初始化成功；如果是后台初始化则无法判断（会直接返回YES）;如果失败请检查资源版本号
 */
- (NSError *)reloadInsightAR:(ARProduct *)arProduct;

/**
 pauseInsightAR （unstable）
 暂停AR（暂不稳定，第三方应避免使用此方法）
 */
- (void)pauseInsightAR;

/**
  resumeInsightAR （unstable）
 恢复AR（暂不稳定，第三方应避免使用此方法）
 */
- (void)resumeInsightAR;

/**
 reloadInsightARWithCloud
 重新打开AR场景， 不关闭摄像机

 @param algModel ARAlgorithmModel
 @return 是否初始化成功；如果是后台初始化则无法判断（会直接返回nil）;如果失败请检查资源版本号
 */
- (NSError *)reloadInsightARWithCloud:(ARAlgorithmModel *)algModel;

/**
 Close AR，It must be called after using AR, should be used pair of startInsightAR, calling stopInsightAR multiple times is not allowed.
 Eample: startInsightAR -> stopInsightAR, and next time startInsightAR -> stopInsightAR
 关闭摄像机，关闭AR场景；需要和 startInsightAR 配对使用；不允许连续多次调用
 案例：首次AR 体验 startInsightAR -> stopInsightAR；第二次 AR 体验：startInsightAR -> stopInsightAR
 
  After stopInsightAR, ARView should be removed and set nil 
 关闭AR场景后，需要移除ARView；
 */
- (void)stopInsightAR;

/**
 use ARScript to communicate with AR Scene
 native端可以通过执行脚本函数，与AR场景通信；
 可以执行的脚本函数，详情请看文档

 @param scriptName 脚本名
 @param param 传参
 @param block InsightARRunScriptBlock
 */
- (void)runInsightARScript:(NSString *)scriptName
                     param:(NSString *)param
                  callback:(InsightARRunScriptBlock)block;

/**
 InsightARView ... 
 获取ar uiview,用于显示所有AR内容；在stopInsightAR之后，原先的arView会失效，需要重新获取对象
 只有当InsightARDelegate开始返回arstate后，才为非nil；建议在arstate = InsightARStateIniting后添加ar uiview
 可以在insightARDidUpdateARState代理方法里面，添加ARView到自身的view
 
 */
- (UIView *)getInsightARView;

/**
 SDK version in a.b.c, like 2.5.1
 */
- (NSString *)sdkVersion;

/**
 性能监测工具
 @param enabled BOOL值，YES开启、NO关闭
 */
- (void)enablePerformanceMonitor:(BOOL)enabled;

#pragma mark - InsightARManager+Deprecated.h  // Deprecated Methods

@end
