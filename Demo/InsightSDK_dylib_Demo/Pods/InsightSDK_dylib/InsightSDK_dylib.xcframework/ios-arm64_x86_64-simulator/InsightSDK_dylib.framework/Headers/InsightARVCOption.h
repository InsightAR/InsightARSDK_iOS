//
//  InsightARVCOption.h
//  InsightSDK
//
//  Created by Dikey on 2018/8/28.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ARAlgPrepareOption;
@class ARPrepareOptions;
@class ARProduct;
@class ARAlgorithmModel;

/** ARVC启动模式 */
typedef NS_ENUM(NSUInteger, ARVCMode) {
    /** Use ARAlgPrepareOption in VC by default */
    ARVCModeCloud,
    /** Use ARPrepareOptions in VC*/
    ARVCModeProductID,
    /** Use ARPrepareOptions in VC*/
    ARVCModeLocalProductID,
    /** Use ARAlgPrepareOption in VC*/
    ARVCModeLocalCloud,
};

/**
 Use this to Start AR in InsightARVC
 */
@interface InsightARVCOption : NSObject

/**
 ARPrepareOptions
 */
@property (nonatomic, strong) ARPrepareOptions *arPrepareOptions;

/**
 ARAlgPrepareOption
 */
@property (nonatomic, strong) ARAlgPrepareOption *arAlgPrepareOption;

/**
 ARProduct
 */
@property (nonatomic, strong) ARProduct *localProduct;

/**
 ARAlgorithmModel
 */
@property (nonatomic, strong) ARAlgorithmModel *localAlgModel;

/**
 default is NO , will show alertController using ceallar data if set to YES
 */
@property (nonatomic, assign ,getter= isCellarAlertOn ) BOOL cellarAlertOn;

/**
 default is YES , will display download HUD on View
 */
@property (nonatomic, assign ,getter= isDownloadHudOn ) BOOL downloadHudOn;

/**
 ARVCMode , default is ARVCModeCloud
 */
@property (nonatomic, assign) ARVCMode currentVCMode;

/**
 default Mode is ARVCModeCloud

 @return InsightARVCOption
 */
- (instancetype)init;

/**
 Cloud Mode ( default Mode); init InsightARVCOption with this method will set currentVCMode to ARVCModeCloud
 
 @param arAlgPrepareOption ARAlgPrepareOption
 @return use to Start AR in Cloud mode in InsightARVC
 */
- (instancetype)initWithARAlgPrepareOption:(ARAlgPrepareOption *)arAlgPrepareOption;

/**
 Use Pid Mode , init InsightARVCOption with this method will set currentVCMode to ARVCModeProductID

 @param arPepareOptions ARPrepareOptions
 @return use to Start AR in Pid mode in InsightARVC
 */
- (instancetype)initWithARPrepareOptions:(ARPrepareOptions *)arPepareOptions;

/**
 Use Local Product

 @param arProduct ARProduct
 @return use to Start AR in Pid mode in InsightARVC
 */
- (instancetype)initWithLocalARProduct:(ARProduct *)arProduct;

/**
 Cloud Mode

 @param algModel Local AlgModel
 @return use to Start AR in Cloud mode in InsightARVC
 */
- (instancetype)initWithLocalAlgorithmModel:(ARAlgorithmModel *)algModel;

@end
