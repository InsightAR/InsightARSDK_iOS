//
//  InsightARVC.h
//  InsightSDK
//
//  Created by Dikey on 2018/8/28.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARDataDefine.h"
#import "InsightAREvent.h"

@protocol InsightARVCDelegate <NSObject>

@optional

- (void)insightARVCOption:(InsightARVCOption *)vcOption downloadingProgress:(NSProgress *)progress;
- (void)insightARVCOption:(InsightARVCOption *)vcOption finishedDownloading:(NSError *)error;
- (void)insightARVCOption:(InsightARVCOption *)vcOption didUpdateARState:(InsightARState)state;
- (void)insightARVCOption:(InsightARVCOption *)vcOption didUpdateAREvent:(InsightAREvent *)event;
- (void)insightARVCOption:(InsightARVCOption *)vcOption authorisedDownloadViaCellar:(BOOL)authorised; //return if User allow download At Cellar
- (BOOL)insightARVCCustomInsightAREventType:(InsightAREventType)type;

@end

/**
 A wrapper for InsightARManager, simpify the use of InsightARManager
 */
@interface InsightARVC : UIViewController

/**
  Callbacks of download progress、download result、InsightARState & InsightAREvent
 */
@property (nonatomic, weak) id<InsightARVCDelegate> delegate;

/**
 Used to Start AR in InsightARVC
 */
@property (nonatomic, strong) InsightARVCOption *vcOption;

/**
 A option to start InsightARVC ,can set AR scene here, see InsightARVCOption.h for detail

 @param option InsightARVC
 */
- (void)startARWithOption:(InsightARVCOption *)option;

@end
