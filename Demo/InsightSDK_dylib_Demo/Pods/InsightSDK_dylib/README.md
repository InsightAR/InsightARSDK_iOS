## 接入指南 （必读）
---

[洞见SDK产品介绍](https://near.yuque.com/docs/share/9582a916-0713-4fd2-8483-38755c30e37b?#)

[洞见SDK接入文档](https://near.yuque.com/books/share/896a072e-9f80-47cb-b2bf-189e87623fdc#) 

## InsightSDK   

简介
---

`InsightSDK `是基于网易洞见AR引擎的轻量级SDK，集成应用验证、产品下载、算法跟踪、AR展示功能；

### SDK流程

参考[洞见SDK产品介绍](https://near.yuque.com/docs/share/9582a916-0713-4fd2-8483-38755c30e37b?# 《网易洞见SDK产品介绍》)更详尽信息。

![image](doc/sdk1.png)

### 体积影响

iPhone 11

- APP Store 下载体积：3.70 MB
- 安装后体积：10.2 MB

详见wiki

https://gitlab.com/InsightAR/InsightARSDK_iOS/-/wikis/home


### 特性
 * 初始化成功后直接添加ARView（UIView类型）就可以使用

使用pod安装 
--- 

* 动态库（推荐）：在podfile中添加``pod 'InsightSDK_dylib’, :git => 'https://gitlab.com/InsightAR/InsightARSDK_iOS.git', :branch => 'master'``

* 静态库版本：在podfile中添加``pod 'InsightSDK’, :git => 'https://gitlab.com/InsightAR/InsightARSDK_iOS.git', :branch => 'master'``


手动集成（弃用）
---
1. 添加以下系统框架 
 * libz.1.2.5.tbd
 * GLKit.framework
 * CoreMotion.framework
 * Accelerate.framework
 * libstdc++.tbd
 * SystemConfiguration.framework
 * libz.1.2.5.tbd
 * CoreTelephony.framework
 * Security.framework
 * libsqlite3.tbd
 * libsqlite3.0.tbd
2. 添加`InsightSDK.framework`到工程中
3. 根据framework位置;添加`Header Search Path` 和`Library search
Path`如 `$(PROJECT_DIR)/InsightSDK.framework`
4. 添加`InsightSDK.framework`资源依赖`Target-> Build Phases -> Copy bundle Resources-> InsightSDK.framework`
5. **如果需要支持iOS8** , 需要添加 `CFNetwork.framework`为optional，疑似xcode的bug，[参考链接](http://stackoverflow.com/questions/24043532/dyld-symbol-not-found-nsurlauthenticationmethodclientcertificate-when-trying),如不能解决，`Foundation.framework`也同样需要设置为optional

### 系统要求

该项目最低支持 **Appple A9**  和 **iOS 11.0** 以及 **Xcode 14.0**  ,需要真机环境。模拟器可以编译通过，但是无法正确运行。

**注意**：2.9.0 版本以后不再支持 armv7, armv7s, and i386 和 bitcode，见 https://developer.apple.com/documentation/Xcode-Release-Notes/xcode-14-release-notes

### Demo示例

集成SDK有四种方式，请根据需要集成的方式仔细参考Demo中的示例。

-  PID模式（常用）

        - (void)pidARStart:(ARPrepareOptions *)options;  

- 云识别模式（常用）
         

        - (void)cloudARStart;  



-  本地PID（不常用）

        - (void)pidLocalARStart;  

-  本地云识别（不常用）

        - (void)cloudLocalARStart;  

### 代码参考

**1. 注册SDK**  

 ``#import <InsightSDK/InsightSDK.h>`` 到 AppDelegate中

       [[InsightARManager shareManager]registerAppKey:@"AR-90ED-84490A604B9D" appSecret:@""] 

**2. 配置初始化模式**
    
    初始化分为两种模式。一种``云识别模式``；一种``PID初始化模式``。后者是比较传统和常用的模式，根据传入的productID来进行初始化，比如初始化ID对应的场景是识别地面，加载资源沙发，然后这个场景+资源沙发的整个标识ID为202，则我们写成以下设置。getLastestVersion表示每一次都去服务器获取最新资源，如果获取失败则返回本地已有资源。如果该设置为NO，则不进行联网更新，直接使用本地资源。更多设置请参考 ``ARPrepareOptions ``
    
      ARPrepareOptions *options = [ARPrepareOptions new];  
      options.productID = @"202"  //资源ID；需要从服务器配置
      options.getLastestVersion = YES; //每次都检查是否有更新


**3. 开始下载**
         
    [[InsightARManager shareManager] prepareARWithOptions:options downloadProgress:^(NSProgress *downloadProgress) {
           //进度反馈
    } completion:^(NSError *error) {
        if (error != nil) {
            NSLog(@"prepareAR error: %@",error);
        }else{
            ARProduct *product = [[InsightARManager shareManager]getARProduct:@"1267"];
            [[InsightARManager shareManager] startInsightAR:product withARDelegate:self]; //初始化
        }
    }];

**4. 进行初始化**    
下载完成之后，进行初始化：

    ARProduct *product = [[InsightARManager shareManager]getARProduct:@"202"];
    [[InsightARManager shareManager] startInsightAR:product withARDelegate:self];

**Tips:AR初始化未完成之前不建议关闭（不然马上再次初始化容易造成线程等待），建议在InsightARStateDetecting 状态回调之后再关闭AR**

```
// 初始化完成的回调

- (void)insightARDidUpdateARState:(InsightARState)state
{
    if (_arView == nil) {
        // 添加ARView，记得不要多次添加
        _arStatus.hidden = NO;
        _arView = [[InsightARManager shareManager] getInsightARView];
        [self.view insertSubview:_arView atIndex:0];
    }
    switch (state) {
        case InsightARStateDetecting:{
            // hide HUD
        }
            break;

        default:
            break;
    }
}
```

[参考实现效果](doc/SDKInit.mov) 

**5. 添加ARView**    

    - (void)insightARDidUpdateARState:(InsightARState)state
    {
        if (_arView == nil) {
            _arView = [[InsightARManager shareManager] getInsightARView];
            [self.view insertSubview:_arView atIndex:0];
        }
    }

**5. 完成销毁：使用完成之后必须完成销毁**    
    
**Tips：这一步骤非常重要**
    

    [_arView removeFromSuperview];
    _arView = nil; //移除AR界面
    [[InsightARManager shareManager]stopInsightAR];  //停止AR
    [[InsightARManager shareManager]stopAllPrepare];  //清理下载相关

**云识别初始化**

云识别一般用于只开启一次相机，但是识别场景不确定的情况。比如打开AR相机之后需要扫A4纸加载沙发，去扫地图出现地球。但是我们预先不知道用户需要扫A4纸或者沙发。整个流程如下：

1. **步骤一：下载AlgorithmModel**（该资源决定能够进行识别场景，云识别表示该资源可以在服务器端配置更新，比如原先扫描A4纸和地图，我们可以增加一种扫描杂志的场景）；该过程无相机界面。
2. **步骤二：使用AlgorithmModel进行初始化和扫描**


        ARAlgPrepareOption *algOptions = [ARAlgPrepareOption new]; //创建ARAlgPrepareOption
        algOptions.updateTimeInterval = 300; //联网检测更新的间隔，默认最低300s，algOptions.getLastestVersion = YES时生效
        algOptions.getLastestVersion = YES; //默认YES，默认获取最新的云识别算法模型资源
        
        //步骤一：下载AlgorithmModel
        [[InsightARManager shareManager] prepareAlgorithmModel:algOptions downloadProgress:^(NSProgress *downloadProgress) {
            NSLog(@"AlgProgress is %@",downloadProgress);
        } completion:^(NSError *error) {
            if (error) {
                 NSLog(@"error is %@",error);
            } else {
              	//步骤二：使用AlgorithmModel进行扫描
                 ARAlgorithmModel *arcloudProduct = [[InsightARManager shareManager] getAlgorithmModel];
                 [[InsightARManager shareManager] startInsightARWithCloud:arcloudProduct withARDelegate:self];
            }
        }];


主要接口
---

**SDK初始化**

    /**
     Appple A7+ & iOS 8.0+ required ; x86 simulator is not supported
      */
    + (BOOL)isSDKSupported;
    
     /**
     get Cloud AR AlgorithmModel
     go ARAlgorithmModel.h file to see detail
     */
    - (void)prepareAlgorithmModel:(ARAlgPrepareOption *)options
                 downloadProgress:(ARDownloadProgressBlock)progressBlock
                       completion:(ARCompletionBlock)completionHandler;
                   
    /**
     stop Downloading AlgorithmModel
     */
    - (void)stopPrepareAlgorithmModel;
                   
    /**
     return local cloud recognition ARProduct,nil if not downloaded
     RegisterAppKey first,else return nil
     */
    - (ARAlgorithmModel *)getAlgorithmModel;               
                   
    /**
     get pid ar product
     */
    - (void)prepareARWithOptions:(ARPrepareOptions *)options
                downloadProgress:(ARDownloadProgressBlock)progressBlock
                      completion:(ARCompletionBlock)completionHandler;
    
    /**
     stop Downloading ARProduct
     */
    - (void)stopPrepareARProduct:(NSString *)pid;
    
    /**
     stop Downloading ARProduct && ARAlgorithmModel
     */
    - (void)stopAllPrepare;

**检查ARProduct大小、是否已下载等状态**

    /**
     return local pid ARProduct,nil if not downloaded；
     used for AR Starting
     RegisterAppKey first,else return nil
     */
    - (ARProduct *)getARProduct:(NSString *)pid;
    
    /**
     get all ARProducts online 
     */
    - (void)checkProductStatus:(ARCompletionBlock)completionHandler
                    result:(void(^)(NSArray <ARProduct *>*))result;

**删除AR场景**

    /**
     Delete local files of product with product ID
     */
    - (BOOL)removeResource:(NSSet <NSString *>*)pids;
    
    /**
     Delete All data ,should register first
     */
    - (void)clearAllData;

**打开AR相机**

    /**
     打开摄像机，开始运行云识别场景
     
     @param algorithmModel 不能为nil，通过getAlgorithmModel获取
     @param arDelegate 用于获取AR场景的State和Event，如果为空，将无法获取AR消息
     */
    - (void)startInsightARWithCloud:(ARAlgorithmModel *)algorithmModel     withARDelegate:(id<InsightARDelegate>)arDelegate;


    /**
     打开摄像机，开始运行指定pid的AR场景。startInsightAR和stopInsightAR必须成对出现
     
     @param arProduct 不能为nil，通过getARProduct
     @param arDelegate 用于获取AR场景的State和Event，如果为空，将无法获取AR消息
     */
    - (void)startInsightAR:(ARProduct *)arProduct withARDelegate:(id<InsightARDelegate>)arDelegate;
    
    /**
     重新打开AR场景， 不关闭摄像机
     
     @param arProduct 为nil时，这默认重新加载当前AR Product;
     */
    - (void)reloadInsightAR:(ARProduct *)arProduct;
    
    /**
     关闭摄像机，关闭AR场景；
     关闭AR场景后，并需要移除ARView；
     
     */
    - (void)stopInsightAR;
    
    /**
     native端可以通过执行脚本函数，与AR场景通信；
     可以执行的脚本函数，详情请看文档
     
     @param param 不能为空
     */
    - (void)runInsightARScript:(NSString *)scriptName param:(NSString *)param callback:(InsightARRunScriptBlock)block;

**正在下载时候进入后台的处理**   

AR无法在后台进行初始化，所以建议在进入后台的时候暂停下载；从后台返回重新走初始化流程

     - (void)onApplicationWillResignActive //到后台
    {
        [_arView removeFromSuperview];
        _arView = nil; //移除AR界面
        [[InsightARManager shareManager]stopInsightAR]; //停止AR
        [[InsightARManager shareManager]stopAllPrepare]; //清理下载相关
    }

**从竖屏转向横屏/或从横屏转向横屏时候使用AR**   

    [self interfaceOrientation: _product.direction];
    [[InsightARManager shareManager] startInsightAR:_product withARDelegate:self];

**处理LBS信息**

某些场景（和产品确认是否有）增加了LBS验证，ARCode限定了使用的地理位置

```objective-c

- (void)insightAREvent:(InsightAREvent *)event {
    switch (event.type) {
        case InsightAREvent_Function_ReloadARProduct: {
            // event.params 包含Pid、是否需要LBS的信息 
            BOOL needLBS  = [[event.params objectForKey:@"needLBS"]boolValue];
            double longitude = .0;
            double latitude = .0;
            if (needLBS) { //如果需要LBS
                latitude =  // 当前纬度;
                longitude = 130.244580000; // 当前经度
            }
            ARPrepareOptions *options = [[ARPrepareOptions alloc]
                                       initWithPrepareDictionary:event.params 
                                       longitude:longitude 
                                       latitude:latitude 
                                       coordiType:coordiType];
            [[InsightARManager shareManager] prepareARWithOptions:options    
                          downloadProgress:^(NSProgress *downloadProgress) {
             } completion:^(NSError *error) {
                            if (!error ) {
                              NSLog(@"prepareAR success");
                            }else{
                              // 处理错误
                            }
                          }];
        }
            break;
        default:
            break;
    }
}
```

**动态加载**

    // SDK 支持传入本地资源进行动态资源加载，具体协议需要和内容约定

    if(event.type == InsightAREvent_Function_RunScript){
        NSString *scriptName =  [event.params objectForKey:@"script"];
        NSString *runScriptName =  [event.params objectForKey:@"identity"];
        
        if ([scriptName isEqualToString:@"LoadModel"] && [runScriptName isEqualToString:@"g_LoadLocalModel"]) {
            // Important: skuName不应重复
            NSString *skuName = [[event.params objectForKey:@"json"] objectForKey:@"skuId"];
            
            // skuName = @"scene1"; sku 名字一般从内容返回，也可以指定本地资源文件名字
            
            // To be impl: 实际中应该由skuName（不会重复）去寻找相应的skuPath，这边是直接设置了。
            // skuPath:本地资源绝对路径
            NSString *skuPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingString:@"/dynamic.bundle/scene/"];
            
            // 最终确保在skuPath下，存在名skuName的.res文件
            NSString *scriptParam = [NSString stringWithFormat:@"{\"path\":\"%@\",\"name\":\"%@\"}", skuPath, skuName];
            [[InsightARManager shareManager] runInsightARScript:runScriptName param:scriptParam callback:nil];
        }
    }

**以上为部分接口用法，云识别和使用本地模型以及检查资源版本等更详尽的示例请参考SDK Demo 和接口文档**

Insight SDK 内容接口协议
---

见《Insight SDK 内容接口协议》


### SDK发送给第三方App信息
IARDelegate的代理方法 - (void)insightAREvent:(IAREvent *)event;

---
1.执行指定脚本（APP必须实现）

| 消息事件详细信息 | Type | Key | Value |
|:--|:--------|:--------|:--------|
| RunScript | 100 | script | NSString |
| | | json | |
| | | api | |

---
2.关闭AR页面（APP必须实现）

| 消息事件详细信息 | Type | Key | Value |
|:--|:--------|:--------|:--------|
| CloseARScene | 101 |  \ | \ |

---
3.重启AR内容（APP必须实现）

| 消息事件详细信息 | Type | Key | Value |
|:--|:--------|:--------|:--------|
| ReloadARProduct | 102 | name | ar product pid |

---
4.跳转链接（APP必须实现）

| 消息事件详细信息 | Type | Key | Value |
|:--|:--------|:--------|:--------|
| OpenURL | 108 | url | URL address |
| | | type | 打开链接的方式（JSON），由lua开发者与APP约定。如约定为：{"urlType":"1","param1":"showBrandInfo"}。APP根据这个type可以进行不同处理|

---
5.截屏（APP必须实现）

| 消息事件详细信息 | Type | Key | Value |
|:--|:--------|:--------|:--------|
| ScreenShot | 110 | image | UIImage |

> ####***v2.0.0升级提示***
> 
> 截屏操作已更新为默认保存相册、如果接入方之前已有保存相册的操作，请注意更新代码；
> 方案参考：
> 
> 1. 若无特殊需求，接入方可移除保存相册相关的代码，遵循SDK的默认保存逻辑（推荐）
> 2. SDK对默认动作提供了可选方案，可以通过实现 ``InsightARDelegate`` 的如下代理方法，关闭默认动作
> 
```objective-c
/**
 自定义指定类型，不使用默认实现，目前可以自定义的传入值有如下几种
 InsightAREvent_Function_ScreenShot
 InsightAREvent_Function_Share
 InsightAREvent_StartScreenRecord
 InsightAREvent_StopScreenRecord
 @param type InsightAREventType
 @return 是否自定义；默认为NO
 */
- (BOOL)customInsightAREventType:(InsightAREventType)type;
```
> 
> 

---
6.分享（APP必须实现）

| 消息事件详细信息 | Type | Key | Value |
|:--|:--------|:--------|:--------|
| Share | 111 | type | NSString  //type : 1 text; 2 image; 3 music; 4 video; 5 url |
| | | title | NSString|
| | | desc ``v2.0.0更新`` | NSString|
| | | url | NSString|
| | | image | UIImage|

> ####***v2.0.0升级提示***
> 
> 参数变动：由 ``description`` 更新为 ``desc``

### 第三方App发送给SDK信息
`- (void)runInsightARScript:(NSString *)scriptName param:(NSString *)param callback:(InsightARRunScriptBlock)block;`

两种情况

1、APP收到SDK发送的RunScript消息，为了通用性，建议与具体AR事件相关的RunScript消息，由客户端发送给服务器，服务器根据AR事件pid做对应的处理，将处理好的json，发送给客户端，客户端再直接将处理好的结果发送给SDK

2、某些特定的AR事件，必须由客户端支持的，可以由客户端直接runInsightARScript来执行，执行结果将在回调中返回给客户端

验证SDK集成是否成功
---

请按照doc/SDK接入-AR  sample测试用例.docx文档里的测试用例，测试AR功能是否集成成功



关于AppKey和AppSecret：
---

！！！当您发布至线上时，请务必使用【正式AppKey和AppSecret】！！！

