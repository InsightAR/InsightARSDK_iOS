//
//  AppDelegate.h
//  InsightSDK_dylib_Demo
//
//  Created by Dikey on 2018/7/11.
//  Copyright © 2018 Dikey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

