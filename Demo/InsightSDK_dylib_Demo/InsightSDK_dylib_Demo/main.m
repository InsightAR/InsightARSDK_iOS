//
//  main.m
//  InsightSDK_dylib_Demo
//
//  Created by Dikey on 2018/7/11.
//  Copyright © 2018 Dikey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
