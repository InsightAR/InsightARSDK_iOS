
local ImageDisappear = {}


function ImageDisappear:New( game_object )
	if self ~= ImageDisappear then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = ImageDisappear } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function ImageDisappear:Start( )
	--self.beginAlpha = 1
	self.endAlpha = 0

	self.time = 0.5
	self.tempTime = 0

	self.speed = 2

	self.imageCom = self.game_object:GetComponent( "Image")

	self.bDisappear = false
	-- local color = self.imageCom.color
	-- self.imageCom.color = Insight.Vector4.New(color.x, color.y, color.z, self.beginAlpha)
end


function ImageDisappear:Update()
	if self.bDisappear then
		if self.tempTime < self.time then
			self.tempTime = self.tempTime + Insight.Time.deltaTime
			local color = self.imageCom.color
			local r = color.r
			local g = color.g
			local b = color.b
			local a = color.a

			local finalA = a - self.speed * Insight.Time.deltaTime
			if finalA < self.endAlpha then
				finalA = self.endAlpha
				self.game_object:SetActive( false  )
			end
			self.imageCom.color = Insight.Vector4.New(r, g, b, finalA)
		end
	end
end


return ImageDisappear;
