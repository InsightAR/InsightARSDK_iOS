
local GameController = {};



function GameController:New( game_object )
	if self ~= GameController then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GameController } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function GameController:Start()
    self.g_BrushBlack = 0
    self.g_BrushWhite = 1

    self.g_AllStates = {}
    self.g_CurState = nil
    self.g_StateGoodToothAppear = 0   
    self.g_StateDirtyToothAppear = 1
    self.g_StateConfirm = 2
    self.g_StateBrushAppear = 3
    self.g_StateBrushSelect = 4    
    self.g_StateCountdown = 6
    self.g_StateStartGame = 7
    self.g_StateRunningGame = 8
    self.g_StateEndGame = 9

    self.selectedBrush = self.g_BrushBlack

    self.goodTooth = Insight.GameObject.Find("Main Camera/GoodTooth")
    self.animGoodTooth = self.goodTooth:GetComponent("Animator")

    self.dirtyTooth = Insight.GameObject.Find("Main Camera/DirtyTooth")
    self.animDirtyTooth = self.dirtyTooth:GetComponent("Animator")

    self.snow = Insight.GameObject.Find("snow_Particle_2type")
    self.sky = Insight.GameObject.Find("qiuti")

    self.bCloseMusic = false

    -- self.close = Insight.GameObject.Find("Canvas/Close")
    -- self.closeMusic = Insight.GameObject.Find("Canvas/closeMusic")
   
	Insight.Debug.Log( "Lua GameController Start: " .. self.game_object.name .. "\n" );

    self.g_AllStates[self.g_StateGoodToothAppear] = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameStateGoodToothAppear.lua")
    self.g_AllStates[self.g_StateDirtyToothAppear] = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameStateDirtyToothAppear.lua")
    self.g_AllStates[self.g_StateConfirm] = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameStateConfirm.lua")
    self.g_AllStates[self.g_StateBrushAppear] = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameStateBrushAppear.lua")
    self.g_AllStates[self.g_StateBrushSelect] = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameStateBrushSelect.lua")    
    self.g_AllStates[self.g_StateCountdown] = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameStateCountdown.lua")
    
    self.g_AllStates[self.g_StateStartGame] = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameStateStart.lua")
    self.g_AllStates[self.g_StateRunningGame] = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameStateRunning.lua")
    self.g_AllStates[self.g_StateEndGame] = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameStateEnd.lua")

end


function GameController:ImageSlowAppear(entity, speed)
    local imageCom = entity:GetComponent("Image")
    local r = imageCom.color.x
    local g = imageCom.color.y
    local b = imageCom.color.z
    local a = imageCom.color.w

    local finalA = a + speed * Insight.Time.deltaTime
    if finalA > 1 then
        finalA = 1
    end
    imageCom.color = Insight.Vector4.New(r, g, b, finalA)
end
function GameController:ImageSlowDisappear(entity, speed)
    local imageCom = entity:GetComponent("Image")
    local r = imageCom.color.x
    local g = imageCom.color.y
    local b = imageCom.color.z
    local a = imageCom.color.w

    local finalA = a - speed * Insight.Time.deltaTime
    if finalA < 0 then
        finalA = 0
    end
    imageCom.color = Insight.Vector4.New(r, g, b, finalA)
end

function GameController:ChangeState(state)
    --Insight.Debug.Log( "Lua GameController ChangeState: " .. "\n" );
    if self.g_AllStates[state] ~= self.g_CurState then
        if self.g_CurState ~= nil then
            self.g_CurState:Exit()
        end
        self.g_CurState = self.g_AllStates[state]
        self.g_CurState:Enter()
    end
end

function GameController:Update()
    if self.g_CurState~= nil then
        self.g_CurState:UpdateState()
    end
end

return GameController;
