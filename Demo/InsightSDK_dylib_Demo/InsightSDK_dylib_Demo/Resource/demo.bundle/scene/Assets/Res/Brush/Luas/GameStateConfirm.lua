
local GameStateConfirm = {};

function GameStateConfirm:New( game_object )
	if self ~= GameStateConfirm then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GameStateConfirm } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function GameStateConfirm:Start()
    self.gameController = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameController.lua")
	Insight.Debug.Log( "Lua GameStateConfirm Start: " .. self.game_object.name .. "\n" );
	
	self.uiRoot = Insight.GameObject.Find("Canvas/Confirm")	
	self.Ok = Insight.GameObject.Find("Canvas/Confirm/Ok")
	self.finger = Insight.GameObject.Find("Canvas/Confirm/finger")
	self.text = Insight.GameObject.Find("Canvas/Confirm/text")
	self.blackBrush = Insight.GameObject.Find("Canvas/Confirm/blackBrush")

	self.video = Insight.GameObject.Find("Main Camera/Video")
	self.videoAnim = self.video:GetComponent("Animator")

	self:Reset()
end

function GameStateConfirm:Reset()
	self.uiRoot:SetActive( false  )	
	self.video:SetActive(false)	
end


function GameStateConfirm:UpdateState() 
	if self.videoAnim.normalizedTime > 0.97 then
		self.videoAnim:Play("video")
	end
end

function GameStateConfirm:Enter()
	Insight.Debug.Log( "Lua GameStateConfirm Enter: " .. self.game_object.name .. "\n" );
	self.uiRoot:SetActive(true)	
	self.video:SetActive(true)
	self.videoAnim:Play("video")
end

function GameStateConfirm:Exit()
	Insight.Debug.Log( "Lua GameStateConfirm Exit: " .. self.game_object.name .. "\n" );
	self.videoAnim:Play("video_disable")
	--self:Reset()
	self.video:SetActive(false)	
	self.Ok:GetComponent("Assets/Res/Brush/Luas/ImageDisappear.lua").bDisappear = true
	self.finger:GetComponent("Assets/Res/Brush/Luas/ImageDisappear.lua").bDisappear = true
	self.text:GetComponent("Assets/Res/Brush/Luas/ImageDisappear.lua").bDisappear = true
	self.blackBrush:GetComponent("Assets/Res/Brush/Luas/ImageDisappear.lua").bDisappear = true
	
end

return GameStateConfirm;
