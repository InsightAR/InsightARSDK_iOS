
local MoveController = {}


function MoveController:New( game_object )
	if self ~= MoveController then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = MoveController } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function MoveController:Start( )
	self.initSpeed = 0.3
	self.speed = self.initSpeed
	self.fAccSpeed = 0.2
	self.bTouch = false
end

function MoveController:SetSpeed(speed)
	self.initSpeed = speed
	self.speed = speed
end

function MoveController:Update()
	if self.bTouch then
		self.speed = self.speed - self.fAccSpeed* Insight.Time.deltaTime
		if self.speed < 0 then
			self.speed = 0
		end			
	else
		if self.speed < self.initSpeed then
			self.speed = self.speed + self.fAccSpeed * Insight.Time.deltaTime
		else 
			self.speed = self.initSpeed
		end
	end
end


return MoveController;
