-- ------------------------------
-- Copyright (c) NetEase Insight
-- All rights reserved.
-- ------------------------------

local ButtonOk = {};

function ButtonOk:New( game_object )
	if self ~= ButtonOk then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = ButtonOk } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;

	return new_instance;
end

function ButtonOk:Start()
	self.gameController = Insight.GameObject.Find("main"):GetComponent("Assets/Res/Brush/Luas/GameController.lua")
end

function ButtonOk:OnPointerDown()
	
end

function ButtonOk:OnPointerUp()
	Insight.Debug.Log( "Ok!\n"  )	
	self.gameController:ChangeState(self.gameController.g_StateBrushAppear)
end

return ButtonOk;
