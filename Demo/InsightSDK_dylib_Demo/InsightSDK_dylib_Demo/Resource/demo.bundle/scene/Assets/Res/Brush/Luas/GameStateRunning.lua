
local GameStateRunning = {};


function GameStateRunning:New( game_object )
	if self ~= GameStateRunning then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GameStateRunning } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function GameStateRunning:Start()
    self.gameController = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameController.lua")
    Insight.Debug.Log( "Lua GameStateRunning Start: " .. self.game_object.name .. "\n" );

    --self.uiRoot = Insight.GameObject.Find("Canvas/GameRunning")
    self.face = Insight.GameObject.Find("Canvas/faceBoy")   

    self.time = Insight.GameObject.Find("Canvas/time")
    self.naozhong = Insight.GameObject.Find("Canvas/naozhong")



    self.GameRunning = Insight.GameObject.Find("GameRunning")

    self.TeethRoot = Insight.GameObject.Find("GameRunning/teeth")
 
    self.teethManager = self.TeethRoot:GetComponent("Assets/Res/Brush/Luas/TeethManager.lua")
   
    self.camera = Insight.GameObject.Find("Main Camera"):GetComponent("Camera") 

    
    self.WaveClean = Insight.GameObject.Find("Main Camera/Wave_1")
    self.WaveDirty = Insight.GameObject.Find("Main Camera/Wave_2")


    self.addFaceMusic = Insight.GameObject.Find("addFaceMusic"):GetComponent("AudioSource", 0)

    self.clearTime = Insight.GameObject.Find("Canvas/clearTime")

    self.close = Insight.GameObject.Find("Canvas/Close")
    self.closeMusic = Insight.GameObject.Find("Canvas/CloseMusic")
    self.openMusic = Insight.GameObject.Find("Canvas/OpenMusic")

    self.yOffset = 200
    if ( Insight.Screen.width == 2436 and Insight.Screen.height == 1125  ) then        
        local pos = self.face:GetComponent("RectTransform").anchoredPosition3D
        self.face:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)  
        pos = self.time:GetComponent("RectTransform").anchoredPosition3D 
        self.time:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)
        pos = self.naozhong:GetComponent("RectTransform").anchoredPosition3D
        self.naozhong:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)  
        pos = self.close:GetComponent("RectTransform").anchoredPosition3D
        self.close:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z) 
        pos = self.closeMusic:GetComponent("RectTransform").anchoredPosition3D
        self.closeMusic:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)
        pos = self.openMusic:GetComponent("RectTransform").anchoredPosition3D
        self.openMusic:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z) 
    end

    print("width and height = " .. Insight.Screen.width .. " " .. Insight.Screen.height )

    -- iPhone X XS XSMax XR
    if ( Insight.Screen.width ==1125 and Insight.Screen.height == 2436 ) or  ( Insight.Screen.width ==1242 and Insight.Screen.height == 2688 ) or  ( Insight.Screen.width ==828 and Insight.Screen.height == 1792 )
        or  ( Insight.Screen.width ==750 and Insight.Screen.height == 1624 ) then
        local pos = self.face:GetComponent("RectTransform").anchoredPosition3D
        self.face:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)  
        pos = self.time:GetComponent("RectTransform").anchoredPosition3D 
        self.time:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)
        pos = self.naozhong:GetComponent("RectTransform").anchoredPosition3D
        self.naozhong:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)  
        pos = self.close:GetComponent("RectTransform").anchoredPosition3D
        self.close:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z) 
        pos = self.closeMusic:GetComponent("RectTransform").anchoredPosition3D
        self.closeMusic:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)
        pos = self.openMusic:GetComponent("RectTransform").anchoredPosition3D
        self.openMusic:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z) 
    end

    if Insight.Screen.width ==2280 and Insight.Screen.height == 1080 then
        local pos = self.face:GetComponent("RectTransform").anchoredPosition3D
        self.face:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)  
        pos = self.time:GetComponent("RectTransform").anchoredPosition3D 
        self.time:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)
        pos = self.naozhong:GetComponent("RectTransform").anchoredPosition3D
        self.naozhong:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)  
        pos = self.close:GetComponent("RectTransform").anchoredPosition3D
        self.close:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z) 
        pos = self.closeMusic:GetComponent("RectTransform").anchoredPosition3D
        self.closeMusic:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)
        pos = self.openMusic:GetComponent("RectTransform").anchoredPosition3D
        self.openMusic:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z) 
    end

    if Insight.Screen.width ==1080 and Insight.Screen.height == 2280 then
        local pos = self.face:GetComponent("RectTransform").anchoredPosition3D
        self.face:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)  
        pos = self.time:GetComponent("RectTransform").anchoredPosition3D 
        self.time:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)
        pos = self.naozhong:GetComponent("RectTransform").anchoredPosition3D
        self.naozhong:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)  
        pos = self.close:GetComponent("RectTransform").anchoredPosition3D
        self.close:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z) 
        pos = self.closeMusic:GetComponent("RectTransform").anchoredPosition3D
        self.closeMusic:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)
        pos = self.openMusic:GetComponent("RectTransform").anchoredPosition3D
        self.openMusic:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z) 
    end

    if Insight.Screen.width ==1792 and Insight.Screen.height == 828 then
        local pos = self.face:GetComponent("RectTransform").anchoredPosition3D
        self.face:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)  
        pos = self.time:GetComponent("RectTransform").anchoredPosition3D 
        self.time:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)
        pos = self.naozhong:GetComponent("RectTransform").anchoredPosition3D
        self.naozhong:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)  
        pos = self.close:GetComponent("RectTransform").anchoredPosition3D
        self.close:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z) 
        pos = self.closeMusic:GetComponent("RectTransform").anchoredPosition3D
        self.closeMusic:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)
        pos = self.openMusic:GetComponent("RectTransform").anchoredPosition3D
        self.openMusic:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z) 
    end

    if Insight.Screen.width ==828 and Insight.Screen.height == 1792 then
        local pos = self.face:GetComponent("RectTransform").anchoredPosition3D
        self.face:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)  
        pos = self.time:GetComponent("RectTransform").anchoredPosition3D 
        self.time:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)
        pos = self.naozhong:GetComponent("RectTransform").anchoredPosition3D
        self.naozhong:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)  
        pos = self.close:GetComponent("RectTransform").anchoredPosition3D
        self.close:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z) 
        pos = self.closeMusic:GetComponent("RectTransform").anchoredPosition3D
        self.closeMusic:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z)
        pos = self.openMusic:GetComponent("RectTransform").anchoredPosition3D
        self.openMusic:GetComponent("RectTransform").anchoredPosition3D = Insight.Vector3.New(pos.x, pos.y - self.yOffset, pos.z) 
    end
    
    Insight.Debug.Log("lua w:" .. Insight.Screen.width)
    Insight.Debug.Log("lua h:" .. Insight.Screen.height)
    self:Reset()
end

function GameStateRunning:Reset()
    --self.uiRoot:SetActive(false)
    self.face:SetActive(false) 
    self.clearTime:SetActive(false)
    self.time:SetActive(false)
    self.naozhong:SetActive(false)
    
    self.GameRunning:SetActive( false  )
 
    self.WaveClean:SetActive(false)
    self.WaveDirty:SetActive(false)

    self.AimBrush = nil

    self.AimTime = 0
    self.CleanTime = 2

    self.tempClearTime = 0

    self.GameDuration = 60
    self.curTime = 0
end

function GameStateRunning:UpdateGameTime()
    if self.curTime > self.GameDuration then
        self.gameController:ChangeState(self.gameController.g_StateEndGame)
    end
    self.curTime = self.curTime + Insight.Time.deltaTime
    
    self.time:GetComponent("Text").text = math.ceil(60 - self.curTime)--60 - self.curTime
end

function GameStateRunning:PlayDirtyEffect(entity)   
    self.WaveClean:SetActive(false)
    self.WaveDirty:SetActive(true)  
    local animLua = entity:GetComponent("Assets/Res/Brush/Luas/AnimPlayer.lua")
    animLua.curAnim = animLua.touch

    local effectLua = self.AimBrush:GetComponent("Assets/Res/Brush/Luas/EffectCleanPlayer.lua")    
    effectLua.bPlay = true  
    effectLua.starEffect = false
   
    local moveCom = self.AimBrush:GetComponent("Assets/Res/Brush/Luas/MoveController.lua")    
    moveCom.bTouch = true

    self.clearTime:SetActive(true)
    if self.tempClearTime > self.CleanTime then
        self.tempClearTime = 0
        self.clearTime:SetActive(false)
    end
    self.tempClearTime = self.tempClearTime + Insight.Time.deltaTime

    local percent = (self.tempClearTime / self.CleanTime)* 100   
    self.clearTime:GetComponent("Text").text = math.ceil(percent) .. "%"
end

function GameStateRunning:PlayDefaultEffect()   
    self.WaveClean:SetActive(true)
    self.WaveDirty:SetActive(false) 
    self.clearTime:SetActive(false)  
    if self.AimBrush ~= nil then
        self.AimBrush:GetComponent("Assets/Res/Brush/Luas/MoveController.lua").bTouch = false
        self.tempClearTime = 0
    end
end

function GameStateRunning:PlayCleanEffect(entity)   
    self:PlayDefaultEffect()
    local animLua = entity:GetComponent("Assets/Res/Brush/Luas/AnimPlayer.lua")
    animLua.curAnim = animLua.touch

    local effectLua = self.AimBrush:GetComponent("Assets/Res/Brush/Luas/EffectCleanPlayer.lua")    
    effectLua.bPlay = true  
    effectLua.starEffect = true
    self.clearTime:SetActive(false)
    -- local moveCom = self.AimBrush:GetComponent("Assets/Res/Brush/Luas/MoveController.lua")    
    -- moveCom.bTouch = false
end

function GameStateRunning:PlayCleanCompleteEffect(entity)
    self.WaveClean:SetActive(true)
    self.WaveDirty:SetActive(false)
  
    local animLua = entity:GetComponent("Assets/Res/Brush/Luas/AnimPlayer.lua")
    animLua.curAnim = animLua.changeMind

    local effectLua = self.AimBrush:GetComponent("Assets/Res/Brush/Luas/EffectPlayer.lua")    
    effectLua.bPlay = true 

    -- local effectCleanLua = self.AimBrush:GetComponent("Assets/Res/Brush/Luas/EffectCleanPlayer.lua")    
    -- effectCleanLua.bPlay = true
   
    entity.transform:Find("Bone001/Bone002/maozi").gameObject:SetActive(true)

    self.clearTime:SetActive(false)
    local moveCom = self.AimBrush:GetComponent("Assets/Res/Brush/Luas/MoveController.lua")    
    moveCom.bTouch = false
    self.tempClearTime = 0
end

function GameStateRunning:ProcessHitEntity()
    local bDirty = self.teethManager:IsDirty(self.AimBrush)
    if self.AimTime > self.CleanTime then  
        if bDirty then
            self.teethManager:CleanTooth(self.AimBrush)
            self:PlayCleanCompleteEffect(self.AimBrush)              
        end                
        --self.AimTime = 0
        --self.AimBrush = nil	
    else        
        if bDirty then
            self:PlayDirtyEffect(self.AimBrush)
        else
            self:PlayCleanEffect(self.AimBrush)
        end
    end
end

function GameStateRunning:FindAndCleanup()
    local ray = self.camera:ViewportPointToRay( Insight.Vector3.New( 0.5, 0.5, 0.0 ));
	local hit,res = Insight.Physics.Raycast( ray.origin, ray.direction, 1000, 4294967295);

    if hit then  
        local hitentity = res.transform.gameObject
        if hitentity and hitentity.activeSelf then    
               
            if self.AimBrush ~= hitentity then 
                if self.AimBrush~= nil then
                    local moveCom = self.AimBrush:GetComponent("Assets/Res/Brush/Luas/MoveController.lua")    
                    moveCom.bTouch = false
                    self.tempClearTime = 0
                end
                self.AimBrush = hitentity;
                self.AimTime = 0;					
            end
            self.AimTime = self.AimTime + Insight.Time.deltaTime          
            self:ProcessHitEntity()
        else           
            self.AimTime = 0;            
            self:PlayDefaultEffect() 
            self.AimBrush = nil;    
                   
        end		
    else
        self.AimTime = 0;        
        self:PlayDefaultEffect() 
        self.AimBrush = nil;   
           
    end
end

function GameStateRunning:AddFaceCount()
    self.faceCount = self.faceCount + 1
    self.face_text.text = self.faceCount   
    
    if not self.gameController.bCloseMusic then
        self.addFaceMusic:Play()
    else
        self.addFaceMusic:Pause()
    end
end

function GameStateRunning:UpdateState()
    
    self:FindAndCleanup()

    self:UpdateGameTime()

end

function GameStateRunning:Enter()
    Insight.Debug.Log( "Lua GameStateRunning Enter: " .. self.game_object.name .. "\n" );    
    
    self.teethManager:FollowOrbit()

    --self.uiRoot:SetActive(true)

    self.time:SetActive(true)
    self.naozhong:SetActive(true)
    self.GameRunning:SetActive( true  )

    self.face:SetActive(true)
    self.face_text = self.face.transform:Find("text").gameObject:GetComponent("Text")    
    self.face_text.text = "0"
    self.faceCount = 0

    self:PlayDefaultEffect()

    self.gameController.sky:SetActive(true)
    --self.gameController.bg:SetActive(false)

end

function GameStateRunning:Exit()
    Insight.Debug.Log( "Lua GameStateRunning Exit: " .. self.game_object.name .. "\n" );
    self:Reset()
end

return GameStateRunning;
