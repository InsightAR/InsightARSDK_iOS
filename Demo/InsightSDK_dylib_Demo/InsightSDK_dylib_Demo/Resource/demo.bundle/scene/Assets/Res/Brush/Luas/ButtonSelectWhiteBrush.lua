-- ------------------------------
-- Copyright (c) NetEase Insight
-- All rights reserved.
-- ------------------------------

local ButtonSelectWhiteBrush = {};

function ButtonSelectWhiteBrush:New( game_object )
	if self ~= ButtonSelectWhiteBrush then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = ButtonSelectWhiteBrush } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;

	return new_instance;
end

function ButtonSelectWhiteBrush:Start()
	self.gameController = Insight.GameObject.Find("main"):GetComponent("Assets/Res/Brush/Luas/GameController.lua")
end

function ButtonSelectWhiteBrush:OnPointerDown()
	
end

function ButtonSelectWhiteBrush:OnPointerUp()
	Insight.Debug.Log( "Girl!\n"  )
	self.gameController.selectedBrush = self.gameController.g_BrushWhite
	self.gameController:ChangeState(self.gameController.g_StateBrushSelect)
end

return ButtonSelectWhiteBrush;
