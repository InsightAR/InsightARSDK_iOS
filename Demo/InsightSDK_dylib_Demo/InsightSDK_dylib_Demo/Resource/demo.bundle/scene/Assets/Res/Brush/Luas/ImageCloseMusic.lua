-- ------------------------------
-- Copyright (c) NetEase Insight
-- All rights reserved.
-- ------------------------------

local ImageCloseMusic = {};

function ImageCloseMusic:New( game_object )
	if self ~= ImageCloseMusic then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = ImageCloseMusic } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;

	return new_instance;
end

function ImageCloseMusic:Start()
	self.gameController = Insight.GameObject.Find("main"):GetComponent("Assets/Res/Brush/Luas/GameController.lua")
	local bgMusic = Insight.GameObject.Find("bgMusic")
	self.backgroundMusic = bgMusic:GetComponent( "AudioSource" , 0 ); 
	
	self.openMusic = Insight.GameObject.Find("Canvas/OpenMusic")
end

function ImageCloseMusic:OnPointerUp()
	Insight.Debug.Log( "ImageOpenMusic!\n"  )	
	self.gameController.bCloseMusic = false
	self.backgroundMusic:Play()
	self.game_object:SetActive(false)
	self.openMusic:SetActive( true  )
end

return ImageCloseMusic;
