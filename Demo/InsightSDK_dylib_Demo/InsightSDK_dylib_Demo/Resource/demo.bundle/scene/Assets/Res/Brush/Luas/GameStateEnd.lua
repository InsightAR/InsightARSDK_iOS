
local GameStateEnd = {};



function GameStateEnd:New( game_object )
	if self ~= GameStateEnd then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GameStateEnd } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function GameStateEnd:Start()
	
	self.rootUI = Insight.GameObject.Find("Canvas/GameEnd")
	self.text = Insight.GameObject.Find("Canvas/GameEnd/text")
	Insight.Debug.Log( "Lua GameStateEnd Start: " .. self.game_object.name .. "\n" );
	self.gameController = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameController.lua")

	

	self:Reset()
end

function GameStateEnd:UpdateState()
	if self.tempDuration > self.duration then
		if not self.bSend then
			self:Send2H5()
			self.bSend = true
		end
	end
	self.tempDuration = self.tempDuration + Insight.Time.deltaTime    
end

function GameStateEnd:Reset()
	self.rootUI:SetActive( false  )
	self.duration = 2
	self.tempDuration = 0
	self.bSend = false
end

function GameStateEnd:Send2H5()
	local stringResult = "https://h5.m.jd.com/babelDiy/Zeus/4HzenEZ3r81mcjsrXPNdkqAcutRP/index.html?gender="
	if self.gameController.selectedBrush == self.gameController.g_BrushBlack then
        stringResult = stringResult .. "0"
    else
        stringResult = stringResult .. "1"
	end
	stringResult = stringResult .. "&result=" .. self.gameController.g_AllStates[self.gameController.g_StateRunningGame].faceCount .. "___null"
	--Insight.Event.Happen( 1 , 1 , 108 , "www.163.com? gender=1&result=0___null")
    Insight.Event.Happen( 1 , 1 , 108 , stringResult );
end

function GameStateEnd:Enter()
	 Insight.Debug.Log( "Lua GameStateEnd Enter: " .. self.game_object.name .. "\n" );
	self.rootUI:SetActive(true)

	self.clearSuc = Insight.GameObject.Find("Canvas/clearSuc")
	self.clearSuc:SetActive(false)
end

function GameStateEnd:Exit()
	Insight.Debug.Log( "Lua GameStateEnd Exit: " .. self.game_object.name .. "\n" );
	self:Reset()
end

return GameStateEnd;
