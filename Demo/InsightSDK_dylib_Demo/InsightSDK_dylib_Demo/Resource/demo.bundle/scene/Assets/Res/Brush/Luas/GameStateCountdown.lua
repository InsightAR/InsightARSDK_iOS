
local GameStateCountdown = {};



function GameStateCountdown:New( game_object )
	if self ~= GameStateCountdown then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GameStateCountdown } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function GameStateCountdown:Start()
    self.gameController = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameController.lua")
	Insight.Debug.Log( "Lua GameStateCountdown Start: " .. self.game_object.name .. "\n" );
	
	self.uiRoot = Insight.GameObject.Find("Canvas/Countdown")
	self.number3 = Insight.GameObject.Find( "Canvas/Countdown/number3"  )
	self.number2 = Insight.GameObject.Find( "Canvas/Countdown/number2"  )
	self.number1 = Insight.GameObject.Find( "Canvas/Countdown/number1"  )	

	self.countDownOverMusic = Insight.GameObject.Find("countDownOverMusic")

	self:Reset()
end

function GameStateCountdown:Reset()
	self.numberTime = 1
	self.tempNumberTime2 = 0

	self.tempNumberTime1 = 0

	self.duration = 3
	self.tempDuration = 0

	self.uiRoot:SetActive( false  )
	self.number3:SetActive(false)
	self.number2:SetActive(false)
	self.number1:SetActive(false)
end

function GameStateCountdown:UpdateNumberTime()
	if self.tempNumberTime2 > self.numberTime then		
		self.number2:SetActive(true)
		self.number1:SetActive(false)
		self.number3:SetActive(false)
	end	
	self.tempNumberTime2 = self.tempNumberTime2 + Insight.Time.deltaTime

	if self.tempNumberTime1 > self.numberTime * 2 then
		self.number2:SetActive(false)
		self.number1:SetActive(true)
		self.number3:SetActive(false)
	end
	self.tempNumberTime1 = self.tempNumberTime1 + Insight.Time.deltaTime
end

function GameStateCountdown:UpdateState()
   
	self:UpdateNumberTime()

	if self.tempDuration > self.duration then
		self.gameController:ChangeState(self.gameController.g_StateStartGame)
	end

	self.tempDuration = self.tempDuration + Insight.Time.deltaTime

end

function GameStateCountdown:Enter()
	Insight.Debug.Log( "Lua GameStateCountdown Enter: " .. self.game_object.name .. "\n" );
	self.uiRoot:SetActive(true)
	self.number3:SetActive(true)
	
	--self.gameController.animTooth:Play("leave")
end

function GameStateCountdown:Exit()
	Insight.Debug.Log( "Lua GameStateCountdown Exit: " .. self.game_object.name .. "\n" );
	self:Reset()

	if self.gameController.bCloseMusic then
		self.countDownOverMusic:GetComponent("AudioSource" , 0):Pause()
	else
		self.countDownOverMusic:GetComponent("AudioSource" , 0):Play()
	end
end

return GameStateCountdown;
