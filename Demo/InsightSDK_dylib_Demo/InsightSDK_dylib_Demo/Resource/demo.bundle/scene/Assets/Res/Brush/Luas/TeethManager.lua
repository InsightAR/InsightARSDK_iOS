
local TeethManager = {}

function TeethManager:New( game_object )
	if self ~= TeethManager then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = TeethManager } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function TeethManager:Start( )	
	self.gameController = Insight.GameObject.Find("main"):GetComponent("Assets/Res/Brush/Luas/GameController.lua")
	Insight.Debug.Log("TeethManager start!" .. "\n")
	-- self.point1Tag = {false,false, false,false,false,false,false,false, false}
	-- self.point2Tag = {false,false, false,false,false,false,false,false, false}
	-- self.point3Tag = {false,false, false,false,false,false,false,false, false}
	-- self.point4Tag = {false,false, false,false,false,false,false,false, false}
end

-- function TeethManager:GetRandomPathPoint(points)
-- 	local cleanPoints = {}
-- 	for k,v in pairs(points) do
-- 		if v == false then
-- 			table.insert(cleanPoints, k)
-- 		end
-- 	end

-- 	local ran = math.random(1, #cleanPoints)
-- 	points[cleanPoints[ran]] = true
-- 	return cleanPoints[ran]
-- end

function TeethManager:FollowOrbit()

	Insight.Debug.Log("TeethManager FollowOrbit!" .. "\n")
	self.DirtyTeeth = {}
	self.DirtyCount = 18
	
	for i=1, self.DirtyCount do
		local dirtyTooth = Insight.GameObject.Find("GameRunning/teeth/Dirty/" .. i)	
		local animLua = dirtyTooth:GetComponent("Assets/Res/Brush/Luas/AnimPlayer.lua")
		animLua.bDirty = true
		dirtyTooth.transform:Find("Bone001/Bone002/maozi").gameObject:SetActive(false)
		table.insert(self.DirtyTeeth, dirtyTooth)		
	end

	self.CleanTeeth = {}
	self.CleanCount = 18

	self.realCleanCount = 10

	local tempCleanCount = 1
	for i=1, self.CleanCount do
		local tooth = Insight.GameObject.Find("GameRunning/teeth/Clean/" .. i)
		local animLua = tooth:GetComponent("Assets/Res/Brush/Luas/AnimPlayer.lua")
		animLua.bDirty = false		
		if tempCleanCount <= self.realCleanCount then
			tooth:SetActive(true)
		else
			tooth:SetActive(false)
		end
		table.insert(self.CleanTeeth,tooth)
		tempCleanCount = tempCleanCount + 1
	end

	---------------------
	self.game_object:SetActive( true  )

	local dirtyCount1 = 0
	local cleanCount1 = 0
	local randomCount1 = math.random(1,4)	
	if randomCount1 == 1 then	
		dirtyCount1 = 3
		cleanCount1 = 6
	elseif randomCount1 == 2 then
		dirtyCount1 = 4
		cleanCount1 = 5
	elseif randomCount1 == 3 then
		dirtyCount1 = 5
		cleanCount1 = 4
	else		
		dirtyCount1 = 6
		cleanCount1 = 3
	end	
	local dirtyCount2 = 0
	local cleanCount2 = 0
	local randomCount2 = math.random(1,4)	
	if randomCount2 == 1 then	
		dirtyCount2 = 3
		cleanCount2 = 6
	elseif randomCount2 == 2 then
		dirtyCount2 = 4
		cleanCount2 = 5
	elseif randomCount2 == 3 then
		dirtyCount2 = 5
		cleanCount2 = 4	
	else
		dirtyCount2 = 6
		cleanCount2 = 3
	end
	local dirtyCount3 = 0
	local cleanCount3 = 0
	local randomCount3 = math.random(1,4)	
	if randomCount3 == 1 then	
		dirtyCount3 = 3
		cleanCount3 = 6
	elseif randomCount3 == 2 then
		dirtyCount3 = 4
		cleanCount3 = 5
	elseif randomCount3 == 3 then
		dirtyCount3 = 5
		cleanCount3 = 4	
	else
		dirtyCount3 = 6
		cleanCount3 = 3
	end
	local dirtyCount4 = 0
	local cleanCount4 = 0
	local randomCount4 = math.random(1,4)	
	if randomCount4 == 1 then	
		dirtyCount4 = 3
		cleanCount4 = 6
	elseif randomCount4 == 2 then
		dirtyCount4 = 4
		cleanCount4 = 5
	elseif randomCount4 == 3 then
		dirtyCount4 = 5
		cleanCount4 = 4	
	else
		dirtyCount4 = 6
		cleanCount4 = 3
	end

	--Insight.Debug.Log("dirtyCount1:" .. dirtyCount1 .. "\n")

	local allPath = Insight.GameObject.Find("Main Camera/Orbit"):GetComponent("Assets/Res/Brush/Luas/AllFollowPath.lua")

	--path 1
	local speed1 = math.random(2, 4) / 10
	local speed2 = math.random(1, 5) / 10
	local speed3 = math.random(1, 3) / 10
	local speed4 = math.random(1, 4) / 10
	local dirtyTeethCount = 1
	local cleanTeethCount = 1

	
	for i = 1, 9 do	
		if dirtyTeethCount <= dirtyCount1 then
			local followPath = self.DirtyTeeth[dirtyTeethCount]:GetComponent( "Assets/Res/Brush/Luas/FollowPath.lua"  )			
			local moveCom = self.DirtyTeeth[dirtyTeethCount]:GetComponent("Assets/Res/Brush/Luas/MoveController.lua")
			--assign randadom	
			--local index = self:GetRandomPathPoint()		
			--self.DirtyTeeth[dirtyTeethCount].transform.position = allPath.point1s[index]
			self.DirtyTeeth[dirtyTeethCount].transform.position = allPath.point1s[i]
			followPath:BuildPath(allPath.point1s)
			followPath.curPointIndex = i
			moveCom:SetSpeed(speed1)
			dirtyTeethCount = dirtyTeethCount + 1
		else
			local followPath = self.CleanTeeth[cleanTeethCount]:GetComponent( "Assets/Res/Brush/Luas/FollowPath.lua"  )	
			local moveCom = self.CleanTeeth[cleanTeethCount]:GetComponent( "Assets/Res/Brush/Luas/MoveController.lua" ) 		
			self.CleanTeeth[cleanTeethCount].transform.position = allPath.point1s[i]
			followPath:BuildPath(allPath.point1s)
			followPath.curPointIndex = i
			moveCom:SetSpeed(speed1)
			cleanTeethCount = cleanTeethCount + 1
		end 
	end
	--path 2	
	for i=10, 18 do
		if dirtyTeethCount <= dirtyCount1 + dirtyCount2 then
			local followPath = self.DirtyTeeth[dirtyTeethCount]:GetComponent( "Assets/Res/Brush/Luas/FollowPath.lua"  )	
			local moveCom = self.DirtyTeeth[dirtyTeethCount]:GetComponent( "Assets/Res/Brush/Luas/MoveController.lua"  )			
			self.DirtyTeeth[dirtyTeethCount].transform.position = allPath.point2s[i-9]
			followPath:BuildPath(allPath.point2s)
			followPath.curPointIndex = i-9
			moveCom:SetSpeed(speed2)
			dirtyTeethCount = dirtyTeethCount + 1
		else
			local followPath = self.CleanTeeth[cleanTeethCount]:GetComponent( "Assets/Res/Brush/Luas/FollowPath.lua"  )	
			local moveCom = self.CleanTeeth[cleanTeethCount]:GetComponent( "Assets/Res/Brush/Luas/MoveController.lua"  )		
			
			self.CleanTeeth[cleanTeethCount].transform.position = allPath.point2s[i-9]
			followPath:BuildPath(allPath.point2s)
			followPath.curPointIndex = i-9
			moveCom:SetSpeed(speed2)
			cleanTeethCount = cleanTeethCount + 1
		end 
	end
	--path 3	
	for i=19, 27 do
		if dirtyTeethCount <= dirtyCount1 + dirtyCount2 + dirtyCount3 then
			local followPath = self.DirtyTeeth[dirtyTeethCount]:GetComponent( "Assets/Res/Brush/Luas/FollowPath.lua"  )	
			local moveCom = self.DirtyTeeth[dirtyTeethCount]:GetComponent( "Assets/Res/Brush/Luas/MoveController.lua"  )		
			self.DirtyTeeth[dirtyTeethCount].transform.position = allPath.point3s[i-9*2]
			followPath:BuildPath(allPath.point3s)
			followPath.curPointIndex = i-9*2
			moveCom:SetSpeed(speed3)
			dirtyTeethCount = dirtyTeethCount + 1
		else
			local followPath = self.CleanTeeth[cleanTeethCount]:GetComponent( "Assets/Res/Brush/Luas/FollowPath.lua"  )			
			local moveCom = self.CleanTeeth[cleanTeethCount]:GetComponent( "Assets/Res/Brush/Luas/MoveController.lua"  )
			self.CleanTeeth[cleanTeethCount].transform.position = allPath.point3s[i-9*2]
			followPath:BuildPath(allPath.point3s)
			followPath.curPointIndex = i-9*2
			moveCom:SetSpeed(speed3)
			cleanTeethCount = cleanTeethCount + 1
		end 
	end
	--path 4	
	for i=28,36 do
		if dirtyTeethCount <= 18 then
			local followPath = self.DirtyTeeth[dirtyTeethCount]:GetComponent( "Assets/Res/Brush/Luas/FollowPath.lua"  )	
			local moveCom = self.DirtyTeeth[dirtyTeethCount]:GetComponent( "Assets/Res/Brush/Luas/MoveController.lua"  )		
			self.DirtyTeeth[dirtyTeethCount].transform.position = allPath.point4s[i-9*3]
			followPath:BuildPath(allPath.point4s)
			followPath.curPointIndex = i-9*3
			moveCom:SetSpeed(speed4)
			dirtyTeethCount = dirtyTeethCount + 1
		else
			local followPath = self.CleanTeeth[cleanTeethCount]:GetComponent( "Assets/Res/Brush/Luas/FollowPath.lua"  )	
			local moveCom = self.CleanTeeth[cleanTeethCount]:GetComponent( "Assets/Res/Brush/Luas/MoveController.lua"  )		
			self.CleanTeeth[cleanTeethCount].transform.position = allPath.point4s[i-9*3]
			followPath:BuildPath(allPath.point4s)
			followPath.curPointIndex = i-9*3
			moveCom:SetSpeed(speed4)
			cleanTeethCount = cleanTeethCount + 1
		end 
	end
end

function TeethManager:CleanTooth( dirtyTeeth)
	
	local bSuc = true
	for k, v in pairs(self.CleanTeeth) do
		if v == dirtyTeeth then
			bSuc = false
			Insight.Debug.Log( "Clean tooth failed! ---> " .. dirtyTeeth.name .. "\n" )
			break
		end
	end
	if bSuc then
		Insight.Debug.Log( "Clean tooth suc ---> " .. dirtyTeeth.name .. "\n" )
		table.insert(self.CleanTeeth, dirtyTeeth)
		local temp = {}
		for key, var in pairs(self.DirtyTeeth) do
			if var ~= dirtyTeeth then
				temp[key] = var
			end
		end
		self.DirtyTeeth = temp

		--dirtyTeeth.transform:Find("huaiyachi").gameObject:GetComponent("Animator"):Play("face_Animation")
		--dirtyTeeth:GetComponent("Animator"):Play("")
		local animLua = dirtyTeeth:GetComponent("Assets/Res/Brush/Luas/AnimPlayer.lua")
		animLua.bChanged = true

		self.gameController.g_AllStates[self.gameController.g_StateRunningGame]:AddFaceCount()
	end
end

function TeethManager:IsDirty(tooth)
	local bDirty = true
	for k, v in pairs(self.CleanTeeth) do
		if v == tooth then	
			bDirty = false
			break
		end
	end
	local animLua = tooth:GetComponent("Assets/Res/Brush/Luas/AnimPlayer.lua")
	animLua.bDirty = bDirty
    return bDirty
end
return TeethManager;
