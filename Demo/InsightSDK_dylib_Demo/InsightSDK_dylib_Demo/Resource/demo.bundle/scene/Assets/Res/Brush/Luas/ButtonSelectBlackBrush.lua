-- ------------------------------
-- Copyright (c) NetEase Insight
-- All rights reserved.
-- ------------------------------

local ButtonSelectBlackBrush = {};

function ButtonSelectBlackBrush:New( game_object )
	if self ~= ButtonSelectBlackBrush then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = ButtonSelectBlackBrush } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;

	return new_instance;
end

function ButtonSelectBlackBrush:Start()
	self.gameController = Insight.GameObject.Find("main"):GetComponent("Assets/Res/Brush/Luas/GameController.lua")
end

function ButtonSelectBlackBrush:OnPointerDown()
	
end

function ButtonSelectBlackBrush:OnPointerUp()	
	Insight.Debug.Log( "Boy!\n"  )
	self.gameController.selectedBrush = self.gameController.g_BrushBlack
	self.gameController:ChangeState(self.gameController.g_StateBrushSelect)
end

return ButtonSelectBlackBrush;
