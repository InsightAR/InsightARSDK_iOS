
local FollowPath = {}


function FollowPath:New( game_object )
	if self ~= FollowPath then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = FollowPath } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function FollowPath:Start( )	
	self.waypointLength = 9
	self.moveCom = self.game_object:GetComponent( "Assets/Res/Brush/Luas/MoveController.lua")
	
end

function FollowPath:BuildPath( points)

	self.WayPoints = {}	

	self.curPointIndex = 1;
	self.minDis = 0.2
	self.waypointLength = 0
	self.mainCamera = Insight.GameObject.Find("Main Camera")
	-----
	self.WayPoints = points	
	self.waypointLength = #points	

	-- local minDis = 10000
	-- for k, v in pairs(self.WayPoints) do		
	-- 	local dis = (self.transform.position - v.transform.position).magnitude
	-- 	if dis < minDis then
	-- 		minDis = dis
	-- 		self.curPointIndex = k
	-- 	end
	-- end
end

function FollowPath:FaceToCamera(target)
	-- target.transform:LookAt ( self.mainCamera.transform.position, self.mainCamera.transform.up)
	self.transform:LookAt ( self.mainCamera.transform.position, Insight.Vector3.New( 0 , 1 , 0 ))
end

function FollowPath:Update()
	if self.waypointLength > 0 then		
		local target = self.WayPoints[self.curPointIndex]

		-- local dis = target.transform.position - self.transform.position + self.mainCamera.transform.position
		local dis = target - self.transform.position
		if dis.magnitude < self.minDis then
			self.curPointIndex = (self.curPointIndex) % (self.waypointLength) + 1		
			--Insight.Debug.Log( "Move to next point!" .. self.curPointIndex .. "\n"  )
		else			
			self.transform.position = self.transform.position +  dis.normalized * self.moveCom.speed		
		end
	self:FaceToCamera(target)
	end
end


return FollowPath;
