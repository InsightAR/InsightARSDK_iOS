
local GameStateStart = {};

function GameStateStart:New( game_object )
	if self ~= GameStateStart then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GameStateStart } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function GameStateStart:Start()
    self.gameController = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameController.lua")
	Insight.Debug.Log( "Lua GameStateStart Start: " .. self.game_object.name .. "\n" );
	
	self.uiRoot = Insight.GameObject.Find("Canvas/GameStart")	
	self.text = Insight.GameObject.Find("Canvas/GameStart/text")

	self:Reset()
end

function GameStateStart:Reset()
	self.tempDuration = 0
	self.duration = 1
	self.uiRoot:SetActive( false  )	
	self.text:SetActive(false)
	self.speed = 5
end


function GameStateStart:UpdateState()
 
	if self.tempDuration > self.duration then	
		self.gameController:ChangeState(self.gameController.g_StateRunningGame)
	else
		--self.gameController.bg.transform.position = self.gameController.bg.transform.position + Insight.Vector3.New(0,self.speed * Insight.Time.deltaTime,0)
	end

	self.tempDuration = self.tempDuration + Insight.Time.deltaTime

end

function GameStateStart:Enter()
	Insight.Debug.Log( "Lua GameStateStart Enter: " .. self.game_object.name .. "\n" );
	self.uiRoot:SetActive(true)	
	self.text:SetActive(true)

	--self.gameController.snow:SetActive(true)
end

function GameStateStart:Exit()
	Insight.Debug.Log( "Lua GameStateStart Exit: " .. self.game_object.name .. "\n" );
	self:Reset()
end

return GameStateStart;
