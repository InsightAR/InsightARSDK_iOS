
--变出牙刷
local GameStateBrushAppear = {};

function GameStateBrushAppear:New( game_object )
	if self ~= GameStateBrushAppear then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GameStateBrushAppear } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function GameStateBrushAppear:Start()
    self.gameController = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameController.lua")
	Insight.Debug.Log( "Lua GameStateBrushAppear Start: " .. self.game_object.name .. "\n" );

	self.uiRoot = Insight.GameObject.Find( "Canvas/BrushAppear"  )
	self.text = self.uiRoot.transform:Find( "text"  ).gameObject
	self.textDisappearCom = self.text:GetComponent("Assets/Res/Brush/Luas/ImageDisappear.lua")
	self.blackBrush = self.uiRoot.transform:Find( "blackBrush"  ).gameObject
	self.maozi = self.blackBrush.transform:Find("maozi").gameObject
	self.whiteBrush = self.uiRoot.transform:Find("whiteBrush").gameObject	
	self.hudie = self.whiteBrush.transform:Find("hudie").gameObject
	

	--self.StarMagic = Insight.GameObject.Find("Main Camera/Star_Magic")
	self:Reset()
end

function GameStateBrushAppear:Reset()	
	--self.StarMagic:SetActive(false)

	self.tempBrushAppearTime = 0
	self.brushAppearTime = 0.5

	self.brushAppearSpeed = 2

	self.uiRoot:SetActive(false)
	--self.text:SetActive(false)
	self.blackBrush:SetActive(false)
	self.whiteBrush:SetActive(false)
	self.maozi:SetActive(false)
	self.hudie:SetActive(false)
	
end

function GameStateBrushAppear:UpdateState()

end


function GameStateBrushAppear:Enter()
	Insight.Debug.Log( "Lua GameStateBrushAppear Enter: " .. self.game_object.name .. "\n" );	
	self:Reset()
	self.uiRoot:SetActive(true)
	self.text:SetActive(true)
	self.blackBrush:SetActive(true)
	self.whiteBrush:SetActive(true)
	self.maozi:SetActive(true)
	self.hudie:SetActive(true)
end

function GameStateBrushAppear:Exit()
    Insight.Debug.Log( "GameStateBrushAppear Exit! \n" );
	--self:Reset()
	--self.text:SetActive(false)
	self.textDisappearCom.bDisappear = true
end

return GameStateBrushAppear;
