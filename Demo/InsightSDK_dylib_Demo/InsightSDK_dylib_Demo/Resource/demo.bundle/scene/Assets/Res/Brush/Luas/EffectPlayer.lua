
local EffectPlayer = {}


function EffectPlayer:New( game_object )
	if self ~= EffectPlayer then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = EffectPlayer } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function EffectPlayer:Start( )
	self.cleanCompletedEffect = self.transform:Find( "Star_Clean"  ).gameObject
	self.clearSuc = Insight.GameObject.Find("Canvas/clearSuc")
	self.clearSuc:SetActive(false)
	self.duration = 1.5
	self.tempDuration = 0
	self.bPlay = false

	self.clearSucTarget = Insight.GameObject.Find("Canvas/faceBoy"):GetComponent( "RectTransform" ).anchoredPosition3D.y
	self.speed = 12
	self.clearSucRectTransform = self.clearSuc:GetComponent("RectTransform")
	self.oldAnchPos = self.clearSucRectTransform.anchoredPosition3D
end


function EffectPlayer:Update()
	if self.bPlay then
		if self.tempDuration < self.duration then
			if self.tempDuration == 0 then
				self.clearSucRectTransform.anchoredPosition3D = self.oldAnchPos
			end
			self.tempDuration = self.tempDuration + Insight.Time.deltaTime
			self.cleanCompletedEffect:SetActive(true)
			self.clearSuc:SetActive(true)	
			self.clearSucRectTransform.anchoredPosition3D = self.clearSucRectTransform.anchoredPosition3D + Insight.Vector3.New(0,self.speed,0)				
		else
			self.tempDuration = 0
			self.clearSuc:SetActive(false)
			self.bPlay = false		
			self.cleanCompletedEffect:SetActive( false  )		
		end
	else
		--self.cleanCompletedEffect:SetActive( false  )	
		self.tempDuration = 0
	end	
end


return EffectPlayer;
