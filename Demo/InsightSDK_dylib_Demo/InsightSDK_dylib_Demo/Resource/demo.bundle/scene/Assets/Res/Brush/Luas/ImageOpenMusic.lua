-- ------------------------------
-- Copyright (c) NetEase Insight
-- All rights reserved.
-- ------------------------------

local ImageOpenMusic = {};

function ImageOpenMusic:New( game_object )
	if self ~= ImageOpenMusic then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = ImageOpenMusic } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;

	return new_instance;
end

function ImageOpenMusic:Start()
	self.gameController = Insight.GameObject.Find("main"):GetComponent("Assets/Res/Brush/Luas/GameController.lua")
	local bgMusic = Insight.GameObject.Find("bgMusic")
	self.backgroundMusic = bgMusic:GetComponent( "AudioSource" , 0 ); 
	
	self.closeMusic = Insight.GameObject.Find("Canvas/CloseMusic")
end

function ImageOpenMusic:OnPointerUp()
	Insight.Debug.Log( "CloseMusic!\n"  )	
	self.gameController.bCloseMusic = true
	self.backgroundMusic:Pause()
	self.game_object:SetActive( false  )
	self.closeMusic:SetActive(true)
end

return ImageOpenMusic;
