
local ImageAppear = {}


function ImageAppear:New( game_object )
	if self ~= ImageAppear then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = ImageAppear } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	new_instance.beginAlpha = 0

	new_instance.imageCom = new_instance.game_object:GetComponent( "Image")
	local color = new_instance.imageCom.color
	new_instance.imageCom.color = Insight.Vector4.New(color.x, color.y, color.z, new_instance.beginAlpha)
	return new_instance;
end

function ImageAppear:Start( )

	self.endAlpha = 1

	self.time = 0.5
	self.tempTime = 0
	self.speed = 2
end


function ImageAppear:Update()
	if self.tempTime < self.time then
		self.tempTime = self.tempTime + Insight.Time.deltaTime
		local color = self.imageCom.color
		local r = color.r
		local g = color.g
		local b = color.b
		local a = color.a

		local finalA = a + self.speed * Insight.Time.deltaTime
		if finalA > self.endAlpha then
			finalA = self.endAlpha
		end
		self.imageCom.color = Insight.Vector4.New(r, g, b, finalA)
	end
end


return ImageAppear;
