
local GameStateDirtyToothAppear = {};



function GameStateDirtyToothAppear:New( game_object )
	if self ~= GameStateDirtyToothAppear then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GameStateDirtyToothAppear } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function GameStateDirtyToothAppear:Start()
    self.gameController = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameController.lua")
	Insight.Debug.Log( "Lua GameStateDirtyToothAppear Start: " .. self.game_object.name .. "\n" );  

    self.uiRoot = Insight.GameObject.Find("Canvas/DirtyToothAppear")   
    self.text1 = Insight.GameObject.Find("Canvas/DirtyToothAppear/text1")
    self.text2 = Insight.GameObject.Find("Canvas/DirtyToothAppear/text2")

    self:Reset()
end

function GameStateDirtyToothAppear:Reset()

    self.uiRoot:SetActive(false)

    local color = self.text1:GetComponent("Image").color
    self.text1:GetComponent("Image").color = Insight.Vector4.New(color.x, color.y, color.z, 0)
    color = self.text2:GetComponent("Image").color
    self.text2:GetComponent("Image").color = Insight.Vector4.New(color.x, color.y, color.z, 0)

    self.animLen = 2.5
    self.tempAnimTime = 0    
    self.animLeaveLen = 1.2
    self.bPlayedLeave = false
    

    self.textAppear = 0.5
    self.tempTextAppear = 0
    self.tempTextDisappear = 0
    self.textAppearSpeed = 2
    self.bTextAppear = false
    self.bTextDisappear = false    

    self.textLastTime = 2

    self.idleAnimTime = self.textLastTime + self.textAppear * 2 - self.animLeaveLen
    self.tempIdleAnimTime = 0

    self.duration = self.animLen + self.textAppear * 2 + self.textLastTime
    self.tempDuration = 0
    
end

function GameStateDirtyToothAppear:UpdateText()
    if self.bTextAppear then
        if self.tempTextAppear < self.textAppear then
            self.tempTextAppear = self.tempTextAppear + Insight.Time.deltaTime
            self.gameController:ImageSlowAppear(self.text1, self.textAppearSpeed)
            self.gameController:ImageSlowAppear(self.text2, self.textAppearSpeed)         
        end           
    end 

    if self.tempDuration > self.duration - self.textAppear then
        self.bTextDisappear = true
    end

    if self.bTextDisappear then
        if self.tempTextDisappear < self.textAppear then
            self.tempTextDisappear = self.tempTextDisappear + Insight.Time.deltaTime
            self.gameController:ImageSlowDisappear(self.text1, self.textAppearSpeed)
            self.gameController:ImageSlowDisappear(self.text2, self.textAppearSpeed)
        end        
    end 

end

function GameStateDirtyToothAppear:PlayIdleAnim() 
    if self.tempIdleAnimTime < self.idleAnimTime then
        self.tempIdleAnimTime = self.tempIdleAnimTime + Insight.Time.deltaTime
        if self.gameController.animGoodTooth.normalizedTime > 0.97 then    
            self.gameController.animGoodTooth:Play("Open_idle")
        end
    else
        if not self.bPlayedLeave then
            self.gameController.animGoodTooth:Play("leave")
            self.bPlayedLeave = true
        end
    end
end

function GameStateDirtyToothAppear:UpdateAnim()
    if self.tempAnimTime < self.animLen then
        self.tempAnimTime = self.tempAnimTime + Insight.Time.deltaTime
    else
        self.bTextAppear = true      
        --self:PlayIdleAnim()  
    end
end

function GameStateDirtyToothAppear:UpdateState()

    self:UpdateAnim()
    self:UpdateText()
    
    if self.tempDuration > self.duration then
        self.gameController:ChangeState(self.gameController.g_StateConfirm)
    end
    self.tempDuration = self.tempDuration + Insight.Time.deltaTime
end

function GameStateDirtyToothAppear:Enter()
    Insight.Debug.Log( "Lua GameStateDirtyToothAppear Enter: " .. self.game_object.name .. "\n" );

    self:Reset()
    self.uiRoot:SetActive(true)
    self.text1:SetActive(true)
    self.text2:SetActive(true)

    self.gameController.goodTooth:SetActive(false)
    --self.gameController.animGoodTooth.enabled = false
    self.gameController.dirtyTooth:SetActive(true)
    self.gameController.animDirtyTooth:Play("anime")
   
end

function GameStateDirtyToothAppear:Exit()
    Insight.Debug.Log( "Lua GameStateDirtyToothAppear Exit: " .. self.game_object.name .. "\n" );
    self:Reset()
end

return GameStateDirtyToothAppear;
