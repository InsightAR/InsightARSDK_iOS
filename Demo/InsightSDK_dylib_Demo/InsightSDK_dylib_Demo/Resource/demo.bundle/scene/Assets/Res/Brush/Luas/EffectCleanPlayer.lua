
local EffectCleanPlayer = {}


function EffectCleanPlayer:New( game_object )
	if self ~= EffectCleanPlayer then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = EffectCleanPlayer } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function EffectCleanPlayer:Start( )
	self.cleanStar = self.transform:Find( "Star_Clean"  ).gameObject

	self.clearDirty = self.transform:Find("CleanCompletedEffect").gameObject
	
	self.duration = 1
	self.tempDuration = 0
	
	self.bPlay = false

	self.starEffect = true --Star true  clearDirty false
end


function EffectCleanPlayer:Update()
	if self.bPlay then
		if self.tempDuration < self.duration then
			self.tempDuration = self.tempDuration + Insight.Time.deltaTime
			
			if self.starEffect then
				self.cleanStar:SetActive(true)
			else
				self.clearDirty:SetActive(true)
			end
			
		else
			self.tempDuration = 0
			self.bPlay = false
		end
	else
		if self.starEffect then
			self.cleanStar:SetActive(false)
		else
			self.clearDirty:SetActive(false)
		end
		
	end	
end


return EffectCleanPlayer;
