
local GameObjectAppear = {}


function GameObjectAppear:New( game_object )
	if self ~= GameObjectAppear then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GameObjectAppear } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function GameObjectAppear:Start( )
	self.beginAlpha = 0
	self.endAlpha = 213.0 / 255.0

	self.time = 5
	self.tempTime = 0

	self.speed = 0.15

	self.mat = self.game_object:GetComponent( "Renderer").material

	local color = self.mat.color
	self.mat.color = Insight.Vector4.New(color.x, color.y, color.z, self.beginAlpha)
end


function GameObjectAppear:Update()
	if self.tempTime < self.time then
		self.tempTime = self.tempTime + Insight.Time.deltaTime
		local color = self.mat.color
		local r = color.r
		local g = color.g
		local b = color.b
		local a = color.a

		local finalA = a + self.speed * Insight.Time.deltaTime
		if finalA > self.endAlpha then
			finalA = self.endAlpha
		end
		self.mat.color = Insight.Vector4.New(r, g, b, finalA)
	end
end


return GameObjectAppear;
