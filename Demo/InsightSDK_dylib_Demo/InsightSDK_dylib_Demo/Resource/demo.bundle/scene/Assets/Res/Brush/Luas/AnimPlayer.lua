
local AnimPlayer = {}


function AnimPlayer:New( game_object )
	if self ~= AnimPlayer then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = AnimPlayer } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	new_instance.bDirty = false
	return new_instance;
end

function AnimPlayer:Start( )
	self.anim = self.game_object:GetComponent("Animator")	
	self.fly = 0
	self.touch = 1
	self.changeMind = 2

	self.curAnim = self.fly 

	self.touchTime = 2
	self.tempTouchTime = 0
	self.bPlayedTouch = false

	self.changeMindTime = 0.5
	self.tempChangeMindTime = 0
	self.bPlayedChangeMind = false

	self.bChanged = false
	
	if self.bChanged or self.bDirty then
		self.anim:Play("fly_2")
	else	
		self.anim:Play("fly")
	end
end

function AnimPlayer:Update()
	if self.curAnim == self.fly then
		if self.anim.normalizedTime > 0.97 then
			if self.bChanged or self.bDirty then
				self.anim:Play("fly_2")
			else	
				self.anim:Play("fly")
			end
		else
			if self.bChanged or self.bDirty then
				self.anim:Play("fly_2",0,self.anim.normalizedTime)
			else	
				self.anim:Play("fly",0,self.anim.normalizedTime)
			end
		end
	elseif self.curAnim == self.touch then
		if self.tempTouchTime < self.touchTime then
			--if self.anim.normalizedTime > 0.97 then
			if	not self.bPlayedTouch then 
				if self.bChanged or self.bDirty then
					self.anim:Play("touch_2")	
				else
					self.anim:Play("touch")
					self.transform:Find("hoayachi").gameObject:GetComponent("Animator"):Play("face_Animation")
				end
				self.bPlayedTouch = true
			end
			self.tempTouchTime = self.tempTouchTime + Insight.Time.deltaTime
		else
			self.tempTouchTime = 0
			self.curAnim = self.fly
			self.bPlayedTouch = false
		end	
	else 
		if self.tempChangeMindTime < self.changeMindTime then	
			if not self.bPlayedChangeMind then
				if self.bDirty then			
					self.transform:Find("huaiyachi").gameObject:GetComponent("Animator"):Play("face_Animation")
				-- else
				-- 	self.transform:Find("hoayachi").gameObject:GetComponent("Animator"):Play("face_Animation")
					self.bPlayedChangeMind = true
				end
			end
			self.tempChangeMindTime = self.tempChangeMindTime + Insight.Time.deltaTime
		else
			self.tempChangeMindTime = 0
			self.curAnim = self.fly
			self.bPlayedChangeMind = false
		end	
	end	
end


return AnimPlayer;
