
local main = {};


function main:New( game_object )
    if self ~= main then return nil, "First argument must be self." end
    local new_instance = setmetatable( {} , { __metatable = {}, __index = main } );
    new_instance.transform = game_object.transform;
    new_instance.game_object = game_object.transform.gameObject;
    return new_instance;
end

function main:Start()
    Insight.Debug.Log( "Lua main Start: " .. self.game_object.name .. "\n" );
    self.gameController = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameController.lua")
    self.bChanged = false
    -- -- Hide Logo
    --  if not Insight.Logo.SetActive("d6fb28b5d1bbedf347c69f2110f8dd3a819fcc8eaef22c1be515f1ed131445aada7a40020063414722763e20af3ec1f6", false) then
    --     Insight.Debug.Log("set logo failed, key is invalid \n")
    -- end
    local active = Insight.Logo.SetActive("e6b36ba28371c6d7c443ced4f7d0bd258c126cf5a75714a123fe40bce48f7323084358e18b828a3a128d4809533dab431b48e7621e69d4a32d0c1b7ba9f5fbf1e2efcd98f2c92f62414bd5d4b1ae3a05b5b4ee186a789bed7c2844c274cbeb70d62f21f440ff3d0242f74d68b05aaf2104bf402e816ff985fa03dcdc09952319c6ebcda9f886723384468fcfbc4542ec01f701150203aee37a42e3202110e3ff0222b72bfbf8dd7508c75a492ed4af91", false)
    if not active then
        Insight.Debug.Log("set logo failed \n")
    end
end

function main:Update()
    if Insight.Tracking.status == g_InsightARState_Tracking then

        if not self.bChanged then
            self.gameController:ChangeState(self.gameController.g_StateGoodToothAppear)
            self.bChanged = true

        end
    end
end


return main;
