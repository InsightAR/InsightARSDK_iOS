-- 这个类用于管理全局变量及引擎功能相关全局函数
-- by tqw

--------------------------------------------------
-- Global Variables

-- AR Checking Status
-- Set:不允许修改
-- Get:GameController.lua, HintTrackingLimitedCtrl.lua
g_InsightARState_Uninitialized        = 0;
g_InsightARState_Initing              = 1;
g_InsightARState_Init_OK              = 2;
g_InsightARState_Init_Fail            = 3;
g_InsightARState_Detecting            = 4;
g_InsightARState_Detect_OK            = 5;
g_InsightARState_Detect_Fail          = 6;
g_InsightARState_Tracking             = 7;
g_InsightARState_Track_Limited        = 8;
g_InsightARState_Track_Lost           = 9;
g_InsightARState_Track_Fail           = 10;
g_InsightARState_Track_Stop           = 11;

-- AR Checking Status Valid 检测的结果是否可用（重新启动后，使用了上一轮的检测数据，因为 AR Status不是每帧更新）
-- Set: GameController.lua
-- Get: HintTrackingLimitedCtrl.lua
g_IsARStateValid = true;

-- AR Tracking Tpye 使用哪种算法在tracking
-- Set:不允许修改
-- Get:UIBehaviourBtnPlace.lua
g_InsightARTrackingTpye_IMU  = 10;
g_InsightARTrackingTpye_VO   = 11;
g_InsightARTrackingTpye_VIO  = 12;


-- 物体是否被放置的状态
-- Set: ButtonPlaceControl.lua
-- Get: GameController.lua
g_IsStuffPlaced = false;

-- Camera FOV = 60 时的摄像机与裁剪平面上下边之间的点积
-- Set: 不允许修改
-- Get: StuffDisappearHintCtrl.lua
g_DotCameraFOV60 = 0.866;

-- Hint for Tracking Limited reason enum
-- Set: 不允许修改
-- Get: HintTrackingLimitedCtrl.lua
g_TrackingLimitedReason_ReasonNone = 0.0;
g_TrackingLimitedReason_LowLight = 1.0;
g_TrackingLimitedReason_ExcessiveMotion = 2.0;
g_TrackingLimitedReason_InsufficientFeatures = 3.0;


-- UI Anchor Presets ( like unity ugui )
-- Set: 不允许修改
-- Get: UIBehaviour.lua
g_AnchorPresets_TopLeft = 1;
g_AnchorPresets_TopCenter = 2;
g_AnchorPresets_TopRight = 3;
g_AnchorPresets_MiddleLeft = 4;
g_AnchorPresets_MiddleCenter = 5;
g_AnchorPresets_MiddleRight = 6;
g_AnchorPresets_BottomLeft = 7;
g_AnchorPresets_BottomCenter = 8;
g_AnchorPresets_BottomRight = 9;


-- Movie Play Control
-- Set: GameController.lua or others
-- Get: PlayMovieControl.lua
g_PlayMoiveControl_1_Playing = false;

--------------------------------------------------------------------------
-- Global Functions

-----------------------------------------------
-- AR Events

-- Functions for Interact With APP
-- methodName : without ()
-- params : if no params, then set nil
-- result : 1 (Succeed) , 0 (Failed)
function g_Event_SendRunScriptCallback( methodName, params, result )
	
	if params then
		Insight.Event.Happen( 1 , 1 , 500 , methodName .."(\"" .. params .. "\")".."___"..result );
	else
		Insight.Event.Happen( 1 , 1 , 500 , methodName .."()".."___"..result );
	end
end

function g_Event_SendRunScript( methodName, params )
	
	if params then
		Insight.Event.Happen( 1 , 1 , 100 , methodName .. "___" .. params );
	else
		Insight.Event.Happen( 1 , 1 , 100 , methodName);
	end

	print("[test event] send message : " .. methodName ); --test
end

function g_Event_CloseARScene()
	
	Insight.Event.Happen( 1 , 1 , 101 , nil);
end

function g_Event_ReloadARProduct()

	Insight.Event.Happen( 1, 1, 102, nil);
end

--type___title___description___url___logoImagePatch
--type : 1 text; 2 image; 3 music; 4 video; 5 url
function g_Event_Share(shareType, title, description, url)
	
	local typeStr, titleStr, descriptionStr, urlStr  = " "," "," "," ";
	if shareType then
		typeStr = shareType;
	end
	if title then
		titleStr = title;
	end
	if description then
		descriptionStr = description;
	end
	if url then
		urlStr = url;
	end
	Insight.Event.Happen( 1, 1, 111, typeStr.. "___" .. titleStr .. "___" .. descriptionStr .. "___" .. urlStr .. "___/ShareLogo/ShareLogo.png" );
end

------------------------------------------------
-- Functions For UI

-- UI的显示设置
-- params:
-- ref: self
-- entity: self.entity
-- anchorPreset :  g_AnchorPresets_TopLeft (e.g.)
-- x, y , h, w : 值均设置为与屏幕长边的比例.
-- x / y :在Center / Middle 模式下，为模型中心与锚点的偏移距离， 在其他模式下，为到相应边的偏移距离。 x,y 的正方向为右和上。
-- z 为沿着Camera Forward方向的深度，在仅有单层UI时可不设置。默认是0，设置为1，2，3等则逐级增加深度。建议设置为整数。注意：不论z处于哪一层都会检查自身占用的矩形区域是否被点击。
-- Author: Tqw

function g_UISetupDisplay ( ref, entity, anchorPreset, x, y, h, w, z )

	local ent = entity;

	ref.g_ui_screenAnchorPreset = anchorPreset; -- Get the defines in GlobalVariables.lua
	ref.g_ui_screenOffsetX = x; -- increase from left to right
	ref.g_ui_screenOffsetY = y; -- increase from bottom to top
	ref.g_ui_screenHeight = h; -- percentage in screen width
	ref.g_ui_screenWidth = w; -- precentage in screen width

	-- ajust height with viewport
	-- local viewport = Insight.Camera.Main():GetCamera():GetViewport();
	-- local viewsize = Insight.Vector2.New( viewport:W() , viewport:H() );
	local viewsize = Insight.Vector2.New( Insight.Screen.width , Insight.Screen.height );

	if viewsize.x == 0 or viewsize.y == 0 then
		return;
	end

	local aspect = viewsize.x / viewsize.y;

	if aspect >= 1 then

		y = aspect * y;
		h = aspect * h;

	else

		x = x / aspect;
		w = w / aspect;

	end

	if( anchorPreset ~= nil ) then
		if anchorPreset <= 0 or anchorPreset > 9 then
			print( "Error: g_UISetupDisplay -- anchorPreset is wrong !!!" );
			anchorPreset = 5;
		end

		-- local anchorPresetProperty = ent:GetRenderer():FindProperty( "_AnchorPreset" , 0 );
		local anchorPresetProperty = ent:GetComponent( "Renderer", 0  ):GetMaterial(0);
		if anchorPresetProperty == nil then
			print( "Error: g_UISetupDisplay -- Shader has no _AnchorPreset !!!" );
			return;
		end

		anchorPresetProperty:SetFloat( "_AnchorPreset", anchorPreset );

		-- local rectTransProperty = ent:GetRenderer():FindProperty( "_RectTrans" , 0 );
		local rectTransProperty = ent:GetComponent( "Renderer", 0  ):GetMaterial(0);
		if rectTransProperty == nil then
			print( "Error: g_UISetupDisplay -- Shader has no _RectTrans !!!" );
			return;
		end
		rectTransProperty:SetVector( "_RectTrans", Insight.Vector4.New(x, y, h ,w) ); --( x, aspect * y, aspect * h , w ) );

		-- print( "[test autoUI]  entity:name : ".. ent:GetName()..",  x,y,h,w = ".. x .. ", ".. y ..", ".. h .. ", ".. w ); --test

		-- process Z depth
		if z then

			if type(z) ~= "number" then

				print("Error: g_UISetupDisplay -- Z depth is not number !!!" );
			else

				ref.g_ui_screenZdepth = z;

				-- local ZdepthProperty = ent:GetRenderer():FindProperty( "_Zdepth" , 0 );
				local ZdepthProperty = ent:GetComponent( "Renderer", 0  ):GetMaterial(0);

				if ZdepthProperty == nil then
					print( "Error: g_UISetupDisplay -- Shader has no _Zdepth !!!" );
					return;
				end

				ZdepthProperty:SetFloat("_Zdepth", z );
			end
		else
			z = 0; -- z equals 0 by default
		end


	else
		print( "Error: g_UISetupDisplay -- anchorPreset is nil !!! ");
	end

end


-- UI的Press事件检测
-- 用于在UI的Update中进行判断是否点中
-- Params:  ref, entity, OnPress
-- Author: Tqw

function g_UICheckIfPressed( ref, entity, OnPress )

	-- 获取参数
	-- entity, screenAnchorPreset, screenOffsetX, screenOffsetY, screenHeight, screenWidth, ref,

	-- local entity = ref.entity;
	local screenAnchorPreset = ref.g_ui_screenAnchorPreset;
	local screenOffsetX = ref.g_ui_screenOffsetX;
	local screenOffsetY = ref.g_ui_screenOffsetY;
	local screenHeight  = ref.g_ui_screenHeight;
	local screenWidth   = ref.g_ui_screenWidth;

	local pressed = false;

	if Insight.Input.GetMouseDown(0) and entity.activeSelf then

		pressed = g_UICheckIfPressedWithUIPosition( ref, screenAnchorPreset, screenOffsetX, screenOffsetY, screenHeight, screenWidth, OnPress );
	end

	return pressed;

end

-- UI的Press事件检测
-- 这里可以直接提供UI的位置和尺寸信息，根据这个尺寸信息来判断是否点击了。可以用于判断其他Lua脚本UI控件是否被点击。
-- Params:  ref, a, x, y, h, w, OnPress, bCheckRelease
-- Author: Tqw

function g_UICheckIfPressedWithUIPosition( ref, a, x, y, h, w, OnPress, bCheckRelease )

	--print("[test press] a = " .. a); --test

	local screenAnchorPreset = a;
	local screenOffsetX = x;
	local screenOffsetY = y;
	local screenHeight  = h;
	local screenWidth   = w;


	-- local viewport = Insight.Camera.Main():GetCamera():GetViewport();
	-- local viewsize = Insight.Vector2.New( viewport:W() , viewport:H() );
	local viewsize = Insight.Vector2.New( Insight.Screen.width , Insight.Screen.height );
	--print( "[test screen] viewsize = ".. Insight.Screen.Width() .. ", " .. Insight.Screen.Height() );
	local aspect = viewsize.x / viewsize.y;

	-- 如果竖屏修改了显示，那么这里做检测也要修改

	if aspect >= 1 then

		screenOffsetY = aspect * screenOffsetY;
		screenHeight = aspect * screenHeight;

	else
		screenOffsetX = screenOffsetX / aspect;
		screenWidth = screenWidth / aspect;
	end

	local touch0 = Insight.Input.GetTouch(0);
	local touchPos = touch0.position / viewsize; -- 归一化

	-- print( "[test touch] touchPos: ".. touchPos:X()..", "..touchPos:Y() ); --test

	if not screenAnchorPreset then
		print( "Error: g_UICheckIfPressedWithUIPosition -- anchorPreset is nil !!! ");
	end

	-- rect for testing point in or not
	local minX,maxX,minY,maxY = 0, 0, 0, 0;

	if( screenAnchorPreset == g_AnchorPresets_TopLeft ) then

		minX = 0.0 + screenOffsetX;
		maxX = 0.0 + screenOffsetX + screenWidth;
		minY = 1.0 + screenOffsetY - screenHeight;
		maxY = 1.0 + screenOffsetY;

	elseif( screenAnchorPreset == g_AnchorPresets_TopCenter ) then

		minX = 0.5 + screenOffsetX - screenWidth / 2.0;
		maxX = 0.5 + screenOffsetX + screenWidth / 2.0;
		minY = 1.0 + screenOffsetY - screenHeight;
		maxY = 1.0 + screenOffsetY;

	elseif( screenAnchorPreset == g_AnchorPresets_TopRight ) then

		minX = 1.0 + screenOffsetX - screenWidth;
		maxX = 1.0 + screenOffsetX;
		minY = 1.0 + screenOffsetY - screenHeight;
		maxY = 1.0 + screenOffsetY;

	elseif( screenAnchorPreset == g_AnchorPresets_MiddleLeft ) then

		minX = 0.0 + screenOffsetX;
		maxX = 0.0 + screenOffsetX + screenWidth;
		minY = 0.5 + screenOffsetY - screenHeight / 2.0;
		maxY = 0.5 + screenOffsetY + screenHeight / 2.0;

	elseif( screenAnchorPreset == g_AnchorPresets_MiddleCenter ) then

		minX = 0.5 + screenOffsetX - screenWidth / 2.0;
		maxX = 0.5 + screenOffsetX + screenWidth / 2.0;
		minY = 0.5 + screenOffsetY - screenHeight / 2.0;
		maxY = 0.5 + screenOffsetY + screenHeight / 2.0;

	elseif( screenAnchorPreset == g_AnchorPresets_MiddleRight ) then

		minX = 1.0 + screenOffsetX - screenWidth;
		maxX = 1.0 + screenOffsetX;
		minY = 0.5 + screenOffsetY - screenHeight / 2.0;
		maxY = 0.5 + screenOffsetY + screenHeight / 2.0;

	elseif( screenAnchorPreset == g_AnchorPresets_BottomLeft ) then

		minX = screenOffsetX;
		maxX = screenOffsetX + screenWidth;
		minY = screenOffsetY;
		maxY = screenOffsetY + screenHeight;

	elseif( screenAnchorPreset == g_AnchorPresets_BottomCenter ) then

		minX = 0.5 + screenOffsetX - screenWidth / 2.0;
		maxX = 0.5 + screenOffsetX + screenWidth / 2.0;
		minY = screenOffsetY;
		maxY = screenOffsetY + screenHeight;

	elseif( screenAnchorPreset == g_AnchorPresets_BottomRight ) then

		minX = 1.0 + screenOffsetX - screenWidth;
		maxX = 1.0 + screenOffsetX;
		minY = screenOffsetY;
		maxY = screenOffsetY + screenHeight;
	end

	--print( "[test touch] minX,maxX,minY,maxY  = " .. minX .. ", ".. maxX .. ", " .. minY .. ", " .. maxY ); --test

	-- Check if the point is in the rect
	if touchPos.x >= minX and touchPos.x <= maxX and touchPos.y >= minY and touchPos.y <= maxY then

		-- print("[test touch] in the rect" ); --test


		--print("[test release] touch.phase from GlobalVariables = " .. touch0:Phase()); --test

		if not bCheckRelease then

			if OnPress then
				OnPress(ref);
			else
				print("Warning: g_UICheckIfPressedWithUIPosition -- OnPress function is nil !!!");
			end

			return true;

		elseif touch0.phase == 3 then -- Check if the phase is ended while bCheckRelease

			if OnPress then
				OnPress(ref);
			else
				print("Warning: g_UICheckIfPressedWithUIPosition -- OnPress function is nil !!!");
			end

			print("[test release] release ");

			return true;
		end


	end

	return false;
end


--------------------------------------------------
-- Place Stuff to Real Insight With Screen point( screen center default )
-- Return: true -- Place Success ; false -- Place failed

function  g_PlaceStuffToInsightWithScreenPoint( ref, target, screenPoint2d, OnTrackingFailed )


	-- Get the Insight position
	-- local viewport = Insight.Camera.Main():GetCamera():GetViewport();
	-- local viewsize = Insight.Vector2.New( viewport:W() , viewport:H() );
	local viewsize = Insight.Vector2.New( Insight.Screen.width , Insight.Screen.height );
	local crossScreenPos = Insight.Vector2.New( viewsize.x / 2.0, viewsize.y / 2.0 );

	if screenPoint2d then

		crossScreenPos = screenPoint2d;
	end

	-- print( "[g_PlaceStuffToInsight] crossScreenPos for Raycasting " .. crossScreenPos:X() .. " , " .. crossScreenPos:Y() ); -- crossScreePos
	-- print( "[g_PlaceStuffToInsight] Tracking Status: " .. Insight.Tracking.status ); -- test the tracking status

	local tracking_result = Insight.Tracking.Raycasting( crossScreenPos.x , crossScreenPos.y );

	local point = tracking_result.point;
	print( "[g_PlaceStuffToInsight] Tracking Result: " .. (tracking_result.tracked and "true" or "false")
		 	.. " (" .. point.x .. "," .. point.y .. "," .. point.z .. ")" );

	if tracking_result.tracked then

		-- Show Target
		target:SetActive ( true );

		-- Place the target to viewport center pos in Insight
		-- target:SetLocalTranslation( point );   --( self.cross:GetLocalTranslation());
		target.transform.localPosition = point;

		--print( "[g_PlaceStuffToInsight] tracked position : " .. point:X() .. ", " ..point:Y() .. ", " ..point:Z()  );

		return true;

	else

		if OnTrackingFailed then

			OnTrackingFailed(ref);
		else
			--print(" Warning : g_PlaceStuffToInsight -- OnTrackingFailed is nil !!!" );

		end

		return false;
	end

end

--------------------------------------------------
-- Switch Tool

function  g_Swap( a, b )
	return b, a;
end


--------------------------------------------------
-- 以entity b为基准，求a的坐标位置



-- function  g_3dCoord_ABaseOnB( a, b )

-- 	local stuffOffset = a:GetLocalTranslation() - b:GetLocalTranslation();

-- 	local x = ( stuffOffset:Dot( b:GetInsightRight()) ) ;
--  	local y = ( stuffOffset:Dot( b:GetInsightUp()) ) ;
--  	local z = ( stuffOffset:Dot( b:GetInsightForward()) );

--  	return Insight.Vector3.New( x, y, z );

-- end


--------------------------------------------------
-- 中文字符串的处理

function g_ChineseCharacters_StringToTable(s)
    local tb = {}

    --[[
    UTF8的编码规则：
    1. 字符的第一个字节范围： 0x00—0x7F(0-127),或者 0xC2—0xF4(194-244); UTF8 是兼容 ascii 的，所以 0~127 就和 ascii 完全一致
    2. 0xC0, 0xC1,0xF5—0xFF(192, 193 和 245-255)不会出现在UTF8编码中
    3. 0x80—0xBF(128-191)只会出现在第二个及随后的编码中(针对多字节编码，如汉字)
    ]]
    for utfChar in string.gmatch(s, "[%z\1-\127\194-\244][\128-\191]*") do
        table.insert(tb, utfChar)
    end

    return tb
end

function g_ChineseCharacters_GetUTFLen(s)
    local sTable = g_ChineseCharacters_StringToTable(s)

    local len = 0
    local charLen = 0

    for i=1,#sTable do
        local utfCharLen = string.len(sTable[i])
        if utfCharLen > 1 then -- 长度大于1的就认为是中文
            charLen = 2
        else
            charLen = 1
        end

        len = len + charLen
    end

    return len
end

function g_ChineseCharacters_GetUTFLenWithCount(s, count)
    local sTable = g_ChineseCharacters_StringToTable(s)

    local len = 0
    local charLen = 0
    local isLimited = (count >= 0)

    for i=1,#sTable do
        local utfCharLen = string.len(sTable[i])
        if utfCharLen > 1 then -- 长度大于1的就认为是中文
            charLen = 2
        else
            charLen = 1
        end

        len = len + utfCharLen

        if isLimited then
            count = count - charLen
            if count <= 0 then
                break
            end
        end
    end
    return len
end

function g_ChineseCharacters_GetMaxLenString(s, maxLen)
    local len = g_ChineseCharacters_GetUTFLen(s)

    local dstString = s
    -- 超长，裁剪，加...
    if len > maxLen then
        dstString = string.sub(s, 1, g_ChineseCharacters_GetUTFLenWithCount(s, maxLen))
        dstString = dstString.."..."
    end

    return dstString
end

-- 这里起止index中，截取的长度 = 2 * ( 汉字个数 + 汉字标点 ) + 1 * ( 英文字符 )
function g_ChineseCharacters_SubString(s, startIndex, endIndex)

	local start = 1;
	if startIndex ~= 1 then
		start = g_ChineseCharacters_GetUTFLenWithCount(s, startIndex - 1) + 1;
	end
	local dstString = string.sub(s,start, g_ChineseCharacters_GetUTFLenWithCount(s, endIndex)  );
	return dstString
end


--------------------------------------------------
-- SetText相关
-- by Cyl

g_TextAlignment = {};
g_TextAlignment.Left = 1;    -- 靠左对齐
g_TextAlignment.Center = 2;  -- 左右居中
g_TextAlignment.Right = 4;   -- 靠右对齐
g_TextAlignment.Top = 8;     -- 靠上对齐
g_TextAlignment.Middle = 16; -- 上下居中
g_TextAlignment.Bottom = 32; -- 靠下对齐

g_TextDirection = {};
g_TextDirection.Normal = 0;       -- 符合现代书写习惯的正常方向，即横向，从左到右
g_TextDirection.LeftToRight = 1;  -- 竖向，从左到右
g_TextDirection.RightToLeft = 2;  -- 竖向，从右到左

-- 文字转贴图
-- 用于将输入的字符串显示在贴图上
-- 目前还不能修改文字颜色，生成的文字贴图是黑底白字（黑色部分会被剔除为透明），如果要反转成白底黑字（并剔除白色部分）可以修改shader，参考天灯demo
-- Params: texture -- 通过 entity 的 GetRenderer():FindProperty( propName , index );方法获取用于显示文字的贴图
--         text -- 要显示的字符串
--         texture_w, texture_h -- 生成的文字贴图的大小（像素值），与材质上赋的原始贴图大小无关
--         padding_left, padding_right, padding_top, padding_bottom -- 显示文字的区域距离贴图左、右、上、下边缘的距离（像素值）
--         font_style -- 字体名称
--         font_width, font_height -- 文字的宽和高（像素值）
--         char_stride, line_stride -- 字间距和行间距（像素值）经过试验，字间距建议为0, 其他值会稍微有点偏差
--         direction -- 文字方向，即 g_TextDirection.Normal，g_TextDirection.LeftToRight，或 g_TextDirection.RightToLeft
--         alignment -- 文字对齐方式。g_TextAlignment中六中对齐方式可以通过加法组合使用，比如靠左居中：g_TextAlignment.Left + g_TextAlignment.Middle
-- Author: Cyl

function g_SetTextToTexture( texture, text, 
							texture_w, texture_h, 
							padding_left, padding_right, padding_top, padding_bottom, 
							font_style, font_width, font_height, 
							char_stride, line_stride, 
							direction, alignment )
	local char_str = 0;
	local line_str = 0;
	if direction == g_TextDirection.Normal then
		line_str = line_stride --+ font_height;
		if char_stride ~= 0 then                   -- 字间距设为0时，会自动调整字间距匹配font_width
			char_str = char_stride --+ font_width;
		end
	else
		line_str = line_stride -- + font_width;
		if char_stride ~= 0 then
			char_str = char_stride -- + font_height;
		end
	end
	
	texture:SetText(  text
                        , texture_w, texture_h
                        , padding_left, padding_top, texture_w - padding_right, texture_h - padding_bottom
                        , font_style, font_width, font_height, 0
                        , char_str, line_str
                        , direction , alignment );

end

-- Example
-- function SetTextTest:Start( )
-- 	self._text_texture = self.entity:GetRenderer():FindProperty( "_text_texture" , 0 );
-- 	if self._text_texture ~= nil then
-- 		Insight.Log.Print( "Lua TextToTexture Start: Found property " .. self._text_texture:GetName() .. "\n" );

-- 		local font_name = "PingFangSC-Regular"; -- font for iOS
-- 		if Insight.OS.Name() == "Android" then
-- 			font_name = "DroidSans";
-- 		end
-- 		local text = "你好！你好！\n哈哈哈哈哈\nA你好！\n心想事成,新年快乐。\n啦啦啦啦";
-- 		g_SetTextToTexture(self._text_texture, text,
-- 						   512, 512,
-- 						   50, 50, 50, 50,
-- 						   font_name, 30, 30,
-- 						   0, 30,
-- 						   g_TextDirection.Normal, g_TextAlignment.Center + g_TextAlignment.Middle );
-- 	end
-- end



--------------------------------------------------
-- Constructor

local GlobalVariables = {};

function GlobalVariables:New( game_object )
	if self ~= GlobalVariables then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GlobalVariables } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;

	new_instance.opened = false;
	return new_instance;
end


return GlobalVariables;
