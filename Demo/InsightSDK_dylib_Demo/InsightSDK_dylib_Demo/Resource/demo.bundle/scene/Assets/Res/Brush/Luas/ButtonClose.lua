-- ------------------------------
-- Copyright (c) NetEase Insight
-- All rights reserved.
-- ------------------------------

local ButtonClose = {};

function ButtonClose:New( game_object )
	if self ~= ButtonClose then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = ButtonClose } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;

	return new_instance;
end

function ButtonClose:Start()
	self.gameController = Insight.GameObject.Find("main"):GetComponent("Assets/Res/Brush/Luas/GameController.lua")
end

function ButtonClose:OnPointerDown()
	
end

function ButtonClose:OnPointerUp()
	Insight.Debug.Log( "Close!\n"  )	
	Insight.Event.Happen( 1 , 1 , 101 , nil)
end

return ButtonClose;
