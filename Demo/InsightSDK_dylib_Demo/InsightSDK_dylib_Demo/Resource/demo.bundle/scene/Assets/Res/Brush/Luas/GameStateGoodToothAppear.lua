
local GameStateGoodToothAppear = {};



function GameStateGoodToothAppear:New( game_object )
	if self ~= GameStateGoodToothAppear then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GameStateGoodToothAppear } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function GameStateGoodToothAppear:Start()
    self.gameController = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameController.lua")
	Insight.Debug.Log( "Lua GameStateGoodToothAppear Start: " .. self.game_object.name .. "\n" );

    local bgMusic = Insight.GameObject.Find("bgMusic")
    self.backgroundMusic = bgMusic:GetComponent( "AudioSource" , 0 );    

    self.uiRoot = Insight.GameObject.Find("Canvas/GoodToothAppear")   
    self.text1 = Insight.GameObject.Find("Canvas/GoodToothAppear/text1")
    self.text2 = Insight.GameObject.Find("Canvas/GoodToothAppear/text2")

    self:Reset()
end

function GameStateGoodToothAppear:Reset()

    self.uiRoot:SetActive(false)

    local color = self.text1:GetComponent("Image").color
    self.text1:GetComponent("Image").color = Insight.Vector4.New(color.x, color.y, color.z, 0)
    color = self.text2:GetComponent("Image").color
    self.text2:GetComponent("Image").color = Insight.Vector4.New(color.x, color.y, color.z, 0)

    self.animLen = 3.733
    self.tempAnimTime = 0    
    self.animLeaveLen = 1.2
    self.bPlayedLeave = false
    

    self.textAppear = 0.5
    self.tempTextAppear = 0
    self.tempTextDisappear = 0
    self.textAppearSpeed = 2
    self.bTextAppear = false
    self.bTextDisappear = false    

    self.textLastTime = 3

    self.idleAnimTime = self.textLastTime + self.textAppear * 2 - self.animLeaveLen
    self.tempIdleAnimTime = 0

    self.duration = self.animLen + self.textAppear * 2 + self.textLastTime
    self.tempDuration = 0
    
end

function GameStateGoodToothAppear:UpdateText()
    if self.bTextAppear then
        if self.tempTextAppear < self.textAppear then
            self.tempTextAppear = self.tempTextAppear + Insight.Time.deltaTime
            self.gameController:ImageSlowAppear(self.text1, self.textAppearSpeed)
            self.gameController:ImageSlowAppear(self.text2, self.textAppearSpeed)         
        end           
    end 

    if self.tempDuration > self.duration - self.textAppear then
        self.bTextDisappear = true
    end

    if self.bTextDisappear then
        if self.tempTextDisappear < self.textAppear then
            self.tempTextDisappear = self.tempTextDisappear + Insight.Time.deltaTime
            self.gameController:ImageSlowDisappear(self.text1, self.textAppearSpeed)
            self.gameController:ImageSlowDisappear(self.text2, self.textAppearSpeed)
        end        
    end 

end

function GameStateGoodToothAppear:PlayIdleAnim() 
    if self.tempIdleAnimTime < self.idleAnimTime then
        self.tempIdleAnimTime = self.tempIdleAnimTime + Insight.Time.deltaTime
        if self.gameController.animGoodTooth.normalizedTime > 0.97 then    
            self.gameController.animGoodTooth:Play("Open_idle")
        end
    else
        if not self.bPlayedLeave then
            self.gameController.animGoodTooth:Play("leave")
            self.bPlayedLeave = true
        end
    end
end

function GameStateGoodToothAppear:UpdateAnim()
    if self.tempAnimTime < self.animLen then
        self.tempAnimTime = self.tempAnimTime + Insight.Time.deltaTime
    else
        self.bTextAppear = true      
        self:PlayIdleAnim()  
    end
end

function GameStateGoodToothAppear:UpdateState()

    self:UpdateAnim()
    self:UpdateText()
    
    if self.tempDuration > self.duration then
        self.gameController:ChangeState(self.gameController.g_StateDirtyToothAppear)
    end
    self.tempDuration = self.tempDuration + Insight.Time.deltaTime
end

function GameStateGoodToothAppear:Enter()
    Insight.Debug.Log( "Lua GameStateGoodToothAppear Enter: " .. self.game_object.name .. "\n" );

    self:Reset()
    self.uiRoot:SetActive(true)
    self.text1:SetActive(true)
    self.text2:SetActive(true)

    self.backgroundMusic.loop = true
    if not self.gameController.bCloseMusic then
        self.backgroundMusic:Play()
    else
        self.backgroundMusic:Pause()
    end

    self.gameController.goodTooth:SetActive(true)
    self.gameController.dirtyTooth:SetActive(false)
  
    self.gameController.animGoodTooth:Play("Open")

    self.gameController.snow:SetActive(true)

    self.gameController.sky:SetActive(true)  
end

function GameStateGoodToothAppear:Exit()
    Insight.Debug.Log( "Lua GameStateGoodToothAppear Exit: " .. self.game_object.name .. "\n" );
    self:Reset()
end

return GameStateGoodToothAppear;
