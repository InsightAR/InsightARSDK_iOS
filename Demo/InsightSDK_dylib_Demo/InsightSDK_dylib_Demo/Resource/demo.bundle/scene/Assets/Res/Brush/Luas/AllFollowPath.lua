
local AllFollowPath = {}

function AllFollowPath:New( game_object )
	if self ~= AllFollowPath then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = AllFollowPath } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function AllFollowPath:Start( )	
	Insight.Debug.Log(" all path start!" .. "\n")
	local s1 = 20
	local s2 = 15
	local s3 = 18
	local s4 = 15

	local s2Offset = Insight.Vector3.New(-6,0,0)
	local s1Offset = Insight.Vector3.New(6,0,0)
	local s3Offset = Insight.Vector3.New(6,0,0)
	local s4Offset = Insight.Vector3.New(-6,0,0)
	local minHeight = -12
	local maxHeight = 24
	local deltaHeight = (maxHeight - minHeight) / 3
	local r2 = 1.4142135623731
	local r3 = 1.7320508075689
	self.point1s = {}
	self.point1Count = 9
	-- for i = 1, self.point1Count do
	-- 	self.point1s[i] = Insight.GameObject.Find("Orbit/Orbit1/" .. i)		
	-- end
	local s = s4 --第四圈最小 左
	self.point1s[1] = Insight.Vector3.New(s,minHeight,0) + s4Offset
	self.point1s[2] = Insight.Vector3.New(s * r2,minHeight,s * r2) + s4Offset
	self.point1s[3] = Insight.Vector3.New(0,minHeight,s)+ s4Offset
	self.point1s[4] = Insight.Vector3.New(-s * r2,minHeight,s * r2)+ s4Offset
	self.point1s[5] = Insight.Vector3.New(-s,minHeight,0)+ s4Offset
	self.point1s[6] = Insight.Vector3.New(-s * r2,minHeight,-s * r2)+ s4Offset
	self.point1s[7] = Insight.Vector3.New(0,minHeight,-s)+ s4Offset
	self.point1s[8] = Insight.Vector3.New(s * 0.5,minHeight,-s * r3)+ s4Offset
	self.point1s[9] = Insight.Vector3.New(s * r3,minHeight,-s * 0.5)+ s4Offset


	self.point2s = {}
	self.point2Count = 9
	-- for i = 1, self.point2Count do
	-- 	self.point2s[i] = Insight.GameObject.Find("Orbit/Orbit2/" .. i)
	-- end
	s = s3
	self.point2s[1] = Insight.Vector3.New(s,minHeight + deltaHeight,0)+ s3Offset
	self.point2s[2] = Insight.Vector3.New(s * r2,minHeight + deltaHeight,s * r2)+ s3Offset
	self.point2s[3] = Insight.Vector3.New(0,minHeight + deltaHeight,s)+ s3Offset
	self.point2s[4] = Insight.Vector3.New(-s * r2,minHeight + deltaHeight,s * r2)+ s3Offset
	self.point2s[5] = Insight.Vector3.New(-s,minHeight + deltaHeight,0)+ s3Offset
	self.point2s[6] = Insight.Vector3.New(-s * r2,minHeight + deltaHeight,-s * r2)+ s3Offset
	self.point2s[7] = Insight.Vector3.New(0,minHeight + deltaHeight,-s)+ s3Offset
	self.point2s[8] = Insight.Vector3.New(s * 0.5,minHeight + deltaHeight,-s * r3)+ s3Offset
	self.point2s[9] = Insight.Vector3.New(s * r3,minHeight + deltaHeight,-s * 0.5)+ s3Offset

	self.point3s = {}
	self.point3Count = 9
	-- for i = 1, self.point3Count do
	-- 	self.point3s[i] = Insight.GameObject.Find("Orbit/Orbit3/" .. i)
	-- end
	s = s2
	self.point3s[1] = Insight.Vector3.New(s,minHeight + deltaHeight*2,0)+ s2Offset
	self.point3s[2] = Insight.Vector3.New(s * r2,minHeight + deltaHeight*2,s * r2)+ s2Offset
	self.point3s[3] = Insight.Vector3.New(0,minHeight + deltaHeight*2,s)+ s2Offset
	self.point3s[4] = Insight.Vector3.New(-s * r2,minHeight + deltaHeight*2,s * r2)+ s2Offset
	self.point3s[5] = Insight.Vector3.New(-s,minHeight + deltaHeight*2,0)+ s2Offset
	self.point3s[6] = Insight.Vector3.New(-s * r2,minHeight + deltaHeight*2,-s * r2)+ s2Offset
	self.point3s[7] = Insight.Vector3.New(0,minHeight + deltaHeight*2,-s)+ s2Offset
	self.point3s[8] = Insight.Vector3.New(s * 0.5,minHeight + deltaHeight*2,-s * r3)+ s2Offset
	self.point3s[9] = Insight.Vector3.New(s * r3,minHeight + deltaHeight*2,-s * 0.5)+ s2Offset

	self.point4s = {}
	self.point4Count = 9
	-- for i = 1, self.point4Count do
	-- 	self.point4s[i] = Insight.GameObject.Find("Orbit/Orbit4/" .. i)
	-- end
	s = s1
	self.point4s[1] = Insight.Vector3.New(s,minHeight + deltaHeight*3,0)+ s1Offset
	self.point4s[2] = Insight.Vector3.New(s * r2,minHeight + deltaHeight*3,s * r2)+ s1Offset
	self.point4s[3] = Insight.Vector3.New(0,minHeight + deltaHeight*3,s)+ s1Offset
	self.point4s[4] = Insight.Vector3.New(-s * r2,minHeight + deltaHeight*3,s * r2)+ s1Offset
	self.point4s[5] = Insight.Vector3.New(-s,minHeight + deltaHeight*3,0)+ s1Offset
	self.point4s[6] = Insight.Vector3.New(-s * r2,minHeight + deltaHeight*3,-s * r2)+ s1Offset
	self.point4s[7] = Insight.Vector3.New(0,minHeight + deltaHeight*3,-s)+ s1Offset
	self.point4s[8] = Insight.Vector3.New(s * 0.5,minHeight + deltaHeight*3,-s * r3)+ s1Offset
	self.point4s[9] = Insight.Vector3.New(s * r3,minHeight + deltaHeight*3,-s * 0.5)+ s1Offset
end

return AllFollowPath;
