
local GameStateBrushSelect = {};


function GameStateBrushSelect:New( game_object )
	if self ~= GameStateBrushSelect then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GameStateBrushSelect } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	return new_instance;
end

function GameStateBrushSelect:Start()
    self.gameController = self.game_object:GetComponent("Assets/Res/Brush/Luas/GameController.lua")
	Insight.Debug.Log( "Lua GameStateBrushSelect Start: " .. self.game_object.name .. "\n" );

	self.uiRoot = Insight.GameObject.Find( "Canvas/BrushAppear"  )
	self.text = self.uiRoot.transform:Find( "text"  ).gameObject

	self.blackBrush = self.uiRoot.transform:Find( "blackBrush"  ).gameObject
	self.blackRectTransform = self.blackBrush:GetComponent( "RectTransform" )
	self.maozi = self.blackBrush.transform:Find("maozi").gameObject
	self.blackBrushDisappearCom = self.blackBrush:GetComponent("Assets/Res/Brush/Luas/ImageDisappear.lua")
	self.maoziDisappearCom = self.maozi:GetComponent("Assets/Res/Brush/Luas/ImageDisappear.lua")
	
	self.whiteBrush = self.uiRoot.transform:Find("whiteBrush").gameObject
	self.whiteRectTransform = self.whiteBrush:GetComponent( "RectTransform" )
	self.hudie = self.whiteBrush.transform:Find("hudie").gameObject
	self.whiteBrushDisappearCom = self.whiteBrush:GetComponent("Assets/Res/Brush/Luas/ImageDisappear.lua")
	self.hudieDisappearCom = self.hudie:GetComponent("Assets/Res/Brush/Luas/ImageDisappear.lua")

	

	self.whiteBack = self.uiRoot.transform:Find("whiteBrushBack").gameObject
	self.blackBack = self.uiRoot.transform:Find("blackBrushBack").gameObject

	self.WaveClean = Insight.GameObject.Find("Main Camera/Wave_1")

	self:Reset()
end

function GameStateBrushSelect:Reset()
	self.speed = 100
	self.moveTime = 2
	self.tempMoveTime = 0
end

function GameStateBrushSelect:MoveWhiteBrush(entity)
	if self.tempMoveTime < self.moveTime then
		self.tempMoveTime = self.tempMoveTime + Insight.Time.deltaTime
		self.whiteRectTransform.anchoredPosition3D = self.whiteRectTransform.anchoredPosition3D + Insight.Vector3.New(-self.speed*Insight.Time.deltaTime,0,0)
		if self.whiteRectTransform.anchoredPosition3D.x < 0 then
			self.whiteRectTransform.anchoredPosition3D = Insight.Vector3.New(0,self.whiteRectTransform.anchoredPosition3D.y,self.whiteRectTransform.anchoredPosition3D.z)
		end
	else
		self.whiteBrushDisappearCom.bDisappear = true
		self.hudieDisappearCom.bDisappear = true
		self.whiteBack:SetActive(true)
		self.WaveClean:SetActive(true)
		self.gameController:ChangeState(self.gameController.g_StateCountdown)
	end
end

function GameStateBrushSelect:MoveBlackBrush(entity)
	if self.tempMoveTime < self.moveTime then
		self.tempMoveTime = self.tempMoveTime + Insight.Time.deltaTime
		self.blackRectTransform.anchoredPosition3D = self.blackRectTransform.anchoredPosition3D + Insight.Vector3.New(self.speed*Insight.Time.deltaTime,0,0)
		if self.blackRectTransform.anchoredPosition3D.x > 0 then
			self.blackRectTransform.anchoredPosition3D = Insight.Vector3.New(0,self.blackRectTransform.anchoredPosition3D.y,self.blackRectTransform.anchoredPosition3D.z)
		end
	else
		self.blackBrushDisappearCom.bDisappear = true
		self.maoziDisappearCom.bDisappear = true
		self.blackBack:SetActive(true)
		self.WaveClean:SetActive(true)
		self.gameController:ChangeState(self.gameController.g_StateCountdown)
	end
end

function GameStateBrushSelect:UpdateState()   
	if self.gameController.selectedBrush == self.gameController.g_BrushBlack then
		self:MoveBlackBrush(self.blackBrush)
		self.whiteBrushDisappearCom.bDisappear = true	
		self.hudieDisappearCom.bDisappear = true
	else
		self.blackBrushDisappearCom.bDisappear = true
		self.maoziDisappearCom.bDisappear = true
		self:MoveWhiteBrush(self.whiteBrush)
	end
end

function GameStateBrushSelect:Enter()
	Insight.Debug.Log( "Lua GameStateBrushSelect Enter: " .. self.game_object.name .. "\n" );
	
	--self.WaveClean:SetActive(true)
	
end

function GameStateBrushSelect:Exit()
	Insight.Debug.Log( "Lua GameStateBrushSelect Exit: " .. self.game_object.name .. "\n" );
	self:Reset()
	
	self.uiRoot:SetActive(true)
	if self.gameController.selectedBrush == self.gameController.g_BrushBlack then
		self.blackBrush:SetActive(true)
		self.whiteBrush:SetActive(false)
	else
		self.blackBrush:SetActive(false)
		self.whiteBrush:SetActive(true)
	end


	--self.StarMagic:SetActive(false)
	--self.WaveClean:SetActive(false)	

end

return GameStateBrushSelect;
