
//////////////////////////////////////////////////////////////////////////
// 
// --------------------------Fragment Include Begin-----------------------
// 
//////////////////////////////////////////////////////////////////////////

#extension GL_EXT_shader_texture_lod : enable
#ifdef INSIGHT3D_ANDROID
    #extension GL_OES_EGL_image_external : require
#else
	#define INSIGHT3D_IOS
#endif
precision highp float;

uniform samplerCube t_skybox_texture_cube;
uniform sampler2D t_skybox_texture;

uniform samplerCube t_reflection_prob0_cube;
uniform vec4 u_reflection_prob0_min;
uniform vec4 u_reflection_prob0_max;
uniform vec4 u_reflection_prob0_pos;
uniform vec4 u_reflection_prob0_hdr;

uniform samplerCube t_reflection_prob1_cube;
uniform vec4 u_reflection_prob1_min;
uniform vec4 u_reflection_prob1_max;
uniform vec4 u_reflection_prob1_pos;
uniform vec4 u_reflection_prob1_hdr;

uniform vec4 u_sphere_harmonic[7]; // Ar, Ag, Ab, Br, Bg, Bb, C

uniform highp mat4 mat_ObjectToWorld;
uniform highp mat4 mat_WorldToObject;
uniform highp mat4 mat_MatrixVP;
uniform highp mat4 mat_MatrixView;
uniform highp mat4 mat_MatrixProjection;
uniform vec3 u_world_space_camera_pos;

uniform vec4 u_cos_time;
vec4 u_sin_time;
uniform vec4 u_time;

uniform vec4 u_light0_pos_world;
uniform vec4 u_light0_color;

// 如果修改了本文件中的变量，请务必在UtilityShaderGenerator.SHADER_EXCLUDED_UNIFORMS中添加相应的变量名！

#ifndef _COMPATIBLE_UNITY_FS_
#define _COMPATIBLE_UNITY_FS_

// USE: tmpvar_31 = impl_low_texture2DLodEXT (unity_SpecCube0, sVec3WorldToTexcoord(tmpvar_30.xyz), tmpvar_30.w);
lowp vec4 impl_low_texture2DLodEXT(lowp sampler2D sampler, highp vec2 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
    return texture2DLodEXT(sampler, coord, lod);
#else
    return texture2D(sampler, coord, lod);
#endif
}

lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
    return textureCubeLodEXT(sampler, coord, lod);
#else
    return textureCube(sampler, coord, lod);
#endif
}

#ifndef SKIN_SHADER
    #define unity_ObjectToWorld mat_ObjectToWorld
#endif

#define unity_WorldToObject mat_WorldToObject
// defined in UnityShaderVaribles.cginc.
// w is usually 1.0, or -1.0 for odd-negative scale transforms
#define unity_WorldTransformParams vec4(0, 0, 0, 1)
#define unity_MatrixVP mat_MatrixVP
#define unity_MatrixV mat_MatrixView
#define glstate_matrix_projection  mat_MatrixProjection
#define _WorldSpaceCameraPos u_world_space_camera_pos

#define _WorldSpaceLightPos0 u_light0_pos_world
#define _LightColor0 (u_light0_color.rgb)

#define unity_SHAr (u_sphere_harmonic[0])
#define unity_SHAg (u_sphere_harmonic[1])
#define unity_SHAb (u_sphere_harmonic[2])
#define unity_SHBr (u_sphere_harmonic[3])
#define unity_SHBg (u_sphere_harmonic[4])
#define unity_SHBb (u_sphere_harmonic[5])
#define unity_SHC (u_sphere_harmonic[6])

#define unity_SpecCube0 t_reflection_prob0_cube
#define unity_SpecCube0_BoxMax u_reflection_prob0_max
#define unity_SpecCube0_BoxMin u_reflection_prob0_min
#define unity_SpecCube0_ProbePosition u_reflection_prob0_pos
#define unity_SpecCube0_HDR u_reflection_prob0_hdr

#define unity_SpecCube1 t_reflection_prob1_cube
#define unity_SpecCube1_BoxMax u_reflection_prob1_max
#define unity_SpecCube1_BoxMin u_reflection_prob1_min
#define unity_SpecCube1_ProbePosition u_reflection_prob1_pos
#define unity_SpecCube1_HDR u_reflection_prob1_hdr

#define _CosTime (u_cos_time)
#define _SinTime (u_sin_time)
#define _Time (u_time.wwww)

#define _ZBufferParams vec4(1,0,0,0)

#endif

//////////////////////////////////////////////////////////////////////////
// 
// --------------------------Fragment Include End-------------------------
// 
//////////////////////////////////////////////////////////////////////////

uniform mediump float _BumpScale;
uniform sampler2D _BumpMap;
uniform highp vec4 _BumpMap_ST;
uniform mediump vec4 _Color;
uniform sampler2D _MainTex1;
uniform highp vec4 _MainTex1_ST;
uniform sampler2D _MainTex2;
uniform highp vec4 _MainTex2_ST;
uniform highp float _LerpFactor;
uniform sampler2D _EmissionMap1;
uniform highp vec4 _EmissionMap1_ST;
uniform sampler2D _EmissionMap2;
uniform highp vec4 _EmissionMap2_ST;
uniform mediump vec4 _EmissionColor;
uniform sampler2D _SpecGlossMap;
uniform highp vec4 _SpecGlossMap_ST;
uniform highp vec4 _SpecGlossColor;
uniform highp float _SmoothnessScale;
uniform mediump float _SmoothnessBias;
uniform sampler2D _OcclusionMap;
uniform highp vec4 _OcclusionMap_ST;
uniform mediump float _OcclusionStrength;
uniform highp float _Cutoff;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying mediump vec3 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1 = vec3(0.0);
  highp vec4 tmpvar_2 = vec4(0.0);
  mediump vec3 tmpvar_3 = vec3(0.0);
  mediump vec3 tmpvar_4 = vec3(0.0);
  lowp vec3 worldN_5 = vec3(0.0);
  lowp vec4 c_6 = vec4(0.0);
  mediump float tmpvar_7 = 0.0;
  lowp vec3 worldViewDir_8 = vec3(0.0);
  lowp vec3 lightDir_9 = vec3(0.0);
  highp vec3 tmpvar_10 = vec3(0.0);
  tmpvar_10.x = xlv_TEXCOORD1.w;
  tmpvar_10.y = xlv_TEXCOORD2.w;
  tmpvar_10.z = xlv_TEXCOORD3.w;
  mediump vec3 tmpvar_11 = vec3(0.0);
  tmpvar_11 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_11;
  highp vec3 tmpvar_12 = vec3(0.0);
  tmpvar_12 = normalize((_WorldSpaceCameraPos - tmpvar_10));
  worldViewDir_8 = tmpvar_12;
  lowp vec3 tmpvar_13 = vec3(0.0);
  lowp vec3 tmpvar_14 = vec3(0.0);
  lowp vec3 tmpvar_15 = vec3(0.0);
  mediump vec3 tmpvar_16 = vec3(0.0);
  mediump float tmpvar_17 = 0.0;
  highp vec4 tex2DNode36_18 = vec4(0.0);
  highp vec4 tex2DNode41_19 = vec4(0.0);
  highp vec4 tex2DNode14_20 = vec4(0.0);
  highp vec2 tmpvar_21 = vec2(0.0);
  tmpvar_21 = ((xlv_TEXCOORD0 * _BumpMap_ST.xy) + _BumpMap_ST.zw);
  tmpvar_15 = (_BumpScale * ((texture2D (_BumpMap, tmpvar_21).xyz * 2.0) - 1.0));
  highp vec2 tmpvar_22 = vec2(0.0);
  tmpvar_22 = ((xlv_TEXCOORD0 * _MainTex1_ST.xy) + _MainTex1_ST.zw);
  lowp vec4 tmpvar_23 = vec4(0.0);
  tmpvar_23 = texture2D (_MainTex1, tmpvar_22);
  tex2DNode14_20 = tmpvar_23;
  highp vec2 tmpvar_24 = vec2(0.0);
  tmpvar_24 = ((xlv_TEXCOORD0 * _MainTex2_ST.xy) + _MainTex2_ST.zw);
  lowp vec4 tmpvar_25 = vec4(0.0);
  tmpvar_25 = texture2D (_MainTex2, tmpvar_24);
  tex2DNode41_19 = tmpvar_25;
  highp vec4 tmpvar_26 = vec4(0.0);
  tmpvar_26 = mix (tex2DNode14_20, tex2DNode41_19, vec4(_LerpFactor));
  tmpvar_13 = (_Color * tmpvar_26).xyz;
  highp vec2 tmpvar_27 = vec2(0.0);
  tmpvar_27 = ((xlv_TEXCOORD0 * _EmissionMap1_ST.xy) + _EmissionMap1_ST.zw);
  highp vec2 tmpvar_28 = vec2(0.0);
  tmpvar_28 = ((xlv_TEXCOORD0 * _EmissionMap2_ST.xy) + _EmissionMap2_ST.zw);
  lowp vec4 tmpvar_29 = vec4(0.0);
  tmpvar_29 = texture2D (_EmissionMap1, tmpvar_27);
  lowp vec4 tmpvar_30 = vec4(0.0);
  tmpvar_30 = texture2D (_EmissionMap2, tmpvar_28);
  highp vec4 tmpvar_31 = vec4(0.0);
  tmpvar_31 = mix (tmpvar_29, tmpvar_30, vec4(_LerpFactor));
  tmpvar_16 = (tmpvar_31 * _EmissionColor).xyz;
  highp vec2 tmpvar_32 = vec2(0.0);
  tmpvar_32 = ((xlv_TEXCOORD0 * _SpecGlossMap_ST.xy) + _SpecGlossMap_ST.zw);
  lowp vec4 tmpvar_33 = vec4(0.0);
  tmpvar_33 = texture2D (_SpecGlossMap, tmpvar_32);
  tex2DNode36_18 = tmpvar_33;
  tmpvar_14 = (tex2DNode36_18 * _SpecGlossColor).xyz;
  tmpvar_17 = ((tex2DNode36_18.w * _SmoothnessScale) + _SmoothnessBias);
  highp vec2 tmpvar_34 = vec2(0.0);
  tmpvar_34 = ((xlv_TEXCOORD0 * _OcclusionMap_ST.xy) + _OcclusionMap_ST.zw);
  lowp vec4 tmpvar_35 = vec4(0.0);
  tmpvar_35 = texture2D (_OcclusionMap, tmpvar_34);
  highp float x_36 = 0.0;
  x_36 = (mix (tex2DNode14_20.w, tex2DNode41_19.w, _LerpFactor) - _Cutoff);
  if ((x_36 < 0.0)) {
    discard;
  };
  tmpvar_7 = ((tmpvar_35.y * _OcclusionStrength) + (1.0 - _OcclusionStrength));
  highp float tmpvar_37 = 0.0;
  tmpvar_37 = dot (xlv_TEXCOORD1.xyz, tmpvar_15);
  worldN_5.x = tmpvar_37;
  highp float tmpvar_38 = 0.0;
  tmpvar_38 = dot (xlv_TEXCOORD2.xyz, tmpvar_15);
  worldN_5.y = tmpvar_38;
  highp float tmpvar_39 = 0.0;
  tmpvar_39 = dot (xlv_TEXCOORD3.xyz, tmpvar_15);
  worldN_5.z = tmpvar_39;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_40 = vec3(0.0);
  Normal_40 = worldN_5;
  mediump float tmpvar_41 = 0.0;
  tmpvar_41 = (1.0 - tmpvar_17);
  mediump vec3 I_42 = vec3(0.0);
  I_42 = -(tmpvar_1);
  mediump vec3 normalWorld_43 = vec3(0.0);
  normalWorld_43 = worldN_5;
  mediump vec4 tmpvar_44 = vec4(0.0);
  tmpvar_44.w = 1.0;
  tmpvar_44.xyz = normalWorld_43;
  mediump vec3 x_45 = vec3(0.0);
  x_45.x = dot (unity_SHAr, tmpvar_44);
  x_45.y = dot (unity_SHAg, tmpvar_44);
  x_45.z = dot (unity_SHAb, tmpvar_44);
  mediump vec4 hdr_46 = vec4(0.0);
  hdr_46 = tmpvar_2;
  mediump vec4 tmpvar_47 = vec4(0.0);
  tmpvar_47.xyz = (I_42 - (2.0 * (
    dot (Normal_40, I_42)
   * Normal_40)));
  tmpvar_47.w = ((tmpvar_41 * (1.7 - 
    (0.7 * tmpvar_41)
  )) * 6.0);
  lowp vec4 tmpvar_48 = vec4(0.0);
  tmpvar_48 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_47.xyz, tmpvar_47.w);
  mediump vec4 tmpvar_49 = vec4(0.0);
  tmpvar_49 = tmpvar_48;
  lowp vec3 tmpvar_50 = vec3(0.0);
  mediump vec3 viewDir_51 = vec3(0.0);
  viewDir_51 = worldViewDir_8;
  mediump vec4 c_52 = vec4(0.0);
  lowp vec3 tmpvar_53 = vec3(0.0);
  tmpvar_53 = normalize(worldN_5);
  mediump vec3 tmpvar_54 = vec3(0.0);
  mediump vec3 albedo_55 = vec3(0.0);
  albedo_55 = tmpvar_13;
  mediump vec3 specColor_56 = vec3(0.0);
  specColor_56 = tmpvar_14;
  mediump float oneMinusReflectivity_57 = 0.0;
  oneMinusReflectivity_57 = (1.0 - max (max (specColor_56.x, specColor_56.y), specColor_56.z));
  tmpvar_54 = (albedo_55 * oneMinusReflectivity_57);
  tmpvar_50 = tmpvar_54;
  mediump vec3 diffColor_58 = vec3(0.0);
  diffColor_58 = tmpvar_50;
  tmpvar_50 = diffColor_58;
  mediump vec3 diffColor_59 = vec3(0.0);
  diffColor_59 = tmpvar_50;
  mediump vec3 specColor_60 = vec3(0.0);
  specColor_60 = tmpvar_14;
  mediump vec3 normal_61 = vec3(0.0);
  normal_61 = tmpvar_53;
  mediump vec3 tmpvar_62 = vec3(0.0);
  mediump vec3 inVec_63 = vec3(0.0);
  inVec_63 = (tmpvar_4 + viewDir_51);
  tmpvar_62 = (inVec_63 * inversesqrt(max (0.001, 
    dot (inVec_63, inVec_63)
  )));
  mediump float tmpvar_64 = 0.0;
  tmpvar_64 = clamp (dot (normal_61, tmpvar_62), 0.0, 1.0);
  mediump float tmpvar_65 = 0.0;
  tmpvar_65 = (1.0 - tmpvar_17);
  mediump float tmpvar_66 = 0.0;
  tmpvar_66 = (tmpvar_65 * tmpvar_65);
  mediump float x_67 = 0.0;
  x_67 = (1.0 - clamp (dot (normal_61, viewDir_51), 0.0, 1.0));
  mediump vec4 tmpvar_68 = vec4(0.0);
  tmpvar_68.w = 1.0;
  tmpvar_68.xyz = (((
    ((diffColor_59 + ((tmpvar_66 / 
      ((max (0.32, clamp (
        dot (tmpvar_4, tmpvar_62)
      , 0.0, 1.0)) * (1.5 + tmpvar_66)) * (((tmpvar_64 * tmpvar_64) * (
        (tmpvar_66 * tmpvar_66)
       - 1.0)) + 1.00001))
    ) * specColor_60)) * tmpvar_3)
   * 
    clamp (dot (normal_61, tmpvar_4), 0.0, 1.0)
  ) + (
    (max (((1.055 * 
      pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD4 + x_45)), vec3(0.4166667, 0.4166667, 0.4166667))
    ) - 0.055), vec3(0.0, 0.0, 0.0)) * tmpvar_7)
   * diffColor_59)) + ((
    (1.0 - ((tmpvar_66 * tmpvar_65) * 0.28))
   * 
    (((hdr_46.x * (
      (hdr_46.w * (tmpvar_49.w - 1.0))
     + 1.0)) * tmpvar_49.xyz) * tmpvar_7)
  ) * mix (specColor_60, vec3(
    clamp ((tmpvar_17 + (1.0 - oneMinusReflectivity_57)), 0.0, 1.0)
  ), vec3(
    ((x_67 * x_67) * (x_67 * x_67))
  ))));
  c_52.xyz = tmpvar_68.xyz;
  c_52.w = 1.0;
  c_6 = c_52;
  c_6.xyz = (c_6.xyz + tmpvar_16);
  gl_FragData[0] = c_6;
}


