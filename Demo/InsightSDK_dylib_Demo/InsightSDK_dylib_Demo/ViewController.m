//
//  ViewController.m
//  InsightSDK_dylib_Demo
//
//  Created by Dikey on 2018/7/11.
//  Copyright © 2018 Dikey. All rights reserved.
//

#import "ViewController.h"
#import <InsightSDK_dylib/InsightSDK.h>

@interface ViewController ()<InsightARDelegate>

@property (nonatomic, strong) UIView *arView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self startCloud];
    
    [self startWithProductID:@"1985"]; // 什么是pid？ 请问我们的商务或者产品g
//    [self startLocally]; // 本地方式
//    [self startEmptyScene]; // 打开空场景
}

- (void)startCloud
{
    ARAlgPrepareOption *option = [ARAlgPrepareOption new];
    [[InsightARManager shareManager]prepareAlgorithmModel:option downloadProgress:^(NSProgress *downloadProgress) {
        NSLog(@"Download Progress is %@" ,downloadProgress);
    } completion:^(NSError *error) {
        ARAlgorithmModel *model = [[InsightARManager shareManager]getAlgorithmModel];
        [[InsightARManager shareManager]startInsightARWithCloud:model withARDelegate:self];
    }];
}

- (void)startEmptyScene
{
    [[InsightARManager shareManager] startInsightAR:nil withARDelegate:self];
}

- (void)startWithProductID:(NSString *)pid
{
    // 1. 开始设置
    ARPrepareOptions *options = [ARPrepareOptions new];
    
    // 如果是pid模式 ，设置pid
    options.productID = pid;
    
    // 2. 下载
    [[InsightARManager shareManager] prepareARWithOptions:options downloadProgress:^(NSProgress *downloadProgress) {
        NSLog(@"progress %@",downloadProgress);
    } completion:^(NSError *error) {
        if (error != nil) {
            NSLog(@"error %@",error);
        }else{
            // 下载完成
            // 3. 读取本地产品，然后启动AR
            ARProduct *product = [[InsightARManager shareManager]getARProduct:pid];
            [[InsightARManager shareManager] startInsightAR:product withARDelegate:self];
        }
    }];
}

- (void)startLocally
{
    NSString *modelPath =[NSString stringWithFormat:@"%@", [[NSBundle mainBundle] pathForResource:@"demo" ofType:@"bundle"]];
    ARProduct *product = [[ARProduct alloc]initWithLocalResourcePath:modelPath];
    [[InsightARManager shareManager] startInsightAR:product withARDelegate:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // 释放AR
    [[InsightARManager shareManager] stopAllPrepare];
    [[InsightARManager shareManager] stopInsightAR];
    _arView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insightARDidUpdateARState:(InsightARState)state
{
    // AR 跟踪回调
    //NSLog(@"ARState : %@", @(state));
    switch (state) {
        case InsightARStateInitOK:{
            if (_arView == nil) {
                _arView = [[InsightARManager shareManager] getInsightARView];
                [self.view insertSubview:_arView atIndex:0];
            }
        }
            break;
            
        case InsightARStateInitFail:{
        }
            break;
            
        case InsightARStateDetecting:{
        }
            break;
            
        case InsightARStateTrackLimited:{
        }
            break;
            
        case InsightARStateTrackingNormal:{
        }
            break;
            
        case InsightARStateTrackLost:{
        }
            break;
            
        default:
            break;
    }
}

- (void)insightAREvent:(InsightAREvent *)event
{
    if (event.type == InsightAREvent_Function_ReloadARProduct) {
        NSString *productID  = [event.params objectForKey:@"name"];
        ARPrepareOptions *options = [ARPrepareOptions new];
        options.productID = productID;
        [[InsightARManager shareManager] prepareARWithOptions:options
                                             downloadProgress:^(NSProgress *downloadProgress) {
                                                 NSLog(@"progress %@",downloadProgress);
                                             } completion:^(NSError *error) {
                                                 ARProduct *product = [[InsightARManager shareManager]getARProductByOption:options];
                                                 [[InsightARManager shareManager]reloadInsightAR:product];
                                             }];
    }
    if(event.type == InsightAREvent_Function_RunScript){
        NSString *scriptName =  [event.params objectForKey:@"script"];
        NSString *runScriptName =  [event.params objectForKey:@"identity"];
        
        if ([scriptName isEqualToString:@"LoadModel"] && [runScriptName isEqualToString:@"g_LoadLocalModel"]) {
            // Important: skuName不应重复
            NSString *skuName = [[event.params objectForKey:@"json"] objectForKey:@"skuId"];
            
            skuName = @"scene1";
            
            // To be impl: 实际中应该由skuName（不会重复）去寻找相应的skuPath，这边是直接设置了。
            NSString *skuPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingString:@"/dynamic.bundle/scene/"];
            
            // 最终确保在skuPath下，存在名skuName的.res文件
            NSString *scriptParam = [NSString stringWithFormat:@"{\"path\":\"%@\",\"name\":\"%@\"}", skuPath, skuName];
            [[InsightARManager shareManager] runInsightARScript:runScriptName param:scriptParam callback:nil];
        }
    }
    
}

@end
