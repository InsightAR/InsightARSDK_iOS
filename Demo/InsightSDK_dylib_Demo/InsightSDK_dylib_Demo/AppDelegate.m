//
//  AppDelegate.m
//  InsightSDK_dylib_Demo
//
//  Created by Dikey on 2018/7/11.
//  Copyright © 2018 Dikey. All rights reserved.
//

#import "AppDelegate.h"
#import <InsightSDK_dylib/InsightSDK.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
     [[InsightARManager shareManager]registerAppKey:@"AR-8AD8-D9DE436F0B95-i-f" appSecret:@"bNZVomtPUW"]; //DemoForSDK正式环境
//    [[InsightARManager shareManager]registerAppKey:@"AR-8AD8-D9DE436F0B95-i-f" appSecret:@"bNZVomtPUW"];

    // 蚂蚁正式
//    [[InsightARManager shareManager]registerAppKey:@"AR-FUQE-IR6NNTHQY8QDF-I-F" appSecret:@"9It1VYEwSf"];
//    [[InsightARManager shareManager]registerAppKey:@"AR-KEL6-SXLYHWF6NOCUG-I-T" appSecret:@"X8LHcxK8s0"];
    
    [[InsightARManager shareManager]checkProductStatus:^(NSError *error) {
        
    } result:^(NSArray<ARProduct *> *arProducts) {
        for (ARProduct *product in arProducts) {
            NSLog(@"%@", product);
        }
    }];
//    [[InsightARManager shareManager]registerAppKey:@"AR-B27F-DDC39A2E654B-i-t" appSecret:@"GcxFjncgFY"];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
