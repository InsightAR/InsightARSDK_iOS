

local TestButton_OpenURL = {};

function TestButton_OpenURL:New( game_object )
	if self ~= TestButton_OpenURL then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = TestButton_OpenURL } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
  
	-------------------
  
	return new_instance;
end

function TestButton_OpenURL:Start()


end

function TestButton_OpenURL:OnPointerUp()
	-- url___urlType
	-- 打开链接的方式，由lua开发者与APP约定。如约定为：1，2，3。APP根据这个type可以进行不同处理
	Insight.Event.Happen( 1 , 1 , 108 , "https://dongjian.163.com___1");
	

end

return TestButton_OpenURL;
