

local UIBehaviourBtnPlace = {};

function UIBehaviourBtnPlace:New( game_object )
	if self ~= UIBehaviourBtnPlace then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = UIBehaviourBtnPlace } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function UIBehaviourBtnPlace:Start()

	
	-- Unity中相机名字修改为了： “Main Camera Auto UI”
	self.cameraComponent = Insight.GameObject.Find("Main Camera Auto UI"):GetComponent( "Camera", 0 ); --  使用此句不会崩溃
	-- self.cameraComponent = Insight.GameObject.Find("Main Camera"):GetComponent( "Camera", 0 ); --  使用此句会启动时崩溃


	self.target = Insight.GameObject.Find( "StuffRoot" );
	self.cross = Insight.GameObject.Find("Cross");
	self.plane = Insight.GameObject.Find("Fw_PlaneTrans");
	self.ButtonsCarControl = Insight.GameObject.Find("ButtonsCarControl");

end

function UIBehaviourBtnPlace:OnPointerUp()

	
	if self.target.activeSelf == false then

		Insight.Debug.Log( "Place Pressed!!!! \n"  )
		-- Show Target 
		self.target:SetActive( true );

		-- Insight.Debug.Log( "self.camera.pixelWidth:  " .. tostring(self.camera:GetComponent( "Camera" , 0 ).pixelWidth) .. "  ;  " .. "self.camera.pixelHeight:  " .. tostring(self.camera:GetComponent( "Camera" , 0 ).pixelHeight) .. "\n" );

		-- Get the world position 
		local viewsize = Insight.Vector2.New( self.cameraComponent.pixelWidth ,  self.cameraComponent.pixelHeight );
		local crossScreenPos = Insight.Vector2.New( viewsize.x / 2.0, viewsize.y / 2.0 );

		
		local tracking_result = Insight.Tracking.Raycasting( crossScreenPos.x , crossScreenPos.y );

		local point = tracking_result.point;


		if tracking_result.tracked then

			-- Place the target to viewport center pos in world 
			self.target.transform.position = point ;   --( self.cross:GetLocalTranslation());



		else
			-- 如果返回的三维点无效，就将目标物放置在十字标的位置
			self.target.transform.position = self.cross.transform.position;

		end

		-- Update g_IsStuffPlaced
		g_IsStuffPlaced = true;

		-- Hide button and cross, plane
		self.game_object:SetActive( false );
		self.cross:SetActive( false );
		self.plane:SetActive( false );

		self.ButtonsCarControl:SetActive(true);


	end

end 



return UIBehaviourBtnPlace;
