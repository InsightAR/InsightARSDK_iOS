

local UIBehaviourColorButton = {};

function UIBehaviourColorButton:New( game_object )
	if self ~= UIBehaviourColorButton then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = UIBehaviourColorButton } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function UIBehaviourColorButton:Start()

	-- KXC COMMENT:
	self.spriteGray = self.game_object:GetComponent( "Button" ).spriteState.disabledSprite;
	self.spriteBlue = self.game_object:GetComponent( "Button" ).spriteState.highLightedSprite;
	self.spriteRed  = self.game_object:GetComponent( "Button" ).spriteState.pressedSprite;

	self.curIndex = 1;
	self.colors = { Insight.Vector4.New(109/255.0,109/255.0,109/255.0,1.0), Insight.Vector4.New(0,0,1.0,1.0), Insight.Vector4.New(1.0,0,0,1.0) }; 
	self.buttonSprites = { self.spriteGray, self.spriteBlue, self.spriteRed };

	self.carColor = Insight.GameObject.Find("outer/car_shell"):GetComponent( "Renderer" ):GetMaterial( 0 );
	if self.carColor then
		self.carColor:SetColor( "_Base_Color" , self.colors[ self.curIndex ] );
		Insight.Debug.Log( "Enter carColor" );
	end

end

function UIBehaviourColorButton:OnPointerUp()
	

	self.curIndex = self.curIndex + 1;
	if self.curIndex > 3 then
		self.curIndex = 1;
	end

	Insight.Debug.Log( "[test] color index  = " .. self.curIndex );

	-- Update Button Sprite
	self.game_object:GetComponent( "Image" ).sprite = self.buttonSprites[ self.curIndex ];

	-- Update Car Color
	if self.carColor then
		self.carColor:SetColor( "_Base_Color" , self.colors[self.curIndex] );
	end


end





return UIBehaviourColorButton;
