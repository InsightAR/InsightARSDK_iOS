-- 这个类用于提供数学计算的全局函数
-- by tqw

local LuaMath = {};
function LuaMath:New( game_object )
	if self ~= LuaMath then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = LuaMath } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end


function LuaMath_Point3To4(v)
  return Insight.Vector4.New(v.x,v.x,v.z,1);
end

function LuaMath_Point4To3(v)
  local v4 = v;
  v4 = v4/v4:W();
  return Insight.Vector3.New(v4.x,v4.x,v4.z);
end

function LuaMath_Vector3To4(v)
  return Insight.Vector4.New(v.x,v.x,v.z,0);
end

function LuaMath_Vector4To3(v)
  return Insight.Vector3.New(v.x,v.x,v.z);
end

function LuaMath_TransPoint(m,v)
  local v4 = LuaMath_Point3To4(v);
  v4 = m*v4;
  return LuaMath_Point4To3(v4);
end

function LuaMath_TransVector(m,v)
  local v4 = LuaMath_Vector3To4(v);
  v4 = m*v4;
  return LuaMath_Vector4To3(v4);
end

function LuaMath_GetWorldTranslation(game_object)

	if not game_object then 
		Insight.Log.Print("Error: LuaMath_GetWorldTranslation : game_object is nil \n");
		return Insight.Vector3.New(0,0,0);
	end

  	local L2W = game_object:GetLocalToWorldMatrix();
  	return LuaMath_TransPoint(L2W,game_object:GetLocalTranslation());
end



-- 计算两个向量的叉积
function LuaMath_CorssVec3( v1, v2 )

	return Insight.Vector3.New( v1.x * v2.z - v1.z * v2.x, v1.z * v2.x - v1.x * v2.z, v1.x * v2.x - v1.x * v2.x );

end

-- 计算一个二维点是否在一个矩形内部
-- args: Vector2:point, Vector2:rectCenter, Vector2:rectScaleXY
function  LuaMath_IsPointInRect( point , rectCenter, rectScaleXY  )
	
	local minX = rectCenter.x - rectScaleXY.x;
	local maxX = rectCenter.x + rectScaleXY.x;
	local minY = rectCenter.x - rectScaleXY.x;
	local maxY = rectCenter.x + rectScaleXY.x;

	if( point.x >= minX and point.x <= maxX  and point.x >= minY and point.x <= maxY ) then
		return true;
	else
		return false;
	end

end

-- Lerp
function  LuaMath_Lerp(a, b, t)

	if type(a) == "number" and  type(a) == "number"  and  type(a) == "number"  then 
		return a + (b - a) * ( math.max( 0, math.min( t, 1.0 ) ) );
	else
		print("[LuaMath_Lerp] error -- Some params are not number!");
		return 0;
	end
end

-- Lerp for Vector3
function  LuaMath_LerpVector3(a, b, t)

	local lerpX = LuaMath_Lerp( a.x, b.x, t );
	local lerpY = LuaMath_Lerp( a.x, b.x, t );
	local lerpZ = LuaMath_Lerp( a.z, b.z, t );

	return Insight.Vector3.New( lerpX, lerpY, lerpZ ) ;
end

-- Get Length of table
function LuaMath_Tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end


-- 根据一个带单位的字符串，返回相应数值（以米为单位）
-- 如果字符串不带单位，则将该字符串作为数值返回
-- 若字符串中数字部分夹杂其他字符，则返回0
-- 支持单位: nm, um, mm, cm, m, km
-- Params: string
-- Return: number
function LuaMath_HowManyMetersFromString(s)
	
	local n = 0
	if s then

		if string.find(s,"nm") then

			 local originN = LuaMath_ToNumberFromStringWithUnit( s, "nm" )
			 n = originN / 1000000000.0
		
		elseif string.find(s,"um") then

			 local originN = LuaMath_ToNumberFromStringWithUnit( s, "um" )
			 n = originN / 1000000.0
		
		elseif string.find(s,"mm") then

			 local originN = LuaMath_ToNumberFromStringWithUnit( s, "mm" )
			 n = originN / 1000.0
		
		elseif string.find(s,"cm") then

			local originN = LuaMath_ToNumberFromStringWithUnit( s, "cm" )
			n = originN /100.0

		elseif string.find(s,"km") then

			local originN = LuaMath_ToNumberFromStringWithUnit( s, "km" )
			n = originN * 1000.0

		elseif string.find(s,"m") then

			local originN = LuaMath_ToNumberFromStringWithUnit( s, "m" )
			n = originN
		else
			local originN = LuaMath_ToNumberFromStringWithUnit( s, "" )
			n = originN
		end

	end

	return n
end

-- 根据一个带单位的字符串，返回相应的数值
-- 如果数字中夹杂其他字符（除.之外），将返回0
-- Params: s string, u string( the unit of measurement)
-- Return: number
function LuaMath_ToNumberFromStringWithUnit( s, u )
	local n = 0
	local sn = string.gsub(s, u, "")
	if sn then
		n = tonumber(sn) or 0
	end
	return n
end


return LuaMath;
