#include "amplify_standard_head_fs.sh"
#include "amplify_compatible_unity_fs.sh"

uniform sampler2D _NormalMap;
uniform highp vec4 _NormalMap_ST;
uniform highp vec4 _Tint;
uniform sampler2D _MainTex;
uniform highp vec4 _MainTex_ST;
uniform highp float _Metallic;
uniform sampler2D _MetallicGlossMap;
uniform highp vec4 _MetallicGlossMap_ST;
uniform highp float _UseMetallicMap;
uniform highp float _Smoothness;
uniform sampler2D _OcclusionMap;
uniform highp vec4 _OcclusionMap_ST;
uniform highp float _Opacity;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying mediump vec3 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1 = vec3(0.0);
  highp vec4 tmpvar_2 = vec4(0.0);
  mediump vec3 tmpvar_3 = vec3(0.0);
  mediump vec3 tmpvar_4 = vec3(0.0);
  lowp vec3 worldN_5 = vec3(0.0);
  lowp vec4 c_6 = vec4(0.0);
  lowp vec3 worldViewDir_7 = vec3(0.0);
  lowp vec3 lightDir_8 = vec3(0.0);
  highp vec3 tmpvar_9 = vec3(0.0);
  tmpvar_9.x = xlv_TEXCOORD1.w;
  tmpvar_9.y = xlv_TEXCOORD2.w;
  tmpvar_9.z = xlv_TEXCOORD3.w;
  mediump vec3 tmpvar_10 = vec3(0.0);
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_10;
  highp vec3 tmpvar_11 = vec3(0.0);
  tmpvar_11 = normalize((_WorldSpaceCameraPos - tmpvar_9));
  worldViewDir_7 = tmpvar_11;
  lowp vec3 tmpvar_12 = vec3(0.0);
  mediump float tmpvar_13 = 0.0;
  mediump float tmpvar_14 = 0.0;
  mediump float tmpvar_15 = 0.0;
  lowp float tmpvar_16 = 0.0;
  highp vec4 tex2DNode16_17 = vec4(0.0);
  highp vec2 tmpvar_18 = vec2(0.0);
  tmpvar_18 = ((xlv_TEXCOORD0 * _NormalMap_ST.xy) + _NormalMap_ST.zw);
  lowp vec3 tmpvar_19 = vec3(0.0);
  tmpvar_19 = ((texture2D (_NormalMap, tmpvar_18).xyz * 2.0) - 1.0);
  highp vec2 tmpvar_20 = vec2(0.0);
  tmpvar_20 = ((xlv_TEXCOORD0 * _MainTex_ST.xy) + _MainTex_ST.zw);
  tmpvar_12 = (_Tint * texture2D (_MainTex, tmpvar_20)).xyz;
  highp vec2 tmpvar_21 = vec2(0.0);
  tmpvar_21 = ((xlv_TEXCOORD0 * _MetallicGlossMap_ST.xy) + _MetallicGlossMap_ST.zw);
  lowp vec4 tmpvar_22 = vec4(0.0);
  tmpvar_22 = texture2D (_MetallicGlossMap, tmpvar_21);
  tex2DNode16_17 = tmpvar_22;
  highp float tmpvar_23 = 0.0;
  tmpvar_23 = clamp ((_Metallic + (tex2DNode16_17.x * _UseMetallicMap)), 0.0, 1.0);
  tmpvar_13 = tmpvar_23;
  highp float tmpvar_24 = 0.0;
  tmpvar_24 = clamp (((_UseMetallicMap * tex2DNode16_17.w) + _Smoothness), 0.0, 1.0);
  tmpvar_14 = tmpvar_24;
  highp vec2 tmpvar_25 = vec2(0.0);
  tmpvar_25 = ((xlv_TEXCOORD0 * _OcclusionMap_ST.xy) + _OcclusionMap_ST.zw);
  lowp vec4 tmpvar_26 = vec4(0.0);
  tmpvar_26 = texture2D (_OcclusionMap, tmpvar_25);
  tmpvar_15 = tmpvar_26.x;
  tmpvar_16 = _Opacity;
  highp float tmpvar_27 = 0.0;
  tmpvar_27 = dot (xlv_TEXCOORD1.xyz, tmpvar_19);
  worldN_5.x = tmpvar_27;
  highp float tmpvar_28 = 0.0;
  tmpvar_28 = dot (xlv_TEXCOORD2.xyz, tmpvar_19);
  worldN_5.y = tmpvar_28;
  highp float tmpvar_29 = 0.0;
  tmpvar_29 = dot (xlv_TEXCOORD3.xyz, tmpvar_19);
  worldN_5.z = tmpvar_29;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_8;
  tmpvar_1 = worldViewDir_7;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_30 = vec3(0.0);
  Normal_30 = worldN_5;
  mediump float tmpvar_31 = 0.0;
  tmpvar_31 = (1.0 - tmpvar_14);
  mediump vec3 I_32 = vec3(0.0);
  I_32 = -(tmpvar_1);
  mediump vec3 normalWorld_33 = vec3(0.0);
  normalWorld_33 = worldN_5;
  mediump vec4 tmpvar_34 = vec4(0.0);
  tmpvar_34.w = 1.0;
  tmpvar_34.xyz = normalWorld_33;
  mediump vec3 x_35 = vec3(0.0);
  x_35.x = dot (unity_SHAr, tmpvar_34);
  x_35.y = dot (unity_SHAg, tmpvar_34);
  x_35.z = dot (unity_SHAb, tmpvar_34);
  mediump vec4 hdr_36 = vec4(0.0);
  hdr_36 = tmpvar_2;
  mediump vec4 tmpvar_37 = vec4(0.0);
  tmpvar_37.xyz = (I_32 - (2.0 * (
    dot (Normal_30, I_32)
   * Normal_30)));
  tmpvar_37.w = ((tmpvar_31 * (1.7 - 
    (0.7 * tmpvar_31)
  )) * 6.0);
  lowp vec4 tmpvar_38 = vec4(0.0);
  tmpvar_38 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_37.xyz, tmpvar_37.w);
  mediump vec4 tmpvar_39 = vec4(0.0);
  tmpvar_39 = tmpvar_38;
  lowp vec3 tmpvar_40 = vec3(0.0);
  mediump vec3 viewDir_41 = vec3(0.0);
  viewDir_41 = worldViewDir_7;
  mediump vec4 c_42 = vec4(0.0);
  lowp vec3 tmpvar_43 = vec3(0.0);
  tmpvar_43 = normalize(worldN_5);
  mediump vec3 tmpvar_44 = vec3(0.0);
  mediump vec3 albedo_45 = vec3(0.0);
  albedo_45 = tmpvar_12;
  mediump vec3 tmpvar_46 = vec3(0.0);
  tmpvar_46 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_45, vec3(tmpvar_13));
  mediump float tmpvar_47 = 0.0;
  tmpvar_47 = (0.7790837 - (tmpvar_13 * 0.7790837));
  tmpvar_44 = (albedo_45 * tmpvar_47);
  tmpvar_40 = tmpvar_44;
  mediump vec3 diffColor_48 = vec3(0.0);
  diffColor_48 = tmpvar_40;
  mediump float alpha_49 = 0.0;
  alpha_49 = tmpvar_16;
  tmpvar_40 = diffColor_48;
  mediump vec3 diffColor_50 = vec3(0.0);
  diffColor_50 = tmpvar_40;
  mediump vec3 normal_51 = vec3(0.0);
  normal_51 = tmpvar_43;
  mediump vec3 tmpvar_52 = vec3(0.0);
  mediump vec3 inVec_53 = vec3(0.0);
  inVec_53 = (tmpvar_4 + viewDir_41);
  tmpvar_52 = (inVec_53 * inversesqrt(max (0.001, 
    dot (inVec_53, inVec_53)
  )));
  mediump float tmpvar_54 = 0.0;
  tmpvar_54 = clamp (dot (normal_51, tmpvar_52), 0.0, 1.0);
  mediump float tmpvar_55 = 0.0;
  tmpvar_55 = (1.0 - tmpvar_14);
  mediump float tmpvar_56 = 0.0;
  tmpvar_56 = (tmpvar_55 * tmpvar_55);
  mediump float x_57 = 0.0;
  x_57 = (1.0 - clamp (dot (normal_51, viewDir_41), 0.0, 1.0));
  mediump vec4 tmpvar_58 = vec4(0.0);
  tmpvar_58.w = 1.0;
  tmpvar_58.xyz = (((
    ((diffColor_50 + ((tmpvar_56 / 
      ((max (0.32, clamp (
        dot (tmpvar_4, tmpvar_52)
      , 0.0, 1.0)) * (1.5 + tmpvar_56)) * (((tmpvar_54 * tmpvar_54) * (
        (tmpvar_56 * tmpvar_56)
       - 1.0)) + 1.00001))
    ) * tmpvar_46)) * tmpvar_3)
   * 
    clamp (dot (normal_51, tmpvar_4), 0.0, 1.0)
  ) + (
    (max (((1.055 * 
      pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD4 + x_35)), vec3(0.4166667, 0.4166667, 0.4166667))
    ) - 0.055), vec3(0.0, 0.0, 0.0)) * tmpvar_15)
   * diffColor_50)) + ((
    (1.0 - ((tmpvar_56 * tmpvar_55) * 0.28))
   * 
    (((hdr_36.x * (
      (hdr_36.w * (tmpvar_39.w - 1.0))
     + 1.0)) * tmpvar_39.xyz) * tmpvar_15)
  ) * mix (tmpvar_46, vec3(
    clamp ((tmpvar_14 + (1.0 - tmpvar_47)), 0.0, 1.0)
  ), vec3(
    ((x_57 * x_57) * (x_57 * x_57))
  ))));
  c_42.xyz = tmpvar_58.xyz;
  c_42.w = alpha_49;
  c_6 = c_42;
  gl_FragData[0] = c_6;
}


