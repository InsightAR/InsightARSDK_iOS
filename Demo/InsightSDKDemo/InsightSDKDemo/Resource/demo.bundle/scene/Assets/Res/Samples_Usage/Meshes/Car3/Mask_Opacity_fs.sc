#include "amplify_standard_head_fs.sh"
#include "amplify_compatible_unity_fs.sh"

uniform sampler2D _NormalMap;
uniform highp vec4 _NormalMap_ST;
uniform sampler2D _Albedo;
uniform highp vec4 _Albedo_ST;
uniform highp vec4 _Base_Color;
uniform sampler2D _Emission;
uniform highp vec4 _Emission_ST;
uniform highp vec4 _Color_Emission;
uniform highp float _lighted;
uniform highp vec4 _Color0;
uniform highp vec4 _Color1;
uniform mediump float _Mask_Height;
uniform mediump float _Mask_Scale;
uniform highp vec4 _Ref_Color;
uniform sampler2D _Occlusion_Mix;
uniform highp vec4 _Occlusion_Mix_ST;
uniform highp float _Smoothness_Ctrl;
uniform highp float _RoadLightOffset;
uniform highp float _RoadLightOffsetSpeed;
uniform highp vec4 _Ref_Scale;
uniform highp float _Metallic_Ctrl;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying mediump vec3 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1 = vec3(0.0);
  highp vec4 tmpvar_2 = vec4(0.0);
  mediump vec3 tmpvar_3 = vec3(0.0);
  mediump vec3 tmpvar_4 = vec3(0.0);
  lowp vec3 worldN_5 = vec3(0.0);
  lowp vec4 c_6 = vec4(0.0);
  lowp vec3 worldViewDir_7 = vec3(0.0);
  lowp vec3 lightDir_8 = vec3(0.0);
  mediump vec3 tmpvar_9 = vec3(0.0);
  mediump vec3 tmpvar_10 = vec3(0.0);
  mediump vec3 tmpvar_11 = vec3(0.0);
  highp vec3 tmpvar_12 = vec3(0.0);
  highp vec3 tmpvar_13 = vec3(0.0);
  tmpvar_13.x = xlv_TEXCOORD1.w;
  tmpvar_13.y = xlv_TEXCOORD2.w;
  tmpvar_13.z = xlv_TEXCOORD3.w;
  mediump vec3 tmpvar_14 = vec3(0.0);
  tmpvar_14 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_14;
  highp vec3 tmpvar_15 = vec3(0.0);
  tmpvar_15 = normalize((_WorldSpaceCameraPos - tmpvar_13));
  worldViewDir_7 = tmpvar_15;
  tmpvar_12 = -(worldViewDir_7);
  tmpvar_9 = xlv_TEXCOORD1.xyz;
  tmpvar_10 = xlv_TEXCOORD2.xyz;
  tmpvar_11 = xlv_TEXCOORD3.xyz;
  lowp vec3 tmpvar_16 = vec3(0.0);
  mediump vec3 tmpvar_17 = vec3(0.0);
  mediump float tmpvar_18 = 0.0;
  mediump float tmpvar_19 = 0.0;
  mediump float tmpvar_20 = 0.0;
  lowp float tmpvar_21 = 0.0;
  highp vec4 tex2DNode13_22 = vec4(0.0);
  highp vec3 ase_worldNormal_23 = vec3(0.0);
  highp vec2 tmpvar_24 = vec2(0.0);
  tmpvar_24 = ((xlv_TEXCOORD0.xy * _NormalMap_ST.xy) + _NormalMap_ST.zw);
  lowp vec3 tmpvar_25 = vec3(0.0);
  tmpvar_25 = ((texture2D (_NormalMap, tmpvar_24).xyz * 2.0) - 1.0);
  highp vec2 tmpvar_26 = vec2(0.0);
  tmpvar_26 = ((xlv_TEXCOORD0.xy * _Albedo_ST.xy) + _Albedo_ST.zw);
  tmpvar_16 = (texture2D (_Albedo, tmpvar_26) * _Base_Color).xyz;
  highp vec2 tmpvar_27 = vec2(0.0);
  tmpvar_27 = ((xlv_TEXCOORD0.zw * _Emission_ST.xy) + _Emission_ST.zw);
  mediump vec3 tmpvar_28 = vec3(0.0);
  tmpvar_28.x = tmpvar_9.z;
  tmpvar_28.y = tmpvar_10.z;
  tmpvar_28.z = tmpvar_11.z;
  ase_worldNormal_23 = tmpvar_28;
  highp vec4 tmpvar_29 = vec4(0.0);
  tmpvar_29 = mix (_Color0, _Color1, vec4(clamp (pow (
    (1.0 - dot (ase_worldNormal_23, normalize((_WorldSpaceCameraPos - tmpvar_13))))
  , 5.0), 0.0, 1.0)));
  highp vec4 tmpvar_30 = vec4(0.0);
  tmpvar_30.w = 1.0;
  tmpvar_30.xyz = tmpvar_13;
  highp float tmpvar_31 = 0.0;
  highp float tmpvar_32 = 0.0;
  tmpvar_32 = clamp (((
    (((unity_WorldToObject * tmpvar_30).y + (_Mask_Height * 0.1)) * _Mask_Scale)
   - -0.1) / 0.2), 0.0, 1.0);
  tmpvar_31 = (tmpvar_32 * (tmpvar_32 * (3.0 - 
    (2.0 * tmpvar_32)
  )));
  highp vec2 tmpvar_33 = vec2(0.0);
  tmpvar_33 = ((xlv_TEXCOORD0.xy * _Occlusion_Mix_ST.xy) + _Occlusion_Mix_ST.zw);
  lowp vec4 tmpvar_34 = vec4(0.0);
  tmpvar_34 = texture2D (_Occlusion_Mix, tmpvar_33);
  tex2DNode13_22 = tmpvar_34;
  highp float tmpvar_35 = 0.0;
  tmpvar_35 = (tex2DNode13_22.z * _Smoothness_Ctrl);
  mediump vec3 tmpvar_36 = vec3(0.0);
  tmpvar_36.x = tmpvar_9.z;
  tmpvar_36.y = tmpvar_10.z;
  tmpvar_36.z = tmpvar_11.z;
  highp vec4 tmpvar_37 = vec4(0.0);
  tmpvar_37.w = 0.0;
  tmpvar_37.xyz = (tmpvar_12 - (2.0 * (
    dot (tmpvar_36, tmpvar_12)
   * tmpvar_36)));
  lowp vec4 tmpvar_38 = vec4(0.0);
  tmpvar_38 = texture2D (_Emission, tmpvar_27);
  highp vec4 tmpvar_39 = vec4(0.0);
  tmpvar_39.z = 0.0;
  tmpvar_39.xy = abs(((
    ((unity_WorldToObject * tmpvar_37).zx * _Ref_Scale.xy)
   * 
    clamp (ase_worldNormal_23.y, 0.0, 1.0)
  ) + (
    (_RoadLightOffset * _RoadLightOffsetSpeed)
   * vec2(3.0, 0.0))));
  tmpvar_39.w = ((1.0 - tmpvar_35) * 10.0);
  lowp vec4 tmpvar_40 = vec4(0.0);
  tmpvar_40 = impl_low_texture2DLodEXT (_Emission, tmpvar_39.xy, tmpvar_39.w);
  tmpvar_17 = (((
    (tmpvar_38.x * _Color_Emission)
   * _lighted) + (tmpvar_29 * tmpvar_31)) + ((_Ref_Color * tmpvar_40.y) * tex2DNode13_22.y)).xyz;
  tmpvar_18 = (tex2DNode13_22.y * _Metallic_Ctrl);
  tmpvar_19 = tmpvar_35;
  tmpvar_20 = tex2DNode13_22.y;
  highp float tmpvar_41 = 0.0;
  tmpvar_41 = clamp (((1.0 - tmpvar_31) + tmpvar_29.w), 0.0, 1.0);
  tmpvar_21 = tmpvar_41;
  highp float tmpvar_42 = 0.0;
  tmpvar_42 = dot (xlv_TEXCOORD1.xyz, tmpvar_25);
  worldN_5.x = tmpvar_42;
  highp float tmpvar_43 = 0.0;
  tmpvar_43 = dot (xlv_TEXCOORD2.xyz, tmpvar_25);
  worldN_5.y = tmpvar_43;
  highp float tmpvar_44 = 0.0;
  tmpvar_44 = dot (xlv_TEXCOORD3.xyz, tmpvar_25);
  worldN_5.z = tmpvar_44;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_8;
  tmpvar_1 = worldViewDir_7;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_45 = vec3(0.0);
  Normal_45 = worldN_5;
  mediump float tmpvar_46 = 0.0;
  tmpvar_46 = (1.0 - tmpvar_19);
  mediump vec3 I_47 = vec3(0.0);
  I_47 = -(tmpvar_1);
  mediump vec3 normalWorld_48 = vec3(0.0);
  normalWorld_48 = worldN_5;
  mediump vec4 tmpvar_49 = vec4(0.0);
  tmpvar_49.w = 1.0;
  tmpvar_49.xyz = normalWorld_48;
  mediump vec3 x_50 = vec3(0.0);
  x_50.x = dot (unity_SHAr, tmpvar_49);
  x_50.y = dot (unity_SHAg, tmpvar_49);
  x_50.z = dot (unity_SHAb, tmpvar_49);
  mediump vec4 hdr_51 = vec4(0.0);
  hdr_51 = tmpvar_2;
  mediump vec4 tmpvar_52 = vec4(0.0);
  tmpvar_52.xyz = (I_47 - (2.0 * (
    dot (Normal_45, I_47)
   * Normal_45)));
  tmpvar_52.w = ((tmpvar_46 * (1.7 - 
    (0.7 * tmpvar_46)
  )) * 6.0);
  lowp vec4 tmpvar_53 = vec4(0.0);
  tmpvar_53 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_52.xyz, tmpvar_52.w);
  mediump vec4 tmpvar_54 = vec4(0.0);
  tmpvar_54 = tmpvar_53;
  lowp vec3 tmpvar_55 = vec3(0.0);
  mediump vec3 viewDir_56 = vec3(0.0);
  viewDir_56 = worldViewDir_7;
  mediump vec4 c_57 = vec4(0.0);
  lowp vec3 tmpvar_58 = vec3(0.0);
  tmpvar_58 = normalize(worldN_5);
  mediump vec3 tmpvar_59 = vec3(0.0);
  mediump vec3 albedo_60 = vec3(0.0);
  albedo_60 = tmpvar_16;
  mediump vec3 tmpvar_61 = vec3(0.0);
  tmpvar_61 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_60, vec3(tmpvar_18));
  mediump float tmpvar_62 = 0.0;
  tmpvar_62 = (0.7790837 - (tmpvar_18 * 0.7790837));
  tmpvar_59 = (albedo_60 * tmpvar_62);
  tmpvar_55 = tmpvar_59;
  mediump vec3 diffColor_63 = vec3(0.0);
  diffColor_63 = tmpvar_55;
  mediump float alpha_64 = 0.0;
  alpha_64 = tmpvar_21;
  tmpvar_55 = diffColor_63;
  mediump vec3 diffColor_65 = vec3(0.0);
  diffColor_65 = tmpvar_55;
  mediump vec3 normal_66 = vec3(0.0);
  normal_66 = tmpvar_58;
  mediump vec3 tmpvar_67 = vec3(0.0);
  mediump vec3 inVec_68 = vec3(0.0);
  inVec_68 = (tmpvar_4 + viewDir_56);
  tmpvar_67 = (inVec_68 * inversesqrt(max (0.001, 
    dot (inVec_68, inVec_68)
  )));
  mediump float tmpvar_69 = 0.0;
  tmpvar_69 = clamp (dot (normal_66, tmpvar_67), 0.0, 1.0);
  mediump float tmpvar_70 = 0.0;
  tmpvar_70 = (1.0 - tmpvar_19);
  mediump float tmpvar_71 = 0.0;
  tmpvar_71 = (tmpvar_70 * tmpvar_70);
  mediump float x_72 = 0.0;
  x_72 = (1.0 - clamp (dot (normal_66, viewDir_56), 0.0, 1.0));
  mediump vec4 tmpvar_73 = vec4(0.0);
  tmpvar_73.w = 1.0;
  tmpvar_73.xyz = (((
    ((diffColor_65 + ((tmpvar_71 / 
      ((max (0.32, clamp (
        dot (tmpvar_4, tmpvar_67)
      , 0.0, 1.0)) * (1.5 + tmpvar_71)) * (((tmpvar_69 * tmpvar_69) * (
        (tmpvar_71 * tmpvar_71)
       - 1.0)) + 1.00001))
    ) * tmpvar_61)) * tmpvar_3)
   * 
    clamp (dot (normal_66, tmpvar_4), 0.0, 1.0)
  ) + (
    (max (((1.055 * 
      pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD4 + x_50)), vec3(0.4166667, 0.4166667, 0.4166667))
    ) - 0.055), vec3(0.0, 0.0, 0.0)) * tmpvar_20)
   * diffColor_65)) + ((
    (1.0 - ((tmpvar_71 * tmpvar_70) * 0.28))
   * 
    (((hdr_51.x * (
      (hdr_51.w * (tmpvar_54.w - 1.0))
     + 1.0)) * tmpvar_54.xyz) * tmpvar_20)
  ) * mix (tmpvar_61, vec3(
    clamp ((tmpvar_19 + (1.0 - tmpvar_62)), 0.0, 1.0)
  ), vec3(
    ((x_72 * x_72) * (x_72 * x_72))
  ))));
  c_57.xyz = tmpvar_73.xyz;
  c_57.w = alpha_64;
  c_6 = c_57;
  c_6.xyz = (c_6.xyz + tmpvar_17);
  gl_FragData[0] = c_6;
}


