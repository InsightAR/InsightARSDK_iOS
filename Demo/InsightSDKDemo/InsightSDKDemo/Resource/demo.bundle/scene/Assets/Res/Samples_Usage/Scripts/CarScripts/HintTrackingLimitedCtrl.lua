local HintTrackingLimitedCtrl = {};

function HintTrackingLimitedCtrl:New( game_object )
	if self ~= HintTrackingLimitedCtrl then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = HintTrackingLimitedCtrl } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function HintTrackingLimitedCtrl:Start()

	-- Hints
	self.HintPleaseMove = Insight.GameObject.Find("HintPleaseMove");
	self.HintTooFast = Insight.GameObject.Find("HintTooFast");
	self.HintTooFar = Insight.GameObject.Find("HintTooFar");
	self.HintTooDark = Insight.GameObject.Find("HintTooDark");

	-- 启动时设置个物体显示状态

	self.HintPleaseMove:SetActive(false);
	self.HintTooFast:SetActive(false);
	self.HintTooFar:SetActive(false);
	self.HintTooDark:SetActive(false);

end

function HintTrackingLimitedCtrl:Update()

	----------------------------------------------------------------------
 	-- 根据检测的状态进行各项设置
 	-- 注意：这个检测的状态并非每帧都更新，所以，有时可能会有几帧的滞后

 	 -- 接下来这些检测状态都必须确保是在初始化成功后才能使用，否则如果重新启动页面，很可能前几帧还是用的是上一轮的检测结果

 	if Insight.Tracking.status > Fw_ARState_Init_OK  then 

 		if Insight.Tracking.status == Fw_ARState_Detecting  then

 			-- print( "[test AR Detecting Status] AR is still dectecting ");

	 		if( self.HintPleaseMove.activeSelf == false ) then
	 			self.HintPleaseMove:SetActive(true);
	 		end

	 		self:HideLimitedHints();


		elseif ( Insight.Tracking.status == Fw_ARState_Detect_OK ) or ( Insight.Tracking.status == Fw_ARState_Tracking )  then

			if( self.HintPleaseMove.activeSelf == true ) then
	 			self.HintPleaseMove:SetActive(false);
	 		end

	 		self:HideLimitedHints();

	 	elseif  Insight.Tracking.status == Fw_ARState_Track_Limited then

	 		-- Show reason for tracking limited

	 		local reason = Insight.Tracking.GetReason();

	 		-- print( "[test AR Detecting Status] AR dectecting is limited for reason : " .. reason ); --test

	 		if reason == Fw_TrackingLimitedReason_ReasonNone then

	 			-- No hint
 				self.HintTooFar:SetActive(false);
 				self.HintTooFast:SetActive(false);
 				self.HintTooDark:SetActive(false);

 			elseif reason == Fw_TrackingLimitedReason_LowLight then

 				-- print( "[test hint] Hint is visible :" .. tostring(self.HintTooDark.activeSelf) ); --test

 				if not(self.HintTooDark.activeSelf) then
 					self.HintTooDark:SetActive(true);
 				end

 				self.HintTooFar:SetActive(false);
 				self.HintTooFast:SetActive(false);
 				self.HintPleaseMove:SetActive(false);


 			elseif reason == Fw_TrackingLimitedReason_ExcessiveMotion then

 				if not(self.HintTooFast.activeSelf) then
 					self.HintTooFast:SetActive(true);
 				end
 				
 				self.HintTooFar:SetActive(false);
 				self.HintTooDark:SetActive(false);
 				self.HintPleaseMove:SetActive(false);

 			elseif reason == Fw_TrackingLimitedReason_InsufficientFeatures then

 				if not(self.HintTooFar.activeSelf) then
 					self.HintTooFar:SetActive(true);
 				end
 				
 				self.HintTooFast:SetActive(false);
 				self.HintTooDark:SetActive(false);
 				self.HintPleaseMove:SetActive(false);

 			else
	 			-- No hint

	 			self:HideLimitedHints();

 			end
	 	end

	 end


end

function HintTrackingLimitedCtrl:HideLimitedHints()

	if self.HintTooFast.activeSelf then
		self.HintTooFast:SetActive(false);
	end
	if self.HintTooDark.activeSelf then
		self.HintTooDark:SetActive(false);
	end
	if self.HintTooFar.activeSelf then
		self.HintTooFar:SetActive(false);
	end

end


return HintTrackingLimitedCtrl;
