

local UIBehaviourLogButton = {};

function UIBehaviourLogButton:New( game_object )
	if self ~= UIBehaviourLogButton then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = UIBehaviourLogButton } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function UIBehaviourLogButton:Start()
        self.spriteLogOff = self.game_object:GetComponent( "Button").spriteState.disabledSprite;
        self.spriteLogOn = self.game_object:GetComponent( "Button").spriteState.highLightedSprite;
end

function UIBehaviourLogButton:OnEnable()
	    if globalLogSwitch then
            self.game_object:GetComponent( "Image").sprite = self.game_object:GetComponent( "Button").spriteState.highLightedSprite;
	    elseif globalLogSwitch == false then
            self.game_object:GetComponent( "Image").sprite = self.game_object:GetComponent( "Button").spriteState.disabledSprite;
	    end
end

function UIBehaviourLogButton:OnPointerUp()
	globalLogSwitch = not globalLogSwitch;
	
	if globalLogSwitch then 
        self.game_object:GetComponent( "Image").sprite = self.spriteLogOn;
	elseif globalLogSwitch == false then
        self.game_object:GetComponent( "Image").sprite = self.spriteLogOff;
	end
end

return UIBehaviourLogButton;
