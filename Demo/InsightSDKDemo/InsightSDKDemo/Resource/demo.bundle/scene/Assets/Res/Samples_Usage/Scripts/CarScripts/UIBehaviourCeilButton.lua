local UIBehaviourCeilButton = {};

function UIBehaviourCeilButton:New( game_object )
	if self ~= UIBehaviourCeilButton then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = UIBehaviourCeilButton } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function UIBehaviourCeilButton:Start()

	-- print( "Lua UIBehaviourCeilButton Start: " .. self.game_object:GetName() );

	self.Ceil = Insight.GameObject.Find( "ajustTrans/BMW_car" );


	self.opened = false;

	-- -- KXC COMMENT:
	self.spriteNormal = self.game_object:GetComponent( "Button").spriteState.disabledSprite;
	self.spriteOn = self.game_object:GetComponent( "Button").spriteState.highLightedSprite;
	-- self.spriteNormal = self.game_object:GetImage():GetSprite();
	-- self.spriteOn = self.game_object:GetButton():GetHighLightedSprite();

end

function UIBehaviourCeilButton:OnPointerUp()

	local nt = self.Ceil:GetComponent( "Animator" ).normalizedTime;
	if self.opened then

		self.Ceil:GetComponent( "Animator" ):Play( "openning" , -1 , nt);
		self.Ceil:GetComponent( "Animator" ).speed = -1;

		self.opened = false;

		if self.spriteNormal then
			-- self.game_object:GetImage():SetSprite( self.spriteNormal );
			self.game_object:GetComponent( "Image").sprite = self.spriteNormal;
		end

	else

		self.Ceil:GetComponent( "Animator" ):Play( "openning" , -1 , nt);
		self.Ceil:GetComponent( "Animator" ).speed = 1;
		-- self.Ceil:GetAnimator():Play( "openning" , nt , 1 );
		self.opened = true;

		if self.spriteOn then
			-- self.game_object:GetImage():SetSprite( self.spriteOn );
			self.game_object:GetComponent( "Image").sprite = self.spriteOn;

		end

	end

end


return UIBehaviourCeilButton;
