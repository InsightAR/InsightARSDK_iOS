local StuffDisappearHintCtrl = {};
local cam;

-- Camera FOV = 60 时的摄像机与裁剪平面上下边之间的点积
-- Set: 不允许修改
-- Get: StuffDisappearHintCtrl.lua
g_DotCameraFOV60 = 0.866;

-- 设置提示文字的位置边界
local boundWidth = 0.3;
local boundHeigth = 0.25;
local marginVertical = 0.12;
local marginHorizontal = 0.05;


function StuffDisappearHintCtrl:New( game_object )
	if self ~= StuffDisappearHintCtrl then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = StuffDisappearHintCtrl } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	
	return new_instance;
end

function StuffDisappearHintCtrl:Start()

	cam = Insight.GameObject.Find( "Main Camera Auto UI"  );

	self.StuffRoot = Insight.GameObject.Find("StuffRoot");
	self.HintMiss = Insight.GameObject.Find("HintMiss");
	self.HintMissArrow = Insight.GameObject.Find("HintMiss/ArrowTrans");

	-- 启动时设置个物体显示状态

	--self.StuffRoot:SetActive(false);
	self.HintMissArrow:SetActive(false);

end

function StuffDisappearHintCtrl:Update()

 	----------------------------------------------------------------------
 	-- 根据StuffRoot的位置判断是否已经移出摄像机屏幕
 	-- 假定Camera FOV = 60 (目前Demo都设置为60)

 	if self.StuffRoot.activeSelf then

 		local vecStuff = (self.StuffRoot.transform.localPosition - cam.transform.localPosition).normalized;
 		local camForward =  cam.transform.forward.normalized;
 		local dotCamStuff = Insight.Vector3.Dot( vecStuff , camForward ) ;
 		
 		if dotCamStuff < g_DotCameraFOV60 then


 			-- print( "[test dot] Out ! dotCamStuff = " ..tostring( dotCamStuff ) );

			if not( self.HintMissArrow.activeSelf ) then

	 			self.HintMissArrow:SetActive(true);
	 		end

 			-- 判断是在哪个位置

 			local stuffOffset = self.StuffRoot.transform.localPosition - cam.transform.localPosition;
 			local x = Insight.Vector3.Dot( stuffOffset, cam.transform.right ) / stuffOffset.magnitude;  --( stuffOffset:Dot( cam:GetWorldRight()) ) / stuffOffset:Length();  -- cos(a)
 			local y = Insight.Vector3.Dot( stuffOffset, cam.transform.up ) / stuffOffset.magnitude; -- cos(b)

 			local cosA = x / math.sqrt( x * x + y * y );
 			local sinA = y / math.sqrt( x * x + y * y );


 			-- 判断横竖屏的情况
 			local cameraComp = cam:GetComponent( "Camera", 0  );
			local viewsize = Insight.Vector2.New( cameraComp.pixelWidth , cameraComp.pixelHeight );
			local screenAspect = viewsize.x / viewsize.y;

			local boundWidthFinal = boundWidth;
			local boundHeigthFinal = boundHeigth;
			local marginHorizontalFinal = marginHorizontal;
			local marginVerticalFinal = marginVertical;

			if screenAspect < 1 then 
				boundWidthFinal, boundHeigthFinal = Fw_Swap( boundWidth, boundHeigth );
				marginHorizontalFinal, marginVerticalFinal = Fw_Swap( marginHorizontal * 3,  marginVertical * 2);
			end

 			-- Hint坐标设置为椭圆上的点
 			local pos = Insight.Vector2.New( ( ( boundWidthFinal - marginHorizontalFinal ) / 2 ) * cosA, ( ( boundHeigthFinal - marginVerticalFinal ) / 2 ) * sinA ); 
 			local posHint = Insight.Vector3.New( pos.x, pos.y, self.HintMiss.transform.localPosition.z );

 			self.HintMiss.transform.localPosition =  posHint ;

 			-- 计算指示箭头的旋转方向

 			local angle;

 			if ( sinA >= 0 ) and ( cosA >= 0 ) then 

 				angle = math.deg( math.asin( sinA ) );

			elseif ( sinA < 0 ) and ( cosA >= 0 ) then

				angle = math.deg( math.asin( sinA ) );

			elseif ( sinA >= 0 ) and ( cosA < 0 ) then

				angle = math.deg( math.acos( cosA ) );
			else
				angle = 180 - math.deg( math.asin( sinA ) ) ;
			end 

			-- print( " [test angle ] angle =  " .. angle );--test

			self.HintMissArrow.transform.localRotation =  Insight.Quaternion.AngleAxis( angle, Insight.Vector3.New( 0, 0, 1) ) ;


 		else
 			-- print( "[test dot]  In ! dotCamStuff = " ..tostring( dotCamStuff ) );

 			if self.HintMissArrow.activeSelf then

	 			self.HintMissArrow:SetActive(false);
	 		end
		end

 	end


end


return StuffDisappearHintCtrl;
