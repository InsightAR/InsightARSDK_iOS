

local TestButton_ReloadAR = {};

function TestButton_ReloadAR:New( game_object )
	if self ~= TestButton_ReloadAR then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = TestButton_ReloadAR } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function TestButton_ReloadAR:Start()

end

function TestButton_ReloadAR:OnPointerUp()
	-- ReloadARProduct
	Insight.Event.Happen( 1 , 1 , 102 , nil );

end

return TestButton_ReloadAR;
