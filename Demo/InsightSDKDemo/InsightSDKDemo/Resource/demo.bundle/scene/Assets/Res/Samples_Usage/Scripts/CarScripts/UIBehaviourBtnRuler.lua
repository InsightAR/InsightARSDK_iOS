
-- UI Behaviour
-- Setup and run the UI
-- by Tqw


local UIBehaviourBtnRuler = {};

function UIBehaviourBtnRuler:New( game_object )
	if self ~= UIBehaviourBtnRuler then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = UIBehaviourBtnRuler } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function UIBehaviourBtnRuler:Start()

	self.BoundBox = Insight.GameObject.Find("BoundBox");
	self.BoundBox:SetActive(false);

	self.StuffRoot = Insight.GameObject.Find("StuffRoot");

	-- -- KXC COMMENT:
	self.spriteNormal = self.game_object:GetComponent( "Button").spriteState.disabledSprite;
	self.spriteOn = self.game_object:GetComponent( "Button").spriteState.highLightedSprite;
	-- self.spriteNormal = self.game_object:GetImage():GetSprite();
	-- self.spriteOn = self.game_object:GetButton():GetHighLightedSprite();


end

function UIBehaviourBtnRuler:OnPointerUp()
	
	local visible = not self.BoundBox.activeSelf;
	self.BoundBox:SetActive( visible );

	if visible then
		
		-- self.game_object:GetImage():SetSprite(self.spriteOn);
		self.game_object:GetComponent( "Image").sprite = self.spriteOn;
	else
		-- self.game_object:GetImage():SetSprite(self.spriteNormal);
		self.game_object:GetComponent( "Image").sprite = self.spriteNormal;
	end


end




return UIBehaviourBtnRuler;
