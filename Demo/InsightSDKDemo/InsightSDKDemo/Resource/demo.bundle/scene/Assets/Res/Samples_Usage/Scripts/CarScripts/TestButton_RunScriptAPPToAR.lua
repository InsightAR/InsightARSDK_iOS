local TestButton_RunScriptAPPToAR = {};

function TestButton_RunScriptAPPToAR:New( game_object )
    if self ~= TestButton_RunScriptAPPToAR then return nil, "First argument must be self." end
    local new_instance = setmetatable( {} , { __metatable = {}, __index = TestButton_RunScriptAPPToAR } );
    new_instance.transform = game_object.transform;
    new_instance.game_object = game_object.transform.gameObject;
    
    ------------------
    -- Members
    -------------------
    
    return new_instance;
end

function g_SetUserInfo( jsonStr )
    
    Insight.Debug.Log("[ARScript Log] g_SetUserInfo , params: " .. tostring(jsonStr) .."\n"); --test
    
    if jsonStr then
        Insight.Debug.Log( "####################################\n" );
        Insight.Debug.Log( "[ARScript Log] JSON:\n" .. tostring( g_CarWithUI_JSON ) .. "\n" );
        local lua_value = g_CarWithUI_JSON:decode( jsonStr ) -- decode example
        
        if lua_value["userId"] then
            g_UserId = lua_value["userId"];
            Insight.Debug.Log( "userId = " .. lua_value["userId"] .. "\n" );
        end
        
        if lua_value["firstTime"] then
            local firstTime = tonumber(lua_value["firstTime"]);
            g_firstTime = firstTime;
            Insight.Debug.Log( "firstTime = " .. lua_value["firstTime"] .. "\n" );
        end
        
        if lua_value["adList"] then
            if   LuaMath_Tablelength( lua_value["adList"] ) > 0 then
                
                -- Clean table first
                for k in pairs (g_AdList) do
                    g_AdList[k] = nil
                end
                
                for i,item in ipairs(lua_value["adList"]) do
                    table.insert(g_AdList,item["content"]);
                    Insight.Debug.Log("index = " .. i .. "  content = " .. item["content"] .. "\n");
                end
            end
        end
        Insight.Debug.Log( "####################################\n" );

    end
end


function TestButton_RunScriptAPPToAR:Start()
    
end

function TestButton_RunScriptAPPToAR:OnPointerUp()
    Fw_Event_SendRunScript("g_SetUserInfo");
end

return TestButton_RunScriptAPPToAR;
