

local TestButton_CloseAR = {};

function TestButton_CloseAR:New( game_object )
	if self ~= TestButton_CloseAR then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = TestButton_CloseAR } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function TestButton_CloseAR:Start()


end

function TestButton_CloseAR:OnPointerUp()
	Insight.Event.Happen( 1 , 1 , 101 , nil);
end

return TestButton_CloseAR;
