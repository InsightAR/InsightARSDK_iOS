#include "amplify_standard_head_vs.sh"
#include "amplify_compatible_unity_vs.sh"

uniform highp vec4 _texcoord_ST;
uniform highp vec4 _texcoord2_ST;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying mediump vec3 xlv_TEXCOORD4;
varying highp vec2 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD7;
void main ()
{
  lowp float tangentSign_1 = 0.0;
  lowp vec3 worldTangent_2 = vec3(0.0);
  lowp vec3 worldNormal_3 = vec3(0.0);
  highp vec4 tmpvar_4 = vec4(0.0);
  highp vec2 tmpvar_5 = vec2(0.0);
  highp vec4 tmpvar_6 = vec4(0.0);
  highp vec4 tmpvar_7 = vec4(0.0);
  tmpvar_7.w = 1.0;
  tmpvar_7.xyz = _glesVertex.xyz;
  tmpvar_4.xy = ((_glesMultiTexCoord0.xy * _texcoord_ST.xy) + _texcoord_ST.zw);
  tmpvar_4.zw = ((_glesMultiTexCoord1.xy * _texcoord2_ST.xy) + _texcoord2_ST.zw);
  highp vec3 tmpvar_8 = vec3(0.0);
  tmpvar_8 = (unity_ObjectToWorld * _glesVertex).xyz;
  highp mat3 tmpvar_9;
  tmpvar_9[0] = unity_WorldToObject[0].xyz;
  tmpvar_9[1] = unity_WorldToObject[1].xyz;
  tmpvar_9[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_10 = vec3(0.0);
  tmpvar_10 = normalize((_glesNormal * tmpvar_9));
  worldNormal_3 = tmpvar_10;
  highp mat3 tmpvar_11;
  tmpvar_11[0] = unity_ObjectToWorld[0].xyz;
  tmpvar_11[1] = unity_ObjectToWorld[1].xyz;
  tmpvar_11[2] = unity_ObjectToWorld[2].xyz;
  highp vec3 tmpvar_12 = vec3(0.0);
  tmpvar_12 = normalize((tmpvar_11 * _glesTANGENT.xyz));
  worldTangent_2 = tmpvar_12;
  highp float tmpvar_13 = 0.0;
  tmpvar_13 = (_glesTANGENT.w * unity_WorldTransformParams.w);
  tangentSign_1 = tmpvar_13;
  lowp vec3 tmpvar_14 = vec3(0.0);
  tmpvar_14 = (((worldNormal_3.yzx * worldTangent_2.zxy) - (worldNormal_3.zxy * worldTangent_2.yzx)) * tangentSign_1);
  highp vec4 tmpvar_15 = vec4(0.0);
  tmpvar_15.x = worldTangent_2.x;
  tmpvar_15.y = tmpvar_14.x;
  tmpvar_15.z = worldNormal_3.x;
  tmpvar_15.w = tmpvar_8.x;
  highp vec4 tmpvar_16 = vec4(0.0);
  tmpvar_16.x = worldTangent_2.y;
  tmpvar_16.y = tmpvar_14.y;
  tmpvar_16.z = worldNormal_3.y;
  tmpvar_16.w = tmpvar_8.y;
  highp vec4 tmpvar_17 = vec4(0.0);
  tmpvar_17.x = worldTangent_2.z;
  tmpvar_17.y = tmpvar_14.z;
  tmpvar_17.z = worldNormal_3.z;
  tmpvar_17.w = tmpvar_8.z;
  mediump vec3 normal_18 = vec3(0.0);
  normal_18 = worldNormal_3;
  mediump vec3 x1_19 = vec3(0.0);
  mediump vec4 tmpvar_20 = vec4(0.0);
  tmpvar_20 = (normal_18.xyzz * normal_18.yzzx);
  x1_19.x = dot (unity_SHBr, tmpvar_20);
  x1_19.y = dot (unity_SHBg, tmpvar_20);
  x1_19.z = dot (unity_SHBb, tmpvar_20);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_7));
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD1 = tmpvar_15;
  xlv_TEXCOORD2 = tmpvar_16;
  xlv_TEXCOORD3 = tmpvar_17;
  xlv_TEXCOORD4 = (x1_19 + (unity_SHC.xyz * (
    (normal_18.x * normal_18.x)
   - 
    (normal_18.y * normal_18.y)
  )));
  xlv_TEXCOORD5 = tmpvar_5;
  xlv_TEXCOORD7 = tmpvar_6;
}


