#include "amplify_standard_head_fs.sh"
#include "amplify_compatible_unity_fs.sh"

uniform highp vec4 _Color;
uniform highp float _Opacity;
uniform highp float _DotLength;
uniform highp float _Scale;
uniform highp float _Length;
varying highp vec3 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 c_1 = vec4(0.0);
  lowp float tmpvar_2 = 0.0;
  tmpvar_2 = 0.0;
  lowp vec3 tmpvar_3 = vec3(0.0);
  lowp float tmpvar_4 = 0.0;
  tmpvar_4 = tmpvar_2;
  tmpvar_3 = _Color.xyz;
  highp vec4 tmpvar_5 = vec4(0.0);
  tmpvar_5.w = 1.0;
  tmpvar_5.xyz = xlv_TEXCOORD1;
  highp vec3 tmpvar_6 = vec3(0.0);
  tmpvar_6 = (unity_WorldToObject * tmpvar_5).xyz;
  highp float tmpvar_7 = 0.0;
  if ((tmpvar_6.x > 0.0)) {
    tmpvar_7 = tmpvar_6.x;
  } else {
    tmpvar_7 = (tmpvar_6.x - (_DotLength * _Scale));
  };
  highp float tmpvar_8 = 0.0;
  tmpvar_8 = floor((_Length / _DotLength));
  highp float tmpvar_9 = 0.0;
  tmpvar_9 = (floor((
    abs(tmpvar_7)
   / 
    (_Scale * _Length)
  )) / tmpvar_8);
  highp float tmpvar_10 = 0.0;
  tmpvar_10 = (fract(abs(tmpvar_9)) * tmpvar_8);
  highp float tmpvar_11 = 0.0;
  if ((tmpvar_9 >= 0.0)) {
    tmpvar_11 = tmpvar_10;
  } else {
    tmpvar_11 = -(tmpvar_10);
  };
  highp float tmpvar_12 = 0.0;
  if ((tmpvar_11 <= (_DotLength / _Length))) {
    tmpvar_12 = 1.0;
  } else {
    tmpvar_12 = 0.0;
  };
  tmpvar_4 = (_Opacity * tmpvar_12);
  tmpvar_2 = tmpvar_4;
  lowp vec4 tmpvar_13 = vec4(0.0);
  tmpvar_13.xyz = vec3(0.0, 0.0, 0.0);
  tmpvar_13.w = tmpvar_4;
  c_1.w = tmpvar_13.w;
  c_1.xyz = tmpvar_3;
  gl_FragData[0] = c_1;
}


