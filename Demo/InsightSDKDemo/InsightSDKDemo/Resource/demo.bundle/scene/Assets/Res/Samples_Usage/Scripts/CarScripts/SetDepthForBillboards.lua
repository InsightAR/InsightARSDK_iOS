
local SetDepthForBillboards = {};
function SetDepthForBillboards:New( game_object )
	if self ~= SetDepthForBillboards then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = SetDepthForBillboards } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function SetDepthForBillboards:Start()

	-- KXC Commented: Should Be Back
	-- self.game_object:GetComponent( "Renderer" ):SetDepth( 0 , 0 , 0, false );

end

return SetDepthForBillboards;