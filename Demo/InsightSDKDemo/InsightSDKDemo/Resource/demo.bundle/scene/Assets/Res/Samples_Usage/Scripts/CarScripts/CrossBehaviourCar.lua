local CrossPosition = {};
function CrossPosition:New( game_object )
  if self ~= CrossPosition then return nil, "First argument must be self." end
  local new_instance = setmetatable( {} , { __metatable = {}, __index = CrossPosition } );
  new_instance.transform = game_object.transform;
  new_instance.game_object = game_object.transform.gameObject;

    -------------------------
    -- Member Variables

    new_instance.camPosY = 0;
    new_instance.isMovingWithCamera = true;

    -------------------------

  return new_instance;
end 


---------------------
-- Life Cycle

function CrossPosition:Start()
  
   self.cam = Insight.GameObject.Find( "Main Camera Auto UI" );

end

function CrossPosition:Update()

  if self.isMovingWithCamera then
    	----------------------------------------------------------------------
      -- 用摄像机的位置及forward设置十字标世界位置

      -- Get Cam position and forward

      local camPosition = self.cam.transform.position;
      local camForward  = self.cam.transform.forward.normalized;

      self.camPosY = 0.12; 
      if self.camPosY == 0 then
        self.camPosY = 0.12;
      end
      -- Calc self position

      local planeDownVec = Insight.Vector3.New( 0, ( -1.0 ) * self.camPosY, 0 );

      local cosAngle = ( camForward.x * planeDownVec.x + camForward.y * planeDownVec.y +camForward.z * planeDownVec.z  ) / ( camForward.magnitude * planeDownVec.magnitude );

      local forwardLength = planeDownVec.magnitude / cosAngle ;
      forwardLength = math.min( math.abs(forwardLength), 10000);
      local targetPos = camPosition + camForward * forwardLength;

      --Insight.Debug.Log(  "[AR] cosAngle : ".. cosAngle .. " forwardLenght: "..forwardLength .."\n"  ); --test

      -- Set target position

      self.transform.position =  targetPos ;
      -- self.transform.rotation =  Insight.Quaternion.LookRotation(  Insight.Vector3.New( camForward.x , 0  , camForward.z ) ,  Insight.Vector3.up );

  else


  end
  
end

return CrossPosition