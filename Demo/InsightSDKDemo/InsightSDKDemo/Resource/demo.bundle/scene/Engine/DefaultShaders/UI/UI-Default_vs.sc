#include "amplify_standard_head_vs.sh"
#include "amplify_compatible_unity_vs.sh"

varying lowp vec4 xlv_COLOR;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;

uniform lowp vec4 _Color;
void main ()
{
  highp vec4 tmpvar_1 = vec4(0.0);
  tmpvar_1 = _glesVertex;
  lowp vec4 tmpvar_2 = vec4(0.0);
  highp vec4 tmpvar_3 = vec4(0.0);
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = tmpvar_1.xyz;
  tmpvar_2 = (_glesColor * _Color);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_COLOR = tmpvar_2;
  xlv_TEXCOORD0 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD1 = tmpvar_1;
}



