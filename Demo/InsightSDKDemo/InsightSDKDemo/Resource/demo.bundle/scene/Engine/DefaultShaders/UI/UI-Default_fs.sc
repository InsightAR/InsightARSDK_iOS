#include "amplify_standard_head_fs.sh"
#include "amplify_compatible_unity_fs.sh"

varying lowp vec4 xlv_COLOR;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;

uniform sampler2D _MainTex;

void main ()
{
  gl_FragData[0] = ((texture2D (_MainTex, xlv_TEXCOORD0)) * xlv_COLOR);

  // lowp vec4 tmpvar_1 = vec4(0.0);
  // mediump vec4 color_2 = vec4(0.0);
  // lowp vec4 tmpvar_3 = vec4(0.0);
  // tmpvar_3 = ((texture2D (_MainTex, xlv_TEXCOORD0) + _TextureSampleAdd) * xlv_COLOR);
  // color_2 = tmpvar_3;
  // highp float tmpvar_4 = 0.0;
  // highp vec2 tmpvar_5 = vec2(0.0);
  // tmpvar_5.x = float((_ClipRect.z >= xlv_TEXCOORD1.x));
  // tmpvar_5.y = float((_ClipRect.w >= xlv_TEXCOORD1.y));
  // highp vec2 tmpvar_6 = vec2(0.0);
  // tmpvar_6 = (vec2(greaterThanEqual (xlv_TEXCOORD1.xy, _ClipRect.xy)) * tmpvar_5);
  // tmpvar_6 = xlv_TEXCOORD1.xy;
  // tmpvar_4 = (tmpvar_6.x * tmpvar_6.y);
  // color_2.w = (color_2.w * tmpvar_4);
  // tmpvar_1 = color_2;
  // gl_FragData[0] = tmpvar_1;
}



