//
//  AppDelegate.m
//  LightSDKDemo
//
//  Created by Dikey on 23/02/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "AppDelegate.h"
#import <InsightSDK/InsightSDK.h>
#import "NetworkHelper.h"
#import "UIWindow+PazLabs.h"

typedef NS_ENUM(NSUInteger, RegisteType) {
    RegisteTypeInsightOnlineFormal, //正式服务器；正式组织  //com.netease.InsightSDK
    RegisteTypeInsightOnlineTest, //正式服务器；测试组织  //com.netease.InsightSDK
    RegisteTypeInsightTestFormal, //测试服务器；正式组织
    RegisteTypeInsightTestTest, //测试服务器；测试组织
    RegisteTypeInsightOffline, //测试服务器；测试组织
    RegisteTypeInsightDianpin,
    RegisteTypeInsightAntfans, // 正式服务器，测试组织 com.antfans.fans / AR-KEL6-SXLYHWF6NOCUG-I-T/X8LHcxK8s0/PID = 2426
};

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self addFabric];
    [self startInsightSDK];
    [NetworkHelper sharedHelper];
    return YES;
}

- (void)startInsightSDK
{
    if ([InsightARManager isSDKSupported]) {
        [self registerInsightSDKWithType:RegisteTypeInsightOnlineFormal];
        [self checkProductsStatus];
    }else{
        NSLog(@"InsightSDK Not Support ,Apple A7 is required");
    }
}

- (void)registerInsightSDKWithType:(RegisteType)type
{
    /*
     刚和于晨确认了下，以后提测利用现在正式环境的发布流程，正常提测都放正式环境；
     如果判断需要用到MR-Test的新功能的，作为特例做特别说明，再放在测试环境。这样可以省去内容制作同学一次手动上传的工作。请大家确认。@所有人 @AUG 17 2018
     */
    switch (type) {
        case RegisteTypeInsightOnlineFormal:{ //mr com.netease.InsightSDK
            [[InsightARManager shareManager]registerAppKey:@"AR-8AD8-D9DE436F0B95-i-f" appSecret:@"bNZVomtPUW"];
        }
            break;
            
        case RegisteTypeInsightOnlineTest:{ //mr com.netease.InsightSDK
            [[InsightARManager shareManager]registerAppKey:@"AR-A3C0-D7FD87F86129-i-t" appSecret:@"ypEWCriwYJ"];
        }
            break;
            
        case RegisteTypeInsightTestFormal:{ //mr-test com.netease.LightSDKDemo
            [[InsightARManager shareManager]registerAppKey:@"AR-9AFE-D0221342wr68" appSecret:@"3Xj0dFjMzG"];
        }
            break;
            
        case RegisteTypeInsightTestTest:{ //mr-test com.netease.LightSDKDemo
            [[InsightARManager shareManager]registerAppKey:@"AR-9AFE-D0291342wr78" appSecret:@"2Xj0dFjMzt"];
        }
            break;
            
        case RegisteTypeInsightOffline:{
            NSError *error = [[InsightARManager shareManager] registerAppKey:@"AR0-85271679bc571cfaa6186656c036166fbb082dc97b142581"
                                                                   appSecret:@"e9bf6c73a91ff90c04f4f98fee4c43826e9cbfa3147918e1"]; //LightSDKDemo 测试账号
            if (error) {
                [self showInvalidateKeyWarning:error.localizedDescription];
            }
        }
            break;
            
        case RegisteTypeInsightDianpin:{
            NSError *error = [[InsightARManager shareManager] registerAppKey:@"AR-A4B6-CE9493B9AB7A9-i-f"
                                                                   appSecret:@"RB7qBcf09v"]; //LightSDKDemo 测试账号
            if (error) {
                [self showInvalidateKeyWarning:error.localizedDescription];
            }
        }
            break;
            
        case RegisteTypeInsightAntfans: {
            // PID = 2426
            [[InsightARManager shareManager]registerAppKey:@"AR-KEL6-SXLYHWF6NOCUG-I-T" appSecret:@"X8LHcxK8s0"];
            break;
        }
    }
}

- (void)checkProductsStatus
{
    if ([[InsightARManager shareManager]getAlgorithmModel] == nil) {
        NSLog(@"nil");
    }else{
        NSLog(@"not nil");
    }
    [[InsightARManager shareManager]checkProductStatus:^(NSError *error) {
        
    } result:^(NSArray<ARProduct *> *items) {
        NSLog(@"result is %@",items);
    }];
    [[InsightARManager shareManager]checkAlgorithmModel:^(NSError *error) {
        NSLog(@"error is %@",error);
    } result:^(ARAlgorithmModel *algModel) {
        NSLog(@"algModel is %@",algModel);
    }];
}

- (void)addFabric
{
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    NSUInteger orientations = UIInterfaceOrientationMaskAllButUpsideDown;
    if(self.window.rootViewController){
        UIViewController *presentedViewController = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
        orientations = [presentedViewController supportedInterfaceOrientations];
    }
    return orientations;
}

- (void)showInvalidateKeyWarning:(NSString *)errorString
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"错误" message:errorString preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *commitAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }];
        [alertController addAction:commitAction];
        [self.window.rootViewController presentViewController:alertController animated:YES completion:^{
        }];
    });
}

@end

