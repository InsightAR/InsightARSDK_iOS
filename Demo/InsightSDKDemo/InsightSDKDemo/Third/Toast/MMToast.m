//
//  MMToast.m
//  MMHousehold
//
//  Created by yunyunzhang on 2017/8/2.
//  Copyright © 2017年 Netease. All rights reserved.
//

#import "MMToast.h"
#import <objc/runtime.h>
#import "Utility.h"

#define kDefaultHiddenDelayTime (2.f)

static const NSString * MMToastLabelTapKey        = @"MMToastLabelTapKey";

@implementation MMToast

#pragma mark - Private Methods

+ (void)toastDidTap:(UIGestureRecognizer *)gr {
    
    MBProgressHUD *hud = (MBProgressHUD *)gr.view;
    CGPoint loc = [gr locationInView:hud];
    
    CGFloat w = hud.size.width;
    CGFloat h = hud.size.height;
    CGFloat x = hud.center.x - w/2 + hud.xOffset;
    CGFloat y = hud.center.y - h/2 + hud.yOffset;
    CGRect hudRect = CGRectMake(x, y, w, h);
    
    if (CGRectContainsPoint(hudRect, loc)) {
        void (^tapHandler)(void) = objc_getAssociatedObject(gr.view, &MMToastLabelTapKey);
        if (tapHandler != nil) {
            tapHandler();
        }
        [self hideHUDInView:hud.superview animated:NO];
    }
}

+ (MBProgressHUD *)createHUDForView:(UIView *)view {
    if (view == nil) {
        view = [self getDisplayWindow];
    }
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:view];

    if (!hud) {
        hud = [[MBProgressHUD alloc] initWithView:view];
        [hud setOpacity:0.8];
        [hud setLabelFont:[UIFont systemFontOfSize:15]];
        [hud setLabelColor:[UIColor whiteColor]];
        [hud setUserInteractionEnabled:NO];
        [hud setRemoveFromSuperViewOnHide:YES];
        [view addSubview:hud];
    }
    return hud;
}

+ (void)hideHUDInView:(UIView *)view animated:(BOOL)animated{
    if (view == nil) {
        view = [self getDisplayWindow];
    }
    [MBProgressHUD hideHUDForView:view animated:animated];
}

+ (UIView *)getDisplayWindow {
    UIView *view = [UIApplication sharedApplication].windows.lastObject;
    if (view == nil || view.hidden == YES) {
        view = [UIApplication sharedApplication].keyWindow;
    }
    return view;
}

+ (void)updateProgress:(NSString *)progress
               forView:(UIView *)view
{
    if (view == nil) {
        view = [self getDisplayWindow];
    }
    MBProgressHUD *hud = [MBProgressHUD HUDForView:view];
    if (hud != nil) {
        [hud setLabelText:@""];
        [hud setDetailsLabelText:progress];
    }else{
        [self toastProgressNoIndicator:progress onView:view];
    }
}

#pragma mark - Public Methods

+ (void)toastHideOnView:(UIView *)view
{
    dispatch_main_async_safe(^{
        [self hideHUDInView:view animated:YES];
    });
}


+ (void)toastHorizontalProgress:(NSString *)title onView:(nullable UIView *)view centerOffset:(CGPoint)centerOffset
{
    dispatch_main_async_safe(^{
    [self hideHUDInView:view animated:NO];
    MBProgressHUD *progress = [self createHUDForView:view];
    CGSize size = [title sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    UIView * customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width+size.height+10,size.height)];
    UIActivityIndicatorView *incView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, size.height, size.height)];
    [incView startAnimating];
    incView.hidesWhenStopped = YES;
    [customView addSubview:incView];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(size.height+10, 0, size.width, size.height)];
    label.text = title;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:15];
    [customView addSubview:label];
    [progress setMode:MBProgressHUDModeCustomView];
    [progress setCustomView:customView];
    [progress show:YES];
    [progress setMargin:10.f];
    
    [progress setXOffset:centerOffset.x];
    [progress setYOffset:centerOffset.y];
    });
}

+ (void)toastProgress:(NSString *)title onView:(nullable UIView *)view centerOffset:(CGPoint)centerOffset
{
    dispatch_main_async_safe(^{
        [self hideHUDInView:view animated:NO];
        MBProgressHUD *progress = [self createHUDForView:view];
        if ([progress.superview isKindOfClass:[UIWindow class]]) {
            [progress setUserInteractionEnabled:YES];
        }
        [progress setMode:MBProgressHUDModeIndeterminate];
        [progress setLabelText:title];
        [progress show:YES];
        
        [progress setXOffset:centerOffset.x];
        [progress setYOffset:centerOffset.y];
    });
}

+ (void)toastProgress:(NSString *)title onView:(UIView *)view {
    [self toastProgress:title onView:view centerOffset:CGPointZero];
}

+ (void)toastProgressNoIndicator:(NSString *)title onView:(nullable UIView *)view centerOffset:(CGPoint)centerOffset {
    dispatch_main_async_safe(^{
    [self hideHUDInView:view animated:NO];
    MBProgressHUD *progress = [self createHUDForView:view];
    if ([progress.superview isKindOfClass:[UIWindow class]]) {
        [progress setUserInteractionEnabled:YES];
    }
    [progress setMode:MBProgressHUDModeText];
    [progress setLabelText:title];
    [progress show:YES];
    
    [progress setXOffset:centerOffset.x];
    [progress setYOffset:centerOffset.y];
     });
}

+ (void)toastProgressNoIndicator:(NSString *)title onView:(nullable UIView *)view {
    [self toastProgressNoIndicator:title onView:view centerOffset:CGPointZero];
}

+ (void)toastSuccess:(NSString *)title onView:(UIView *)view afterDelay:(NSTimeInterval)delay {
    dispatch_main_async_safe(^{
    [self hideHUDInView:view animated:NO];
    MBProgressHUD *hud = [self createHUDForView:view];
    [hud setMode:MBProgressHUDModeCustomView];
    [hud setCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tipSuccess"]]];
    [hud setLabelText:title];
    [hud show:YES];
    [hud hide:YES afterDelay:delay];
    });
}

+ (void)toastSuccess:(NSString *)title onView:(UIView *)view {
    [self toastSuccess:title onView:view afterDelay:kDefaultHiddenDelayTime];
}

+ (void)toastError:(NSString *)title onView:(UIView *)view afterDelay:(NSTimeInterval)delay {
    dispatch_main_async_safe(^{
    [self hideHUDInView:view animated:NO];
    MBProgressHUD *hud = [self createHUDForView:view];
    [hud setMode:MBProgressHUDModeCustomView];
    [hud setCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"toastError"]]];
    [hud setLabelText:title];
    [hud show:YES];
    [hud hide:YES afterDelay:delay];
    });
}

+ (void)toastError:(NSString *)title onView:(UIView *)view {
    [self toastError:title onView:view afterDelay:kDefaultHiddenDelayTime];
}

+ (void)toast:(NSString *)title onView:(UIView *)view afterDelay:(NSTimeInterval)delay centerOffset:(CGPoint)centerOffset tapHandler:(void(^)(void))tapHandler
{
    dispatch_main_async_safe(^{
    [self hideHUDInView:view animated:NO];
    MBProgressHUD *hud = [self createHUDForView:view];
    [hud setMode:MBProgressHUDModeText];
    [hud setDetailsLabelFont:[UIFont systemFontOfSize:15]];
    [hud setDetailsLabelText:title];
    [hud setDetailsLabelColor:[UIColor whiteColor]];
//    [hud setLabelText:title];
//    [hud setMargin:10.f];
    [hud show:YES];
    
    [hud setXOffset:centerOffset.x];
    [hud setYOffset:centerOffset.y];
    
    if (tapHandler != nil) {
        UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toastDidTap:)];
        gr.cancelsTouchesInView = NO;
        [hud setUserInteractionEnabled:YES];
        hud.tag = 20170509;
        [hud addGestureRecognizer:gr];
        objc_setAssociatedObject(hud, &MMToastLabelTapKey, tapHandler, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    else {
        [view bringSubviewToFront:hud];
        [hud hide:YES afterDelay:delay];
    }
    });
}


+ (void)toast:(NSString *)title onView:(UIView *)view centerOffset:(CGPoint)centerOffset {
    [self toast:title onView:view afterDelay:kDefaultHiddenDelayTime centerOffset:centerOffset tapHandler:nil];
}

+ (void)toast:(NSString *)title onView:(UIView *)view afterDelay:(NSTimeInterval)delay {
    [self toast:title onView:view afterDelay:delay centerOffset:CGPointZero tapHandler:nil];
}

+ (void)toast:(NSString *)title onView:(UIView *)view {
    [self toast:title onView:view afterDelay:kDefaultHiddenDelayTime centerOffset:CGPointZero tapHandler:nil];
}

@end


@implementation MBProgressHUD (HitArea)

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    if (self.tag == 20170509) {
        CGFloat w = self.size.width;
        CGFloat h = self.size.height;
        CGFloat x = self.center.x - w/2 + self.xOffset;
        CGFloat y = self.center.y - h/2 + self.yOffset;
        CGRect hudRect = CGRectMake(x, y, w, h);
        return CGRectContainsPoint(hudRect, point);
    }
    else {
        return [super pointInside:point withEvent:event];
    }
}
@end
