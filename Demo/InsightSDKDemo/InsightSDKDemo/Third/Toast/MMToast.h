//
//  MMToast.h
//  MMHousehold
//
//  Created by yunyunzhang on 2017/8/2.
//  Copyright © 2017年 Netease. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

NS_ASSUME_NONNULL_BEGIN

/// 创建Toast时若传入的view为nil，则将当前最上层的window作为父视图。
@interface MMToast : NSObject

/// 隐藏提示
+ (void)toastHideOnView:(nullable UIView *)view;

+ (void)updateProgress:(NSString *)progress
               forView:(UIView *)view;

/// 加载中提示，需手动调用隐藏方法。
+ (void)toastProgress:(NSString *)title onView:(nullable UIView *)view;

+ (void)toastHorizontalProgress:(NSString *)title onView:(nullable UIView *)view centerOffset:(CGPoint)centerOffset;

+ (void)toastProgress:(NSString *)title onView:(nullable UIView *)view centerOffset:(CGPoint)centerOffset;

/// 加载中提示，无菊花
+ (void)toastProgressNoIndicator:(NSString *)title onView:(nullable UIView *)view centerOffset:(CGPoint)centerOffset;

+ (void)toastProgressNoIndicator:(NSString *)title onView:(nullable UIView *)view;

/// 成功提示
+ (void)toastSuccess:(NSString *)title onView:(nullable UIView *)view afterDelay:(NSTimeInterval)delay;

+ (void)toastSuccess:(NSString *)title onView:(nullable UIView *)view;

/// 错误提示
+ (void)toastError:(NSString *)title onView:(nullable UIView *)view afterDelay:(NSTimeInterval)delay;

+ (void)toastError:(NSString *)title onView:(nullable UIView *)view;

/// 自定义消失时间、相对于父视图中心偏移量、点击回调。模态。当有点击block时不会消失
+ (void)toast:(NSString *)title
       onView:(nullable UIView *)view
   afterDelay:(NSTimeInterval)delay
 centerOffset:(CGPoint)centerOffset
   tapHandler:(nullable void(^)(void))tapHandler;

/// 自定义相对于父视图中心偏移量
+ (void)toast:(NSString *)title onView:(nullable UIView *)view centerOffset:(CGPoint)centerOffset;

/// 自定义消失时间
+ (void)toast:(NSString *)title onView:(nullable UIView *)view afterDelay:(NSTimeInterval)delay;

/// 默认父视图正中、消失时间为2秒
+ (void)toast:(NSString *)title onView:(nullable UIView *)view;


@end

// 缩小Toast的点击相应为可见区域
@interface MBProgressHUD (HitArea)
@end
NS_ASSUME_NONNULL_END
