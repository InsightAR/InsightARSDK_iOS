local UIBehaviourMoreButton = {};

function UIBehaviourMoreButton:New( game_object )
	if self ~= UIBehaviourMoreButton then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = UIBehaviourMoreButton } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function UIBehaviourMoreButton:Start()

	self.whitePanel = Insight.GameObject.Find("Canvas/WhitePanel");
	self.ButtonsCarControl = Insight.GameObject.Find("Canvas/ButtonsCarControl");

end

function UIBehaviourMoreButton:OnPointerUp()

	self.whitePanel:SetActive( true );
	self.ButtonsCarControl:SetActive( false );


end





return UIBehaviourMoreButton;
