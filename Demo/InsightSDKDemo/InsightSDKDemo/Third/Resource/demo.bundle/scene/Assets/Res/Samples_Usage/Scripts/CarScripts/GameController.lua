
-- 用于实时根据检测状态，更新相关全局变量，并管理各个相关对象的状态

local GameController = {};

-- Local Variables
local onPlaneColor = Insight.Vector4.New( 1.0, 1.0, 1.0, 1.0);
local hangInAirColor = Insight.Vector4.New( 1.0, 0.5, 0.5, 0.6);


-- FW_CarWithUI JSON USE:
g_CarWithUI_JSON = nil;
g_AdList = {};
g_UserId = nil;
g_firstTime = nil;

function GameController:New( game_object )
	if self ~= GameController then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = GameController } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function GameController:Start()

	g_CarWithUI_JSON = dofile( "Assets/Res/Samples_Usage/Scripts/CarScripts/json.lua" ); -- one-time load of the routines
	-- g_CarWithUI_JSON = self.game_object:GetComponent( "Assets/Res/Samples_Usage/Scripts/CarScripts/json.lua" ); -- one-time load of the routines

	self.Cross = Insight.GameObject.Find("Cross");
	self.ButtonPlace = Insight.GameObject.Find("ButtonPlace");
	self.StuffRoot = Insight.GameObject.Find("StuffRoot");
	self.plane = Insight.GameObject.Find("Fw_PlaneTrans");
	self.ButtonsCarControl = Insight.GameObject.Find("ButtonsCarControl");

	self.Cross:SetActive(true);

	self.ButtonPlace:SetActive(false);
	self.StuffRoot:SetActive(false);
	self.plane:SetActive(false);
	self.ButtonsCarControl:SetActive(false);

end

function GameController:Update()


 	----------------------------------------------------------------------
 	-- 根据检测的状态进行各项设置
 	-- 注意：这个检测的状态并非每帧都更新，所以，有时可能会有几帧的滞后

	-- print( "[test AR Detecting Status] Insight.Tracking.status = ".. Insight.Tracking.status); --test


	if Insight.Tracking.status == Fw_ARState_Initing then

 	 	print( "[test AR Detecting Status] AR is Initing ");

		if self.ButtonPlace.activeSelf then
	 		self:EnablePlacingStuff(false);
	 	end

 	 elseif Insight.Tracking.status == Fw_ARState_Init_OK then

 	 	print( "[test AR Detecting Status] AR Init OK ");

 	 	if self.ButtonPlace.activeSelf then
	 		self:EnablePlacingStuff(false);
	 	end

 	elseif Insight.Tracking.status == Fw_ARState_Init_Fail then

 	 	print( "[test AR Detecting Status] AR Init failed ");

		if self.ButtonPlace.activeSelf then
	 		self:EnablePlacingStuff(false);
	 	end

 	 -- 接下来这些检测状态都必须确保是在初始化成功后才能使用，否则如果重新启动页面，很可能前几帧还是用的是上一轮的检测结果

 	elseif Insight.Tracking.status == Fw_ARState_Detecting  then

		-- print( "[test AR Detecting Status] AR is still dectecting ");

		if self.ButtonPlace.activeSelf then
			self:EnablePlacingStuff(false);
		end

 	elseif ( Insight.Tracking.status == Fw_ARState_Detect_OK ) or ( Insight.Tracking.status == Fw_ARState_Tracking )  then

 		-- print( "[test AR Detecting Status] AR is detect_OK or tracking " ); --test

 		self:EnablePlacingStuff(true);

 	elseif  Insight.Tracking.status == Fw_ARState_Track_Limited then

		if self.ButtonPlace.activeSelf then
			self:EnablePlacingStuff(false);
		end

	elseif Insight.Tracking.status == Fw_ARState_Track_Lost then

		-- print( "[test AR Detecting Status] AR reasult is lost ");

		if self.ButtonPlace.activeSelf then
			self:EnablePlacingStuff(false);
		end

	elseif Insight.Tracking.status == Fw_ARState_Track_Fail then

 		-- print( "[test AR Detecting Status] AR tracking failed ");

		if self.ButtonPlace.activeSelf then
	 		self:EnablePlacingStuff(false);
	 	end

	else

		-- print( "[test AR Detecting Status] AR status: " .. Insight.Tracking.status  );

		if self.ButtonPlace.activeSelf then
	 		self:EnablePlacingStuff(false);
	 	end
 	end

 	----------------------------------------------------------------------
 	-- 设置平面的显示

 	-- 当物体还没有被放置时，才显示平面及设置其位置
 	if Fw_IsStuffPlaced == false then

		if Insight.Tracking.quadCount > 0 then


			self.plane:SetActive(true);

			local name = Insight.Tracking.QuadGetName( 0 );
			local center = Insight.Tracking.QuadGetCenter( name );
			local scale = Insight.Tracking.QuadGetScale( name );
			local rotation = Insight.Tracking.QuadGetRotation( name );
			local valid = Insight.Tracking.QuadGetValid( name );

			-- 如果不先确定是visible,那么会将它显示出来，因为目前隐藏是通过Scale = (0,0,0)来实现的
			if self.plane.activeSelf then

				-- Set proper position, scale , rotation for ARPlane
				self.plane.transform.position = center;
				self.plane.transform.scale = Insight.Vector3.New( scale.x, 1, scale.x);  -- 平面修改成强制显示成正方形
				self.plane.transform.rotation = rotation;
			end

		else

			-- 平面丢失了就隐藏该Plane
			if self.plane.activeSelf then
				self.plane:SetActive(false);
			end

		end
	end




end


function GameController:EnablePlacingStuff( enable )

	-- 如果物体没有显示，则显示放置按钮，反之亦然

	if self.StuffRoot.activeSelf == false then

		self.ButtonPlace:SetActive( enable );

	end

end


return GameController;
