

local UIBehaviourCloseButton = {};

function UIBehaviourCloseButton:New( game_object )
	if self ~= UIBehaviourCloseButton then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = UIBehaviourCloseButton } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function UIBehaviourCloseButton:Start()

	-- print( "Lua UIBehaviourCloseButton Start: " .. self.game_object:GetName() );

	self.whitePanel = Insight.GameObject.Find("Canvas/WhitePanel");
	self.ButtonsCarControl = Insight.GameObject.Find("Canvas/ButtonsCarControl");


end

function UIBehaviourCloseButton:OnPointerUp()

	self.whitePanel:SetActive( false );
	self.ButtonsCarControl:SetActive( true );

end

return UIBehaviourCloseButton;
