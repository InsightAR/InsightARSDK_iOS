#include "amplify_standard_head_vs.sh"
#include "amplify_compatible_unity_vs.sh"

uniform highp vec4 _texcoord_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying mediump vec3 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  lowp float tangentSign_1 = 0.0;
  lowp vec3 worldTangent_2 = vec3(0.0);
  lowp vec3 worldNormal_3 = vec3(0.0);
  highp vec4 tmpvar_4 = vec4(0.0);
  highp vec4 tmpvar_5 = vec4(0.0);
  tmpvar_5.w = 1.0;
  tmpvar_5.xyz = _glesVertex.xyz;
  highp vec3 tmpvar_6 = vec3(0.0);
  tmpvar_6 = (unity_ObjectToWorld * _glesVertex).xyz;
  highp mat3 tmpvar_7;
  tmpvar_7[0] = unity_WorldToObject[0].xyz;
  tmpvar_7[1] = unity_WorldToObject[1].xyz;
  tmpvar_7[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_8 = vec3(0.0);
  tmpvar_8 = normalize((_glesNormal * tmpvar_7));
  worldNormal_3 = tmpvar_8;
  highp mat3 tmpvar_9;
  tmpvar_9[0] = unity_ObjectToWorld[0].xyz;
  tmpvar_9[1] = unity_ObjectToWorld[1].xyz;
  tmpvar_9[2] = unity_ObjectToWorld[2].xyz;
  highp vec3 tmpvar_10 = vec3(0.0);
  tmpvar_10 = normalize((tmpvar_9 * _glesTANGENT.xyz));
  worldTangent_2 = tmpvar_10;
  highp float tmpvar_11 = 0.0;
  tmpvar_11 = (_glesTANGENT.w * unity_WorldTransformParams.w);
  tangentSign_1 = tmpvar_11;
  lowp vec3 tmpvar_12 = vec3(0.0);
  tmpvar_12 = (((worldNormal_3.yzx * worldTangent_2.zxy) - (worldNormal_3.zxy * worldTangent_2.yzx)) * tangentSign_1);
  highp vec4 tmpvar_13 = vec4(0.0);
  tmpvar_13.x = worldTangent_2.x;
  tmpvar_13.y = tmpvar_12.x;
  tmpvar_13.z = worldNormal_3.x;
  tmpvar_13.w = tmpvar_6.x;
  highp vec4 tmpvar_14 = vec4(0.0);
  tmpvar_14.x = worldTangent_2.y;
  tmpvar_14.y = tmpvar_12.y;
  tmpvar_14.z = worldNormal_3.y;
  tmpvar_14.w = tmpvar_6.y;
  highp vec4 tmpvar_15 = vec4(0.0);
  tmpvar_15.x = worldTangent_2.z;
  tmpvar_15.y = tmpvar_12.z;
  tmpvar_15.z = worldNormal_3.z;
  tmpvar_15.w = tmpvar_6.z;
  mediump vec3 normal_16 = vec3(0.0);
  normal_16 = worldNormal_3;
  mediump vec3 x1_17 = vec3(0.0);
  mediump vec4 tmpvar_18 = vec4(0.0);
  tmpvar_18 = (normal_16.xyzz * normal_16.yzzx);
  x1_17.x = dot (unity_SHBr, tmpvar_18);
  x1_17.y = dot (unity_SHBg, tmpvar_18);
  x1_17.z = dot (unity_SHBb, tmpvar_18);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_5));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _texcoord_ST.xy) + _texcoord_ST.zw);
  xlv_TEXCOORD1 = tmpvar_13;
  xlv_TEXCOORD2 = tmpvar_14;
  xlv_TEXCOORD3 = tmpvar_15;
  xlv_TEXCOORD4 = (x1_17 + (unity_SHC.xyz * (
    (normal_16.x * normal_16.x)
   - 
    (normal_16.y * normal_16.y)
  )));
  xlv_TEXCOORD6 = tmpvar_4;
}


