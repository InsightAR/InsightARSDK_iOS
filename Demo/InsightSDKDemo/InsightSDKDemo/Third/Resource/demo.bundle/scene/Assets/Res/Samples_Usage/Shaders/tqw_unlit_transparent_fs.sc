#include "amplify_standard_head_fs.sh"
#include "amplify_compatible_unity_fs.sh"

uniform highp vec4 _Tint;
uniform sampler2D _MainTex;
uniform highp vec4 _MainTex_ST;
uniform highp float _Opacity;
varying highp vec2 xlv_TEXCOORD0;
void main ()
{
  lowp vec4 c_1 = vec4(0.0);
  lowp vec3 tmpvar_2 = vec3(0.0);
  lowp float tmpvar_3 = 0.0;
  highp vec4 tex2DNode3_4 = vec4(0.0);
  highp vec2 tmpvar_5 = vec2(0.0);
  tmpvar_5 = ((xlv_TEXCOORD0 * _MainTex_ST.xy) + _MainTex_ST.zw);
  lowp vec4 tmpvar_6 = vec4(0.0);
  tmpvar_6 = texture2D (_MainTex, tmpvar_5);
  tex2DNode3_4 = tmpvar_6;
  tmpvar_2 = (_Tint * tex2DNode3_4).xyz;
  tmpvar_3 = (tex2DNode3_4.w * _Opacity);
  lowp vec4 tmpvar_7 = vec4(0.0);
  tmpvar_7.xyz = vec3(0.0, 0.0, 0.0);
  tmpvar_7.w = tmpvar_3;
  c_1.w = tmpvar_7.w;
  c_1.xyz = tmpvar_2;
  gl_FragData[0] = c_1;
}


