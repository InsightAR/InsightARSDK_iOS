local LogPrintController = {};

globalLogSwitch = false;

function LogPrintController:New( game_object )
	if self ~= LogPrintController then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = LogPrintController } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function LogPrintController:Start()

	self.logCounter = 0;

end

function LogPrintController:Update()

	self.logCounter = self.logCounter + 1;
	if self.logCounter % 5 == 0 then 
		if globalLogSwitch then 
			Fw_PrintToText("KXC Log Test: print log counter" .. self.logCounter);
		end
	end
end

return LogPrintController;
