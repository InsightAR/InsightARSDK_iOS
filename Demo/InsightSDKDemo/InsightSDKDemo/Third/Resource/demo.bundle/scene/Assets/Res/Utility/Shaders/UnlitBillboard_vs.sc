#include "amplify_standard_head_vs.sh"
#include "amplify_compatible_unity_vs.sh"

uniform highp vec4 _texcoord_ST;
varying highp vec2 xlv_TEXCOORD0;
varying mediump vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying lowp vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  highp vec3 shlight_1 = vec3(0.0);
  lowp vec3 worldNormal_2 = vec3(0.0);
  mediump vec3 tmpvar_3 = vec3(0.0);
  lowp vec3 tmpvar_4 = vec3(0.0);
  highp vec4 tmpvar_5 = vec4(0.0);
  highp vec4 tmpvar_6 = vec4(0.0);
  tmpvar_6.w = 1.0;
  tmpvar_6.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_7;
  tmpvar_7[0] = unity_WorldToObject[0].xyz;
  tmpvar_7[1] = unity_WorldToObject[1].xyz;
  tmpvar_7[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_8 = vec3(0.0);
  tmpvar_8 = normalize((_glesNormal * tmpvar_7));
  worldNormal_2 = tmpvar_8;
  tmpvar_3 = worldNormal_2;
  lowp vec4 tmpvar_9 = vec4(0.0);
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = worldNormal_2;
  mediump vec4 normal_10 = vec4(0.0);
  normal_10 = tmpvar_9;
  mediump vec3 res_11 = vec3(0.0);
  mediump vec3 x_12 = vec3(0.0);
  x_12.x = dot (unity_SHAr, normal_10);
  x_12.y = dot (unity_SHAg, normal_10);
  x_12.z = dot (unity_SHAb, normal_10);
  mediump vec3 x1_13 = vec3(0.0);
  mediump vec4 tmpvar_14 = vec4(0.0);
  tmpvar_14 = (normal_10.xyzz * normal_10.yzzx);
  x1_13.x = dot (unity_SHBr, tmpvar_14);
  x1_13.y = dot (unity_SHBg, tmpvar_14);
  x1_13.z = dot (unity_SHBb, tmpvar_14);
  res_11 = (x_12 + (x1_13 + (unity_SHC.xyz * 
    ((normal_10.x * normal_10.x) - (normal_10.y * normal_10.y))
  )));
  mediump vec3 tmpvar_15 = vec3(0.0);
  tmpvar_15 = max (((1.055 * 
    pow (max (res_11, vec3(0.0, 0.0, 0.0)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  res_11 = tmpvar_15;
  shlight_1 = tmpvar_15;
  tmpvar_4 = shlight_1;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_6));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _texcoord_ST.xy) + _texcoord_ST.zw);
  xlv_TEXCOORD1 = tmpvar_3;
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
  xlv_TEXCOORD3 = tmpvar_4;
  xlv_TEXCOORD5 = tmpvar_5;
}


