

local UIBehaviourRunButton = {};

function UIBehaviourRunButton:New( game_object )
	if self ~= UIBehaviourRunButton then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = UIBehaviourRunButton } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function UIBehaviourRunButton:Start()

	self.roadLightOffset = Insight.GameObject.Find( "outer/car_shell" ):GetComponent( "Renderer" ):GetMaterial( 0 );
	self.carFrontWheels = Insight.GameObject.Find("wheel/Front");
	self.carBackWheels  = Insight.GameObject.Find("wheel/Back");

	-- self.AngleAxis = Insight.Quaternion.AngleAxis;

	self.timer = 0.0;
	self.running = false;

	self.opened = false;

	self.disabledSprite = self.game_object:GetComponent( "Button").spriteState.disabledSprite;
	self.highLightedSprite = self.game_object:GetComponent( "Button").spriteState.highLightedSprite;

end

function UIBehaviourRunButton:OnPointerUp()

	self.running = not self.running;

	if self.opened then
		self.opened = false;
		self.game_object:GetComponent( "Image" ).sprite = self.disabledSprite;
	else
		self.opened = true;
		self.game_object:GetComponent( "Image").sprite = self.highLightedSprite;
	end

end

function UIBehaviourRunButton:Update()

	if self.running then
		self.timer = self.timer + Insight.Time.DeltaTime();
		if self.timer > 40.0 then
			self.timer = 0.0;
		end

		-- 流光效果
		local timeOffset = Insight.Vector4.New( self.timer, self.timer, self.timer, self.timer );
		self.roadLightOffset:SetFloat( "_RoadLightOffset" , self.timer );

		-- 车轮转动
		local rotation = Insight.Quaternion.AngleAxis( 400 * self.timer , Insight.Vector3.New( 1 , 0 , 0 ) );
		self.carFrontWheels.transform.localRotation = rotation ;
		self.carBackWheels.transform.localRotation = rotation ;

	end


end



return UIBehaviourRunButton;
