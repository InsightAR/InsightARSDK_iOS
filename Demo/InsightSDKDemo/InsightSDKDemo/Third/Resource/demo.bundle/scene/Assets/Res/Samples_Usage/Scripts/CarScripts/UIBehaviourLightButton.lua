


local UIBehaviourLightButton = {};


function UIBehaviourLightButton:New( game_object )
	if self ~= UIBehaviourLightButton then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = UIBehaviourLightButton } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function UIBehaviourLightButton:Start()

	-- print( "Lua UIBehaviourLightButton Start: " .. self.game_object:GetName() );

	self.light = Insight.GameObject.Find( "BMW_car/BMW_car" );

	--self.Pressed = self.game_object:GetRenderer():FindProperty("_Pressed",0)

	self.opened = false;

	-- -- KXC COMMENT:
	self.spriteLightNormal = self.game_object:GetComponent( "Button").spriteState.disabledSprite;
	self.spriteLightOn = self.game_object:GetComponent( "Button").spriteState.highLightedSprite;
	-- self.spriteLightNormal = self.game_object:GetImage():GetSprite();
	-- self.spriteLightOn = self.game_object:GetButton():GetHighLightedSprite();

end

function UIBehaviourLightButton:OnPointerUp()

	print( "[test input] press button");
	local nt = self.light:GetComponent( "Animator" ).normalizedTime;

	if self.opened then

		--self.Pressed:Set(0.0);
		self.light:GetComponent( "Animator" ):Play( "lightup" , -1 , nt);
		self.light:GetComponent( "Animator" ).speed = -1;

		self.opened = false;

		if self.spriteLightNormal then
			-- self.game_object:GetImage():SetSprite( self.spriteLightNormal );
			self.game_object:GetComponent( "Image").sprite = self.spriteLightNormal;
		end

	else

		--self.Pressed:Set(1.0);

		self.light:GetComponent( "Animator" ):Play( "lightup" , -1 , nt);
		self.light:GetComponent( "Animator" ).speed = 1;

		self.opened = true;

		if self.spriteLightOn then
			-- self.game_object:GetImage():SetSprite( self.spriteLightOn );
			self.game_object:GetComponent( "Image").sprite = self.spriteLightOn;
		end
	end

end





return UIBehaviourLightButton;
