local TouchBegan = 0;
local TouchMoved = 1;
local TouchStationary = 2;
local TouchEnded = 3;
local TouchCanceled = 4;

local TouchControl = {};

function TouchControl:New( game_object )
	if self ~= TouchControl then return nil, "First argument must be self." end
	local new_instance = setmetatable( {} , { __metatable = {}, __index = TouchControl } );
	new_instance.transform = game_object.transform;
	new_instance.game_object = game_object.transform.gameObject;
	
  
	------------------
	-- Members
	-------------------
  
	return new_instance;
end

function TouchControl:Start()
	-- print( "Lua TouchControl Start: " .. self.game_object:GetName() );
	self.camera = Insight.GameObject.Find("Main Camera Auto UI")
	self.localRotation = self.transform.localRotation.eulerAngles;
	self.localScale = self.transform.localScale;
	self.localPosition = Insight.Vector3.New(0,0,0);

end

function TouchControl:Update()
	if Insight.Input.touchCount == 1 then
		local ROTATE_RATIO = 200;
		local viewsize = Insight.Vector2.New( self.camera:GetComponent( "Camera", 0  ).pixelWidth , self.camera:GetComponent( "Camera", 0 ).pixelHeight  );
		local touch0 = Insight.Input.GetTouch(0);
		local d10 = touch0.deltaPosition/ viewsize;
		self.localRotation.y = self.localRotation.y - d10.x * ROTATE_RATIO;
		self.game_object.transform.localRotation = Insight.Quaternion.Euler( self.localRotation ) ;
	elseif Insight.Input.touchCount == 2 then
		local ROTATE_RATIO = 100;
		local SCALE_RATIO = 1;
		local PAN_RATIO = 0.2; --2
		local viewsize = Insight.Vector2.New( self.camera:GetComponent( "Camera", 0  ).pixelWidth , self.camera:GetComponent( "Camera", 0  ).pixelHeight  );
		local touch0 = Insight.Input.GetTouch(0);
		local touch1 = Insight.Input.GetTouch(1);
		local d10 = touch0.deltaPosition/ viewsize;
		local d11 = touch1.deltaPosition/ viewsize;
		local p10 = touch0.position/ viewsize;
		local p11 = touch1.position/ viewsize;
		local p00 = p10 - d10;
		local p01 = p11 - d11;

		-- scale or pan?
		local angle = Insight.Vector2.Dot( d10 , d11 );
		if angle < 0 then
			-- scale
			self.localScale = self.transform.localScale; -- by tqw
			self.localScale = self.localScale * ( (p10 - p11).magnitude / (p00 - p01).magnitude ) * SCALE_RATIO;
			-- print( "[test touch] angle:   " .. tostring(angle) .. "\n");
			-- print( "[test touch] ( (p10 - p11).magnitude / (p00 - p01).magnitude ) * SCALE_RATIO:    " .. tostring(( (p10 - p11).magnitude / (p00 - p01).magnitude ) * SCALE_RATIO)  .. "\n");
			-- print( "[test touch] currentScale:   " .. tostring(self.scale.x) .. " , ".. tostring(self.scale.y) .. " , ".. tostring(self.scale.z) .. "\n");
			self.transform.localScale = self.localScale;
		else
			-- pan

			local added = d10 + d11;
			local forward = self.camera.transform.forward;
			forward = Insight.Vector3.New(forward.x,0,forward.z).normalized;
			local right = self.camera.transform.right;
			right = Insight.Vector3.New(right.x,0,right.z).normalized;

			self.localPosition = self.transform.localPosition; -- by tqw
			self.localPosition = self.localPosition + ( right * added.x + forward * added.y ) * PAN_RATIO;
			self.transform.localPosition = self.localPosition;
		end
	end
end

return TouchControl;
