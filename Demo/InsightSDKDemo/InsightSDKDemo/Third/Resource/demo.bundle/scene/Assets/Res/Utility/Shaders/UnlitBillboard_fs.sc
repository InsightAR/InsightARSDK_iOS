#include "amplify_standard_head_fs.sh"
#include "amplify_compatible_unity_fs.sh"

uniform highp vec4 _Color;
uniform sampler2D _MainTex;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
void main ()
{
  lowp vec4 c_1 = vec4(0.0);
  lowp vec3 tmpvar_2 = vec3(0.0);
  lowp float tmpvar_3 = 0.0;
  highp vec2 tmpvar_4 = vec2(0.0);
  tmpvar_4 = ((xlv_TEXCOORD0 * _MainTex_ST.xy) + _MainTex_ST.zw);
  lowp vec4 tmpvar_5 = vec4(0.0);
  tmpvar_5 = texture2D (_MainTex, tmpvar_4);
  highp vec4 tmpvar_6 = vec4(0.0);
  tmpvar_6 = (_Color * tmpvar_5);
  tmpvar_2 = tmpvar_6.xyz;
  highp float tmpvar_7 = 0.0;
  tmpvar_7 = tmpvar_6.w;
  tmpvar_3 = tmpvar_7;
  lowp vec4 tmpvar_8 = vec4(0.0);
  tmpvar_8.xyz = vec3(0.0, 0.0, 0.0);
  tmpvar_8.w = tmpvar_3;
  c_1.w = tmpvar_8.w;
  c_1.xyz = tmpvar_2;
  gl_FragData[0] = c_1;
}


