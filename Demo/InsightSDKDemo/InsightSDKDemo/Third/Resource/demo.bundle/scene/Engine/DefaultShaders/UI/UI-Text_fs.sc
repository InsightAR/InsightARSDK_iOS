#include "amplify_standard_head_fs.sh"
#include "amplify_compatible_unity_fs.sh"

varying lowp vec4 xlv_COLOR;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;

uniform sampler2D _MainTex;

void main ()
{
  lowp vec4 color = texture2D (_MainTex, xlv_TEXCOORD0) * xlv_COLOR;
  gl_FragData[0] = color;
}

