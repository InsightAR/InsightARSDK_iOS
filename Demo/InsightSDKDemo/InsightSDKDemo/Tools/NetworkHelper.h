//
//  NetworkHelper.h
//  LightSDKDemo
//
//  Created by Dikey on 10/04/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworkReachabilityManager.h"

typedef void(^CompletionBlock)(AFNetworkReachabilityStatus status );

@interface NetworkHelper : NSObject

+ (instancetype)sharedHelper;

//网络状态
- (AFNetworkReachabilityStatus)networkStatus;

//网络可访问状态
- (BOOL)isNetworkAvaliable;

- (void)monitorNetworkStatus:(CompletionBlock)block;

@end
