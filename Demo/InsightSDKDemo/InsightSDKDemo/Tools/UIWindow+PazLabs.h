//
//  UIWindow+PazLabs.h
//  LightSDKDemo
//
//  Created by Dikey on 08/03/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (PazLabs)

- (UIViewController *)visibleViewController;

@end
