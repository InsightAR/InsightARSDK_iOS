//
//  ARTestUserDefault.m
//  LightSDKDemo
//
//  Created by Dikey on 21/12/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "ARTestUserDefault.h"
#import "ARDetectType.h"

static NSString * const kARTestAppkey = @"kARTestAppkey";

@interface ARTestUserDefault()

@property (nonatomic, strong) NSMutableDictionary *appKeysDictionary;

@end

@implementation ARTestUserDefault

+(instancetype)sharedInstance
{
    static ARTestUserDefault *instance ;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[ARTestUserDefault alloc]init];
    });
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _appKeysDictionary = [[[NSUserDefaults standardUserDefaults]dictionaryForKey:kARTestAppkey]mutableCopy];
        if (!_appKeysDictionary) {
            _appKeysDictionary = [NSMutableDictionary new];
        }
    }
    return self;
}

- (void)saveAppKey:(NSString *)appKey
         appSecret:(NSString *)appSecret
{
    if (appKey.length && appSecret.length) {
        [_appKeysDictionary setObject:appSecret forKey:appKey];
        [[NSUserDefaults standardUserDefaults]setObject:_appKeysDictionary forKey:kARTestAppkey];
     }
}

- (NSDictionary *)appKeys
{
    return _appKeysDictionary;
}

- (void)removeAppKey:(NSString *)appKey
{
    if (appKey.length) {
        _appKeysDictionary = [[[NSUserDefaults standardUserDefaults]dictionaryForKey:kARTestAppkey]mutableCopy];
        [_appKeysDictionary removeObjectForKey:appKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[NSUserDefaults standardUserDefaults]setObject:_appKeysDictionary forKey:kARTestAppkey];
    }
}

@end
