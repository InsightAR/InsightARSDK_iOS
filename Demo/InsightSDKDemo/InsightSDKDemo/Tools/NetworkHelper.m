//
//  NetworkHelper.m
//  LightSDKDemo
//
//  Created by Dikey on 10/04/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "NetworkHelper.h"

@interface NetworkHelper()

@property (nonatomic, strong) AFNetworkReachabilityManager *reachabilityManager;
@property (nonatomic, assign) AFNetworkReachabilityStatus reachabilityStatus;
@end

@implementation NetworkHelper

+(instancetype)sharedHelper
{
    static NetworkHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        sharedInstance = [[NetworkHelper alloc]init];
    });
    return sharedInstance;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        [self monitorNetworkStatus];
    }
    return self;
}

-(void)monitorNetworkStatus
{
    _reachabilityManager =  [AFNetworkReachabilityManager sharedManager];
    [_reachabilityManager startMonitoring];
}

-(AFNetworkReachabilityStatus)networkStatus
{
    return _reachabilityManager.networkReachabilityStatus;
}

-(void)monitorNetworkStatus:(CompletionBlock)block
{
    [_reachabilityManager startMonitoring];
    // 设置网络状态变化回调
    [_reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:{
                block(status);
            }
                NSLog(@"断网状态");
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:{
                NSLog(@"切换到cellar");
                block(status);
            }
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:{
                block(status);
            }
                break;
            default:{
                block(status);
            }
                break;
        }
    }];
}

-(BOOL)isNetworkAvaliable
{
    return _reachabilityManager.reachable;
}

@end
