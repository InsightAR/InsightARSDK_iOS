//
//  UIImage+Video.h
//  MMHousehold
//
//  Created by yunyunzhang on 2018/1/9.
//  Copyright © 2018年 Netease. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMedia/CMTime.h>

@interface UIImage (Video)
//获取视频的第一帧
+ (UIImage *)thumbnailImageForVideo:(NSURL *)videoURL atTime:(NSTimeInterval)time;

// 获取当前视频的某一帧
+ (UIImage *)thumbnailImageForVideo:(NSURL *)videoURL withCMTime:(CMTime)time;
@end
