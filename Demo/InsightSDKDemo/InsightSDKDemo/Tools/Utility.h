//
//  Utility.h
//  Unity-iPhone
//
//  Created by Dikey on 6/6/16.
//
//

#ifndef Utility_h
#define Utility_h

#endif /* Utility_h */

#define IOS7_OR_LATER ([[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending)
#define IOS8_OR_LATER ([[[UIDevice currentDevice] systemVersion] compare:@"8.0"] != NSOrderedAscending)
#define IOS9_OR_LATER ([[[UIDevice currentDevice] systemVersion] compare:@"9.0"] != NSOrderedAscending)
#define IOS10_OR_LATER ([[[UIDevice currentDevice] systemVersion] compare:@"10.0"] != NSOrderedAscending)


#define DEVICE_3_5_INCH ([[UIScreen mainScreen] bounds].size.height == 480)
#define DEVICE_4_0_INCH ([[UIScreen mainScreen] bounds].size.height == 568)
#define DEVICE_4_7_INCH ([[UIScreen mainScreen] bounds].size.height == 667)
#define DEVICE_5_5_INCH ([[UIScreen mainScreen] bounds].size.height == 736)

#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)

//#define SCREEN_WIDTH (([UIScreen mainScreen].bounds.size.width>[UIScreen mainScreen].bounds.size.height)?[UIScreen mainScreen].bounds.size.width:[UIScreen mainScreen].bounds.size.height)
//#define SCREEN_HEIGHT (([UIScreen mainScreen].bounds.size.width>[UIScreen mainScreen].bounds.size.height)?[UIScreen mainScreen].bounds.size.height:[UIScreen mainScreen].bounds.size.width)

// ratio (以375的屏宽为基准)
#define ratio_scale (SCREEN_WIDTH/375.f)
#define UIScale(x) ((x)*ratio_scale)

//#ifdef DEBUG
//#define DLog(format, ...) NSLog(format, ## __VA_ARGS__)
//#else
//#define DLog(format, ...)
//#endif

#define weakify(x) \
try{} @finally{} \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Wshadow\"") \
__weak __typeof__(x) __weak_##x##__ = x; \
_Pragma("clang diagnostic pop")

#define strongify(x) \
try{} @finally{} \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Wshadow\"") \
__typeof__(x) x = __weak_##x##__; \
_Pragma("clang diagnostic pop")


#define dispatch_main_async_safe(block)\
if ([NSThread isMainThread]) {\
block();\
} else {\
dispatch_async(dispatch_get_main_queue(), block);\
}

