//
//  ARTestUserDefault.h
//  LightSDKDemo
//
//  Created by Dikey on 21/12/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARTestUserDefault : NSObject

+(instancetype)sharedInstance;

- (NSDictionary *)appKeys;

- (void)saveAppKey:(NSString *)appKey
         appSecret:(NSString *)appSecret;

- (void)removeAppKey:(NSString *)appKey;

@end
