//
//  ARDetectType.h
//  LightSDKDemo
//
//  Created by Dikey on 10/07/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#ifndef ARDetectType_h
#define ARDetectType_h

static NSString* const kIsvMode = @"isvMode";
static NSString* const kShowLua = @"kShowLua";
static NSString* const kUpdateAlgModel = @"kUpdateAlgModel";
static NSString* const kUpdateProductToLatest = @"kUpdateProductToLatest";
static NSString* const kBackToDelete = @"kBackToDelete";
static NSString* const kAlgMinUpdateTimeinterval = @"kAlgMinUpdateTimeinterval";

//DemoForSDK正式环境
typedef NS_ENUM(NSUInteger, ARDetectType) {
    ARDetectTypeLuaTest = 1413,
    ARDetectTypeiOSTest = 720, //QA测试
    ARDetectType2DTrack0 = 150, //QA测试
    ARDetectType2DTrack1 = 133, //场景中放置家具,动态模型下载·
    ARDetectType2DTrack2 = 135, //场景中放置汽车
    ARDetectType2DTrack3 = 134, //扫描地图升起地球
    ARDetectType2DTrack4 = 130, //扫描饮料载入产品信息
    ARDetectType2DTrack5 = 131, //扫描海报播放视频
    ARDetectType2DTrack6 = 1056, //放飞心灯
    ARDetectType2DTrack7 = 1166, //戴森
    ARDetectTypeKaola = 1050, //考拉抓娃娃，
    ARDetectTypeMusicChanyin = 416, //禅音
    ARDetectTypeMusicStars = 415,   //星空
    ARDetectTypeWangsansan = 528,   //王三三
    ARDetectTypeLocal = 90001,      //本地加载->场景中放置汽车
    ARDetectTypeCloudOnline = 90002,//云识别Online demo
    ARDetectTypeCloudLocal = 90003,//云识别Online demo
    ARDetectTypeAddNewAPP = 1000001,
    ARDetectTypeAPITest = 1000002,
    ARDetectTypeOldProductList = 1000003,
    ARDetectTypeQADynamicTest = 699, //扫描海报播放视频
};

static NSString * const ARDetectTypeToString[] = {
    //online Server
    [ARDetectTypeLuaTest] = @"1413 LuaTest",
    [ARDetectTypeKaola] = @"考拉",
    [ARDetectTypeiOSTest] = @"iOS测试",
    [ARDetectType2DTrack0] = @"QA测试",
    [ARDetectType2DTrack1] = @"场景中放置沙发家具,动态模型下载",
    [ARDetectType2DTrack2] = @"场景中放置汽车",
    [ARDetectType2DTrack3] = @"扫描地图升起地球",
    [ARDetectType2DTrack4] = @"扫描饮料载入产品信息",
    [ARDetectType2DTrack5] = @"扫描海报播放视频",
    [ARDetectType2DTrack6] = @"放飞心灯",
    [ARDetectType2DTrack7] = @"戴森",
    [ARDetectTypeWangsansan] = @"王三三" ,
    [ARDetectTypeMusicChanyin] = @"禅音餐厅",
    [ARDetectTypeMusicStars] = @"星空",
    [ARDetectTypeLocal] = @"本地加载->娃娃机",
    [ARDetectTypeCloudOnline] = @"云识别Online demo",
    [ARDetectTypeCloudLocal] = @"CloudLocal",
    [ARDetectTypeAddNewAPP] = @"+模拟App(需列表非空 & bundleID绑定)",
    [ARDetectTypeOldProductList] = @"旧版入口",
    [ARDetectTypeAPITest] = @"动态下载测试(需保证Product已下载)",
    [ARDetectTypeQADynamicTest] = @"动态下载测试",
};

#endif /* ARDetectType_h */
