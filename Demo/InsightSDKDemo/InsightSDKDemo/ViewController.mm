//
//  ViewController.m
//  LightSDKDemo
//
//  Created by Dikey on 23/02/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "ViewController.h"
#import "ARViewController.h"
#import "TableViewCell.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UISwitch *readLocal;

@end

@implementation ViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.layer.contents = (id)[UIImage imageNamed:@"底色"].CGImage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDelegate


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TableViewCell";
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (nil == cell){
        cell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.enterButton.tag = indexPath.row;
    [cell addTargetToButton:self action:@selector(pushAction:)];
    [cell configureCell:indexPath];
    
    return cell;
}

- (void)pushAction:(UIButton *)button
{
    ARViewController *arVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ARViewController"];
    
    NSString *arNumber = @" ";
    ARDetectType type = button.tag;
    
    arNumber = [NSString stringWithFormat:@"%@",@(type)];
    [arVC setArDetectType:type];
    [arVC setReadLocal:_readLocal.on];
    
    CATransition* transition = [CATransition animation];
    transition.duration = kMMAnimationDuration;
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:arVC animated:NO];
    
    NSLog(@"button .tag is %ld",(long)button.tag);
}

@end

