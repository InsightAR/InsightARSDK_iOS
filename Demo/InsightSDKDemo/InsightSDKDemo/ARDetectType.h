//
//  ARDetectType.h
//  LightSDKDemo
//
//  Created by Dikey on 10/07/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#ifndef ARDetectType_h
#define ARDetectType_h

static NSString* const kUpdateAlgModel = @"kUpdateAlgModel";
static NSString* const kUpdateProductToLatest = @"kUpdateProductToLatest";
static NSString* const kBackToDelete = @"kBackToDelete";
static NSString* const kAlgMinUpdateTimeinterval = @"kAlgMinUpdateTimeinterval";

//DemoForSDK正式环境
typedef NS_ENUM(NSUInteger, ARDetectType) {
    ARDetectType2DTrack1 = 1180,  //汽车展示
    ARDetectType2DTrack2 = 1181,  //家居展示
    ARDetectType2DTrack3 = 1182, //考拉抓娃娃机
    ARDetectType2DTrack4 = 1185,  //扫描饮料，展示产品信息
    ARDetectType2DTrack5 = 1186,  //扫描地图，化身地球
    ARDetectType2DTrack6 = 1187,  //扫描海报，播放视频
    ARDetectType2DTrack7 = 1183, //禅音心灯祈福
    ARDetectType2DTrack8 = 1184, //Dyson产品展示
    
    ARDetectType2DTrack9 = 720, //Dyson产品展示
    ARDetectType2DTrack10 = 1719, //Dyson产品展示
    ARDetectType2DTrack11 = 1754, //Dyson产品展示

    ARDetectTypeLocal = 90001,      //本地加载->汽车展示
    ARDetectTypeCloudOnline = 90002,//云识别Online demo
    ARDetectTypeCloudLocal = 90003, //云识别Local demo
    ARDetectTypeAddNewAPP = 1000001,
    ARDetectTypeAPITest = 1000002
};

static NSString * const ARDetectTypeToString[] = {
    //online Server
    [ARDetectType2DTrack1] = @"汽车展示",
    [ARDetectType2DTrack2] = @"家居展示",
    [ARDetectType2DTrack3] = @"考拉抓娃娃机",
    [ARDetectType2DTrack4] = @"扫描饮料，展示产品信息",
    [ARDetectType2DTrack5] = @"扫描地图，化身地球",
    [ARDetectType2DTrack6] = @"扫描海报，播放视频",
    [ARDetectType2DTrack7] = @"禅音心灯祈福",
    [ARDetectType2DTrack8] = @"Dyson产品展示",
    [ARDetectType2DTrack9] = @"Dyson产品展示",
    [ARDetectType2DTrack10] = @"Dyson产品展示",
    [ARDetectType2DTrack11] = @"Dyson产品展示",
    [ARDetectTypeCloudLocal] = @"云识别本地",
    [ARDetectTypeLocal] = @"本地加载->戴森",
    [ARDetectTypeCloudOnline] = @"云识别",
    [ARDetectTypeAddNewAPP] = @"+模拟App(需列表非空 & bundleID绑定)",
    [ARDetectTypeAPITest] = @"动态下载测试(需保证Product已下载)",
};

#endif /* ARDetectType_h */
