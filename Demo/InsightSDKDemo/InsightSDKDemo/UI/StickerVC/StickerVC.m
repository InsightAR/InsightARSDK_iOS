//
//  StickerVC.m
//  LightSDKDemo
//
//  Created by Carmine on 2018/6/21.
//  Copyright © 2018年 DikeyKing. All rights reserved.
//

#import "StickerVC.h"
#import "MMToast.h"

@interface StickerVC ()

@end

@implementation StickerVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Override Actions

//如果需要在拍照之后做一些动画之类；覆盖方法，调用[super snapStillImage];
//- (void)snapStillImage {
//    [super snapStillImage];
//    [MMToast toast:@"拍照" onView:self.view afterDelay:1];
//}

- (void)openAlbum {
    [MMToast toast:@"相册" onView:self.view afterDelay:1];
}

//- (void)startRecording {
//    [MMToast toast:@"开始录像" onView:self.view afterDelay:1];
//}

- (void)switchCameraMode {
    [MMToast toast:@"切换模式" onView:self.view afterDelay:1];
}

@end
