//
//  MainTableViewCell.h
//  LightSDKDemo
//
//  Created by Dikey on 27/03/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;

- (void)configureCellWithImage:(NSString *)imageName
                     highlight:(NSString *)hightligtName;

@end
