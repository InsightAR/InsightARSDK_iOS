//
//  MainTableViewCell.m
//  LightSDKDemo
//
//  Created by Dikey on 27/03/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "MainTableViewCell.h"
#import "Utility.h"

@interface MainTableViewCell()

@property (nonatomic, copy) NSString *hightLightImageName;
@property (nonatomic, copy) NSString *imageName;

@end

@implementation MainTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.frame = CGRectMake(0, 0, UIScale(355),  UIScale(190));
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    _titleImageView.image = [UIImage imageNamed:_imageName];
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    if (highlighted) {
        _titleImageView.image = [UIImage imageNamed:_hightLightImageName];
    }else{
        _titleImageView.image = [UIImage imageNamed:_imageName];
    }
}

- (void)configureCellWithImage:(NSString *)imageName
                     highlight:(NSString *)hightligtName
{
    _imageName = imageName;
    _hightLightImageName = hightligtName;
    _titleImageView.image = [UIImage imageNamed:_imageName];
}

@end
