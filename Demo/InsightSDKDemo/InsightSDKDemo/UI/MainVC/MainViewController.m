//
//  MainViewController.m
//  LightSDKDemo
//
//  Created by Dikey on 27/03/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "MainViewController.h"
#import "MainTableView.h"
#import "MainTableViewCell.h"
#import "Utility.h"
#import "ARSettingViewController.h"
#import "ARListVC.h"
#import "ARViewController.h"
#import <InsightSDK/InsightSDK.h>
#import "ARDetectType.h"
#import "MMToast.h"
#import "StickerVC.h"
#import "MMArPreviewVC.h"

@interface MainViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *cellImages;
@property (nonatomic, strong) NSMutableArray *cellHightlights;
@property (nonatomic, assign) BOOL isvMode;
@property (weak, nonatomic) IBOutlet MainTableView *mainTableView;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addRightBarButton];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self configureDataSource];
    if (_isvMode != [[NSUserDefaults standardUserDefaults]boolForKey:kIsvMode]) {
        _isvMode = [[NSUserDefaults standardUserDefaults]boolForKey:kIsvMode];
        [_mainTableView reloadData];
    }
}

- (void)configureDataSource
{
    _cellImages = [@[@"扫一扫ic_nor",@"贴纸ic_nor",@"list_ic_nor"] mutableCopy];
    _cellHightlights = [@[@"扫一扫ic_pre",@"贴纸ic_pre",@"list_ic_pre"] mutableCopy];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:kIsvMode]) {
        [_cellImages insertObject:@"扫一扫ic_pre" atIndex:0];
        [_cellHightlights insertObject:@"扫一扫ic_pre" atIndex:0];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)addRightBarButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 32, 32);
    [button setImage:[UIImage imageNamed:@"设置"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(openSettingAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButton=[[UIBarButtonItem alloc] init];
    [barButton setCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
}

#pragma mark - Actions

- (void)openSettingAction
{
    ARSettingViewController *settingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ARSettingViewController"];
    [self.navigationController pushViewController:settingVC animated:YES];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger indexPathRow = indexPath.row;
    if (_isvMode) {
        indexPathRow -= 1;
    }
    switch (indexPathRow) {
        case -1:{
            [self showLocalARViewController];
        }
            break;
            
        case 0:{
            [self showCloudARViewController];
        }
            break;
          
        case 1:{
            StickerVC * stickerVC = [[StickerVC alloc]init];
            NSString * savePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
            stickerVC.recordSavePath = savePath;
            stickerVC.recordSaveName = @"InsightSticker.mp4";
            
            __weak typeof(stickerVC) weakStickerVC = stickerVC;
            stickerVC.arRecordResultCallback = ^(InsightRecordResult result, NSString * videoPath) {
                switch (result) {
                    case InsightRecordResultSuccess: {
                        [MMToast toast:@"成功" onView:nil];
                        MMArPreviewVC * vc = [[MMArPreviewVC alloc] initWithDataPath:videoPath previewType:MMArPreviewTypeVideo];
                        [weakStickerVC presentViewController:vc animated:NO completion:nil];
                    }
                        break;
                        
                    case InsightRecordResultInitFail:
                        [MMToast toastError:@"初始化失败，请重试" onView:nil];
                        break;

                    case InsightRecordResultSaveError:
                        [MMToast toastError:@"录屏失败，请重试" onView:nil];
                        break;
                        
                    default:
                        break;
                }
            };
            [self presentViewController:stickerVC animated:YES completion:nil];
        }
            break;
           
        case 2:{
            ARListVC *listVc = [ARListVC new];
            listVc.view.frame = self.view.bounds;
            [self.navigationController pushViewController:listVc animated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _cellImages.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UIScale(190.0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainTableViewCell *cell ;
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"MainTableViewCell" forIndexPath:indexPath];
    }
    [cell configureCellWithImage:_cellImages[indexPath.row] highlight:_cellHightlights[indexPath.row]];
     
    return cell;
}

#pragma mark - showCloudARViewController

- (void)showCloudARViewController
{
    ARProduct *product = [ARProduct new];
    product.pid = [[NSNumber numberWithInt:ARDetectTypeCloudOnline] stringValue];
    product.name = ARDetectTypeToString[[product.pid integerValue]];
    ARViewController *arVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ARViewController"];
    [arVC setProduct:product];
    [arVC setModalPresentationStyle:UIModalPresentationCustom];
    [arVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:arVC animated:YES completion:^{
    }];
}

#pragma mark - showLocalARViewController

- (void)showLocalARViewController
{
    ARProduct *product = [ARProduct new];
    product.pid = [[NSNumber numberWithInt:ARDetectTypeLocal] stringValue];
    product.name = ARDetectTypeToString[[product.pid integerValue]];
    ARViewController *arVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ARViewController"];
    [arVC setProduct:product];
    [arVC setModalPresentationStyle:UIModalPresentationCustom];
    [arVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:arVC animated:YES completion:^{
    }];
}

@end
