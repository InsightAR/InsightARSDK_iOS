//
//  UserARScenesVC.h
//  LightSDKDemo
//
//  Created by Dikey on 20/12/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ARProduct;

@interface UserARScenesVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray<ARProduct *> *userDefinedARScenes;
@property (nonatomic, copy) NSString *appKey;
@property (nonatomic, copy) NSString *appSecret;
@property (weak, nonatomic) IBOutlet UILabel *appKeyLabel;
@property (weak, nonatomic) IBOutlet UILabel *appSecretLabel;
@property (weak, nonatomic) IBOutlet UILabel *bundleIDLabel;

@end
