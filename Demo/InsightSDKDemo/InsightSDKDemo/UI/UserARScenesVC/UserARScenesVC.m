//
//  UserARScenesVC.m
//  LightSDKDemo
//
//  Created by Dikey on 20/12/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "UserARScenesVC.h"
#import "TableViewCell.h"
#import "ARViewController.h"
#import "ARTestUserDefault.h"
#import <InsightSDK/InsightSDK.h>

@interface UserARScenesVC ()<UITableViewDelegate,UITableViewDataSource,TableViewCellDelegate>

@end

@implementation UserARScenesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.layer.contents = (id)[UIImage imageNamed:@"底色"].CGImage;
    if (!_userDefinedARScenes) {
        _userDefinedARScenes = [NSMutableArray new];
    }
    _appKeyLabel.text = self.appKey;
    _appSecretLabel.text = self.appSecret;
    _bundleIDLabel.text = [[NSBundle mainBundle]bundleIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _userDefinedARScenes.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TableViewCellDemoApp";
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (nil == cell){
        cell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.delegate = self;
    [cell configureCellWithProduct:_userDefinedARScenes[indexPath.row]];
    return cell;
}

- (void)didClickedButtonOfCell:(TableViewCell *)cell
{
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    [self userPushAction:_userDefinedARScenes[indexPath.row]];
}

- (void)userPushAction:(ARProduct *)product
{
    [[InsightARManager shareManager]registerAppKey:_appKey appSecret:[[ARTestUserDefault sharedInstance].appKeys objectForKey:_appKey]];
    
    ARViewController *arVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ARViewController"];
    [arVC setProduct:product];
    [arVC setModalPresentationStyle:UIModalPresentationCustom];
    [arVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:arVC animated:YES completion:^{
    }];
}

- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)deleteAppAction:(UIButton *)sender {
    [self deleteApp];
}

- (void)deleteApp
{
    UIAlertController *deleteAppAC = [UIAlertController alertControllerWithTitle:@"是否确认删除" message:@" 删除当前AppDemo（但是并不会删除已经下载的数据）" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"取消");
    }];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        if (_appKey.length) {
            [[ARTestUserDefault sharedInstance]removeAppKey:_appKey];
            [self.navigationController popViewControllerAnimated:YES];
            //clear App Data?
        }
    }];
    [deleteAppAC addAction:cancelAction];
    [deleteAppAC addAction:deleteAction];
    [self presentViewController:deleteAppAC animated:YES completion:^{
    }];
}

@end
