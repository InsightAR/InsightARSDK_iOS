//
//  ARViewController+UIhandler.h
//  LightSDKDemo
//
//  Created by Dikey on 10/04/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "ARViewController.h"
#import <InsightSDK/InsightSDK.h>

@interface ARViewController (UIhandler)<InsightARDelegate>

- (void)startWithCloudProductID:(NSString *)productID;
- (void)showCloudDownloadAlertControllerWithPid:(NSString *)productID;
- (void)setARLabelText:(NSString *)text;
- (void)handleProgress:(NSProgress *)downloadProgress;
- (void)handleProgress:(NSProgress *)downloadProgress
                 cloud:(BOOL)cloud;

#pragma mark - Animations
- (void)startScanAnimation;
- (void)stopScanAnimation;

- (void)saveScreenShot;
- (void)runStillImageCaptureAnimation;
- (void)snapshotScreenInView:(UIView *)contentView;

#pragma mark - view Change
- (void)startViewConfigrations;
- (void)setDetectImageViewHidden:(BOOL)hide;
- (void)setFunBtnHiddenHidden:(BOOL)hide;

@end
