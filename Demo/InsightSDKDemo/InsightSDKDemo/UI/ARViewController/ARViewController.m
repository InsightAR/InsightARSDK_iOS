//
//  ARViewController.m
//  LightSDKDemo
//
//  Created by Dikey on 27/03/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "ARViewController.h"
#import "UIButton+EnlargeArea.h"
#import "INSIntroView.h"
#import "Utility.h"
#import "MMScanningView.h"
#import "ARDetectType.h"
#import "NetworkHelper.h"
#import "ARViewController+UIhandler.h"

float const kMMAnimationDuration = 0.33;

@interface ARViewController ()<UINavigationControllerDelegate,INSIntroViewDelegate>

@end

@implementation ARViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self observeGlobalNotifications];
    [self startAR];
    [self interfaceOrientation:_product.direction];
}

- (void)dealloc
{
    [self unobserveGlobalNotifications];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self startViewConfigrations];
    [self enlargeButtonEdge];
    [self checkDownloadStatusApiDemo];
}

- (void)enlargeButtonEdge
{
    float enlargeEdge = 6.0;
    [self.funBtn1 setEnlargeEdge:enlargeEdge];
    [self.funBtn2 setEnlargeEdge:enlargeEdge];
    [self.funBtn3 setEnlargeEdge:enlargeEdge];
    [self.infoButton setEnlargeEdge:enlargeEdge];
    [self.backButton setEnlargeEdge:enlargeEdge];
}

- (void)checkDownloadStatusApiDemo
{
    [[InsightARManager shareManager]checkProductStatus:^(NSError *error) {
        NSLog(@"error is %@",error.localizedDescription);
    } result:^(NSArray <ARProduct *>* status) {
        for (ARProduct *productStuats in status) {
            NSLog(@"productStuats is %@",productStuats.description);
        }
    }];
    [self checkDownloadStatus:@"133"];
}

- (void)observeGlobalNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onApplicationWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onApplicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)unobserveGlobalNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)onApplicationWillResignActive
{
    [self stopScanAnimation];
}

- (void)onApplicationDidBecomeActive
{
    [self startScanAnimation];
    _arStatus.hidden = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self checkProductSize];
    [self checkLastUpdateTime];
    [self.view insertSubview:self.introView belowSubview:self.backButton];
    [self.view insertSubview:self.scanningView belowSubview:self.self.detectImageView];
    [self.scanningView updateConstraints];
    self.introView.alpha = .0;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - Orientation

-(BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if (_product.direction != UIInterfaceOrientationUnknown ) {
        switch (_product.direction) {
            case UIInterfaceOrientationLandscapeRight:
                return UIInterfaceOrientationMaskLandscapeRight;
                break;
                
            case UIInterfaceOrientationPortrait:
                return UIInterfaceOrientationMaskPortrait;
                break;
                
            default:
                return UIInterfaceOrientationMaskLandscapeRight;
                break;
        }
    }else
        return UIInterfaceOrientationMaskLandscapeRight;
}

/**
 Private API
 */
- (void)interfaceOrientation:(UIInterfaceOrientation)orientation
{
    if (orientation != UIInterfaceOrientationUnknown) {
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
            SEL selector  = NSSelectorFromString(@"setOrientation:");
            NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
            [invocation setSelector:selector];
            [invocation setTarget:[UIDevice currentDevice]];
            int val = orientation;
            // 从2开始是因为0 1 两个参数已经被selector和target占用
            [invocation setArgument:&val atIndex:2];
            [invocation invoke];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (INSIntroView *)introView
{
    if (self.introImages.count && _introView == nil) {
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"INSIntroView"
                                                          owner:self
                                                        options:nil];
        _introView = (INSIntroView*)[nibViews objectAtIndex:0];
        _introView.delegate = self;
        float width = SCREEN_WIDTH;
        float height = SCREEN_HEIGHT;
        _introView.frame = CGRectMake(0.0, 0.0, width, height);
        _introView.center = self.view.center;
        [_introView setNeedsUpdateConstraints];
        [_introView setNeedsLayout];
        [_introView layoutIfNeeded];
        [_introView addImages:self.introImages];
    }
    return _introView;
}

- (void)checkDownloadStatus:(NSString *)pid
{
    [[InsightARManager shareManager]checkDownloadStatus:pid error:^(NSError *error) {
        NSLog(@"error is %@",error.localizedDescription);
    } result:^(BOOL result) {
        NSLog(@"result is %d",result);
    }];
}

- (void)checkProductSize
{
    // use checkProductStatus
}

- (void)checkLastUpdateTime
{
    // use checkProductStatus
}

- (void)cloudARStartLocally
{
    NSBundle *bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"InsightResource" ofType:@"bundle"]];
    NSString *algModelFolder = [bundle.resourcePath stringByAppendingPathComponent:@"AlgModelPath"];
    ARAlgorithmModel *arcloudProduct = [[ARAlgorithmModel alloc]initWithAlgModelPath:algModelFolder];
    [[InsightARManager shareManager] startInsightARWithCloud:arcloudProduct withARDelegate:self];
}

/** v1.3.1 or later */

- (void)cloudARStart
{
    ARAlgPrepareOption *algOptions = [ARAlgPrepareOption new];
    algOptions.updateTimeInterval = [[NSUserDefaults standardUserDefaults]integerForKey:kAlgMinUpdateTimeinterval];
    algOptions.getLastestVersion = [[NSUserDefaults standardUserDefaults]boolForKey:kUpdateAlgModel];
    
    [[InsightARManager shareManager] prepareAlgorithmModel:algOptions downloadProgress:^(NSProgress *downloadProgress) {
        NSLog(@"AlgProgress is %@",downloadProgress);
    } completion:^(NSError *error) {
        if (error) {
            _loadingLabel.text = error.description;
            NSLog(@"error is %@",error);
        }else{
            _loadingLabel.hidden = YES;
            NSLog(@"prepareAR success");
            ARAlgorithmModel *arcloudProduct =  [[InsightARManager shareManager] getAlgorithmModel];
            //            arcloudProduct.insightButtonOption.buttonYPosition = 800.0;
            //            arcloudProduct.insightButtonOption.customSetting = YES;
            //            arcloudProduct.insightButtonOption.buttonTextColor = [UIColor blueColor];
            //            arcloudProduct.insightButtonOption.buttonImage = [UIImage imageNamed:@"首页_logo"];
            [[InsightARManager shareManager] startInsightARWithCloud:arcloudProduct withARDelegate:self];
        }
    }];
}

/** v1.3 or later */
- (void)startAR
{
    CFAbsoluteTime startTime = CFAbsoluteTimeGetCurrent();
    NSLog(@"completion start Time %@" ,@(startTime));
    
    ARPrepareOptions *options = [ARPrepareOptions new];
    
    if([_product.pid integerValue] == ARDetectTypeLocal){
        NSString *modelPath =[NSString stringWithFormat:@"%@", [[NSBundle mainBundle] pathForResource:@"demo" ofType:@"bundle"]];
        ARProduct *product = [ARProduct new];
        ARStartOption *startOption = [ARStartOption new];
        startOption.useLocalFiles = YES;
        startOption.modelsPaths = @[modelPath];
        product.startOption = startOption;
        [self interfaceOrientation:product.direction];
        [[InsightARManager shareManager] startInsightAR:product withARDelegate:self];
        return;
    }else if([_product.pid integerValue] == ARDetectTypeCloudOnline){
        [self cloudARStart];
        return;
    }else if([_product.pid integerValue] == ARDetectTypeCloudLocal){
        [self cloudARStartLocally];
        return;
    }else{
        //动态下载模式
        options.dynamicDownloadMode = ([_product.pid integerValue] == ARDetectType2DTrack1);
        options.productID = _product.pid;
        options.protocolClasses = nil; //设置代理服务器
    }
    
    if ([_product.pid integerValue] == ARDetectTypeiOSTest) {
    }
    
    [[InsightARManager shareManager] prepareARWithOptions:options downloadProgress:^(NSProgress *downloadProgress) {
        [self handleProgress:downloadProgress cloud:NO];
    } completion:^(NSError *error) {
        _progress.hidden = YES;
        _detectImageView.hidden = NO;
        if (error != nil) {
            _loadingLabel.hidden = NO;
            _loadingLabel.text = error.localizedDescription;
            NSLog(@"prepareAR error: %@",error);
            [self setARLabelText: error.description];
        }else{
            _loadingLabel.hidden = YES;
            NSLog(@"prepareAR success");
            ARProduct *product = [[InsightARManager shareManager]getARProduct:options.productID];
            if (product.shouldUpdate) {
                NSLog(@"资源需要更新 %@",product);
            }
            if (product.downloaded) {
                [self interfaceOrientation:product.direction];
                [[InsightARManager shareManager] startInsightAR:product withARDelegate:self];
            }else{
                NSLog(@"资源未被下载或者已经损坏");
            }
        }
    }];
}

#pragma mark -- InsightARDelegate

- (void)insightARDidUpdateARState:(InsightARState)state
{
    if (_arView == nil) {
        _arStatus.hidden = NO;
        _arView = [[InsightARManager shareManager] getInsightARView];
        [self.view insertSubview:_arView atIndex:0];
        
        if (@available(iOS 11.0, *)) {                _arView.translatesAutoresizingMaskIntoConstraints = NO;
                UILayoutGuide * guide = self.view.safeAreaLayoutGuide;
                [_arView.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
                [_arView.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
                [_arView.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
                [_arView.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
                [self.view layoutIfNeeded];
        }
    }
    
    switch (state) {
        case InsightARStateInitOK:{
            [self setARLabelText:@"初始化成功"];
            [self setFunBtnHiddenHidden:YES];
            _detectImageView.hidden = NO;
            _arStatus.hidden = NO;
            if (_arView == nil) {
                _arView = [[InsightARManager shareManager] getInsightARView];
                [self.view insertSubview:_arView atIndex:0];
            }
        }
            break;
            
        case InsightARStateInitFail:{
            [self setARLabelText:@"初始化失败"];
            [self setFunBtnHiddenHidden:YES];
            _detectImageView.hidden = NO;
            _arStatus.hidden = NO;
        }
            break;
            
        case InsightARStateDetecting:{
            [self setARLabelText:@"正在检测"];
            [self setFunBtnHiddenHidden:YES];
            _detectImageView.hidden = NO;
            _arStatus.hidden = NO;
        }
            break;
            
        case InsightARStateTrackLimited:{
            //[self setFunBtnHiddenHidden:NO];
            _detectImageView.hidden = YES;
            _arStatus.hidden = NO;
            self.infoButton.hidden = YES;
        }
            break;
            
        case InsightARStateTrackingNormal:{
            //[self setFunBtnHiddenHidden:NO];
            _detectImageView.hidden = YES;
            _arStatus.hidden = NO;
            self.infoButton.hidden = YES;
            _loadingLabel.hidden = YES;
            _loadingLabel.text = @" ";
        }
            break;
            
        case InsightARStateTrackLost:{
            [self setARLabelText:@"lost"];
            [self setFunBtnHiddenHidden:YES];
            _detectImageView.hidden = NO;
            _arStatus.hidden = NO;
        }
            break;
            
        default:
            break;
    }
}

-(int)getRandomNumber:(int)from to:(int)to
{
    return (int)(from + (arc4random() % (to - from + 1)));
}

- (void)insightAREvent:(InsightAREvent *)event
{
    NSInteger type = event.type;
    if(type == InsightAREvent_Function_RunScript)
    {
        NSString *scriptName =  [event.params objectForKey:@"script"];
        NSString *runScriptName =  [event.params objectForKey:@"identity"];
        
        if ([scriptName isEqualToString:@"LoadModel"] && [runScriptName isEqualToString:@"g_LoadLocalModel"]) {
            // Important: skuName不应重复
            NSString *skuName = [[event.params objectForKey:@"json"] objectForKey:@"skuId"];
            
//            skuName = @"scene1";
            // 内容脚本中指定的名字可能对不上
            
            // To be impl: 实际中应该由skuName（不会重复）去寻找相应的skuPath，这边是直接设置了。
            NSString *skuPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingString:@"/dynamic.bundle/scene/"];
            
            // 最终确保在skuPath下，存在名skuName的.res文件
            NSString *scriptParam = [NSString stringWithFormat:@"{\"path\":\"%@\",\"name\":\"%@\"}", skuPath, skuName];
            [[InsightARManager shareManager] runInsightARScript:runScriptName param:scriptParam callback:nil];
        }
        
        if ([scriptName isEqualToString:@"g_ShowText"])
        {
            [[InsightARManager shareManager] runInsightARScript:@"g_ShowText" param:@"今天吃鸡大吉大利" callback:nil];
        }
        else if ([scriptName isEqualToString:@"g_ShowModelInfo"])
        {
            NSLog(@"IAREvent_Function_RunScript g_ShowModelInfo %@", [event.params objectForKey:@"json"]);
        }
        else if ([scriptName isEqualToString:@"g_SetUserInfo"])
        {
            NSLog(@"IAREvent_Function_RunScript g_SetUserInfo %@", [event.params objectForKey:@"json"]);
            
            // Test Send user info
            NSString* jsonString = @"{\
            \"userId\":\"a123\", \
            \"coin\":\"5\", \
            \"firstTime\":\"0\", \
            \"probability\":\"0.65\", \
            \"showMenu\":\"1\", \
            \"dollList\":[ \
            {\
            \"dollType\":\"a\",\
            \"dollCount\":\"5\"\
            },\
            {\
            \"dollType\":\"b\",\
            \"dollCount\":\"2\"\
            }\
            ],\
            \"bulletList\":[\
            {\
            \"content\":\"吐**司中了5元优惠券\"\
            },\
            {\
            \"content\":\"吐**司中了美国考拉啦啦啦啦这串文字很长测试看长度怎么样\"\
            },\
            {\
            \"content\":\"吐**司中了法国考拉\"\
            },\
            {\
            \"content\":\"悟**空中了3元优惠券\"\
            }\
            ],\
            \"adList\":[\
            {\
            \"content\":\" 百雀羚满200减20\"\
            },\
            {\
            \"content\":\"风油精满100减30 \"\
            }\
            ]\
            }";
            [[InsightARManager shareManager] runInsightARScript:scriptName param:jsonString callback:^(BOOL result) {
                NSLog(@"g_SetUserInfo result %d", result);
            }];
        }
        else if ([scriptName isEqualToString:@"g_SaveUserInfo"])
        {
            NSLog(@"IAREvent_Function_RunScript g_SaveUserInfo %@", [event.params objectForKey:@"json"]);
        }
        else if ( [scriptName isEqualToString:@"g_UpdateUserInfo"] )
        {
            NSString* jsonStr = @"{\"coin\":\"4\",\"showMenu\":\"1\" ,\"probability\":\"1.0\",\"situation\":\"1\"}";
            [[InsightARManager shareManager] runInsightARScript:@"g_UpdateUserInfo" param:jsonStr callback:nil];
        }
    }
    else if(type == 101)
    {
        NSLog(@"IAREvent_Function_CloseARProduct");
    }
    else if(type == 102)
    {
        NSString *productID  = [event.params objectForKey:@"name"];
        [self startWithCloudProductID:productID];
    }
    else if(type == InsightAREvent_Function_OpenURL)
    {
        NSString *url = [event.params objectForKey:@"url"];
        NSString *viewType = [event.params objectForKey:@"type"];
        NSLog(@"IAREvent_Function_OpenURL : %@ %@", url, viewType);
    }
    else if(type == InsightAREvent_Function_ScreenShot)
    {
        UIImage *screen = [event.params objectForKey:@"image"];
        NSLog(@"IAREvent_Function_ScreenShot");
        
        UIImageWriteToSavedPhotosAlbum(screen, nil, nil, nil);
        
    }
    else if(type == InsightAREvent_Function_Share)
    {
        NSString *shareType = [event.params objectForKey:@"type"];
        NSString *title = [event.params objectForKey:@"title"];
        UIImage *screen = [event.params objectForKey:@"image"];
        NSString *description = [event.params objectForKey:@"description"];
        NSString *url = [event.params objectForKey:@"url"];
        NSLog(@"IAREvent_Function_Share %@ %@ %@ %@", shareType, title, description, url);
        UIImageWriteToSavedPhotosAlbum(screen, nil, nil, nil);
    }
    else if(type == 103)
    {
        NSLog(@"IAREvent_Function_PlayMusic : %@", [event.params objectForKey:@"name"]);
    }
    else if(type == 104)
    {
        NSLog(@"IAREvent_Function_PauseMusic : %@", [event.params objectForKey:@"name"]);
    }
    else if(type == 105)
    {
        NSLog(@"IAREvent_Function_StopMusic : %@", [event.params objectForKey:@"name"]);
    }
    else if(type == 106)
    {
        NSLog(@"IAREvent_Function_StartRecordAudio");
    }
    else if(type == 107)
    {
        int randNum = [self getRandomNumber:0 to:5];
        if (randNum == 0) {
            [[InsightARManager shareManager] runInsightARScript:@"g_RecordResult" param:@"1,你好吗?" callback:^(BOOL result) {
                //
            }];
        }
        else if (randNum == 1)
        {
            [[InsightARManager shareManager] runInsightARScript:@"g_RecordResult" // scriptName
                                                          param:@"1,做人最重要的就是开心" // json
                                                       callback:nil];
        }
        else if (randNum == 2)
        {
            [[InsightARManager shareManager] runInsightARScript:@"g_RecordResult" param:@"1,Please看下我的显示位置是否正常？（试试文字中有括号）我显示的高度是否有超过自己的背景边框？或者我的背景边框是否因为我高度的变化而变得过长？" callback:nil];
        }
        else if (randNum == 3)
        {
            [[InsightARManager shareManager] runInsightARScript:@"g_RecordResult" param:@"2" callback:nil];
        }
        else if (randNum == 4)
        {
            [[InsightARManager shareManager] runInsightARScript:@"g_RecordResult" param:@"3" callback:nil];
        }
        else if (randNum == 5)
        {
            [[InsightARManager shareManager] runInsightARScript:@"g_RecordResult" param:@"5" callback:nil];
        }
        NSLog(@"IAREvent_Function_StopRecordAudio");
    }
    else if(type == 108)
    {
        NSLog(@"IAREvent_Function_OpenURL : %@", [event.params objectForKey:@"url"]);
    }
    else if(type == 109)
    {
        NSLog(@"IAREvent_Function_DetectEnvironment");
    }
    else if(type == 110)
    {
        UIImage *screen = [event.params objectForKey:@"image"];
        UIImageWriteToSavedPhotosAlbum(screen, nil, nil, nil);
        NSLog(@"IAREvent_Function_ScreenShot");
    }
    else if(type == 111)
    {
        UIImage *screen = [event.params objectForKey:@"image"];
        NSString *info = [event.params objectForKey:@"info"];
        NSString *link = [event.params objectForKey:@"link"];
        NSLog(@"info：%@", info);
        NSLog(@"link：%@", link);
        UIImageWriteToSavedPhotosAlbum(screen, nil, nil, nil);
        [[InsightARManager shareManager] runInsightARScript:@"g_FinishSharing" param:nil callback:nil];
        NSLog(@"IAREvent_Function_Share");
    }
}

#pragma mark - dissmissVC

- (IBAction)backAction:(UIButton *)sender
{
    [[[InsightARManager shareManager] getInsightARView] removeFromSuperview];
    [[InsightARManager shareManager] stopAllPrepare];
    [[InsightARManager shareManager] stopInsightAR];
    
    [self dissmissVC];
}

- (void)dissmissVC
{
    NSMutableSet *set = [NSMutableSet new];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:kBackToDelete]) {
        if ([_product.pid integerValue] >= 90000) {
            
        }else{
            [set addObject:_product.pid];
        }
    }
    BOOL result = [[InsightARManager shareManager] removeResource:set];
    NSLog(@"删除成功:%d",result);
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark - Actions

- (IBAction)buttonActions:(UIButton *)sender
{
    switch (sender.tag) {
        case 1:{
            [self openInsightInAPPStore];
        }
            break;
            
        case 2:{
            [self openInsightAPP];
        }
            break;
            
        case 3:{
            [self saveScreenShot];
        }
            break;
            
        case 4:{
            [self showIntroView];
        }
            break;
        case 5:{
            [self dowmloadARModel];
        }
            break;
            
        default:{
            NSLog(@"undefined buttonFunction");
        }
            break;
    }
}

- (void)showIntroView
{
    _detectImageView.hidden = YES;
    NSLog(@"_detectImageView.hidden is %d",_detectImageView.hidden);
    [_introView show:YES];
    [_detectImageView setNeedsDisplay];
    _arStatus.alpha = .0;
}

- (void)openInsightAPP
{
    NSURL *url = [NSURL URLWithString:@"insight1://"];
    url = [NSURL URLWithString:@"http://dongjian.163.com"];
    if (![[UIApplication sharedApplication] openURL:url]) {
        NSLog(@"%@%@",@"Failed to open url:",[url description]);
    }
}

- (void)openInsightInAPPStore
{
    NSURL *url = [NSURL URLWithString:@"itms://itunes.apple.com/cn/app/wang-yi-dong-jianar-dong-cha/id1142482530?mt=8"];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)dowmloadARModel
{
    
}

#pragma mark - Setters

#pragma mark - INSIntroViewDelegate

- (void)insIntroViewWillDissappear:(INSIntroView *)introView
{
    _arStatus.alpha = .0;
    self.detectImageView.hidden = NO;
}

#pragma mark - 动态模型下载Demo

- (IBAction)dmResetAction:(UIButton *)sender {
    [[InsightARManager shareManager] runInsightARScript:@"g_RemoveAllModels" param:nil callback:^(BOOL result) {
        NSLog(@"g_RemoveAllModels %d", result);
    }];
}

- (IBAction)dmDownloadAction:(UIButton *)sender {
    
//    ARDynamicModelInput *input = [[ARDynamicModelInput alloc] initWithPid:_product.pid materailID:@"1305013"];
//    [[InsightARManager shareManager] downloadDynamicModel:input progress:^(NSProgress *downloadProgress) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self handleProgress:downloadProgress cloud:NO];
//        });
//    } finished:^(NSError *error, ARDynamicModel *dynamicModel) {
//        _progress.hidden = YES;
//        if (error) {
//            _loadingLabel.hidden = NO;
//            _loadingLabel.text = error.localizedDescription;
//            NSLog(@"prepareAR error: %@",error);
//            [self setARLabelText: error.description];
//        } else {
//            _loadingLabel.hidden = YES;
//            [[InsightARManager shareManager] runInsightARScript:@"g_LoadModel" param:dynamicModel.materailID callback:^(BOOL result){
//                NSLog(@"loadARModel %d", result);
//            }];
//        }
//    }];
}

- (IBAction)dmDeleteAction:(UIButton *)sender {
    [[InsightARManager shareManager] runInsightARScript:@"g_DeleteModel" param:nil callback:^(BOOL result) {
        NSLog(@"g_DeleteModel %d", result);
    }];
}

- (IBAction)dmSwitchAction:(UIButton *)sender {
    //1306025 1305015 1305016_x14 1305016_x16 1305013 1306023 1306020
//    ARDynamicModelInput *input = [[ARDynamicModelInput alloc] initWithPid:_product.pid materailID:@"1306025"];
//    [[InsightARManager shareManager] downloadDynamicModel:input progress:^(NSProgress *downloadProgress) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self handleProgress:downloadProgress cloud:NO];
//        });
//    } finished:^(NSError *error, ARDynamicModel *dynamicModel) {
//        _progress.hidden = YES;
//        if (error) {
//            _loadingLabel.hidden = NO;
//            _loadingLabel.text = error.localizedDescription;
//            NSLog(@"prepareAR error: %@",error);
//            [self setARLabelText: error.description];
//        } else {
//            _loadingLabel.hidden = YES;
//            [[InsightARManager shareManager] runInsightARScript:@"g_SwitchModel" param:dynamicModel.materailID callback:^(BOOL result){
//                NSLog(@"switchARModel %d", result);
//            }];
//        }
//    }];
}

- (IBAction)dmConfirmAction:(UIButton *)sender {
    [[InsightARManager shareManager] runInsightARScript:@"g_DeSelectModel" param:nil callback:^(BOOL result){
        NSLog(@"runScriptInsightAR %d", result);
    }];
}

@end
