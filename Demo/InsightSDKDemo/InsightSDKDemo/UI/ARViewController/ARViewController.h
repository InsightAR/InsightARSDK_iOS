//
//  ARViewController.h
//  LightSDKDemo
//
//  Created by Dikey on 27/03/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

@class InsightADelegate;
@class INSIntroView;
@class ARProduct;
extern float const kMMAnimationDuration;
@class MMScanningView;

@interface ARViewController : UIViewController

@property (nonatomic, strong) INSIntroView *introView;
@property (nonatomic, strong) ARProduct *product;
@property (nonatomic, strong) UIView *arView;
@property (nonatomic, copy) NSArray *introImages; /**引导图片 */
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progress;
@property (weak, nonatomic) IBOutlet UIButton *funBtn1;
@property (weak, nonatomic) IBOutlet UIButton *funBtn2;
@property (weak, nonatomic) IBOutlet UIButton *funBtn3;
@property (weak, nonatomic) IBOutlet UIImageView *detectImageView;
@property (weak, nonatomic) IBOutlet UILabel *arStatus;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UIButton *downloadBtn;
@property (weak, nonatomic) IBOutlet MMScanningView *scanningView;
@property (weak, nonatomic) IBOutlet UIView *actionButtonsView;

@end
