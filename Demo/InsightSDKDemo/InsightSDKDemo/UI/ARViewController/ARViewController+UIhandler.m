//
//  ARViewController+UIhandler.m
//  LightSDKDemo
//
//  Created by Dikey on 10/04/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "ARViewController+UIhandler.h"
#import "Utility.h"
#import <InsightSDK/InsightSDK.h>
#import "NetworkHelper.h"
#import "INSIntroView.h"
#import "ARDetectType.h"
#import "MMScanningView.h"
#import "MMToast.h"

@implementation ARViewController (UIhandler)

- (void)startWithCloudProductID:(NSString *)productID
{
    switch ([[NetworkHelper sharedHelper]networkStatus]) {
        case AFNetworkReachabilityStatusReachableViaWiFi:{
            [self startDownloadCloudWithPid:productID];
        }
            break;
            
        case AFNetworkReachabilityStatusReachableViaWWAN:{
            [self showCloudDownloadAlertControllerWithPid:productID];
        }
            break;
            
        default:
            [self showErrorDownloadProductID:productID];
            break;
    }
}

- (void)showCloudDownloadAlertControllerWithPid:(NSString *)productID
{
    [self getProductSize:productID finish:^(NSString *size) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:@"已找到目标!\n下载将消耗%@流量",size] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action){
            [self retryCloudAR];
        }];
        UIAlertAction *commitAction = [UIAlertAction actionWithTitle:@"下载" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
            [self startDownloadCloudWithPid:productID];
        }];
        [alertController addAction:cancleAction];
        [alertController addAction:commitAction];
        [self presentViewController:alertController animated:YES completion:^{
        }];
    }];
}

- (void)showErrorDownloadProductID:(NSString *)productID
                             error:(NSString *)error
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:error preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    UIAlertAction *commitAction = [UIAlertAction actionWithTitle:@"重试" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        [self retryCloudAR];
    }];
    [alertController addAction:cancleAction];
    [alertController addAction:commitAction];
    [self presentViewController:alertController animated:YES completion:^{
    }];
}

- (void)showErrorDownloadProductID:(NSString *)productID
{
    [self showErrorDownloadProductID:productID error:@"网络出错了，请稍后再试"];
}

- (void)retryCloudAR
{
    [MMToast toastHideOnView:self.view];
    [[InsightARManager shareManager]stopInsightAR];
    [self.arView removeFromSuperview];
    self.arView = nil;
    ARAlgorithmModel *algModel = [[InsightARManager shareManager]getAlgorithmModel];
    [[InsightARManager shareManager]startInsightARWithCloud:algModel withARDelegate:self];
    if (self.arView == nil) {
        self.arView = [[InsightARManager shareManager] getInsightARView];
        [self.view insertSubview:self.arView atIndex:0];
    }
}

- (void)getProductSize:(NSString *)productID
                finish:(void(^)(NSString *size))sizeBlock
{
    __block NSString *string = @"几M";
    __block float size  = 0;
    
    if ([NetworkHelper sharedHelper].isNetworkAvaliable) {
        [[InsightARManager shareManager]checkProductStatus:^(NSError *error) {
            NSLog(@"error is %@",error.localizedDescription);
            sizeBlock(string);
        } result:^(NSArray <ARProduct *>* status) {
            for (ARProduct *productStuats in status) {
                if ([productID isEqualToString:productStuats.pid]) {
                    size = productStuats.size/1024.0/1024.0;
                    string = [NSString stringWithFormat:@"%.1fM",size];
                    break;
                }
            }
            sizeBlock(string);
        }];
    }else{
        sizeBlock(string);
    }
}

- (void)startDownloadCloudWithPid:(NSString *)productID
{
    if (productID != nil && productID.length > 0){
        self.product.pid = productID;
    }
    ARPrepareOptions *options = [ARPrepareOptions new];
    options.productID = self.product.pid;
    options.protocolClasses = nil; //设置代理服务器    
    if (!options.productID.length) {
        return;
    }
    [[InsightARManager shareManager] prepareARWithOptions:options downloadProgress:^(NSProgress *downloadProgress) {
        [self handleProgress:downloadProgress];
    } completion:^(NSError *error) {
        self.progress.hidden = YES;
        self.detectImageView.hidden = NO;
        [MMToast toastHideOnView:self.view];
        if (error != nil) {
            if (error.code == 701) {
                [self showErrorDownloadProductID:productID error:error.localizedDescription];
            }else{
                [self showErrorDownloadProductID:productID];
            }
        }else{
            self.loadingLabel.hidden = YES;
            NSLog(@"prepareAR success");
            ARProduct *product = [[InsightARManager shareManager]getARProduct:options.productID];
            [[InsightARManager shareManager] reloadInsightAR:product];
        }
    }];
    NSLog(@"IAREvent_Function_ReloadARProduct");
}

- (void)handleProgress:(NSProgress *)downloadProgress
                 cloud:(BOOL)cloud
{
    float progress = (float)downloadProgress.completedUnitCount*100.0/(float)downloadProgress.totalUnitCount*1.0;
    self.progress.progress = progress;
    NSString *progressString = [NSString stringWithFormat:@"已找到目标！\n正在载入，请不要移动\n手机。(%@%%)",@(progress)];
    if (!cloud) {
        progressString = [NSString stringWithFormat:@"正在载入(%@%%)",@(progress)];
    }
    [MMToast updateProgress:progressString forView:self.view];
    if (progress == 100) {
        [MMToast toastHideOnView:self.view];
        self.infoButton.hidden = !self.introImages.count;
        self.arStatus.hidden = NO;
        //假如需要放置汽车
        [self startScanAnimation];
    }
}

- (void)handleProgress:(NSProgress *)downloadProgress
{
    float progress = (float)downloadProgress.completedUnitCount*100.0/(float)downloadProgress.totalUnitCount*1.0;
    self.progress.progress = progress;
    NSString *progressString = [NSString stringWithFormat:@"已找到目标！\n正在载入，请不要移动\n手机。(%@%%)",@(progress)];
    [MMToast updateProgress:progressString forView:self.view];
    if (progress == 100) {
        self.infoButton.hidden = !self.introImages.count;
        self.arStatus.hidden = NO;
        //假如需要放置汽车
        [self startScanAnimation];
        [MMToast toastHideOnView:[[[UIApplication sharedApplication] delegate] window]];
    }
}

-(void)setARLabelText:(NSString *)text
{
    UIColor *color =[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    self.arStatus.layer.shadowColor = [color CGColor];
    self.arStatus.layer.shadowRadius = 4.0f;
    self.arStatus.layer.shadowOpacity = .9;
    self.arStatus.layer.shadowOffset = CGSizeZero;
    self.arStatus.layer.masksToBounds = NO;
}

- (void)saveScreenShot
{
    [self runStillImageCaptureAnimation];
    [self snapshotScreenInView:[[InsightARManager shareManager] getInsightARView]];
}

- (void)runStillImageCaptureAnimation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIView *view = [[InsightARManager shareManager] getInsightARView];
        if (view != nil) {
            [[view layer] setOpacity:.0];
            [UIView animateWithDuration:.33 animations:^{
                [[view layer] setOpacity:1.0];
            }];
        }
    });
}

- (void)snapshotScreenInView:(UIView *)contentView{// 需要截屏的UIView
    CGSize size = contentView.bounds.size; //设置大小
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale); //开始上下文
    CGRect rect = contentView.bounds; //设置坐标
    [contentView drawViewHierarchyInRect:rect afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext(); //截屏
    UIGraphicsEndImageContext(); //结束
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil); //保存image到相册
}

#pragma mark - Animations

- (void)stopScanAnimation
{
    self.detectImageView.hidden = YES;
}

- (void)startScanAnimation
{
    if (self.detectImageView.layer.animationKeys) {
        return;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.33 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.detectImageView.frame = CGRectMake(0, -SCREEN_HEIGHT , self.view.bounds.size.width, SCREEN_HEIGHT);
        [UIView animateWithDuration:3.0 delay:.0 options:UIViewAnimationOptionRepeat  animations:^{
            self.detectImageView.frame = CGRectMake(0, SCREEN_HEIGHT , self.view.bounds.size.width, SCREEN_HEIGHT);
        } completion:^(BOOL finished) {
            self.detectImageView.frame = CGRectMake(0, -SCREEN_HEIGHT , self.view.bounds.size.width, SCREEN_HEIGHT);
        }];
    });
}

#pragma mark - view Change

- (void)startViewConfigrations
{
    [self setFunBtnHiddenHidden:YES];
    [self startScanAnimation];
    
    self.infoButton.hidden = YES;
    self.scanningView.hidden = YES;
    self.loadingLabel.text = [NSString stringWithFormat:@"Loading 0 %%"];
    self.arStatus.text = @"扫描图片";
    self.loadingLabel.hidden = NO;
    self.loadingLabel.text =  self.product.name;
    self.detectImageView.hidden = YES;
    [self.progress setProgress:.0];
    self.arStatus.alpha = .0;
    self.actionButtonsView.hidden = !([self.product.pid integerValue] == ARDetectType2DTrack1);
}

- (void)setDetectImageViewHidden:(BOOL)hide
{
    self.detectImageView.hidden = hide;
    [self setFunBtnHiddenHidden:!hide];
}

- (void)setFunBtnHiddenHidden:(BOOL)hide
{
    self.funBtn1.hidden = hide;
    self.funBtn2.hidden = hide;
    self.funBtn3.hidden = hide;
    self.funBtn1.userInteractionEnabled = !hide;
    self.funBtn2.userInteractionEnabled = !hide;
    self.funBtn3.userInteractionEnabled = !hide;
    self.infoButton.hidden = !hide;
}

@end
