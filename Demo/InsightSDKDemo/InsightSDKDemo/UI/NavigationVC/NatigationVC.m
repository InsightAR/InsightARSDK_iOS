//
//  NatigationVC.m
//  LightSDKDemo
//
//  Created by Dikey on 08/03/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "NatigationVC.h"

@interface NatigationVC ()

@end

@implementation NatigationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blueColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)shouldAutorotate {
    //是否支持自动旋转
    return [[self.viewControllers lastObject] shouldAutorotate];
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations {
    //设备支持的方向
    return [[self.viewControllers lastObject] supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    //方向标识
    return [[self.viewControllers lastObject] preferredInterfaceOrientationForPresentation];
}

@end
