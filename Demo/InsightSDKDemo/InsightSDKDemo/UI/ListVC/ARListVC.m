//
//  ARListVC.m
//  LightSDKDemo
//
//  Created by Dikey on 11/04/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "ARListVC.h"
#import "ARListVCCell.h"
#import "UIColor+HexColor.h"
#import <InsightSDK/InsightSDK.h>
#import "ARViewController.h"
#import "ARListTableViewHeader.h"
#import "ARTestUserDefault.h"
#import "UserARScenesVC.h"
#import "ARDetectType.h"
#import "ARDMTestVC.h"
#import "ARListVCOtherTestCell.h"
#import "ARListVCNewAppCell.h"
#import "ViewController.h"

@interface ARListVC ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,assign) NSInteger cellCount;
@property (nonatomic,copy) NSArray *localARScenes;
@property (nonatomic,assign) BOOL hideSection1;
@property (nonatomic,assign) BOOL hideSection2;
@property (nonatomic,assign) BOOL hideSection3;
@property (nonatomic,strong) NSMutableArray *otherScenes;

@end

@implementation ARListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds) - 44) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"ARListVCCell" bundle:nil] forCellReuseIdentifier:@"ARListVCCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ARListVCOtherTestCell" bundle:nil] forCellReuseIdentifier:@"ARListVCOtherTestCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ARListVCNewAppCell" bundle:nil] forCellReuseIdentifier:@"ARListVCNewAppCell"];
    _tableView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!_otherScenes) {
        _otherScenes = [NSMutableArray new];
        [_otherScenes addObject:[NSNumber numberWithInt:ARDetectTypeAPITest]];
        [_otherScenes addObject:[NSNumber numberWithInt:ARDetectTypeOldProductList]];
    }
    _localARScenes = [NSArray new];
    [self.tableView reloadData];
    [self loadAllProduct];
}

- (void)loadAllProduct
{
    [[InsightARManager shareManager]checkProductStatus:^(NSError *error) {
        NSLog(@"error is %@",error.localizedDescription);
    } result:^(NSArray<ARProduct *> *arProducts) {
        _localARScenes = arProducts;
        NSIndexSet *setIndex = [[NSIndexSet alloc]initWithIndex:0];
        [self.tableView reloadSections:setIndex withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        if (_hideSection1 == YES) {
            return 0;
        }else
            return _localARScenes.count;
    }else if (section == 1) {
        if (_hideSection2) {
            return 0;
        }else{
            return [ARTestUserDefault sharedInstance].appKeys.count+1;
        }
    }else if (section == 2) {
        if (_hideSection3) {
            return 0;
        }else{
            return _otherScenes.count;
        }
    }else
        return _cellCount;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"ARListVCCell";
    ARListVCCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (nil == cell){
        cell = [[ARListVCCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if (indexPath.section == 0) {
        [cell configureCellWithProduct:_localARScenes[indexPath.row]];
    }else if(indexPath.section == 1){
        if (indexPath.row == 0) {
            ARListVCNewAppCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ARListVCNewAppCell"];
            return cell;
        }else{
            NSString *name = [ARTestUserDefault sharedInstance].appKeys.allKeys[indexPath.row -1];
            [cell configureCellWithString:name];
        }
    }else{
        ARListVCOtherTestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ARListVCOtherTestCell"];
        NSString *titleString = ARDetectTypeToString[[_otherScenes[indexPath.row]integerValue]];
        cell.titleLabel.text = titleString;
        return cell;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:{
            [self pushARScense:indexPath.row];
        }
            break;
         
        case 1:{
            if (indexPath.row == 0) {
                [self addNewTestApp];
            }else{
                [self showUserDefinedARScenes:indexPath.row];
            }
        }
            break;
            
        case 2:{
            if (indexPath.row == 0) {
                [self showARDMTestVC];
            }else{
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ViewController *arVC = [sb instantiateViewControllerWithIdentifier:@"ViewController"];
                [self.navigationController pushViewController:arVC animated:YES];
            } 
        }
            break;
            
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)pushARScense:(NSInteger)row
{
    ARProduct *productLocal = _localARScenes[row];
    ARProduct *product = [ARProduct new];
    product.name = productLocal.name ;
    product.pid = productLocal.pid ;
    product.direction = productLocal.direction ;
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ARViewController *arVC = [sb instantiateViewControllerWithIdentifier:@"ARViewController"];
    [arVC setProduct:product];
    [arVC setModalPresentationStyle:UIModalPresentationCustom];
    [arVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:arVC animated:YES completion:^{
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2) {
        return 110;
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            return 85.0;
        }
    }
    
    return 300;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ARListTableViewHeader *headerView = [[ARListTableViewHeader alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width , 52) sectionRow:section];

    UITapGestureRecognizer *singleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    [singleTapRecognizer setDelegate:self];
    singleTapRecognizer.numberOfTouchesRequired = 1;
    singleTapRecognizer.numberOfTapsRequired = 1;
    headerView.tag = 100 + section;
    [headerView addGestureRecognizer:singleTapRecognizer];
    if (section == 0) {
        [headerView rotateArrowImageView:_hideSection1];
    }else if (section == 1){
        [headerView rotateArrowImageView:_hideSection2];
    }else if (section == 2){
        [headerView rotateArrowImageView:_hideSection3];
    }
    return headerView;
}

-(void)handleGesture:(UIGestureRecognizer *)gestureRecognizer
{
    int index = gestureRecognizer.view.tag % 100;
    if (index == 0) {
        [self setHideSection1:!_hideSection1];
    }else if (index == 1){
        [self setHideSection2:!_hideSection2];
    }else if (index == 2){
        [self setHideSection3:!_hideSection3];
    }
    NSIndexSet *indexSet = [[NSIndexSet alloc]initWithIndex:index];
    [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 52;
}

- (void)addNewTestApp
{
    UIAlertController *newTestAppAC = [UIAlertController alertControllerWithTitle:@"添加一个新的模拟应用" message:@" " preferredStyle:UIAlertControllerStyleAlert];
    [newTestAppAC addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = @"请输入AppKey";
        textField.text = @"AR-9AFE-D0221342wr68";
        [[InsightARManager shareManager]registerAppKey:@"AR-9AFE-D0221342wr68" appSecret:@"3Xj0dFjMzG"];
    }];
    [newTestAppAC addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"请输入AppSecret";
        textField.text = @"3Xj0dFjMzG";
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"取消");
    }];
    
    UIAlertAction *archiveAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *appKey = newTestAppAC.textFields.firstObject;
        UITextField *appSecret = newTestAppAC.textFields.lastObject;
        NSLog(@"确定");
        if (appKey.text.length && appSecret.text.length) {
            [self refreshWithAppKey:appKey.text appSecret:appSecret.text];
        }
    }];
    [newTestAppAC addAction:cancelAction];
    [newTestAppAC addAction:archiveAction];
    [self presentViewController:newTestAppAC animated:YES completion:^{
    }];
}

- (void)refreshWithAppKey:(NSString *)appKey
                appSecret:(NSString *)appSecret
{
    [[InsightARManager shareManager]registerAppKey:appKey appSecret:appSecret]; //严选测试环境
    [[InsightARManager shareManager]checkProductStatus:^(NSError *error) {
        if (error) {
            NSLog(@"error is %@",error);
            UIAlertController *errorAppAC = [UIAlertController alertControllerWithTitle:@"" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            }];
            [errorAppAC addAction:cancelAction];
            [self presentViewController:errorAppAC animated:YES completion:^{
            }];
        }
    } result:^(NSArray<ARProduct *> *array) {
        if (array.count) {
            [[ARTestUserDefault sharedInstance]saveAppKey:appKey appSecret:appSecret];
        }
        [_tableView reloadData];
    }];
}


#pragma mark - Push Actions

- (void)showUserDefinedARScenes:(NSInteger )row
{
    NSString *appKey = [ARTestUserDefault sharedInstance].appKeys.allKeys[row -1];
    NSString *appSecret = [[ARTestUserDefault sharedInstance].appKeys objectForKey:appKey];
    [[InsightARManager shareManager]registerAppKey:appKey appSecret:appSecret]; //严选测试环境
    [[InsightARManager shareManager]checkProductStatus:^(NSError *error) {
        if (error) {
            NSLog(@"error is %@",error);
        }
    } result:^(NSArray<ARProduct *> *array) {
        if (array.count) {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UserARScenesVC *arVC = [sb instantiateViewControllerWithIdentifier:@"UserARScenesVC"];
            [arVC setAppKey:appKey];
            [arVC setAppSecret:appSecret];
            [arVC setUserDefinedARScenes:[array mutableCopy]];
            [self.navigationController pushViewController:arVC animated:YES];
        }
    }];
}

- (void)showARDMTestVC
{
    UIAlertController *showARDMTestVC = [UIAlertController alertControllerWithTitle:@"" message:@"不再支持该测试；需要移步script中" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *archiveAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }];
    [showARDMTestVC addAction:archiveAction];
    [self presentViewController:showARDMTestVC animated:YES completion:^{
    }];
}

@end
