//
//  ARListTableViewHeader.m
//  LightSDKDemo
//
//  Created by Dikey on 2018/4/16.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "ARListTableViewHeader.h"
#import "UIColor+HexColor.h"

@implementation ARListTableViewHeader

- (instancetype)initWithFrame:(CGRect)frame
                   sectionRow:(NSInteger)section
{
    self = [super initWithFrame:frame];
    if (self) {

        self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width , 52);
        //headerView.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1/1.0];
        self.backgroundColor = [UIColor colorFromHexCode:@"#F4F4F4"];
        UILabel *label = [[UILabel alloc] init];
        label.frame = CGRectMake(41, 14, 70, 24);
        label.font = [UIFont fontWithName:@"PingFangSC-Medium" size:14];
        label.textColor = [UIColor colorFromHexCode:@"#47485D"];
        [self addSubview:label];
        
        UIImageView *headerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 19, 16, 16)];
        UIImage *image = nil;
        
        UIImageView *arrowImageView = [[UIImageView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 26, 23 , 11, 7)];
        arrowImageView.image = [UIImage imageNamed:@"list箭头"];
        [self addSubview:arrowImageView];
    
        switch (section) {
            case 0:{
                label.text = @"固定内容";
                image = [UIImage imageNamed:@"固定内容"];
            }
                break;
                
            case 1:{
                label.text = @"手动添加";
                image = [UIImage imageNamed:@"手动添加"];
            }
                break;
                
            case 2:{
                label.text = @"其他测试";
                image = [UIImage imageNamed:@"其他测试"];
            }
                break;
            default:
                break;
        }
        headerImageView.image = image;
        
        self.headerIcon = headerImageView;
        self.arrowImageView = arrowImageView;
        self.textLabel = label;

        [self addSubview:headerImageView];
    }
    return self;
}

- (void)rotateArrowImageView:(BOOL)rotate
{
    if (rotate) {
        [UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
            self.arrowImageView.transform = CGAffineTransformMakeRotation(M_PI);
        } completion:nil];
    }else{
        [UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
            self.arrowImageView.transform = CGAffineTransformMakeRotation(0);
        } completion:nil];
    }
}

@end
