//
//  ARListVCOtherTestCell.m
//  LightSDKDemo
//
//  Created by Dikey on 2018/4/16.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "ARListVCOtherTestCell.h"
#import "Utility.h"

@implementation ARListVCOtherTestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.frame = CGRectMake(0, 0, 200, 110);
    _bgView.layer.masksToBounds = YES;
    _bgView.layer.cornerRadius = 5.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
