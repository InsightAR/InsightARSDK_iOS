//
//  ARListTableViewHeader.h
//  LightSDKDemo
//
//  Created by Dikey on 2018/4/16.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARListTableViewHeader : UIView

@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UIImageView *arrowImageView;
@property (nonatomic, strong) UIImageView *headerIcon;

- (instancetype)initWithFrame:(CGRect)frame
                   sectionRow:(NSInteger)section;

- (void)rotateArrowImageView:(BOOL)rotate;

@end
