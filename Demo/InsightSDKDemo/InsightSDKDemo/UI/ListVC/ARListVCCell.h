//
//  ARListVCCell.h
//  LightSDKDemo
//
//  Created by Dikey on 11/04/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ARProduct;

@interface ARListVCCell : UITableViewCell

- (void)configureCell;

- (void)configureCellWithProduct:(ARProduct *)arProduct;

- (void)configureCellWithSection:(NSInteger )section;

- (void)configureCellWithString:(NSString *)string;

@end
