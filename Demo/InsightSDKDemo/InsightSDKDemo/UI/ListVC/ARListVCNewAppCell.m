//
//  ARListVCNewAppCell.m
//  LightSDKDemo
//
//  Created by Dikey on 2018/4/17.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "ARListVCNewAppCell.h"

@implementation ARListVCNewAppCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _bgView.layer.masksToBounds = YES;
    _bgView.layer.cornerRadius = 8.0f;
    _titleLabel.text = @"新建模拟应用";
    [self sendSubviewToBack:_bgView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
