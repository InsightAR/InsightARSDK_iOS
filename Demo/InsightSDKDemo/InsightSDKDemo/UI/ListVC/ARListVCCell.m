//
//  ARListVCCell.m
//  LightSDKDemo
//
//  Created by Dikey on 11/04/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "ARListVCCell.h"
#import <InsightSDK/InsightSDK.h>

@interface ARListVCCell()

@property (nonatomic,strong) UIImageView *bgImageView;
@property (nonatomic,strong) UILabel *cellTitleLabel;

@end

@implementation ARListVCCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _bgImageView = [[UIImageView alloc]initWithFrame:self.bounds];
    _bgImageView.image = [UIImage imageNamed:@"卡片-空状态"];
    _bgImageView.backgroundColor = [UIColor clearColor];
    
    [self addSubview:_bgImageView];
    [self addConstrains];
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(35, 256, 320, 28);
    label.text = @"";
    label.font = [UIFont fontWithName:@"PingFangSC-Medium" size:20];
    label.textColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1/1.0];

    _cellTitleLabel = label;
    [self addSubview:_cellTitleLabel];
}

-(void)addConstrains
{
    _bgImageView.translatesAutoresizingMaskIntoConstraints = NO;
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [self addConstraints:@[
                                
                                //view1 constraints
                                [NSLayoutConstraint constraintWithItem:_bgImageView
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1.0
                                                              constant:padding.top],
                                
                                [NSLayoutConstraint constraintWithItem:_bgImageView
                                                             attribute:NSLayoutAttributeLeft
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self
                                                             attribute:NSLayoutAttributeLeft
                                                            multiplier:1.0
                                                              constant:padding.left],
                                
                                [NSLayoutConstraint constraintWithItem:_bgImageView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1.0
                                                              constant:-padding.bottom],
                                
                                [NSLayoutConstraint constraintWithItem:_bgImageView
                                                             attribute:NSLayoutAttributeRight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self
                                                             attribute:NSLayoutAttributeRight
                                                            multiplier:1
                                                              constant:-padding.right],
                                ]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCell
{
    _bgImageView.image = [UIImage imageNamed:@"卡片-空状态"];
}

- (void)configureCellWithSection:(NSInteger )section
{
    if (section == 1) {
        
    }else if(section == 2){
        _cellTitleLabel.text = @"动态下载测试";
    }
}

- (void)configureCellWithString:(NSString *)string
{
    _cellTitleLabel.text = string;
}

- (void)configureCellWithProduct:(ARProduct *)arProduct
{
    NSString *arProductName = [NSString stringWithFormat:@"%@(pid=%@)",arProduct.name,arProduct.pid];
    _cellTitleLabel.text = arProductName;
}

@end
