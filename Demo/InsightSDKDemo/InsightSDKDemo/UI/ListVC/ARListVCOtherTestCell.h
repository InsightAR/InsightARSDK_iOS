//
//  ARListVCOtherTestCell.h
//  LightSDKDemo
//
//  Created by Dikey on 2018/4/16.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARListVCOtherTestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
