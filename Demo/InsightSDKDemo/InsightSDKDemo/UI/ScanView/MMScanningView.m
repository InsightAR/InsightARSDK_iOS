//
//  AHDScanningView.m
//  ARHomeDecorate
//
//  Created by yww on 8/17/16.
//  Copyright © 2016 yww. All rights reserved.
//

#import "MMScanningView.h"

#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)

@interface MMScanningView () {
    CGFloat animationViewHeight;
}

@property (nonatomic, strong) UIImageView *scanFrameImage;
@property (nonatomic, strong) UIView *animationView;
@property (nonatomic, strong) UIImageView *lineImage;

@end

@implementation MMScanningView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self updateConstraints];
    }
    return self;
}

- (void)configureViews
{
    [self.animationView removeFromSuperview];
    [self.lineImage removeFromSuperview];
    _animationView = nil;
    _lineImage = nil;
    
    self.hidden = YES;
    self.animationView = [[UIView alloc] init];
    animationViewHeight = 0;
    self.animationView.layer.anchorPoint = CGPointMake(0.5, 0);
    
    [self addSubview:self.animationView];
    
    self.lineImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"扫描"]];
    self.lineImage.clipsToBounds = YES;
    self.lineImage.frame = CGRectMake(self.lineImage.frame.origin.x, self.lineImage.frame.origin.y, self.frame.size.width, self.lineImage.frame.size.height);
    [self.animationView addSubview:self.lineImage];
}

- (void)updateConstraints {
    self.scanFrameImage.frame = self.frame;
    [super updateConstraints];
}

- (void)startScanAnimation {
}

- (void)stopScanAnimation {
    
}

- (CGSize)intrinsicContentSize {
    return CGSizeMake(556, 406);
}

@end
