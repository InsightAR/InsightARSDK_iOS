//
//  AHDScanningView.h
//  ARHomeDecorate
//
//  Created by yww on 8/17/16.
//  Copyright © 2016 yww. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMScanningView : UIView

- (void)startScanAnimation;
- (void)stopScanAnimation;

@end
