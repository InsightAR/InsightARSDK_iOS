//
//  TableViewCell.m
//  LightSDKDemo
//
//  Created by Dikey on 19/04/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "TableViewCell.h"
#import "ARViewController.h"
#import <InsightSDK/InsightSDK.h>

@implementation TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)configureCellWithString:(NSString *)string
{
    [self.enterButton setTitle:string forState:UIControlStateNormal];
}

- (IBAction)buttonAction:(UIButton *)sender {
    if ([_delegate respondsToSelector:@selector(didClickedButtonOfCell:)]) {
        [_delegate didClickedButtonOfCell:self];
    }
}

- (void)configureCellWithProduct:(ARProduct *)product
{
    NSString *title = [product.name stringByAppendingString: product.pid];
    [self.enterButton setTitle:title forState:UIControlStateNormal];
}

//for demo app
- (IBAction)openDemoAction:(UIButton *)sender {
    if ([_delegate respondsToSelector:@selector(didClickedButtonOfCell:)]) {
        [_delegate didClickedButtonOfCell:self];
    }
}

@end
