//
//  TableViewCell.h
//  LightSDKDemo
//
//  Created by Dikey on 19/04/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARDetectType.h"

@class ARProduct;
@class TableViewCell;

@protocol TableViewCellDelegate <NSObject>

@optional

- (void )didClickedButtonOfCell:(TableViewCell *)cell;

@end

@interface TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *enterButton;
@property (weak, nonatomic) id<TableViewCellDelegate> delegate;

- (void)configureCellWithProduct:(ARProduct *)product;
- (void)configureCellWithString:(NSString *)string;

@end
