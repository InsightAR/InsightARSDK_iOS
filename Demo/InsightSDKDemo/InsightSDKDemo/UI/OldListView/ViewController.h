//
//  ViewController.h
//  LightSDKDemo
//
//  Created by Dikey on 23/02/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "BaseViewController.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end


