//
//  ViewController.m
//  LightSDKDemo
//
//  Created by Dikey on 23/02/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "ViewController.h"
#import "ARViewController.h"
#import "TableViewCell.h"
#import "UserARScenesVC.h"
#import "ARDMTestVC.h"
#import "ARTestUserDefault.h"
#import <InsightSDK/InsightSDK.h>
#import "UIWindow+PazLabs.h"
#import "UIColor+HexColor.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

static CGFloat const kTableViewHeaderHeight = 30.0;

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource,TableViewCellDelegate>

@property (nonatomic, strong) NSMutableArray *localARScenes;
@property (nonatomic, strong) NSMutableArray *otherScenes;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configureARScenes];
    self.view.layer.contents = (id)[UIImage imageNamed:@"底色"].CGImage;
    self.view.backgroundColor = [UIColor lightGrayColor];
}

- (void)configureARScenes
{
    NSMutableArray *localScens = [NSMutableArray new];
    [localScens addObject:[NSNumber numberWithInt:ARDetectTypeLuaTest]];
    [localScens addObject:[NSNumber numberWithInt:ARDetectTypeiOSTest]];
    //[localScens addObject:[NSNumber numberWithInt:ARDetectTypeWangsansan]];
    [localScens addObject:[NSNumber numberWithInt:ARDetectTypeKaola]];
    [localScens addObject:[NSNumber numberWithInt:ARDetectType2DTrack1]];

    [localScens addObject:[NSNumber numberWithInt:ARDetectType2DTrack2]];
    [localScens addObject:[NSNumber numberWithInt:ARDetectType2DTrack3]];
    [localScens addObject:[NSNumber numberWithInt:ARDetectType2DTrack4]];
    [localScens addObject:[NSNumber numberWithInt:ARDetectType2DTrack5]];
    [localScens addObject:[NSNumber numberWithInt:ARDetectType2DTrack6]];
    [localScens addObject:[NSNumber numberWithInt:ARDetectType2DTrack7]];
    //[localScens addObject:[NSNumber numberWithInt:ARDetectTypeMusicChanyin]];
    //[localScens addObject:[NSNumber numberWithInt:ARDetectTypeMusicStars]];
    [localScens addObject:[NSNumber numberWithInt:ARDetectTypeLocal]];
    [localScens addObject:[NSNumber numberWithInt:ARDetectTypeCloudOnline]];
    [localScens addObject:[NSNumber numberWithInt:ARDetectTypeCloudLocal]];

    if (!_localARScenes) {
        _localARScenes = [NSMutableArray new];
        for (NSNumber *type in localScens) {
            ARProduct *product = [ARProduct new];
            product.pid = [type stringValue];
            product.name = ARDetectTypeToString[[product.pid integerValue]];
            ARProduct *localProduct = [[InsightARManager shareManager]getARProduct:[type stringValue]];
            if (localProduct.downloaded == YES) {
                product = localProduct;
            }
            [_localARScenes addObject:product];
        }
    }
    if (!_otherScenes) {
        _otherScenes = [NSMutableArray new];
        [_otherScenes addObject:[NSNumber numberWithInt:ARDetectTypeAPITest]];
    }
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - orientations

-(BOOL)shouldAutorotate
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        return YES;
    }else{
        UIViewController *visibleVC = [self.view.window visibleViewController];
        if (visibleVC == self) {
            NSLog(@"visibleVC ");
            return YES;
        }else{
            NSLog(@"unvisibleVC ");
            return NO;
        }
    }
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    UIInterfaceOrientationMask mask = UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeRight;
    return mask;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [_tableView reloadData];
}

#pragma mark - UITableViewDelegate

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kTableViewHeaderHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kTableViewHeaderHeight)];
    sectionHeaderView.backgroundColor = [UIColor clearColor];

    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(sectionHeaderView.frame.origin.x,sectionHeaderView.frame.origin.y, sectionHeaderView.frame.size.width, kTableViewHeaderHeight)];
    headerLabel.backgroundColor = [UIColor clearColor];
    [headerLabel setTextColor:[UIColor whiteColor]];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    [headerLabel setAdjustsFontSizeToFitWidth:YES];
    [sectionHeaderView addSubview:headerLabel];
    switch (section) {
        case 0:
            headerLabel.text = @"--------固定内容--------";
            return sectionHeaderView;
            break;
        case 1:
            headerLabel.text = @"--------手动添加--------";
            return sectionHeaderView;
            break;
        case 2:
            headerLabel.text = @"--------其他测试--------";
            return sectionHeaderView;
            break;
        default:
        break;
    }
    return sectionHeaderView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:{
            return _localARScenes.count;
        }
            break;
         
        case 1:{
            return [ARTestUserDefault sharedInstance].appKeys.count+1;
        }
            break;
            
        case 2:{
            return _otherScenes.count;
        }
            break;
            
        default:
            return 0;
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TableViewCell";
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (nil == cell){
        cell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.delegate = self;
    if (indexPath.section == 0) {
        [cell configureCellWithProduct:_localARScenes[indexPath.row]];
    }else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            [cell configureCellWithString:ARDetectTypeToString[ARDetectTypeAddNewAPP]];
        }else{
            NSString *name = [ARTestUserDefault sharedInstance].appKeys.allKeys[indexPath.row -1];
            [cell configureCellWithString:name];
        }
    }else if (indexPath.section == 2){
        [cell configureCellWithString:ARDetectTypeToString[[_otherScenes[indexPath.row]integerValue]]];
    }
    
    return cell;
}

- (void)didClickedButtonOfCell:(TableViewCell *)cell
{
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    switch (indexPath.section) {
        case 1:{
            if (indexPath.row == 0) {
                [self addNewTestApp];
            }else{
                [self showUserDefinedARScenes:indexPath.row];
            }
        }
            break;
            
        case 2:{
            [self showARDMTestVC];
        }
            break;
    
        default:{
            [self pushAction:indexPath.row];
        }
            break;
    }
}

- (void)pushAction:(NSInteger)row
{
//    [[InsightARManager shareManager]registerAppKey:@"AR-94F3-E892A3593AB1" appSecret:@"3mDy3NHlPF"]; //严选测试环境
    ARProduct *productLocal = _localARScenes[row];
    ARProduct *product = [ARProduct new];
    product.name = productLocal.name ;
    product.pid = productLocal.pid ;

    ARViewController *arVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ARViewController"];
    NSArray *array = [self getImageArrayOfType:product.pid.integerValue];
    [arVC setIntroImages:array];
    [arVC setProduct:product];
    [arVC setModalPresentationStyle:UIModalPresentationCustom];
    [arVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:arVC animated:YES completion:^{
    }];
}

- (void)showARDMTestVC
{
    ARDMTestVC *arVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ARDMTestVC"];
    [self presentViewController:arVC animated:YES completion:^{
    }];
}

- (NSArray *)getImageArrayOfType:(ARDetectType )type
{
    NSMutableArray *array = [NSMutableArray new];
    
    switch (type) {
            
        case ARDetectType2DTrack0:{
            
        }
            break;
            
        case ARDetectType2DTrack1:{
            
        }
            break;
            
        case ARDetectTypeLocal:{
           
        }
            break;
                    
        default:
            break;
    }
    
    if (array.count) {
        return array;
    }else
        return nil;
}

- (void)addNewTestApp
{
    UIAlertController *newTestAppAC = [UIAlertController alertControllerWithTitle:@"添加一个新的模拟应用" message:@" " preferredStyle:UIAlertControllerStyleAlert];
    [newTestAppAC addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = @"请输入AppKey";
        textField.text = @"AR-9AFE-D0221342wr68";
        [[InsightARManager shareManager]registerAppKey:@"AR-9AFE-D0221342wr68" appSecret:@"3Xj0dFjMzG"];
    }];
    [newTestAppAC addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"请输入AppSecret";
        textField.text = @"3Xj0dFjMzG";
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"取消");
    }];
    
    UIAlertAction *archiveAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *appKey = newTestAppAC.textFields.firstObject;
        UITextField *appSecret = newTestAppAC.textFields.lastObject;
        NSLog(@"确定");
        if (appKey.text.length && appSecret.text.length) {
            [self refreshWithAppKey:appKey.text appSecret:appSecret.text];
        }
    }];
    [newTestAppAC addAction:cancelAction];
    [newTestAppAC addAction:archiveAction];
    [self presentViewController:newTestAppAC animated:YES completion:^{
    }];
}

- (void)refreshWithAppKey:(NSString *)appKey
                appSecret:(NSString *)appSecret
{
    [[InsightARManager shareManager]registerAppKey:appKey appSecret:appSecret]; //严选测试环境
    [[InsightARManager shareManager]checkProductStatus:^(NSError *error) {
        if (error) {
            NSLog(@"error is %@",error);
            UIAlertController *errorAppAC = [UIAlertController alertControllerWithTitle:@"" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            }];
            [errorAppAC addAction:cancelAction];
            [self presentViewController:errorAppAC animated:YES completion:^{
            }];
        }
    } result:^(NSArray<ARProduct *> *array) {
        if (array.count) {
            [[ARTestUserDefault sharedInstance]saveAppKey:appKey appSecret:appSecret];
        }
        [_tableView reloadData];
    }];
}

- (void)showUserDefinedARScenes:(NSInteger )row
{
    __block NSString *appKey = [ARTestUserDefault sharedInstance].appKeys.allKeys[row -1];
    __block NSString *appSecret = [[ARTestUserDefault sharedInstance].appKeys objectForKey:appKey];
    [[InsightARManager shareManager]registerAppKey:appKey appSecret:appSecret];
    [[InsightARManager shareManager]checkProductStatus:^(NSError *error) {
        if (error) {
            NSLog(@"error is %@",error);
        }
    } result:^(NSArray<ARProduct *> *array) {
        if (array.count) {
            UserARScenesVC *arVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserARScenesVC"];
            [arVC setAppKey:appKey];
            [arVC setAppSecret:appSecret];
            [arVC setUserDefinedARScenes:[array mutableCopy]];
            [self.navigationController pushViewController:arVC animated:YES];
        }
    }];
}

@end
