//
//  INSIntroPageControl.m
//  LightSDKDemo
//
//  Created by Dikey on 06/07/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "INSIntroPageControl.h"

@interface INSIntroPageControl()

@property (nonatomic, strong) UIImage *currentImage;
@property (nonatomic, strong) UIImage *inactiveImage;

@end

@implementation INSIntroPageControl

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.pageIndicatorTintColor = [UIColor clearColor];
    self.currentPageIndicatorTintColor = [UIColor clearColor];
}

- (void)configureCurrentImage:(UIImage *)currentImage
               indicatorImage:(UIImage *)indicatorImage
{
    self.currentImage = currentImage;
    self.inactiveImage = indicatorImage;
}

- (void)updateDots {
    for (int i = 0; i < [self.subviews count]; i++) {
        UIImageView * dot = [self imageViewForSubview:  [self.subviews objectAtIndex: i]];
        if (i == self.currentPage) dot.image = self.currentImage;
        else dot.image = self.inactiveImage;
    }
}

- (UIImageView *)imageViewForSubview:(UIView *) view {
    UIImageView * dot = nil;
    if ([view isKindOfClass: [UIView class]]) {
        for (UIView* subview in view.subviews) {
            if ([subview isKindOfClass:[UIImageView class]]) {
                dot = (UIImageView *)subview;
                break;
            }
        }
        if (dot == nil) {
            dot = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, view.frame.size.width*1.8, view.frame.size.height/3.0)];
            [view addSubview:dot];
        }
    }else {
        dot = (UIImageView *) view;
    }
    
    return dot;
}

- (void)setCurrentPage:(NSInteger)currentPage {
    [super setCurrentPage:currentPage];
    [self updateDots];
}

@end
