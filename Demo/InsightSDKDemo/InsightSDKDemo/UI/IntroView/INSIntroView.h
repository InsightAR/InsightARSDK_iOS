//
//  INSIntroView.h
//  LightSDKDemo
//
//  Created by Dikey on 05/07/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

@class INSIntroPageControl;
@class INSIntroView;

@protocol INSIntroViewDelegate <NSObject>

@optional

- (void)insIntroViewWillDissappear:(INSIntroView *)introView;

@end

@interface INSIntroView : UIView<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet INSIntroPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) id<INSIntroViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *introContent;

- (void)addImages:(NSArray *)images;

- (void)show:(BOOL)animate;

@end
