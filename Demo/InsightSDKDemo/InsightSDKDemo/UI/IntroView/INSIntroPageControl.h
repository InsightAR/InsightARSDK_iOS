//
//  INSIntroPageControl.h
//  LightSDKDemo
//
//  Created by Dikey on 06/07/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface INSIntroPageControl : UIPageControl

- (void)configureCurrentImage:(UIImage *)currentImage
               indicatorImage:(UIImage *)indicatorImage;

@end
