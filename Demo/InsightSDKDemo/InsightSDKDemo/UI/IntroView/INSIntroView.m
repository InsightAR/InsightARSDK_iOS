//
//  INSIntroView.m
//  LightSDKDemo
//
//  Created by Dikey on 05/07/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "INSIntroView.h"
#import "Utility.h"
#import "INSIntroPageControl.h"

@interface INSIntroView()

@end

@implementation INSIntroView

-(void)awakeFromNib
{
    [super awakeFromNib];
    _scrollView.delegate = self;
    
    _introContent.layer.cornerRadius = 5;
    _introContent.layer.masksToBounds = true;
    
    UITapGestureRecognizer *dissmissTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissView)];
    [self.bgView addGestureRecognizer:dissmissTap];
    
    [self.pageControl configureCurrentImage:[UIImage imageNamed:@"Rectangle_dot_white"] indicatorImage:[UIImage imageNamed:@"Rectangle_dot_color"]];
}

- (void)show:(BOOL)animate
{
    if (animate) {
        [UIView animateWithDuration:0.33 delay:.0 options:UIViewAnimationOptionCurveLinear animations:^{
            self.alpha = 1.0;
        } completion:^(BOOL finished) {
        }];
    }else{
        self.alpha = 1.0;
    }
}

- (void)updateConstraints
{
    float width = SCREEN_WIDTH;
    float height = SCREEN_HEIGHT;
    self.frame = CGRectMake(0.0, 0.0, width, height);
    [self setNeedsLayout];
    [self layoutIfNeeded];
    [super updateConstraints];
}

- (void)addImages:(NSArray *)images
{
    _pageControl.numberOfPages = images.count;
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width*[images count], _scrollView.frame.size.height);
    
    for(int i = 0; i < images.count; i++)
    {
        UIImage *image = [images objectAtIndex:i];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(_scrollView.frame.size.width * i, 0.0, _scrollView.frame.size.width, _scrollView.frame.size.height);
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_scrollView addSubview:imageView];
    }
}

#pragma mark - Actions

- (IBAction)dismissViewAction:(UIButton *)sender
{
    [self dismissView];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float width = scrollView.frame.size.width;
    float xPos = scrollView.contentOffset.x+10;
    _pageControl.currentPage = (int)xPos/width;
}

#pragma mark - Gestures

- (void)dismissView
{
    if ([_delegate respondsToSelector:@selector(insIntroViewWillDissappear:)]) {
        [_delegate insIntroViewWillDissappear:self];
    }
    [UIView animateWithDuration:0.33 delay:.0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.alpha = .0;
    } completion:^(BOOL finished) {

    }];
}

@end
