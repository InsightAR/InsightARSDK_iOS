//
//  ARDMTestVC.h
//  LightSDKDemo
//
//  Created by Dikey on 01/12/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARDMTestVC : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *inputPidTextFiled;
@property (weak, nonatomic) IBOutlet UITextField *inputMidTextFiled;
@property (weak, nonatomic) IBOutlet UITextView *resultLogTextView;

@end
