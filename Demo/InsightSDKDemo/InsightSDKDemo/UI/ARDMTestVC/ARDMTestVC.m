//
//  ARDMTestVC.m
//  LightSDKDemo
//
//  Created by Dikey on 01/12/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "ARDMTestVC.h"
#import <InsightSDK/InsightSDK.h>

@interface ARDMTestVC ()

@property (nonatomic, assign) NSInteger clearCount;

@end

@implementation ARDMTestVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions

- (IBAction)backAction:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)checkStatus:(UIButton *)sender
{
//    ARDynamicModelInput *input = [[ARDynamicModelInput alloc]initWithPid:_inputPidTextFiled.text materailID:_inputMidTextFiled.text];
//    [[InsightARManager shareManager]checkDynamicModelStatus:input finished:^(NSError *error, ARDynamicModel *model) {
//        if (error) {
//            [self logTextViewAppendingText:error.description];
//        }else{
//            NSLog(@"model is %@",model);
//            NSLog(@"model.hasDownloaded %d",model.hasDownloaded);
//            [self logTextViewAppendingText:model.description];
//        }
//    }];
}

- (IBAction)downloadAction:(UIButton *)sender
{
//    ARDynamicModelInput *input = [[ARDynamicModelInput alloc]initWithPid:_inputPidTextFiled.text materailID:_inputMidTextFiled.text];
//    [[InsightARManager shareManager]downloadDynamicModel:input progress:^(NSProgress *downloadProgress) {
//        [self logTextViewAppendingText:downloadProgress.description];
//    } finished:^(NSError *error, ARDynamicModel *dynamicModel) {
//        if (error) {
//            [self logTextViewAppendingText:error.description];
//        }else{
//            [self logTextViewAppendingText:dynamicModel.description];
//        }
//    }];
}

- (IBAction)updateAction:(UIButton *)sender
{
//    ARDynamicModelInput *input = [[ARDynamicModelInput alloc]initWithPid:_inputPidTextFiled.text materailID:_inputMidTextFiled.text];
//    [[InsightARManager shareManager]updateDynamicModel:input progress:^(NSProgress *downloadProgress) {
//        [self logTextViewAppendingText:downloadProgress.description];
//    } finished:^(NSError *error, ARDynamicModel *dynamicModel) {
//        if (error) {
//            [self logTextViewAppendingText:error.description];
//        }else{
//            [self logTextViewAppendingText:dynamicModel.description];
//        }
//    }];
}

- (IBAction)deleteAction:(UIButton *)sender {
//    ARDynamicModelInput *input = [[ARDynamicModelInput alloc]initWithPid:_inputPidTextFiled.text materailID:_inputMidTextFiled.text];
//    BOOL deleted = [[InsightARManager shareManager]deleteDynamicModel:input];
//    NSString *resultString = deleted?@"删除成功":@"删除失败";
//    NSString *string = [NSString stringWithFormat:@"pid %@, mid %@, %@", _inputPidTextFiled.text ,_inputMidTextFiled.text , resultString];
//    [self logTextViewAppendingText:string];
}

-(IBAction)clearTextFiledAction:(UIButton *)sender {
    if ([_resultLogTextView.text isEqualToString: @"没事别乱点"] ) {
        _resultLogTextView.text = @"没事别乱点🙄️";
        return;
    }
    if ([_resultLogTextView.text isEqualToString: @"没事别乱点🙄️"]) {
        return;
    }
    if ([_resultLogTextView.text isEqualToString: @"我是log~"]) {
        _resultLogTextView.text = @"我还是log~";
        return;
    }
    if ([_resultLogTextView.text isEqualToString: @"我还是log~"]) {
        _resultLogTextView.text = @"我仍然是log~";
        return;
    }
    if ([_resultLogTextView.text isEqualToString: @"我仍然是log~"]) {
        _resultLogTextView.text = @"你再点还是我还是Log~";
        return;
    }
    if ([_resultLogTextView.text isEqualToString: @"你再点还是我还是Log~"]) {
        _clearCount ++;
        if (_clearCount == 5) {
            _resultLogTextView.text = @"没事别乱点";
            _clearCount = 0;
        }
    }else{
        _resultLogTextView.text = @"我是log~";
    }
}

#pragma mark - Text Filed

- (void)updateResultLogTextView:(NSString *)text
{
    if (text.length) {
        _resultLogTextView.text = text;
    }
}

- (void)logTextViewAppendingText:(NSString *)text
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (text.length) {
            _resultLogTextView.text = [_resultLogTextView.text stringByAppendingString:@"\n"];
            _resultLogTextView.text = [_resultLogTextView.text stringByAppendingString:text];
        }
    });
}

@end
