//
//  ARSettingViewController.m
//  LightSDKDemo
//
//  Created by Dikey on 16/10/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import "ARSettingViewController.h"
#import "ARDetectType.h"
#import <InsightSDK/InsightSDK.h>

@interface ARSettingViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *updateAlg;
@property (weak, nonatomic) IBOutlet UISwitch *updateAlgProduct;
@property (weak, nonatomic) IBOutlet UISwitch *backToDelete;
@property (weak, nonatomic) IBOutlet UITextField *minUpdateTimeinterval;
@property (weak, nonatomic) IBOutlet UISwitch *showLua;
@property (weak, nonatomic) IBOutlet UISwitch *isvMode; //离线模型是否提前到首页

@end

@implementation ARSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _updateAlg.on = [[NSUserDefaults standardUserDefaults]boolForKey:kUpdateAlgModel];
    _updateAlgProduct.on = [[NSUserDefaults standardUserDefaults]boolForKey:kUpdateProductToLatest];
    _backToDelete.on = [[NSUserDefaults standardUserDefaults]boolForKey:kBackToDelete];
    _showLua.on = [[NSUserDefaults standardUserDefaults]boolForKey:kShowLua];
    _isvMode.on = [[NSUserDefaults standardUserDefaults]boolForKey:kIsvMode];
    NSInteger minUpdateTimeinterval = [[NSUserDefaults standardUserDefaults]integerForKey:kAlgMinUpdateTimeinterval];
    if (minUpdateTimeinterval < 300) {
        minUpdateTimeinterval = 300;
    }
    _minUpdateTimeinterval.text = [NSString stringWithFormat:@"%ld",(long)minUpdateTimeinterval];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSUserDefaults standardUserDefaults]setBool:_updateAlg.isOn forKey:kUpdateAlgModel];
    [[NSUserDefaults standardUserDefaults]setBool:_updateAlgProduct.isOn forKey:kUpdateProductToLatest];
    [[NSUserDefaults standardUserDefaults]setBool:_backToDelete.isOn forKey:kBackToDelete];
    [[NSUserDefaults standardUserDefaults]setBool:_showLua.isOn forKey:kShowLua];
    [[NSUserDefaults standardUserDefaults]setBool:_isvMode.isOn forKey:kIsvMode];

    if ([_minUpdateTimeinterval.text integerValue]<300) {
        [[NSUserDefaults standardUserDefaults]setInteger:300 forKey:kAlgMinUpdateTimeinterval];
    }else{
        [[NSUserDefaults standardUserDefaults]setInteger:[_minUpdateTimeinterval.text integerValue] forKey:kAlgMinUpdateTimeinterval];
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
}

#pragma mark - Actions

- (IBAction)popVC:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Settings

- (IBAction)updateAlgModel:(UISwitch *)sender {
    
}

- (IBAction)updateProductToLatest:(UISwitch *)sender {
    
}

- (IBAction)backToDelete:(UISwitch *)sender {
    
}

- (IBAction)deleteAllData:(UIButton *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"是否删除所有InsightSDK数据" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action){
    }];
    UIAlertAction *commitAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        [[InsightARManager shareManager]clearAllData];
    }];
    [alertController addAction:cancleAction];
    [alertController addAction:commitAction];
    [self presentViewController:alertController animated:YES completion:^{
    }];
}

- (IBAction)deleteAllStickers:(UIButton *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"是否删除所有Sticker数据" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action){
    }];
    UIAlertAction *commitAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        [[InsightARManager shareManager]clearAllData];
    }];
    [alertController addAction:cancleAction];
    [alertController addAction:commitAction];
    [self presentViewController:alertController animated:YES completion:^{
    }];
}

- (IBAction)showLuaView:(UISwitch *)sender {
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

@end
