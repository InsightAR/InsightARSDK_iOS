//
//  BaseViewController.m
//  LightSDKDemo
//
//  Created by Dikey on 28/03/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self configureNatigationView];
}


- (void)configureNatigationView
{
    [self makeNativationViewClear];
    [self addNatigationView];
}

- (void)makeNativationViewClear
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}

- (void)addNatigationView
{
    if (!_natigationView) {
        UIView * centerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 138, 22)];
        UIImage *logo = [UIImage imageNamed:@"首页_logo"];
        UIImageView *logoImageView = [[UIImageView alloc]initWithImage:logo];
        [centerView addSubview:logoImageView];
        logoImageView.center = CGPointMake(logoImageView.center.x, centerView.center.y);
        
        UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectMake(35, 0, 103, 22)];
        textLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size:16];
        textLabel.textColor = [UIColor colorWithRed:29/255.0 green:29/255.0 blue:39/255.0 alpha:1/1.0];
        textLabel.center = CGPointMake(textLabel.center.x, centerView.center.y);
        textLabel.text = @"网易洞见 SDK";
        [textLabel sizeToFit];
        [centerView addSubview:textLabel];
        _natigationView = centerView;
        self.navigationItem.titleView = _natigationView;
        self.navigationController.navigationBarHidden = NO;
    }
}

@end
