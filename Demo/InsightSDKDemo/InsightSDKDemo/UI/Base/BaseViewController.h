//
//  BaseViewController.h
//  LightSDKDemo
//
//  Created by Dikey on 28/03/2018.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property (nonatomic, strong) UIView *natigationView;

@end
