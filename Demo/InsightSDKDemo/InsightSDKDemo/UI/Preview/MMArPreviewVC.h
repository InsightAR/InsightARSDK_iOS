//
//  MMArPreviewVC.h
//  InsightSDK
//
//  Created by Carmine on 2018/8/7.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    MMArPreviewTypePhoto,
    MMArPreviewTypeVideo,
} MMArPreviewType;

@interface MMArPreviewVC : UIViewController

@property (nonatomic, copy) NSString * path;
@property (nonatomic, assign) MMArPreviewType type;

- (instancetype)initWithDataPath:(NSString *)dataPath previewType:(MMArPreviewType)type;

@end
