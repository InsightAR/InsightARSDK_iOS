//
//  TMVideoPlayer.h
//  CarLoan
//
//  Created by Teemo on 5/3/17.
//  Copyright © 2017 wanyou. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AVPlayer;
@class AVPlayerLayer;
@interface TMVideoPlayerView : UIView
@property(nonatomic) AVPlayer *player;
@property(nonatomic,readonly) AVPlayerLayer *playerLayer;
@property (nonatomic) NSString *videoFillMode;
@end

