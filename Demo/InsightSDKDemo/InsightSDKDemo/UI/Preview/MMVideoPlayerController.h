//
//  MMVideoPlayerController.h
//  MMHousehold
//
//  Created by yunyunzhang on 2018/1/9.
//  Copyright © 2018年 Netease. All rights reserved.
//

#import "TMVideoPlayerController.h"

@interface MMVideoPlayerController : TMVideoPlayerController
@property (nonatomic, strong) NSURL * assetUrl;
@end
