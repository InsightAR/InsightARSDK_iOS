//
//  TMVideoPlayer.m
//  CarLoan
//
//  Created by Teemo on 5/3/17.
//  Copyright © 2017 wanyou. All rights reserved.
//

#import "TMVideoPlayerView.h"
#import <AVFoundation/AVFoundation.h>

@implementation TMVideoPlayerView

+ (Class)layerClass{
    return [AVPlayerLayer class];
}


- (void)setPlayer:(AVPlayer *)player{
    [(AVPlayerLayer*)[self layer] setPlayer:player];
}

- (AVPlayer*)player{
    return [(AVPlayerLayer*)[self layer]player];
}

- (AVPlayerLayer*)playerLayer{
    return (AVPlayerLayer*)self.layer;
}

- (void)setVideoFillMode:(NSString *)videoFillMode{
    [self playerLayer].videoGravity = videoFillMode;
}

- (NSString*)videoFillMode{
    return [self playerLayer].videoGravity;
}

#pragma mark - init

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.playerLayer.backgroundColor = [UIColor blackColor].CGColor;
    }
    return self;
}
@end
