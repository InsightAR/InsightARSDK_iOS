//
//  MMVideoPlayerController.m
//  MMHousehold
//
//  Created by yunyunzhang on 2018/1/9.
//  Copyright © 2018年 Netease. All rights reserved.
//

#import "MMVideoPlayerController.h"
#import "utility.h"
#import "UIImage+Video.h"

@interface MMVideoPlayerController ()<TMVideoPlayerControllerDelegate>
@property (nonatomic, strong) UIButton * playButton;
@property (nonatomic, strong) UIImageView *thumbImageView;
@property (nonatomic, assign) BOOL readyToPlay;
@end

@implementation MMVideoPlayerController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    [self setupThumbImage];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(resignActiveAction) name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)resignActiveAction
{
    _readyToPlay = NO;
}

- (void)setupThumbImage
{
    _thumbImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:_thumbImageView];
    _thumbImageView.contentMode = UIViewContentModeScaleAspectFit;
    _playButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    _playButton.center = self.view.center;
    [_playButton setImage:[UIImage imageNamed:@"播放ic-nor"] forState:UIControlStateNormal];
    [_playButton addTarget:self action:@selector(playButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_playButton];
}

- (void)setAssetUrl:(NSURL *)assetUrl
{
    _assetUrl = assetUrl;
    dispatch_async(dispatch_get_main_queue(), ^{
         _thumbImageView.image = [UIImage thumbnailImageForVideo:assetUrl atTime: 1];
    });    
}

- (void)playButtonClicked
{
    if (_readyToPlay)
    {
        [self playFromCurrentTime];
    }
}

- (void)videoPlayerPlaybackStateDidChange:(TMVideoPlayerController *)videoPlayer{
    
    switch (videoPlayer.playbackState ) {

        case TMVideoPlayerPlaybackStatePlaying:
        {
            [self playButtonState:YES];
            break;
        }
        case TMVideoPlayerPlaybackStateStopped:
        {
            [self playButtonState:NO];
            break;
        }
        case TMVideoPlayerPlaybackStatePaused:
        {
            [self playButtonState:NO];
            break;
        }
        case TMVideoPlayerPlaybackStateFailed:
        {
            [self playButtonState:NO];
            break;
        }
        default:
            break;
    }
}

- (void)videoPlayerReady:(TMVideoPlayerController *)videoPlayer
{
    _readyToPlay = YES;
}

- (void)playButtonState:(BOOL)hidden
{
    _thumbImageView.hidden = hidden;
    _playButton.hidden = hidden;
    _thumbImageView.image = [UIImage thumbnailImageForVideo:_assetUrl withCMTime:self.playerItem.currentTime];
}

- (void)dealloc
{
    NSLog(@"zzzz dealloc");
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
}





@end
