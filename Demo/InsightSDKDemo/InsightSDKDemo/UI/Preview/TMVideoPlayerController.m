//
//  TMVideoPlayerController.m
//  CarLoan
//
//  Created by Teemo on 5/3/17.
//  Copyright © 2017 wanyou. All rights reserved.
//

#import "TMVideoPlayerController.h"

static NSString * const TMVideoPlayerItemObserverContext = @"TMVideoPlayerItemObserverContext";
static NSString * const TMVideoPlayerLayerObserverContext = @"TMVideoPlayerLayerObserverContext";
static NSString * const TMVideoPlayerControllerTracksKey = @"tracks";
static NSString * const TMVideoPlayerControllerPlayableKey = @"playable";
static NSString * const TMVideoPlayerControllerDurationKey = @"duration";
static NSString * const TMVideoPlayerControllerRateKey = @"rate";
static NSString * const TMVideoPlayerControllerStatusKey = @"status";
static NSString * const TMVideoPlayerControllerCurrentTimeKey = @"currentTime";
static NSString * const TMVideoPlayerControllerEmptyBufferKey = @"playbackBufferEmpty";
static NSString * const TMVideoPlayerControllerPlayerKeepUpKey = @"playbackLikelyToKeepUp";
static NSString * const TMVideoPlayerControllerReadyForDisplay = @"readyForDisplay";


@implementation TMVideoPlayerController

#pragma mark - getters/setters

- (void)setVideoFillMode:(NSString *)videoFillMode
{
    if (_videoFillMode != videoFillMode) {
        _videoFillMode = videoFillMode;
        _videoView.videoFillMode = _videoFillMode;
    }
}

- (void)setVideoPath:(NSString *)videoPath
{
    if (!videoPath || [videoPath length] == 0) {
        _videoPath = nil;
        [self setAsset:nil];
        return;
    }
    NSURL *videoURL = [NSURL URLWithString:videoPath];
    if (!videoURL || ![videoURL scheme]) {
        videoURL = [NSURL fileURLWithPath:videoPath];
    }
    _videoPath = [videoPath copy];
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:videoURL options:nil];
    [self setAsset:asset];
}

- (void)setPlaybackLoops:(BOOL)playbackLoops
{
    _playbackLoops = playbackLoops;
    if (!_player)
        return;
    if (!_playbackLoops) {
        _player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
    } else {
        _player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    }
}

- (NSTimeInterval)maxDuration
{
    NSTimeInterval maxDuration = -1;
    if (CMTIME_IS_NUMERIC(_playerItem.duration)) {
        maxDuration = CMTimeGetSeconds(_playerItem.duration);
    }
    return maxDuration;
}

- (void)setVolume:(float)volume
{
    _volume = volume;
    if (!_player) {
        return;
    }
    _player.volume = volume;
}

- (void)setMuted:(BOOL)muted
{
    _muted = muted;
    _player.muted = muted;
}

- (void)setAsset:(AVAsset *)asset
{
    if (_asset == asset) {
        return;
    }
    if (_playbackState == TMVideoPlayerPlaybackStatePlaying) {
        [self pause];
    }
    _bufferingState = TMVideoPlayerBufferingStateUnknown;
    if ([_delegate respondsToSelector:@selector(videoPlayerBufferringStateDidChange:)]){
        [_delegate videoPlayerBufferringStateDidChange:self];
    }
    _asset = asset;
    if (!_asset) {
        [self _setPlayerItem:nil];
    }
    NSArray *keys = @[TMVideoPlayerControllerTracksKey, TMVideoPlayerControllerPlayableKey, TMVideoPlayerControllerDurationKey];
    [_asset loadValuesAsynchronouslyForKeys:keys completionHandler:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // check the keys
            for (NSString *key in keys) {
                NSError *error = nil;
                AVKeyValueStatus keyStatus = [asset statusOfValueForKey:key error:&error];
                if (keyStatus == AVKeyValueStatusFailed) {
                    _playbackState = TMVideoPlayerPlaybackStateFailed;
                    if ([_delegate respondsToSelector:@selector(videoPlayerPlaybackStateDidChange:)]){
                        [_delegate videoPlayerPlaybackStateDidChange:self];
                    }
                    return;
                }
            }
            // check playable
            if (!_asset.playable) {
                _playbackState = TMVideoPlayerPlaybackStateFailed;
                if ([_delegate respondsToSelector:@selector(videoPlayerPlaybackStateDidChange:)]){
                    [_delegate videoPlayerPlaybackStateDidChange:self];
                }
                return;
            }
            // setup player
            AVPlayerItem *playerItem = [AVPlayerItem playerItemWithAsset:_asset];
            [self _setPlayerItem:playerItem];
        });
    }];
}

- (void)_setPlayerItem:(AVPlayerItem *)playerItem
{
    if (_playerItem == playerItem)
        return;
    // remove observers
    if (_playerItem) {
        // AVPlayerItem KVO
        [_playerItem removeObserver:self forKeyPath:TMVideoPlayerControllerEmptyBufferKey context:(__bridge void *)(TMVideoPlayerItemObserverContext)];
        [_playerItem removeObserver:self forKeyPath:TMVideoPlayerControllerPlayerKeepUpKey context:(__bridge void *)(TMVideoPlayerItemObserverContext)];
        [_playerItem removeObserver:self forKeyPath:TMVideoPlayerControllerStatusKey context:(__bridge void *)(TMVideoPlayerItemObserverContext)];
        // notifications
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:_playerItem];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemFailedToPlayToEndTimeNotification object:_playerItem];
    }
    
    _playerItem = playerItem;
    
    // add observers
    if (_playerItem) {
        // AVPlayerItem KVO
        [_playerItem addObserver:self forKeyPath:TMVideoPlayerControllerEmptyBufferKey options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:(__bridge void *)(TMVideoPlayerItemObserverContext)];
        [_playerItem addObserver:self forKeyPath:TMVideoPlayerControllerPlayerKeepUpKey options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:(__bridge void *)(TMVideoPlayerItemObserverContext)];
        [_playerItem addObserver:self forKeyPath:TMVideoPlayerControllerStatusKey options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:(__bridge void *)(TMVideoPlayerItemObserverContext)];
        // notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_playerItemDidPlayToEndTime:) name:AVPlayerItemDidPlayToEndTimeNotification object:_playerItem];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_playerItemFailedToPlayToEndTime:) name:AVPlayerItemFailedToPlayToEndTimeNotification object:_playerItem];
    }
    
    if (!_playbackLoops) {
        _player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
    } else {
        _player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    }
    [_player replaceCurrentItemWithPlayerItem:_playerItem];
}

#pragma mark - view lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    _player = [[AVPlayer alloc] init];
    _player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
    
    // load the playerLayer view
    _videoView = [[TMVideoPlayerView alloc] initWithFrame:CGRectZero];
    _videoView.videoFillMode = AVLayerVideoGravityResizeAspect;
    _videoView.playerLayer.hidden = YES;
    [self.view addSubview: _videoView];
    _videoView.frame = self.view.bounds;
    
    // playerLayer KVO
    [_videoView.playerLayer addObserver:self forKeyPath:TMVideoPlayerControllerReadyForDisplay options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:(__bridge void *)(TMVideoPlayerLayerObserverContext)];
    
    // Application NSNotifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    // time observer
    CMTime timeInterval = CMTimeMake(5, 25);
    if ([self.delegate respondsToSelector:@selector(videoPlayerTimeIntervalForPlaybackProgress:)]) {
        timeInterval = [self.delegate videoPlayerTimeIntervalForPlaybackProgress:self];
    }
    
    __block __weak __typeof(self) weakSelf = self;
    _timeObserver = [_player addPeriodicTimeObserverForInterval:timeInterval
                                                          queue:dispatch_get_main_queue()
                                                     usingBlock:^(CMTime time)
     {
         if ([weakSelf.delegate respondsToSelector:@selector(videoPlayer:didUpdatePlayBackProgress:)])
             [weakSelf.delegate videoPlayer:weakSelf didUpdatePlayBackProgress:(CGFloat)CMTimeGetSeconds(time)];
     }];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if (_playbackState == TMVideoPlayerPlaybackStatePlaying)
        [self pause];
}

- (void)setVideoFrame:(CGRect)frame
{
    self.view.frame = frame;
    _videoView.frame = self.view.bounds;
}

#pragma mark - public methods

- (void)playFromBeginning
{
    if ([_delegate respondsToSelector:@selector(videoPlayerPlaybackWillStartFromBeginning:)]) {
        [_delegate videoPlayerPlaybackWillStartFromBeginning:self];
    }
    [_player seekToTime:kCMTimeZero];
    [self playFromCurrentTime];
}

- (void)playFromCurrentTime
{
    _playbackState = TMVideoPlayerPlaybackStatePlaying;
    if ([_delegate respondsToSelector:@selector(videoPlayerPlaybackStateDidChange:)]) {
        [_delegate videoPlayerPlaybackStateDidChange:self];
    }
    [_player play];
}

- (void)pause
{
    if (_playbackState != TMVideoPlayerPlaybackStatePlaying)
        return;
    [_player pause];
    _playbackState = TMVideoPlayerPlaybackStatePaused;
    if ([_delegate respondsToSelector:@selector(videoPlayerPlaybackStateDidChange:)]) {
        [_delegate videoPlayerPlaybackStateDidChange:self];
    }
}

- (void)stop
{
    if (_playbackState == TMVideoPlayerPlaybackStateStopped)
        return;
    [_player pause];
    _playbackState = TMVideoPlayerPlaybackStateStopped;
    if ([_delegate respondsToSelector:@selector(videoPlayerPlaybackStateDidChange:)]) {
        [_delegate videoPlayerPlaybackStateDidChange:self];
    }
}

- (void)seekTo:(CMTime)time{
    [_playerItem seekToTime:time];
}

#pragma mark - UIResponder

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_videoPath || _asset) {
        
        switch (_playbackState) {
            case TMVideoPlayerPlaybackStateStopped:
            {
                [self playFromBeginning];
                break;
            }
            case TMVideoPlayerPlaybackStatePaused:
            {
                [self playFromCurrentTime];
                break;
            }
            case TMVideoPlayerPlaybackStatePlaying:
            case TMVideoPlayerPlaybackStateFailed:
            default:
            {
                [self pause];
                break;
            }
        }
        
    } else {
        [super touchesEnded:touches withEvent:event];
    }
    
}

#pragma mark - AV NSNotificaions

- (void)_playerItemDidPlayToEndTime:(NSNotification *)aNotification
{
    if (_playbackLoops || !_playbackFreezesAtEnd) {
        [_player seekToTime:kCMTimeZero];
    }
    
    if (!_playbackLoops) {
        [self stop];
        if ([_delegate respondsToSelector:@selector(videoPlayerPlaybackDidEnd:)]) {
            [_delegate videoPlayerPlaybackDidEnd:self];
        }
    }
}

- (void)_playerItemFailedToPlayToEndTime:(NSNotification *)aNotification
{
    _playbackState = TMVideoPlayerPlaybackStateFailed;
    if ([_delegate respondsToSelector:@selector(videoPlayerPlaybackStateDidChange:)]) {
        [_delegate videoPlayerPlaybackStateDidChange:self];
    }
    
}

#pragma mark - App NSNotifications

- (void)_applicationWillResignActive:(NSNotification *)aNotfication
{
    if (_playbackState == TMVideoPlayerPlaybackStatePlaying)
        [self pause];
}

- (void)_applicationDidEnterBackground:(NSNotification *)aNotfication
{
    if (_playbackState == TMVideoPlayerPlaybackStatePlaying)
        [self pause];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
      if ( context == (__bridge void *)(TMVideoPlayerItemObserverContext) ) {
        
        // PlayerItem KVO
        
        if ([keyPath isEqualToString:TMVideoPlayerControllerEmptyBufferKey]) {
            if (_playerItem.playbackBufferEmpty) {
                _bufferingState = TMVideoPlayerBufferingStateDelayed;
                if ([_delegate respondsToSelector:@selector(videoPlayerBufferringStateDidChange:)]) {
                    [_delegate videoPlayerBufferringStateDidChange:self];
                }
                
            }
        } else if ([keyPath isEqualToString:TMVideoPlayerControllerPlayerKeepUpKey]) {
            if (_playerItem.playbackLikelyToKeepUp) {
                _bufferingState = TMVideoPlayerBufferingStateReady;
                if ([_delegate respondsToSelector:@selector(videoPlayerBufferringStateDidChange:)]) {
                    [_delegate videoPlayerBufferringStateDidChange:self];
                }
                
                if (_playbackState == TMVideoPlayerPlaybackStatePlaying) {
                    [self playFromCurrentTime];
                }
            }
        }
        
        AVPlayerStatus status = [change[NSKeyValueChangeNewKey] integerValue];
        switch (status)
        {
            case AVPlayerStatusReadyToPlay:
            {
                _videoView.playerLayer.backgroundColor = [[UIColor blackColor] CGColor];
                [_videoView.playerLayer setPlayer:_player];
                _videoView.playerLayer.hidden = NO;
                break;
            }
            case AVPlayerStatusFailed:
            {
                _playbackState = TMVideoPlayerPlaybackStateFailed;
                if ([_delegate respondsToSelector:@selector(videoPlayerPlaybackStateDidChange:)]) {
                    [_delegate videoPlayerPlaybackStateDidChange:self];
                }
                break;
            }
            case AVPlayerStatusUnknown:
            default:
                break;
        }
        
    } else if ( context == (__bridge void *)(TMVideoPlayerLayerObserverContext) ) {
        // PlayerLayer KVO
        if ([keyPath isEqualToString:TMVideoPlayerControllerReadyForDisplay]) {
            if (_videoView.playerLayer.readyForDisplay) {
                if ([_delegate respondsToSelector:@selector(videoPlayerReady:)]) {
                    [_delegate videoPlayerReady:self];
                }
            }
        }
        
    } else {
        
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)dealloc
{
    _videoView.player = nil;
    _delegate = nil;
    
    // notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // Layer KVO
    [_videoView.layer removeObserver:self forKeyPath:TMVideoPlayerControllerReadyForDisplay context:(__bridge void *)TMVideoPlayerLayerObserverContext];
    
    // player time observer
    if (_timeObserver) {
        [_player removeTimeObserver:_timeObserver];
    }
    
    // player
    [_player pause];
    
    // player item
    [self _setPlayerItem:nil];
}

@end
