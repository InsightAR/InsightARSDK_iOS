//
//  TMVideoPlayerController.h
//  CarLoan
//
//  Created by Teemo on 5/3/17.
//  Copyright © 2017 wanyou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "TMVideoPlayerView.h"

typedef NS_ENUM(NSInteger, TMVideoPlayerPlaybackState) {
    TMVideoPlayerPlaybackStateStopped = 0,
    TMVideoPlayerPlaybackStatePlaying,
    TMVideoPlayerPlaybackStatePaused,
    TMVideoPlayerPlaybackStateFailed,
};

typedef NS_ENUM(NSInteger, TMVideoPlayerBufferingState) {
    TMVideoPlayerBufferingStateUnknown = 0,
    TMVideoPlayerBufferingStateReady,
    TMVideoPlayerBufferingStateDelayed,
};

@protocol TMVideoPlayerControllerDelegate;
@interface TMVideoPlayerController : UIViewController
@property (nonatomic, weak) id<TMVideoPlayerControllerDelegate> delegate;
@property (nonatomic, copy) NSString *videoPath;
@property (nonatomic, copy) AVAsset *asset;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, copy, setter=setVideoFillMode:) NSString *videoFillMode; // default, AVLayerVideoGravityResizeAspect
@property (nonatomic, readonly) TMVideoPlayerPlaybackState playbackState;
@property (nonatomic, readonly) TMVideoPlayerBufferingState bufferingState;
@property (nonatomic, readonly) NSTimeInterval maxDuration;
@property (nonatomic, assign) float volume;
@property (nonatomic, assign) BOOL muted;
@property (nonatomic,strong) AVPlayerItem * playerItem;
@property (nonatomic,strong) TMVideoPlayerView *videoView;;
@property (nonatomic,assign) BOOL playbackLoops;
@property (nonatomic,assign) BOOL playbackFreezesAtEnd;
@property (nonatomic,weak) id <NSObject> timeObserver;
- (void)playFromBeginning;
- (void)playFromCurrentTime;
- (void)pause;
- (void)stop;
- (void)seekTo:(CMTime)time;
- (void)setVideoFrame:(CGRect)frame;
@end

@protocol TMVideoPlayerControllerDelegate <NSObject>
@optional
- (void)videoPlayerReady:(TMVideoPlayerController *)videoPlayer;
- (void)videoPlayerPlaybackStateDidChange:(TMVideoPlayerController *)videoPlayer;
- (void)videoPlayerPlaybackWillStartFromBeginning:(TMVideoPlayerController *)videoPlayer;
- (void)videoPlayerPlaybackDidEnd:(TMVideoPlayerController *)videoPlayer;
- (void)videoPlayerBufferringStateDidChange:(TMVideoPlayerController *)videoPlayer;
- (void)videoPlayer:(TMVideoPlayerController *)videoPlayer didUpdatePlayBackProgress:(CGFloat)progress;
- (CMTime)videoPlayerTimeIntervalForPlaybackProgress:(TMVideoPlayerController *)videoPlayer;
@end

