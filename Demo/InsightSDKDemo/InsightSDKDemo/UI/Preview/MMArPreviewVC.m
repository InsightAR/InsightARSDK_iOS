//
//  MMArPreviewVC.m
//  InsightSDK
//
//  Created by Carmine on 2018/8/7.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "MMArPreviewVC.h"
#import "MMVideoPlayerController.h"
//#import "UIImage+SDK.h"

@interface MMArPreviewVC ()



@property (nonatomic, strong) UIImageView * picImageView;
@property (nonatomic, strong) MMVideoPlayerController * playerVC;
@property (nonatomic, strong) UIButton * saveButton;    // custom
@property (nonatomic, strong) UILabel * saveLabel;    // custom

//var shareVC = ShareViewController()   // 暂不实现

@end

@implementation MMArPreviewVC

- (instancetype)initWithDataPath:(NSString *)dataPath previewType:(MMArPreviewType)type {
    self = [super init];
    if (self) {
        _path = dataPath;
        _type = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    switch (_type) {
        case MMArPreviewTypePhoto:
            [self createPhotoUI];
            break;
            
        case MMArPreviewTypeVideo:
            [self createVideoUI];
            break;
            
        default:
            break;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self setupView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI

- (void)setupView {
    CGFloat const kScreenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat const kScreenHeight = [UIScreen mainScreen].bounds.size.height;
    
    UIImageView * backgroundImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, kScreenHeight - 120, kScreenWidth, 120)];
    backgroundImage.image = [UIImage imageNamed:@"sticker_保存_遮罩"];
    [self.view addSubview:backgroundImage];
    
    UIButton * resetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    resetBtn.frame = CGRectMake(30, kScreenHeight - 115, 64, 64);
    [resetBtn setImage:[UIImage imageNamed:@"sticker_重新拍摄_nor"] forState:UIControlStateNormal];
    [resetBtn setImage:[UIImage imageNamed:@"sticker_重新拍摄_pre"] forState:UIControlStateHighlighted];
    [resetBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:resetBtn];
    
    UILabel * resetLabel = [[UILabel alloc]init];
    resetLabel.frame = CGRectMake(30, kScreenHeight - 44, 64, 15);
    resetLabel.text = @"重新拍摄";
    resetLabel.textColor = [UIColor whiteColor];
    resetLabel.textAlignment = NSTextAlignmentCenter;
    resetLabel.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:resetLabel];
    
    self.saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.saveButton.frame = CGRectMake(kScreenWidth/2 - 32, kScreenHeight - 115, 64, 64);
    [self.saveButton setImage:[UIImage imageNamed:@"sticker_保存_nor"] forState:UIControlStateNormal];
    [self.saveButton setImage:[UIImage imageNamed:@"sticker_保存_pre"] forState:UIControlStateHighlighted];
    [self.saveButton addTarget:self action:@selector(saveBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.saveButton];
    
    self.saveLabel = [[UILabel alloc]init];
    self.saveLabel.frame = CGRectMake(kScreenWidth/2 - 32, kScreenHeight - 44, 64, 15);
    self.saveLabel.text = @"保存";
    self.saveLabel.textColor = [UIColor whiteColor];
    self.saveLabel.textAlignment = NSTextAlignmentCenter;
    self.saveLabel.font =  [UIFont systemFontOfSize:13];
    [self.view addSubview:self.saveLabel];
    
    UIButton * shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    shareBtn.frame = CGRectMake(kScreenWidth - 94, kScreenHeight - 115, 64, 64);
    [shareBtn setImage:[UIImage imageNamed:@"sticker_分享_nor"] forState:UIControlStateNormal];
    [shareBtn setImage:[UIImage imageNamed:@"sticker_分享_pre"] forState:UIControlStateHighlighted];
    [shareBtn addTarget:self action:@selector(saveBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shareBtn];
    
    UILabel * shareLabel = [[UILabel alloc]init];
    shareLabel.frame = CGRectMake(kScreenWidth - 94, kScreenHeight - 44, 64, 15);
    shareLabel.text = @"分享";
    shareLabel.textColor = [UIColor whiteColor];
    shareLabel.textAlignment = NSTextAlignmentCenter;
    shareLabel.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:shareLabel];
    
//    self.view.addSubview(self.shareVC.view);
//    self.shareVC.view.isHidden = true
}

- (void)createPhotoUI {
    
}

- (void)createVideoUI {
    self.playerVC = [[MMVideoPlayerController alloc]init];
    self.playerVC.view.frame = self.view.bounds;
    [self.view addSubview:self.playerVC.view];
    self.playerVC.assetUrl = [NSURL fileURLWithPath:self.path];
    self.playerVC.videoPath = self.path;
}

#pragma mark - Action Events

- (void)backAction {
    // 直接隐藏
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)saveBtnAction {
#warning 暂不实现
}

- (void)shareBtnAction {
#warning 暂不实现
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
