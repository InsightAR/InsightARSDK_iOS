////
////  InsightARDataSource.h
////  InsightSDK
////
////  Created by Dikey on 2019/1/8.
////  Copyright © 2019 DikeyKing. All rights reserved.
////
//
//#ifndef InsightARDataSource_h
//#define InsightARDataSource_h
//
//#import "InsightAREvent.h"
//
//@protocol InsightARDataSource <NSObject>
//
///**
// 自定义指定类型，不使用默认实现，目前可以自定义的传入值有如下几种
// InsightAREvent_Function_ScreenShot
// InsightAREvent_Function_Share
// InsightAREvent_StartScreenRecord
// InsightAREvent_StopScreenRecord
// 
// @param type InsightAREventType
// @return 是否自定义；默认为NO
// */
//- (BOOL)customInsightAREventType:(InsightAREventType)type;
//
//@end
//
//#endif /* InsightARDataSource_h */
