//
//  MMRecorderNotificationItem.h
//  InsightSDK
//
//  Created by Dikey on 2018/10/9.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InsightRecordDefine.h"

extern NSString* const kInsightARRecorderNotification;

@interface InsightRecorderNotificationItem : NSObject

@property (nonatomic,assign) InsightARRecorderState recorderState;
@property (nonatomic,strong) NSError *error;
@property (nonatomic,copy) NSString *videoPath;

-(instancetype)initWithState:(InsightARRecorderState )state
                   videoPath:(NSString *)path
                       error:(NSError *)error;

@end
