//
//  ARStartOption.h
//  InsightSDK
//
//  Created by Dikey on 01/09/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 默认使用线上数据的数据，如果需要使用本地模型，需要设置 useLocalFiles = YES 并且传入相应的文件路径
 */
@interface ARStartOption : NSObject

/**
 使用本地资源，默认为NO；设置为YES之后，会优先使用本地资源
 */
@property (assign, nonatomic) BOOL useLocalFiles;

/**
 模型文件绝对路径
 */
@property (copy, nonatomic) NSArray<NSString *> *modelsPaths;

/**
 使用前置相机，默认为NO
 */
@property (assign, nonatomic) BOOL useFrontCamera;


@end
