//
//  InsightARManager+Recorder.h
//  InsightSDK
//
//  Created by Dikey on 2018/10/8.
//  Copyright © 2018 DikeyKing. All rights reserved.
//

#import "InsightARManager.h"
#import "InsightRecorderNotificationItem.h"

/*
 Get callbacks:
 
[[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(insightARRecorderNotification:) name:kInsightARRecorderNotification object:nil];

- (void)insightARRecorderNotification:(NSNotification *)notification
{
    InsightARRecorderNotificationItem *item = notification.object;
    if (item != nil) {
        switch (item.recorderState) {}
    }
}
*/
@interface InsightARManager (Recorder)

/**
 After finishing recording,you can get recording result from path

 @param path recording Path
 */
- (void)startInsightARScreenRecording:(NSString *)path;

/**
 After finishing recording,you can get recording result from pat

 */
- (void)startInsightARScreenRecordingAndSaveAutomaticaclly;

- (void)startInsightARScreenRecordingPath:(NSString *)path
                      saveToPhotoLibarary:(BOOL)save;

/**
 Stop Recording
 */
- (void)stopInsightARScreenRecording;

/**
  default ratio = 1 , range from 0.25 ~ 4.0. Affect record result in effect and size

 @param ratio default ratio = 1, unrecommended to change
 */
- (void)setRecordRatio:(float)ratio;

/**
   set to YES if want to disable Record Animation, default is NO
 */
- (void)setDisableRecordAnimation:(BOOL)disableRecordAnimation;

@end
