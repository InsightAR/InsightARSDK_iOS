////
////  ARSFaceManager.h
////  ARSFaceDemo
////
////  Created by Dikey on 2019/1/30.
////  Copyright © 2019 Dikey. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//
//@class ARSBeautyModel;
//@class ARProduct;
//@class ARSFaceItem;
//
//@interface ARSFaceManager : NSObject
//
//#pragma mark - 初始化方法
//
///**
// 初始化方法
//
// @param json 初始化JSON
// @return ARSFaceManager
// */
//- (instancetype)initWithLuaJson:(NSDictionary *)json;
//
///**
// 必须释放，等同于调用 InsightARManager中的 stopInsightAR
// */
//- (void)stopManager;
//
//#pragma mark - 滤镜效果
//
//
///**
// 加载美颜数据
//
// @param json json字符串
// @param path 资源根目录
// */
//- (void)reloadJsonWithBeautyString:(NSString *)json basePath:(NSString *)path;
////- (void)reloadJsonWithBeautyModel:(ARSBeautyModel *)model;
//
///**
// 可用滤镜列表
//
// @return ARSFaceItem
// */
//- (NSArray <ARSFaceItem *> *)filterArray;
//
///**
// 可用美颜列表
//
// @return ARSFaceItem
// */
//- (NSArray <ARSFaceItem *> *)beautyArray;
//
///**
// 编辑美颜/滤镜效果
//
// @param item ARSFaceItem
// @param value 0-1的值
// */
//- (void)modifyEffect:(ARSFaceItem *)item
//               value:(float)value;
//
///**
// 移出美颜/滤镜效果
//
// @param item ARSFaceItem
// */
//- (void)removeEffect:(ARSFaceItem *)item;
//
//
//- (void)modifyFilter:(ARSFaceItem *)item
//               value:(float)value;
//
///**
// 重置所有美肤效果
// */
//- (void)resetBeautyEffect;
//
///**
// 重置滤镜
// */
//- (void)resetFilterEffects;
//
///**
// 保存美颜效果
// */
//- (void)saveSettings;
//
//#pragma mark - 加载
//
///**
// 本地滤镜列表
//
// @return ARProduct
// */
//- (NSArray <ARProduct *> *)localStickers;
//
///**
// 获取本地列表中的sticker 可能会为空
// 
// @return ARProduct
// */
//- (ARProduct *)getLocalSticker:(NSString *)pid;
//
///**
// 保存sticker列表
// */
//- (void)saveSticker:(NSArray *)stickerList;
//
///**
// 从服务端下载Facesticker列表
//
// @param finishBlock 完成后的回调
// @param failedBlock 失败回调
// */
//- (void)loadStickersOnSuc:(void(^)(NSArray <ARProduct *> *stickerList))finishBlock
//                   failed:(void(^)(NSError *error))failedBlock;
//
///**
// 下载指定FaceSticker资源
//
// @param sticker ARProduct
// @param progressBlock 进度回调
// @param finishBlock 结果回调
// */
//- (void)downloadFaceSticker:(ARProduct *)sticker
//                   progress:(void(^)(NSProgress *progress))progressBlock
//                     finish:(void(^)(NSError *error))finishBlock;
//
//#pragma mark - 录屏
//
///**
// 开始录屏
//
// @param recordPath 录屏的路径
// */
//- (void)startRecording:(NSString *)recordPath;
//
///**
// 结束录屏
// */
//- (void)stopRecording;
//
///**
// 结束录屏
// @param finishBlock 录屏完成的回调
// */
//- (void)stopRecording:(void(^)(NSError  *error))finishBlock;
//
//- (void)saveVideo:(void(^)(NSError *error))finishBlock;
//
//#pragma mark - 切换相机
//
///**
// 切换相机
// */
//- (void)changeCamera;
//
//@end
