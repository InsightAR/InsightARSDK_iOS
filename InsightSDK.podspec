Pod::Spec.new do |s|

  s.name         = "InsightSDK"
  s.version      = "2.10.1"
  s.summary      = "Insight AR SDK."

  s.homepage     = "https://gitlab.com/InsightAR/InsightARSDK_iOS.git"
  s.license      = "MIT"
  s.author             = { "Dikey" => "dikeyking@gmail.com" }
  s.social_media_url   = "https://gitlab.com/DikeyKing"

  # 支持平台
  s.platform     = :ios
  s.platform     = :ios, "11.0"
  s.ios.deployment_target = "11.0"

  s.source       = { :git => 'https://gitlab.com/InsightAR/InsightARSDK_iOS.git', :tag => s.version }

  s.default_subspec = 'static'
   
  s.subspec 'static' do |static|
    # s.source_files  = 'InsightSDK.framework/Headers/*.{h}'  
    # s.resource = ['Resources/*']
    # s.resource_bundles = {'InsightSDK' => ['*.xcassets']}
    # s.resource_bundles = {'InsightSDK' => ['Resources/*.xcassets',
    #                                      'Resources/*.p12',
    #                                      'Resources/*.cer',]}
    s.frameworks = 'CoreMotion',  'CoreTelephony',  'Security',  'SystemConfiguration',  'AVFoundation',  'CoreMedia',   'Accelerate' , 'GLKit'   , 'CFNetwork'   , 'AssetsLibrary'
    s.weak_framework = 'ARKit'
    s.libraries = 'z.1.2.5', 'stdc++', 'sqlite3', 'sqlite3.0'  
    s.requires_arc = true  
    s.dependency "SSZipArchive" , ">= 2.0.0"  
    # s.vendored_frameworks = 'MMTrackerSDK/**/*.xcframework'
    s.vendored_frameworks = ['InsightSDK.xcframework', 'MMTrackerSDK/MMSDTrackerSDK.xcframework']

  end

  s.subspec 'InsightSDK' do |ss|
    #  s.source_files  = 'InsightSDK.framework/Headers/*.{h}'
    # s.resource = ['Resources/*']
    s.resource_bundles = {'InsightSDK' => ['Resources/*.xcassets',
                                          'Resources/*.p12',
                                          'Resources/*.cer',]}
    s.frameworks = 'CoreMotion',  'CoreTelephony',  'Security',  'SystemConfiguration',  'AVFoundation',  'CoreMedia',   'Accelerate' , 'GLKit'   , 'CFNetwork'   , 'AssetsLibrary'
    s.weak_framework = 'ARKit'
    s.libraries = 'z.1.2.5', 'stdc++', 'sqlite3', 'sqlite3.0'
    s.requires_arc = true
    s.dependency "SSZipArchive" , ">= 2.0.0"
    s.vendored_frameworks = ['InsightSDK.xcframework', 'MMTrackerSDK/MMSDTrackerSDK.xcframework']

  end

end
