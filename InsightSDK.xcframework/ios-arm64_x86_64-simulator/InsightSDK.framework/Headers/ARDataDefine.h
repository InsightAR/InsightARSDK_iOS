//
//  ARDataDefine.h
//  LightSDKDemo
//
//  Created by Dikey on 21/03/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//


#ifndef ARDataDefine_h
#define ARDataDefine_h

#import <Foundation/Foundation.h>
#import <OpenGLES/EAGL.h>

#import "ARProduct.h"
#import "ARStartOption.h"
#import "ARPrepareOptions.h"
#import "ARAlgPrepareOption.h"
#import "ARAlgorithmModel.h"
#import "ARDynamicModel.h"
#import "InsightARVCOption.h"
#import "InsightARState.h"
#import "InsightAREvent.h"
#import "InsightRecordDefine.h"

#pragma mark - NS_ENUM

/**
 AR错误码
 */
typedef NS_ENUM(NSUInteger, ARError) {
    
    /** 预留冗余位*/
    ARErrorNone = 0,
    /** 不支持的机型*/
    ARErrorNotSuppoted = 1,

    /**  识别失败 */
    ARVisualRecognitionFailed  = 201,
    
    //用户
    /** appkey appid为空 */
    ARErrorEmptyAppKeyOrID = 301,
    /** productID为空 */
    ARErrorEmptyProductID = 302,
    /**  传入modelsPaths必须非空 */
    ARErrorEmptyPaths = 303,
    /**  传入algModel为空 */
    ARErrorEmptyAlgModel  = 304,
    /**  InvalideKey */
    ARErrorInvalideKey  = 305,
    
    //内部错误
    /**  内部错误，详情看Description */
    ARErrorDynamicModelError = 401, //

    //ARCode
    /**  CodeInfo */
    ARErrorARCodeError = 601,
    
    //网络
    /**  下载失败 */
    ARErrorDownloadFailed = 601,
    /**  下载内部错误 */
    ARErrorDownloadError = 602,
    /**  网络出错 */
    ARErrorNetworkError = 603,
    /**  检测更新出错 */
    ARErrorCheckPidUpdateTimeError = 604,
    /**  常见原因：服务端未配置、已删除、产品版本高过SDK网络接口版本 */
    ARErrorProductMissing = 605,
    /**  服务器返回空数据 */
    ARErrorAlgModelMissing = 606,
    /**  第三方服务器返回的错误*/
    ARErrorNetworkErrorThird = 607,

    //Validate
    /**  该机型不支持 */
    ARErrorValidateFailedSDKNotSuppoted = 700,
    /**  key/secret验证失败 */
    ARErrorValidateFailed = 701,
    /**  无法解析pid、materailID或服务器返回的url为空；本地未下载Product */
    ARErrorValidateDynamicModelFailed  = 702,
    /**  资源非法 */
    ARErrorValidateFailedInvalideResource  = 703,
    /**  二维码非法 */
    ARErrorValidateInvalidedQR = 704,
    /**  二维码非法 */
    
    //算法 & AR
    /**  AR界面初始化失败 */
    ARErrorInitFailed = 801,
    /**  资源版本号偏高 , 需要升级SDK */
    ARErrorLoadResourceVersionTooHigh = 802,
    /**  资源版本号偏低 ， 需要更新资源 */
    ARErrorLoadResourceVersionTooLow = 803,
    /**  传入场景为空，请检查传入资源 */
    ARErrorLoadEmptyScene = 804,
    /**  内存出错，请检查传入资源 */
    ARErrorLoadMemoryError = 805,
    
    // 文件 & 缓存
    ARErrorPublicAlgModelMissing = 900,
};

#pragma mark - block

/**
 错误回调，error = nil 为成功回调

 @param error NSError
 */
typedef void(^ARCompletionBlock)(NSError *error);

/**
 数据block

 @param object updateBlock
 */
typedef void(^ARUpdateBlock)(id object);

/**
 返回下载进度

 @param downloadProgress 下载进度
 */
typedef void(^ARDownloadProgressBlock)(NSProgress *downloadProgress);

/**
 返回下载情况
 
 @param error error
 @param model ARDynamicModel
 */

typedef void(^ARDMCompletion)(NSError *error ,ARDynamicModel *model);

/*
 InputBufferRef color space must be kCVPixelFormatType_420YpCbCr8BiPlanarFullRange && AVCaptureSessionPreset1280x720 && AVCaptureVideoOrientationLandscapeRight
 Fov:AVCaptureDeviceFormat videoFieldOfView
 Capturefps: 30fps or 60fps
 Direction: 0 is LandscapeRight, 1 is Portrait
 Context: bridge EAGLContext 
 */
typedef struct AREngineCameraParam
{
    int width;
    int height;
    float fov;
    int capturefps;
    int deviceDirection;
    void * context;
} AREngineCameraParam;


#endif /* ARDataDefine_h */

