////
////  ARSFaceDataDefine.h
////  ARSFaceDemo
////
////  Created by Dikey on 2019/1/21.
////  Copyright © 2019 Dikey. All rights reserved.
////
//
//#ifndef ARSFaceDataDefine_h
//#define ARSFaceDataDefine_h
//
//typedef NS_ENUM(NSUInteger, ARSFaceCaptureMode) {
//    ARSFaceCaptureModePhoto, //拍照模式
//    ARSFaceCaptureModeVideo, //录屏模式
//};
//
//typedef NS_ENUM(NSUInteger, ARSFaceViewType) {
//    ARSFaceViewTypeNormal, //主界面
//    ARSFaceViewTypeSticker, //点击进入sticker界面
//    ARSFaceViewTypeFilter, //点击进入滤镜界面
//    ARSFaceViewTypeSkin, //美颜美肤界面
//    ARSFaceViewTypeRecording, //录屏界面
//    ARSFaceViewTypeSaving, //正在保存
//    ARSFaceViewTypeShare, //分享页面
//    ARSFaceViewTypeNone//分享页面
//
//};
//
//#endif /* ARSFaceDataDefine_h */
