//
//  InsightARStickerViewController.h
//  InsightSDK
//
//  Created by Carmine on 2018/6/4.
//  Copyright © 2018年 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ARStickerProduct;

typedef enum : NSUInteger {
    InsightRecordResultInitSuccess,
    InsightRecordResultInitFail,
    InsightRecordResultSuccess,
    InsightRecordResultSaveError,
} InsightRecordResult;

/**
 ARSticker
 */
@interface InsightARStickerViewController : UIViewController

#pragma mark - Default Layout
/// 选中框颜色
@property (nonatomic, strong) UIColor * focusCycleColor;
/// 未读提醒颜色
@property (nonatomic, strong) UIColor * unreadDotColor;
/// 拍照切图 normal
@property (nonatomic, strong) UIImage * cameraPhotoImageNormal;
/// 拍照切图 pressed
@property (nonatomic, strong) UIImage * cameraPhotoImagePressed;
/// 相册入口图
@property (nonatomic, strong) UIImage * albumImage;
/// 切换视频图
@property (nonatomic, strong) UIImage * switchVideoImage;
/// 切换拍照图
@property (nonatomic, strong) UIImage * switchPhotoImage;
/// 视频切图 normal
@property (nonatomic, strong) UIImage * cameraVideoImageNormal;
/// 视频切图 pressed
@property (nonatomic, strong) UIImage * cameraVideoImagePressed;

#pragma mark - Recording Options

/// 录屏保存文件名（只支持mp4格式）
@property (nonatomic, copy) NSString * recordSaveName;
/// 录屏保存路径
@property (nonatomic, copy) NSString * recordSavePath;
/// 录屏结果回调
@property (nonatomic, copy) void (^arRecordResultCallback)(InsightRecordResult result, NSString * videoPath);

#pragma mark - Other Options

/// WiFi网络下，是否自动下载资源，默认为YES
@property (nonatomic, assign) BOOL automaticallyDownloadOverWifi;
/// 录屏时长，10s-20s之间，默认布局有效
@property (nonatomic, assign) NSInteger recordingTime;

#pragma mark - Methods for subclass to override

/// 拍照，fullyCustomizeUI为NO时生效，功能实现可选
- (void)snapStillImage;
/// 开始录像，fullyCustomizeUI为NO时生效
- (void)startRecording;
/// 停止录像，fullyCustomizeUI为NO时生效
- (void)stopRecording;
/// 打开相册，fullyCustomizeUI为NO时生效，具体功能需实现
- (void)openAlbum;
/// 切换拍摄模式，fullyCustomizeUI为NO时生效，具体功能需实现
- (void)switchCameraMode;

#pragma mark - Third-party Customize

/// 自定义sticker，支持传入sticker资源
- (void)addAdditionalStickers:(NSArray<ARStickerProduct *> *)stickers;

/// 屏幕截图，默认保存到相册，fullyCustomizeUI为YES时生效
//- (void)snapStillImage;
/// 开始录制，fullyCustomizeUI为YES时生效
- (void)startArRecording;
/// 停止录制，fullyCustomizeUI为YES时生效
- (void)stopArRecording;

/// 加载单个本地资源
@property (nonatomic, copy) NSString * localARResPath;

/**
 UI是否第三方完全自定义，默认NO；
 如果开启，则默认的相机相关的视图不再展示，包括底部：相册/拍照/录屏/切换按钮，以及录屏的进度提醒。
 以上所有视图，均由外部实现。
 开启此项，以下内容无需继续使用：
 - recordingTime属性
 - 【Default Layout】中，focusCycleColor等9个属性配置
 */
@property (nonatomic, assign) BOOL fullyCustomizeUI;

@end
