//
//  InsightARDelegate.h
//  InsightSDK
//
//  Created by Dikey on 31/08/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#ifndef InsightARDelegate_h
#define InsightARDelegate_h
#import <Foundation/Foundation.h>
#import "InsightARState.h"

@class InsightAREvent;

@protocol InsightARDelegate <NSObject>

@required

/**
AR事件回调；具体查看 InsightAREventType
     InsightAREventType type = event.type;
     if(type == InsightAREvent_Function_ScreenShot){
          UIImage *screen = [event.params objectForKey:@"image"];
          NSLog(@"IAREvent_Function_ScreenShot");
          UIImageWriteToSavedPhotosAlbum(screen, nil, nil, nil);
     }else if(type == InsightAREvent_Function_Share){
          NSString *shareType = [event.params objectForKey:@"type"];
          NSString *title = [event.params objectForKey:@"title"];
          UIImage *screen = [event.params objectForKey:@"image"];
          NSString *description = [event.params objectForKey:@"description"];
          NSString *url = [event.params objectForKey:@"url"];
          UIImageWriteToSavedPhotosAlbum(screen, nil, nil, nil);
     }
 
 @param event AR事件回调，请务必实现InsightAREvent_Function_ScreenShot 和 InsightAREvent_Function_Share
 */
- (void)insightAREvent:(InsightAREvent *)event;

@optional

/**
 自定义指定类型，不使用默认实现，目前可以自定义的传入值有如下几种
 
     InsightAREvent_Function_ScreenShot // 内容发起截屏，默认会写入相册
     InsightAREvent_Function_Share  // 内容发起分享，默认会写入相册
     InsightAREvent_StartScreenRecord // 内容发起录屏
     InsightAREvent_StopScreenRecord // 内容发起录屏，默认会写入到相册
 
如果不希望默认保存到相册，可以自定义AR SDK的行为，参考代码如下
 
- (BOOL)customInsightAREventType:(InsightAREventType)type
{
    BOOL custom = NO;
    switch (type) {
        case InsightAREvent_Function_ScreenShot:{
            custom = YES;  // 自定义截屏行为，SDK 将不会写入相册
        }
            break;
        case InsightAREvent_Function_Share:{
            custom = NO;  // 自定义分享行为，SDK 将不会写入相册
        }
            break;
        default:{
            custom = NO;
        }
            break;
    }
    return result;
}

 @param type InsightAREventType
 @return 是否自定义；默认为NO
 */
- (BOOL)customInsightAREventType:(InsightAREventType)type;

- (void)insightARDidUpdateARState:(InsightARState)state;

@end

/**
 runInsightARScript回调，暂未使用

 @param result 是否完成
 */
typedef void(^InsightARRunScriptBlock)(BOOL result);

#endif /* InsightARDelegate_h */
