//
//  InsightSDK.h
//  InsightSDK
//
//  Created by Dikey on 17/03/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <UIKit/UIKit.h>

#if __has_include(<InsightSDK/InsightSDK.h>)

#import <InsightSDK/ARDynamicModelInput.h>
#import <InsightSDK/ARDynamicModel.h>

// **** 主要类 ****
// ARManager .. 用于自定义控制生命周期等
#import <InsightSDK/InsightARManager.h>
// ARManager .. 用于自定义控制生命周期等
#import <InsightSDK/InsightARManager+Deprecated.h>
#import <InsightSDK/InsightARManager+DMManager.h>
#import <InsightSDK/InsightARManager+NativeAPI.h>

// AROptions  && ARModel .. AR初始化等参数配置
#import <InsightSDK/ARAlgPrepareOption.h>
#import <InsightSDK/ARStartOption.h>
#import <InsightSDK/ARPrepareOptions.h>
#import <InsightSDK/ARInsightButtonOption.h>
#import <InsightSDK/ARAlgorithmModel.h>
#import <InsightSDK/ARProduct.h>

// **** 建议使用类****
// ARViewController .. 高度集成AR，继承ViewController就可以完成AR下载、展示、进度回调等
#import <InsightSDK/InsightARCloudVC.h>
#import <InsightSDK/InsightARVC.h>

// 数据定义
// AR Data Define
#import <InsightSDK/ARDataDefine.h>

// 回调 
// AR Call backs
#import <InsightSDK/InsightARDelegate.h>
#import <InsightSDK/InsightARState.h>
#import <InsightSDK/InsightAREvent.h>

// 录屏
// Screen Record
#import <InsightSDK/InsightARManager+Recorder.h>

// ARSSticker （一般用不到）
#import <InsightSDK/ARStickerProduct.h>
#import <InsightSDK/InsightARStickerViewController.h>

// ARSFace （一般用不到）
//#import <InsightSDK/ARSFaceViewController.h>
//#import <InsightSDK/ARSFaceViewData.h>
//#import <InsightSDK/ARSFaceDataDefine.h>
//#import <InsightSDK/ARSFaceManager.h>
//#import <InsightSDK/ARSFaceItem.h>

// ARCode
#import <InsightSDK/ARSCodeParser.h>
#import <InsightSDK/ARSCodeParserResult.h>

#else

#import "InsightARManager.h"
#import "InsightARManager+Deprecated.h"
#import "InsightARManager+DMManager.h"
#import "InsightARManager+NativeAPI.h"
#import "InsightARStickerViewController.h"
#import "InsightARCloudVC.h"
#import "InsightARVC.h"
#import "InsightARDelegate.h"
#import "ARDynamicModelInput.h"
#import "ARDynamicModel.h"
#import "ARDataDefine.h"
#import "ARAlgPrepareOption.h"
#import "ARPrepareOptions.h"
#import "ARStartOption.h"
#import "ARAlgorithmModel.h"
#import "ARProduct.h"
#import "ARInsightButtonOption.h"
#import "ARStickerProduct.h"
#import "InsightARState.h"
#import "InsightAREvent.h"
#import "InsightARManager+Recorder.h"
#import "ARSCodeParser.h"
#import "ARSCodeParserResult.h"

//#import "ARSFaceViewController.h"
//#import "ARSFaceViewData.h"
//#import "ARSFaceDataDefine.h"
//#import "ARSFaceManager.h"
//#import "ARSFaceItem.h"

#endif

