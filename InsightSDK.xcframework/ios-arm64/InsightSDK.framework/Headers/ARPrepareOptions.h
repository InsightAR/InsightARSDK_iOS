//
//  ARPrepareOptions.h
//  InsightSDK
//
//  Created by Dikey on 01/09/2017.
//  Copyright © 2017 DikeyKing. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ARAlgorithmModel;

/// 事件渠道配置
typedef enum : NSInteger {
    ARPrepareOptionChannelNormal = 0, // 普通事件
    ARPrepareOptionChannelQRCode = 1, // 二维码
    ARPrepareOptionChannelFaceSticker = 2, // 二维码
} ARPrepareOptionChannel;

/*
1-GCJ02-----------火星坐标系
2-WGS84-----------大地坐标系
3-BD09------------百度坐标系
4-mapbar----------图吧私有坐标系
 */
typedef NS_ENUM(NSUInteger, ARPrepareCoordiType) {
    ARPrepareCoordiTypeGCJ02 = 1,
    ARPrepareCoordiTypeWGS84 = 2,
    ARPrepareCoordiTypeBD09 = 3,
    ARPrepareCoordiTypeMapbar = 4
};

/** 用于下载ARProduct */
@interface ARPrepareOptions : NSObject

/**
 默认 ** YES **
 更新到最新版本的数据:
 数据更新需要耗费一定流量，一般在几十KB~几MB
 如果更新失败则返回本地已有模型
 */
@property (assign,nonatomic) BOOL getLastestVersion;

/**
 需要展示的productID：加载下载的数据
 */
@property (copy,nonatomic) NSString *productID;

/**
 需要展示的versionID：加载下载的数据
 */
@property (copy,nonatomic) NSString *versionID;

/**
 事件渠道，普通/二维码
 */
@property (assign,nonatomic) ARPrepareOptionChannel channel;

/**
 设置代理服务器,可以为空
 */
@property (copy,nonatomic) NSArray<Class> *protocolClasses;

/**
 默认为NO，如果为YES，则默认不会下载动态模型（该配置需要和服务器协定）
 */
@property (assign, nonatomic) BOOL dynamicDownloadMode;

/**
 是否需要LBS;
 
 某些场景（和产品确认是否有）增加了LBS验证，ARCode限定了使用的地理位置
 如果需要处理，参考以下代码设置
- (void)insightAREvent:(InsightAREvent *)event {
    switch (event.type) {
        case InsightAREvent_Function_ReloadARProduct: {
            // event.params 包含Pid、是否需要LBS的信息
            BOOL needLBS  = [[event.params objectForKey:@"needLBS"]boolValue];
            double longitude = .0;
            double latitude = .0;
            if (needLBS) { //如果需要LBS
                latitude =  // 当前纬度;
                longitude = 130.244580000; // 当前经度
            }
            ARPrepareOptions *options = [[ARPrepareOptions alloc]
                                         initWithPrepareDictionary:event.params
                                         longitude:longitude
                                         latitude:latitude
                                         coordiType:coordiType];
            [[InsightARManager shareManager] prepareARWithOptions:options
                                                 downloadProgress:^(NSProgress *downloadProgress) {
                                                 } completion:^(NSError *error) {
                                                     if (!error ) {
                                                         NSLog(@"prepareAR success");
                                                     }else{
                                                         // 处理错误
                                                     }
                                                 }];
        }
            break;
        default:
            break;
    }
}
 */
@property (assign, nonatomic) BOOL needLBS;

/**
 1-GCJ02-----------火星坐标系
 2-WGS84-----------大地坐标系
 3-BD09------------百度坐标系
 4-Sogou-----------搜狗私有坐标系，使用墨卡托
 5-mapbar----------图吧私有坐标系
 see https://ardoc.ai.163.com/#sort=dongjian&doc=3_ARSDK/3_1/iOSFAQ.md for detail 
 */
@property (assign, nonatomic) ARPrepareCoordiType coordiType;

/**
 经度
 */
@property (assign, nonatomic) double longitude;

/**
 纬度
 */
@property (assign, nonatomic) double latitude;

@property (copy, nonatomic) NSString *arCode;

/**
 传入初始化信息，可能需要经纬度

 @param dictionary 传入 - (void)insightAREvent:(InsightAREvent *)event 中的event.params
 @param longitude longitude
 @param latitude latitude
 @param coordiType 坐标系
 @return ARPrepareOptions
 */
- (instancetype)initWithPrepareDictionary:(NSDictionary *)dictionary
                                longitude:(double)longitude
                                 latitude:(double)latitude
                               coordiType:(ARPrepareCoordiType)coordiType;

@end
